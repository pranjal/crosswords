#!/usr/bin/python3
#
# Converts puz files to ipuz files
#

import argparse
import textwrap
import getopt
import sys
import json
import os
import regex
from convertor import Convertor

# This module is unlikely to be installed on people's computers. Warn
# when we hit this error.
try:
    import puz
except ModuleNotFoundError:
    print ("Missing puzpy module. You can install it by running `pip install puzpy`", file=sys.stderr)
    sys.exit (1)

# NOTE: It's really confusing to have code with similar-sounding names
# from both the puz and ipuz constructs. To try and make it more clear,
# we're prefixing the ipuz ones with an 'i'

class PUZConvertor(Convertor):
    EXTENSIONS=['.puz', '.PUZ']
    MAGIC=b'ACROSS&DOWN'

    def __init__(self, src):
        super().__init__()
        self.puzzle = puz.load (src)

    def build_clue (self, clue, grid, cn):
        offset = clue['cell']
        number = clue['num']
        grid[cn.row (offset)][cn.col (offset)] = number
        (clue_text, enumeration_text) = self.extract_enumeration(clue['clue'].strip())
        clue_text = self.clean_html (clue_text)
        if enumeration_text:
            iclue = {}
            iclue['number'] = number
            iclue['clue'] = clue_text
            iclue['enumeration'] = enumeration_text
        else:
            iclue = []
            iclue.append (number)
            iclue.append (clue_text)
        return iclue

    def convert_to_ipuz(self):
        if hasattr(self, "ipuz_dict"):
            return self.ipuz_dict
        self.ipuz_dict = {}
        self.ipuz_dict["version"] = "http://ipuz.org/v2"
        self.ipuz_dict["block"] = "#"


        if self.puzzle.puzzletype == puz.PuzzleType.Diagramless:
            self.ipuz_dict['kind'] = [ "http://ipuz.org/crossword/diagramless" ]
        else:
            self.ipuz_dict['kind'] = [ "http://ipuz.org/crossword" ]
        dimensions = {}
        dimensions['width'] = self.puzzle.width
        dimensions['height'] = self.puzzle.height
        self.ipuz_dict['dimensions'] = dimensions

        cn = self.puzzle.clue_numbering ()

        grid = self.build_grid (cn.width, cn.height)
        solution = self.build_grid (cn.width, cn.height)

        for offset in range (0, len (cn.grid)):
            if puz.is_blacksquare(cn.grid[offset]):
                grid[cn.row (offset)][cn.col (offset)] = "#"
                solution[cn.row (offset)][cn.col (offset)] = "#"
            else:
                grid[cn.row (offset)][cn.col (offset)] = 0
                solution[cn.row (offset)][cn.col (offset)] = self.puzzle.solution[offset]

        self.ipuz_dict['puzzle'] = grid
        self.ipuz_dict['solution'] = solution
        # Set up the clues
        self.ipuz_dict['clues'] = {}
        self.ipuz_dict['clues']['Across'] = []
        self.ipuz_dict['clues']['Down'] = []
        for clue in cn.across:
            iclue = self.build_clue (clue, grid, cn)
            self.ipuz_dict ['clues']['Across'].append (iclue)
            if isinstance (iclue, dict):
                self.ipuz_dict["showenumerations"] = True

        for clue in cn.down:
            iclue = self.build_clue (clue, grid, cn)
            self.ipuz_dict ['clues']['Down'].append (iclue)
            if isinstance (iclue, dict):
                self.ipuz_dict["showenumerations"] = True

        # Parse the markup
        if self.puzzle.has_markup():
            m = self.puzzle.markup()
            for offset in m.get_markup_squares():
                # There's no way to capture Incorrect or Revealed in .ipuz right now
                if m.markup[offset] & puz.GridMarkup.Circled:
                    style = {}
                    style['style'] = {}
                    style['style']['shapebg'] = 'circle'

                    old_cell = grid[cn.row (offset)][cn.col (offset)]
                    if old_cell != 0:
                        style['cell'] = old_cell
                    grid[cn.row (offset)][cn.col (offset)] = style

        # Parse rebus puzzles
        if self.puzzle.has_rebus():
            r = self.puzzle.rebus()
            for offset in r.get_rebus_squares():
                rebus_solution = r.get_rebus_solution (offset)
                if rebus_solution != None:
                    self.ipuz_dict['solution'][cn.row (offset)][cn.col (offset)] = rebus_solution


        self.ipuz_dict["showenumerations"] = self.has_enumerations
        # Optional metadata
        if self.puzzle.notes:
            # FIXME: What format are notes in? plaintext?
            self.ipuz_dict ["notes"] = self.clean_html (str.strip (self.puzzle.notes, "\n\t "))
        if self.puzzle.title:
            self.ipuz_dict ["title"] = self.clean_html (str.strip (self.puzzle.title, "\n\t "))
        if self.puzzle.author:
            self.ipuz_dict ["author"] = self.clean_html (str.strip (self.puzzle.author, "\n\t "))
        if self.puzzle.copyright:
            self.ipuz_dict ["copyright"] = self.clean_html (str.strip (self.puzzle.copyright, "\n\t "))

        return self.ipuz_dict

    def convert_from_ipuz(self):
        pass
