#!/usr/bin/python

import argparse
import textwrap
import getopt
import sys
import json
import os
import regex
from convertor import Convertor
from lxml import etree
from lxml import html

class JPZConvertor(Convertor):
    EXTENSIONS=['.xml', '.jpz', '.JPZ']
    MAGIC=b'<?xml'
    NSMAP={'cross':'http://crossword.info/xml/crossword-compiler',
           'rect':'http://crossword.info/xml/rectangular-puzzle'}
    VALID_TAGS=['span', 'b', 'i', 'sub', 'sup']

    def __init__(self, src):
        super().__init__()
        try:
            doc = etree.fromstring(src)
        except etree.XMLSyntaxError:
            print ("Invalid JPZ file.")
            exit (1)

        # So far, I've seen two types of jpz files. Those with a
        # namespace (regular?), those without (applet).
        # as a result, we need two different xpath
        #
        # We set a prefix to point to the rectangular-puzzle node,
        # which they both share.
        if doc.xpath('/cross:crossword-compiler', namespaces=self.NSMAP) != []:
            self.prefix = '/cross:crossword-compiler/rect:rectangular-puzzle'
            self.namespace = 'rect'
        elif doc.xpath('/crossword-compiler-applet', namespaces=self.NSMAP) != []:
            self.prefix = '/crossword-compiler-applet/rectangular-puzzle'
            self.namespace = ''
        else:
            self.ipuz_assert(False, "Unknown root in JPZ file.")
        self.doc = doc

    def build_path(self, path):
        if self.namespace:
            path = path.replace('/', '/%s:' % self.namespace)
        return self.prefix + path


    def localname(self, el):
        return etree.QName(el).localname


    def get_markup_text(self, el):
        text = ''
        for child in el.getiterator():
            tag = self.localname(child).lower()
            if tag in self.VALID_TAGS:
                text += ("<%s>" % tag)
            if child.text:
                text += child.text.strip()
            if tag in self.VALID_TAGS:
                text += ("</%s>" % tag)
        return text

    def get_raw_text(self, el):
        text = self.get_markup_text(el)
        return html.fromstring(text).text_content()

    def check_tag(self, e, tag):
        if not self.localname(e) == tag:
            return False
        return True

    def check_tag_text(self, e, tag):
        if self.check_tag (e, tag):
            return e.text
        return None

    def parse_metadata(self):
        # charset attribute. This is on the prefix node
        el = self.doc.xpath(self.prefix, namespaces=self.NSMAP)
        self.ipuz_assert (len(el) == 1, "error 1")
        e = el[0]
        if e.get('alphabet'):
            self.ipuz_dict['charset'] = e.get('alphabet')

        # metadata elements
        el = self.doc.xpath(self.build_path('/metadata'), namespaces=self.NSMAP)
        self.ipuz_assert (len(el) == 1, "error 2")
        e = el[0]
        for child in list(e):
            if self.check_tag(child, 'title'):
                self.ipuz_dict['title'] = self.check_tag_text(child, 'title')
            elif self.check_tag(child, 'creator'):
                self.ipuz_dict['author'] = self.check_tag_text(child, 'creator')
            elif self.check_tag(child, 'description'):
                self.ipuz_dict['intro'] = self.check_tag_text(child, 'description')
            elif self.check_tag(child, 'id'):
                self.ipuz_dict['uniqueid'] = self.check_tag_text(child, 'id')
            elif self.check_tag(child, 'checksum'):
                self.ipuz_dict['checksum'] = ['', self.check_tag_text(child, 'checksum') ]
            elif self.check_tag(child, 'copyright'):
                self.ipuz_dict['copyright'] = self.check_tag_text(child, 'copyright')
            elif self.check_tag(child, 'created_at'):
                self.ipuz_dict['date'] = self.check_tag_text(child, 'created_at')
            #FIXME: dateparser is messed up due to a regex
            #change. We'll just copy the string for now
            #date = dateparser.parse(self.check_tag_text(child, 'created_at'))
            elif self.check_tag(child, 'difficulty'):
                self.ipuz_dict['difficulty'] = self.check_tag_text(child, 'difficulty')
            elif self.check_tag(child, 'notes'):
                self.ipuz_dict['notes'] = self.check_tag_text(child, 'notes')
            elif self.check_tag(child, 'publisher'):
                self.ipuz_dict['publisher'] = self.check_tag_text(child, 'publisher')
            elif self.check_tag(child, 'puzzle_link_id'): # I'm not sure of this one
                self.ipuz_dict['url'] = self.check_tag_text(child, 'puzzle_link_id')
            elif self.check_tag(child, 'identifier'):     # I'm not sure of this one either
                self.ipuz_dict['url'] = self.check_tag_text(child, 'identifier')
            # We skip 'type', 'updated_at', and 'user_difficulty' until I
            # get better examples

    def parse_board(self):
        el = self.doc.xpath(self.build_path('/crossword/grid'), namespaces=self.NSMAP)
        self.ipuz_assert (len(el) == 1, "Multiple grid tags in puzzle")
        e = el[0]
        if e.get('width') and e.get('height'):
            self.ipuz_dict['dimensions'] = {}
            try:
                self.ipuz_dict['dimensions']['width'] = int(e.get('width'))
                self.ipuz_dict['dimensions']['height'] = int(e.get('height'))
            except ValueError:
                self.ipuz_assert(False, "Invalid width and height field")
        else:
            self.ipuz_assert(False, "Missing width and height field")

        # build the two grids
        ipuzzle = self.build_grid(self.ipuz_dict['dimensions']['width'], self.ipuz_dict['dimensions']['height'])
        isolution = self.build_grid(self.ipuz_dict['dimensions']['width'], self.ipuz_dict['dimensions']['height'])
        e = el[0]
        for child in list(e):
            if not self.localname(child) =='cell':
                # skip grid-look for now
                continue
            try:
                x = int(child.get('x'))
                y = int(child.get('y'))
            except ValueError:
                self.ipuz_assert(False, "Invalid cell x and y")
            cell_type = child.get('type')
            cell_solution = child.get('solution')

            try:
                cell_number = int(child.get('number'))
            except:
                #It's a label, or None
                cell_number = child.get('number')

            if not cell_number:
                cell_number = self.EMPTY_CHAR
            if cell_type == "block":
                ipuzzle[y-1][x-1] = self.BLOCK_CHAR
                isolution[y-1][x-1] = self.BLOCK_CHAR
            else:
                ipuzzle[y-1][x-1] = cell_number
                isolution[y-1][x-1] = cell_solution
        self.ipuz_dict['puzzle'] = ipuzzle
        self.ipuz_dict['solution'] = isolution

    # Gosh, this function is weird. Currently, jpz supports x="3" as
    # well as x="3-5" when defining words. As a result, this function
    # returns a tuple with the start and end of the range, or a
    # tuple. Note, the x abd y are in fact rows and coords, and
    # 1-centered, so our index into the board is [y-1][x-1]
    def get_coord(self, el, coord_str):
        c = el.get(coord_str)
        if c == None:
            return None
        try:
            coord = int(c)
            return coord
        except:
            pass
        s = regex.match (r'^(\d+)-(\d+)$', c)
        if s:
            c1 = int(s.groups()[0])
            c2 = int(s.groups()[1])
            if c2 > c1 and c1 > 0 and c2 > 0:
                return (c1, c2)
        return None

    def get_cells(self, cell):
        cells = []
        x = self.get_coord(cell, 'x')
        y = self.get_coord(cell, 'y')
        if x == None or y == None:
            return []
        if type(x) is tuple:
            for r in range(x[0]-1, x[1]):
                self.ipuz_assert (type(y) is int, "We don't handle clues with two-dimensional answers")
                cells.append( [r, y-1] )
        elif type(y) is tuple:
            for c in range(y[0]-1, y[1]):
                cells.append( [x-1, c] )
        else:
            cells.append( [x-1, y-1] )
        return cells

    def get_words(self):
        iwords = {}
        el = self.doc.xpath(self.build_path('/crossword/word'),
                            namespaces=self.NSMAP)
        for word in el:
            id = word.get('id')
            if not id:
                continue

            # Words are complex. the coords can be ranges. They can
            # also be directly in the <word> tag or in <cells>
            # childernSee the get_coord() docs for a bit more
            # information.
            #
            # First, we check to see if any cells are included within
            # the word
            cells = self.get_cells(word)

            # Next, we add any child cells. These elements don't
            # contain info other than coords.
            for cell in list(word):
                cells += self.get_cells(cell)
            iwords[id] = cells
        return iwords


    def build_clue (self, el):
        try:
            number = int(el.get('number'))
        except:
            # It's a label
            number = el.get('number')
        self.ipuz_assert(number != None, "Clue without number")
        word = el.get('word')
        cells = self.words[word]
        clue_text = self.get_markup_text (el)
        enumeration_text = el.get('format')
        if enumeration_text:
            enumeration_text = enumeration_text.replace(',', ' ')
        else:
            #try to extract it from the text
            (clue_text, enumeration_text) = self.extract_enumeration(clue_text)

        if enumeration_text or cells:
            iclue = {}
            if type (number) is int:
                iclue['number'] = number
            else:
                iclue['label'] = number
            iclue['clue'] = clue_text
            if enumeration_text:
                self.has_enumerations = True
                iclue['enumeration'] = enumeration_text
            if cells:
                iclue['cells'] = cells
        else:
            iclue = []
            iclue.append (number)
            iclue.append (clue_text)
        return iclue

    def parse_clue_set(self, el):
        direction = None
        clue_list = []
        # FIXME: there's no equiv of 'ordering' in ipuz. We should adjust
        # the clue_list to match this
        for child in list(el):
            if self.localname(child) == 'title':
                direction = self.get_raw_text (child)
            elif self.localname(child) == 'clue':
                clue = self.build_clue (child)
                if clue:
                    clue_list.append (clue)
        if direction not in self.DIRECTIONS:
            direction = 'Clues'
        return (direction, clue_list)

    def parse_clues(self):
        iclues = {}
        self.words = self.get_words()
        el = self.doc.xpath(self.build_path('/crossword/clues'), namespaces=self.NSMAP)
        for clue_set in el:
            if not self.check_tag(clue_set, 'clues'):
                continue
            (direction, clue_list) = self.parse_clue_set (clue_set)
            iclues[direction] = clue_list
        self.ipuz_dict['clues'] = iclues

    def convert_to_ipuz(self):
        if hasattr(self, "ipuz_dict"):
            return self.ipuz_dict
        self.ipuz_dict = {}

        self.ipuz_dict['version'] = "http://ipuz.org/v2"
        self.ipuz_dict['kind'] = ["http://ipuz.org/crossword#1"]
        self.ipuz_dict['empty'] = self.EMPTY_CHAR
        self.ipuz_dict['block'] = self.BLOCK_CHAR

        self.parse_metadata()
        self.parse_board()
        self.parse_clues()
        self.ipuz_dict['showenumerations'] = self.has_enumerations

        return self.ipuz_dict

    def convert_from_ipuz(self, ipuz_dict):
        pass
