# TODO

# Player
* Reveal / give up button
* ..and always, add more puzzles

# Editor
* Editor Window
    * Preview
* Basic
    * More templates
* Grid
    * Letter histogram
    * Word-size histogram
    * Grid "score"
    * Autofill dialog
         * Randomize fill
         * Click + drag for cell selection
         * Ctrl+click for extending selection
    * Highlight crossing letters / better suggestions
* Clues:
    * Clue letter selection tool:
         * Anagram generator
         * Dictionary lookup
         * Cryptic suggestion
    * Rich text editor for clues
* Style
    * Hide for initial release
    * In the long-run, add a style editor
* Metadata
    * Define licenses in license tag
    * Clean up dialog
    * Show enumerations
    * Maybe charset support?

# Wordlist
* Support Multi-word vs single-word in the word-list
* Consider a syntax like Qat
