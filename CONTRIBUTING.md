# Contributing

We welcome all contributions, both code and puzzles. If you would like
like help building or distributing crosswords, please let us know!

## Code of Conduct

The GNOME Crosswords project is governed by the
[GNOME Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct)
and contributors are expected to adhere to that.

## Coding Style

GNOME Crosswords follows the GTK coding style which is similar to the
GNU style.
