#!/bin/sh
#
# Runs scan-build as usual, but passes extra flags.  Using a script like this is the scheme
# recommended by Meson:
#
# https://mesonbuild.com/howtox.html#use-clang-static-analyzer

scan-build -v --status-bugs "$@"
