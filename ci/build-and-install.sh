#!/bin/sh

set -eu -o pipefail

meson setup _libipuz_build subprojects/libipuz --prefix /usr -Dintrospection=disabled -Ddocumentation=disabled
meson compile -C _libipuz_build
meson install -C _libipuz_build

meson setup _build --prefix /usr
meson compile -C _build
meson install -C _build
