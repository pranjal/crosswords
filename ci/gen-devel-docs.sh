#!/bin/sh

set -eu

source /usr/local/python/bin/activate

mkdir -p public/devel-docs
sphinx-build docs public/devel-docs
