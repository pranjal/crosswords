/* edit-histogram.c
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include "crosswords-enums.h"
#include <libipuz/libipuz.h>
#include <glib/gi18n-lib.h>
#include "edit-histogram.h"


enum
{
  PROP_0,
  PROP_MODE,
  N_PROPS,
};

static GParamSpec *obj_props[N_PROPS] =  {NULL, };


struct _EditHistogram
{
  GtkWidget parent_instance;
  EditHistogramMode mode;
  guint columns;
  guint rows;
  guint max_rows;
  GtkWidget *grid;
  GtkWidget *summary_label;
};


G_DEFINE_FINAL_TYPE (EditHistogram, edit_histogram, GTK_TYPE_WIDGET);


static void edit_histogram_init              (EditHistogram      *self);
static void edit_histogram_class_init        (EditHistogramClass *klass);
static void edit_histogram_set_property      (GObject            *object,
                                              guint               prop_id,
                                              const GValue       *value,
                                              GParamSpec         *pspec);
static void edit_histogram_get_property      (GObject            *object,
                                              guint               prop_id,
                                              GValue             *value,
                                              GParamSpec         *pspec);
static void edit_histogram_dispose           (GObject            *object);


static void
edit_histogram_init (EditHistogram *self)
{
  GtkLayoutManager *box_layout;

  gtk_widget_init_template (GTK_WIDGET (self));

  self->mode = EDIT_HISTOGRAM_MODE_LETTERS;

  self->columns = 0;
  self->rows = 0;
  self->max_rows = 10;

  box_layout = gtk_widget_get_layout_manager (GTK_WIDGET (self));
  gtk_box_layout_set_spacing (GTK_BOX_LAYOUT (box_layout), 8);
  gtk_orientable_set_orientation (GTK_ORIENTABLE (box_layout), GTK_ORIENTATION_VERTICAL);
}

static void
edit_histogram_class_init (EditHistogramClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->set_property = edit_histogram_set_property;
  object_class->get_property = edit_histogram_get_property;
  object_class->dispose = edit_histogram_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-histogram.ui");
  gtk_widget_class_bind_template_child (widget_class, EditHistogram, grid);
  gtk_widget_class_bind_template_child (widget_class, EditHistogram, summary_label);

  obj_props[PROP_MODE] = g_param_spec_enum ("mode", NULL, NULL,
                                            EDIT_TYPE_HISTOGRAM_MODE,
                                            EDIT_HISTOGRAM_MODE_LETTERS,
                                            G_PARAM_READWRITE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);

  gtk_widget_class_set_css_name (widget_class, "histogram");
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BOX_LAYOUT);
}

static void
edit_histogram_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  EditHistogram *self;

  self = EDIT_HISTOGRAM (object);

  switch (prop_id)
    {
    case PROP_MODE:
      self->mode = g_value_get_enum (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
edit_histogram_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  EditHistogram *self;

  self = EDIT_HISTOGRAM (object);

  switch (prop_id)
    {
    case PROP_MODE:
      g_value_set_enum (value, self->mode);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
edit_histogram_dispose (GObject *object)
{
  GtkWidget *child;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  G_OBJECT_CLASS (edit_histogram_parent_class)->dispose (object);
}

/* Public methods */

static GtkWidget *
create_grid_label (gboolean bar_label)
{
  GtkWidget *label;
  const gchar *css_class;

  label = gtk_label_new (NULL);
  if (bar_label)
    {
#if 0
      /* I'm not sure if this helps. */
      gtk_label_set_ellipsize (GTK_LABEL (label), PANGO_ELLIPSIZE_MIDDLE);
      gtk_label_set_width_chars (GTK_LABEL (label), 25);
#endif
      css_class = "bar";
    }
  else
    {
      css_class = "index";
    }
  gtk_label_set_xalign (GTK_LABEL (label), 0.0);
  gtk_widget_add_css_class (label, css_class);
  return label;
}

static void
resize_grid (EditHistogram *self,
             guint          n_elements)
{
  guint new_rows, new_columns;

  /* Calculate the dimensions of the new grid */
  if (n_elements > self->max_rows)
    {
      new_rows = self->max_rows;
      new_columns = ((n_elements + self->max_rows - 1)/self->max_rows) * 2;
    }
  else
    {
      new_rows = n_elements;
      if (new_rows > 0)
        new_columns = 2;
      else
        new_columns = 0;
    }

  if (new_columns > self->columns)
    {
      for (guint c = self->columns; c < new_columns; c++)
        {
          for (guint r = 0; r < new_rows; r++)
            {
              gtk_grid_attach (GTK_GRID (self->grid),
                               create_grid_label (c%2),
                               c, r,
                               1, 1);
            }
        }
    }
  else if (new_columns < self->columns)
    {
      for (guint c = self->columns; c > new_columns; c--)
        {
          gtk_grid_remove_column (GTK_GRID (self->grid), c - 1);
        }
    }

  if (new_rows > self->rows)
    {
      for (guint c = 0; c < MIN (self->columns, new_columns); c++)
        {
          for (guint r = self->rows; r < new_rows; r++)
            {
              gtk_grid_attach (GTK_GRID (self->grid),
                               create_grid_label (c%2),
                               c, r,
                               1, 1);
            }
        }
    }
  else if (new_rows < self->rows)
    {
      for (guint r = self->rows; r > new_rows; r--)
        {
          gtk_grid_remove_row (GTK_GRID (self->grid), r - 1);
        }
    }

  self->columns = new_columns;
  self->rows = new_rows;
}

static void
update_graph (EditHistogram  *self,
              IpuzPuzzleInfo *info)
{
  IpuzCharsetIter *iter;
  IpuzCharset *charset;
  GString *str;
  guint r, c;

  if (self->mode == EDIT_HISTOGRAM_MODE_LETTERS)
    charset = ipuz_puzzle_info_get_solution_chars (info);
  else
    charset = ipuz_puzzle_info_get_clue_lengths (info);

  resize_grid (self, ipuz_charset_get_n_chars (charset));

  str = g_string_new (NULL);
  r = 0; c = 0;

  for (size_t index = 0;
       index < ipuz_charset_get_n_values (charset);
       index++)
    {
      IpuzCharsetIterValue value;
      GtkWidget *index_label;
      GtkWidget *bar_label;

      value = ipuz_charset_get_value (charset, index);

      index_label = gtk_grid_get_child_at (GTK_GRID (self->grid), c, r);
      bar_label =  gtk_grid_get_child_at (GTK_GRID (self->grid), c + 1, r);
      gtk_widget_set_visible (index_label, TRUE);
      gtk_widget_set_visible (bar_label, TRUE);

      /* Set up the index */
      g_string_set_size (str, 0);
      if (self->mode == EDIT_HISTOGRAM_MODE_LETTERS)
        g_string_append_unichar (str, value.c);
      else
        g_string_append_printf (str, "Clue length %u", (guint) value.c);
      g_string_append_printf (str, ":");
      gtk_label_set_markup (GTK_LABEL (index_label), str->str);

      /* Build the bar graph */
      g_string_set_size (str, 0);
      for (guint i = 0; i < value.count; i++)
        g_string_append_printf (str, "█");
      g_string_append_printf (str, " (%u)", value.count);
      gtk_label_set_markup (GTK_LABEL (bar_label), str->str);

      r++;
      if (r >= self->max_rows)
        {
          r = 0;
          c += 2;
        }
    }

  if (r !=0)
    {
     for (; r < self->max_rows && r < self->rows; r++)
       {
         GtkWidget *index_label;
         GtkWidget *bar_label;

         index_label = gtk_grid_get_child_at (GTK_GRID (self->grid), c, r);
         bar_label =  gtk_grid_get_child_at (GTK_GRID (self->grid), c + 1, r);
         gtk_widget_set_visible (index_label, FALSE);
         gtk_widget_set_visible (bar_label, FALSE);
       }
     }

  g_string_free (str, TRUE);
}

static void
update_summary (EditHistogram  *self,
                IpuzPuzzleInfo *info)
{
  gboolean show_summary = FALSE;
  GString *summary_text;

  summary_text = g_string_new (NULL);

  if (self->mode == EDIT_HISTOGRAM_MODE_LETTERS)
    {
      IpuzCharset *charset;
      IpuzCharset *letters;
      guint n_letters;

      charset = ipuz_puzzle_info_get_charset (info);
      letters = ipuz_puzzle_info_get_solution_chars (info);
      n_letters = ipuz_charset_get_n_chars (letters);

      g_string_append_printf (summary_text,
                              "Total number of letters used: %u⁄%u",
                              n_letters,
                              (guint) ipuz_charset_get_n_chars (charset));
      show_summary = TRUE;
    }
  else
    {
      guint total_count = 0;
      guint n_clues = 0;
      IpuzCharsetIter *iter;
      IpuzCharset *charset = ipuz_puzzle_info_get_clue_lengths (info);

      /* Calculate the average */
      for (size_t index = 0;
           index < ipuz_charset_get_n_values (charset);
           index++)
        {
          IpuzCharsetIterValue value;

          value = ipuz_charset_get_value (charset, index);
          total_count += ((guint) value.c * value.count);
          n_clues += value.count;
          show_summary = TRUE;
        }

      if (n_clues > 0)
        g_string_append_printf (summary_text,
                                "Average clue length: %.2f",
                                (float)total_count / n_clues);
    }

  gtk_label_set_text (GTK_LABEL (self->summary_label), summary_text->str);
  gtk_widget_set_visible (self->summary_label, show_summary);
  g_string_free (summary_text, TRUE);
}

void
edit_histogram_update (EditHistogram  *self,
                       IpuzPuzzleInfo *info)
{
  update_graph (self, info);
  update_summary (self, info);
}
