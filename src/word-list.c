/* word-list.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <libipuz/libipuz.h>
#include <json-glib/json-glib.h>

#include "word-list.h"
#include "word-list-index.h"
#include "word-list-misc.h"
#include "word-list-resources.h"


#define WORD_LIST_PRIORITY(word) ((gint) (word-WORD_OFFSET)[0])


enum
{
  PROP_0,
  PROP_TRACK_SETTINGS,
  N_PROPS,
};

enum
{
  RESOURCE_CHANGED,
  LAST_SIGNAL,
};

static GParamSpec *obj_props[N_PROPS] = { NULL, };
static guint obj_signals[LAST_SIGNAL] = { 0 };


struct _WordList
{
  GObject parent_object;
  WordListIndex *index;

  /* The mmapped data we get */
  gboolean track_settings;
  const guchar *data; /* Owned by bytes and mmapped. Do not touch */
  gsize data_size;
  GBytes *bytes;

  gulong resource_changed_handler;

  /* Set externally */
  gchar *filter;
  gint threshold;
  WordListMode mode;

  /* Internally used to calculate the current filter */
  gboolean word_list_only;
  gint filter_len;
  GArray *list;
};

/* A struct containing the data for a filter fragment. data is
 * mmapped, and shouldn't be freed.
 */
typedef struct
{
  gushort len;  /* Length of the word */
  gushort *data; /* location of the charset from letter_list_offset*/
} FilterFragmentList;

/* A struct for the list of anagram hashes. Note, the pragma pack(1)
 * is because we are packed in repeating 9-byte blocks in the on-disk
 * data, and we don't want the compiler to pad this struct
 */
#pragma pack(push, 1)
typedef struct
{
  guint offset;
  guint hash;
  guchar len;
} AnagramHashIndex;
#pragma pack(pop)


static void         word_list_init                   (WordList           *self);
static void         word_list_class_init             (WordListClass      *klass);
static void         word_list_set_property           (GObject            *object,
                                                      guint               prop_id,
                                                      const GValue       *value,
                                                      GParamSpec         *pspec);
static void         word_list_get_property           (GObject            *object,
                                                      guint               prop_id,
                                                      GValue             *value,
                                                      GParamSpec         *pspec);
static void         word_list_dispose                (GObject            *object);
static void         word_list_load_index             (WordList           *self);
static const gchar *word_list_lookup_word            (WordList           *word_list,
                                                      WordListSection    *section,
                                                      WordIndex           word);
static gboolean     word_list_lookup_fragment        (WordList           *word_list,
                                                      FilterFragment      fragment,
                                                      FilterFragmentList *list);
static void         resource_changed_cb              (WordList           *word_list);
static void         copy_intersect_list_to_word_list (WordList           *word_list,
                                                      GArray             *intersect_list);
static GArray      *get_filter_intersect_list        (WordList           *word_list,
                                                      const gchar        *filter);


G_DEFINE_TYPE (WordList, word_list, G_TYPE_OBJECT);


static void
word_list_init (WordList *self)
{
  self->mode = WORD_LIST_NONE;

  self->list = g_array_new (FALSE, TRUE, sizeof (gchar *));
  self->resource_changed_handler = 0;

  word_list_set_track_settings (self, FALSE);
}

static void
word_list_class_init (WordListClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  object_class->set_property = word_list_set_property;
  object_class->get_property = word_list_get_property;
  object_class->dispose = word_list_dispose;

  obj_signals[RESOURCE_CHANGED] =
    g_signal_new ("resource-changed",
                  G_OBJECT_CLASS_TYPE (object_class),
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);

  obj_props[PROP_TRACK_SETTINGS] = g_param_spec_boolean ("track-settings",
                                                         NULL, NULL,
                                                         FALSE,
                                                         G_PARAM_READWRITE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);
}

static void
word_list_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  switch (prop_id)
    {
    case PROP_TRACK_SETTINGS:
      word_list_set_track_settings (WORD_LIST (object),
                                    g_value_get_boolean (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
word_list_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  WordList *self;

  self = WORD_LIST (object);

  switch (prop_id)
    {
    case PROP_TRACK_SETTINGS:
      g_value_set_boolean (value, self->track_settings);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
word_list_dispose (GObject *object)
{
  WordList *self;

  self = WORD_LIST (object);

  g_clear_pointer (&self->index, word_list_index_free);
  g_clear_pointer (&self->bytes, g_bytes_unref);
  g_clear_pointer (&self->filter, g_free);
  g_clear_pointer (&self->list, g_array_unref);

  /* Be careful here. Calling WORD_LIST_RESOURCES_DEFAULT will
   * necessarily check GSettings. That's fine for the main app, but
   * for tests we want to control the environment and use the test
   * word-list. WORD_LIST_RESOURCES_DEFAULT will crash in the test
   * environment
   */
  if (self->resource_changed_handler)
    g_clear_signal_handler (&self->resource_changed_handler, WORD_LIST_RESOURCES_DEFAULT);

  G_OBJECT_CLASS (word_list_parent_class)->dispose (object);
}

/* find the index at the back of mmapped data. */
static void
word_list_load_index (WordList *self)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (JsonParser) parser = NULL;
  gint i;

  for (i = self->data_size - 1; i > 0; i--)
    {
      if (self->data[i] == '\0')
        break;
      g_assert (i > 0);
    }
  /* We went back a step too far */
  i++;

  parser = json_parser_new_immutable ();
  json_parser_load_from_data (parser, (const gchar *) self->data + i,
                              (gint) self->data_size - i,
                              &error);
  if (error)
    {
      g_warning ("Failed to parse index from word list: %s\n", error->message);
      g_warning ("Index:\n%s\n", (const gchar *) self->data + i);
      return;
    }
  self->index = word_list_index_new_from_json (json_parser_get_root (parser));
}

static const gchar *
word_list_lookup_word (WordList        *word_list,
                       WordListSection *section,
                       WordIndex        word)
{
  WordListSection real_section;

  if (section == NULL)
    {
      real_section = word_list_index_get_section (word_list->index, word.length);
      section = &real_section;
    }

  return (const gchar *) word_list->data +
    (section->stride * word.index) +
    section->offset + WORD_OFFSET;
}

static gboolean
word_list_lookup_fragment (WordList           *word_list,
                           FilterFragment      fragment,
                           FilterFragmentList *list)
{
  gint offset;
  guint frag_offset;

  offset = word_index_fragment_index (word_list->index->min_length,
                                      ipuz_charset_get_n_chars (word_list->index->charset),
                                      fragment);
  offset *= (sizeof (guint) + sizeof (gushort));
  offset += word_list->index->letter_index_offset;

  /* FIXME(serialize) */
  frag_offset = *(guint *) (word_list->data + offset);

  list->data = (gushort *)(word_list->data + frag_offset);
  list->len = *(gushort *)(word_list->data + offset + sizeof (guint));

  return TRUE;
}

static void
resource_changed_cb (WordList *self)
{
  GError *error = NULL;
  GResource *resource;
  g_autofree gchar *path = NULL;
  GBytes *bytes;

  resource = word_list_resources_get_default_resource (WORD_LIST_RESOURCES_DEFAULT);
  path = g_strdup_printf ("/org/gnome/Crosswords/word-list/%s.dict",
                          word_list_resources_get_default_id (WORD_LIST_RESOURCES_DEFAULT));
  bytes = g_resource_lookup_data (resource, path,
                                  0,
                                  &error);

  if (bytes == NULL)
    {
      g_warning ("Broken word-list gresource; %s should exist: %s",
                 path,
                 error->message);
    }
  word_list_set_bytes (self, bytes);
  g_bytes_unref (bytes);
}


/* Public methods */

WordList *
word_list_new (void)
{
  WordList *word_list;

  word_list = g_object_new (WORD_TYPE_LIST,
                            "track-settings", TRUE,
                            NULL);

  return word_list;
}

/**
 * word_list_get_charset:
 * @self: A WordList
 *
 * Returns the charset used by all the words in @self.
 *
 * Returns: an IpuzCharset
 **/
IpuzCharset *
word_list_get_charset (WordList *self)
{
  g_return_val_if_fail (WORD_IS_LIST (self), NULL);

  return self->index->charset;
}

/**
 * @bytes: (transfer none) Binary data with the computed word list.
 *
 * Creates a word list database by parsing a pre-generated buffer.
 */
WordList *
word_list_new_from_bytes (GBytes *bytes)
{
  WordList *self;

  self = g_object_new (WORD_TYPE_LIST,
                       "track-settings", FALSE,
                       NULL);
  word_list_set_bytes (self, bytes);

  return self;
}

void
word_list_set_bytes (WordList *self,
                     GBytes   *bytes)
{
  g_return_if_fail (WORD_IS_LIST (self));

  if (bytes == NULL)
    {
      word_list_set_filter (self, NULL, self->mode);
    }
  else
    {
      g_autofree gchar *filter = NULL;
      self->bytes = g_bytes_ref (bytes);
      self->data = g_bytes_get_data (self->bytes, &self->data_size);
      g_assert (self->data != NULL);

      word_list_load_index (self);

      /* Force a reload of the filter */
      filter = self->filter;
      self->filter = NULL;
      word_list_set_filter (self, filter, self->mode);
    }
  g_signal_emit (self, obj_signals[RESOURCE_CHANGED], 0);
}

void
word_list_set_track_settings (WordList *self,
                              gboolean  track_settings)
{
  g_return_if_fail (WORD_IS_LIST (self));

  track_settings = !! track_settings;

  if (self->track_settings == track_settings)
    return;

  self->track_settings = track_settings;

  if (self->track_settings)
    {
      g_assert (self->resource_changed_handler == 0);

      self->resource_changed_handler =
        g_signal_connect_swapped (WORD_LIST_RESOURCES_DEFAULT,
                                  "resource-changed",
                                  G_CALLBACK (resource_changed_cb),
                                  self);
      resource_changed_cb (self);
    }
  else
    {
      g_assert (self->resource_changed_handler != 0);

      g_clear_signal_handler (&self->resource_changed_handler, WORD_LIST_RESOURCES_DEFAULT);
      word_list_set_bytes (self, NULL);
    }
}

static void
word_list_set_list_words (WordList *word_list)
{
  word_list->word_list_only = TRUE;
}

static void
fragment_list_intersect (GArray             *intersect_list,
                         FilterFragmentList  fragment_list)
{
  /* We will never have a result longer than the shortest list */
  gushort *target = g_new0 (gushort, MIN (intersect_list->len, fragment_list.len));
  guint u = 0, f = 0, len = 0;

  do
    {
      gushort u_val, f_val;

      u_val = g_array_index (intersect_list, gushort, u);
      f_val = fragment_list.data[f];

      if (u_val == f_val)
        {
          target[len++] = u_val;
          u++; f++;
        }
      else if (u_val < f_val)
        u++;
      else
        f++;
    }
  while ((u < intersect_list->len) && (f < fragment_list.len));

  g_array_set_size (intersect_list, len);
  memcpy (intersect_list->data, target, len*sizeof(gushort));

  g_free (target);
}

static void
copy_intersect_list_to_word_list (WordList *word_list,
                                  GArray   *intersect_list)
{
  g_array_set_size (word_list->list, intersect_list->len);

  if (intersect_list->len > 0)
    {
      for (guint i = 0; i < intersect_list->len; i++)
        {
          WordIndex word = {
            .length = word_list->filter_len,
          };
          const gchar **word_ptr;

          word.index = (int) g_array_index (intersect_list, gushort, i);

          word_ptr = &(g_array_index (word_list->list, const gchar *, i));
          *word_ptr = word_list_lookup_word (word_list, NULL, word);
        }
    }
}


/**
 * validate_filter:
 * @self: A @WordList
 * @filter: the filter to validate
 * @n_chars: the number of non-wildcards in the filter
 *
 * We double check the filter before using it. First, we walk
 * through it making sure that it contains characters we contain. If
 * it has invalid characters, we can't find any results at all.
 *
 * We also keep track of how many characters we explicitly give. For
 * MATCH, if we only have 0 or 1 characters, we can short-circuit some
 * work. (eg. "???" or "C??"). Two or more requires calculating the
 * intersection of the fragments, which is more expensive
 * computationally.
 *
 * Returns: #TRUE, if the filter is valid
 **/
static gboolean
validate_filter (WordList    *self,
                 const gchar *filter,
                 guint       *n_chars)
{
  const gchar *ptr;
  gint filter_len;

  g_assert (WORD_IS_LIST (self));
  g_assert (n_chars);

  *n_chars = 0;

  if (filter == NULL)
    return FALSE;

  filter_len = g_utf8_strlen (filter, -1);

  if (filter_len < self->index->min_length ||
      filter_len > self->index->max_length)
    return FALSE;

  for (ptr = filter; ptr[0] != '\0'; ptr = g_utf8_next_char (ptr))
    {
      gunichar c;
      gint index;

      c = g_utf8_get_char (ptr);

      /* Skip '?' characters */
      if (g_utf8_strchr (WORD_LIST_CHAR_WILDCARDS, -1, c))
        continue;

      /* Check to see if the character is a valid character */
      index = ipuz_charset_get_char_index (self->index->charset, c);
      if (index == -1)
        return FALSE;

      (*n_chars) ++;
    }

  return TRUE;
}

static void
word_list_set_list_filters (WordList *word_list)
{
  const gchar *ptr;
  gint pos = 0;
  g_autoptr (GArray) intersect_list = NULL;

  word_list->word_list_only = FALSE;

  for (ptr = word_list->filter; ptr[0] != '\0'; ptr = g_utf8_next_char (ptr))
    {
      FilterFragment fragment;
      FilterFragmentList fragment_list;
      gunichar c;

      /* FIXME (magichars): We should both catch this at the input
       * time and centralize this */
      if (ptr[0] == '?' || ptr[0] == ' ')
        {
          pos++;
          continue;
        }

      c = g_utf8_get_char (ptr);
      /* This should be fine. We call this after sanitizing the
       * filter. */
      fragment.length = word_list->filter_len;
      fragment.position = pos;
      fragment.char_index = ipuz_charset_get_char_index (word_list->index->charset, c);

      word_list_lookup_fragment (word_list, fragment, &fragment_list);
      if (fragment_list.len == 0)
        {
          /* There are no fragments matching this description */
          g_array_set_size (word_list->list, 0);
          return;
        }

      if (intersect_list == NULL)
        {
          intersect_list = g_array_new (FALSE, FALSE, sizeof (gushort));
          g_array_set_size (intersect_list, fragment_list.len);
          memcpy (intersect_list->data, fragment_list.data, fragment_list.len * sizeof (gushort));
        }
      else
        {
          fragment_list_intersect (intersect_list, fragment_list);
          if (intersect_list->len == 0)
            {
              g_array_set_size (word_list->list, 0);
              return;
            }
        }
      pos++;
    }

  /* The loop above skips over '?', but we can assert the following because the caller of
   * this function, word_list_set_filter(), already ensured that there is at least one
   * non-'?' character in the query string.
   */
  g_assert (intersect_list != NULL);

  copy_intersect_list_to_word_list (word_list, intersect_list);
}

static AnagramHashIndex *
find_hash_index (WordList *self,
                 guint     hash)
{
  AnagramHashIndex *hash_data;
  guint start, end, middle;

  start = 0;
  end = self->index->anagram_hash_index_length;
  hash_data = (AnagramHashIndex *) (self->data + self->index->anagram_hash_index_offset);

  while (start <= end)
    {
      middle = start + (end - start)/2;

      if (hash_data [middle].hash == hash)
        return &hash_data [middle];
      if (hash_data [middle].hash < hash)
        start = middle + 1;
      else
        end = middle - 1;
    }

  return NULL;
}

static void
word_list_set_anagrams (WordList *self)
{
  AnagramHashIndex *hash_index;
  guint hash;

  hash = word_list_hash_func (self->filter);
  hash_index = find_hash_index (self, hash);

  if (hash_index)
    {
      g_autoptr (GArray) intersect_list = NULL;

      intersect_list = g_array_new (FALSE, FALSE, sizeof (gushort));
      g_array_set_size (intersect_list, (guint) hash_index->len);
      memcpy (intersect_list->data, self->data + hash_index->offset,
              (size_t) (((guint)hash_index->len) * sizeof (gushort)));
      copy_intersect_list_to_word_list (self, intersect_list);
    }
  else /* No hash hit */
    {
      g_array_set_size (self->list, 0);
    }

}


//#define TIME_FILTER

void
word_list_set_filter (WordList     *self,
                      const char   *filter,
                      WordListMode  mode)
{
  guint n_chars = 0;

#ifdef TIME_FILTER
  g_autoptr (GTimer) timer = NULL;
  timer = g_timer_new ();
  g_timer_start (timer);
#endif

  g_return_if_fail (WORD_IS_LIST (self));

  /* short-circuit setting the same filter / mode */
  if (mode == self->mode &&
      ! g_strcmp0 (self->filter, filter))
    return;

  g_clear_pointer (&self->filter, g_free);
  self->filter_len = 0;

  /* This doesn't cause a realloc for the GArray, so is safe to call
   * at the start */
  g_array_set_size (self->list, 0);

  if (filter == NULL)
    return;

  if (! validate_filter (self, filter, &n_chars))
    return;

  self->filter = g_strdup (filter);
  self->filter_len = g_utf8_strlen (filter, -1);
  self->mode = mode;

  if (self->mode == WORD_LIST_MATCH)
    {
      /* It's just the word list of len. */
      if (n_chars == 0)
        word_list_set_list_words (self);
      else
        word_list_set_list_filters (self);
    }
  else if (self->mode == WORD_LIST_ANAGRAM)
    {
      /* We don't support ANAGRAMS with wildcards */
      if (self->filter_len != (gint)n_chars)
        g_array_set_size (self->list, 0);
      else
        word_list_set_anagrams (self);
    }

#ifdef TIME_FILTER
  g_timer_stop (timer);
  g_print ("word_liset_set_filter: Total time %f\n", g_timer_elapsed (timer, NULL));
#endif
}

const gchar *
word_list_get_filter (WordList *word_list)
{
  g_return_val_if_fail (WORD_IS_LIST (word_list), NULL);

  return word_list->filter;
}

guint
word_list_get_n_items (WordList *word_list)
{
  g_return_val_if_fail (WORD_IS_LIST (word_list), 0);

  if (word_list->word_list_only)
    {
      WordListSection section;

      section = word_list_index_get_section (word_list->index, word_list->filter_len);

      if (section.word_len == -1)
        /* We don't have any words of filter_len length */
        return 0;

      return section.count;
    }
  return word_list->list->len;
}

const gchar *
word_list_get_word (WordList *word_list,
                    guint     position)
{
  const gchar *word;
  g_return_val_if_fail (WORD_IS_LIST (word_list), 0);

  if (word_list->word_list_only)
    {
      WordListSection section;
      WordIndex word_index = {
        .length = word_list->filter_len,
      };

      word_index.index = position;


      section = word_list_index_get_section (word_list->index, word_list->filter_len);

      if (section.word_len == -1)
        /* We don't have any words of filter_len length */
        return NULL;

      word = word_list_lookup_word (word_list, &section, word_index);
      return word;
    }

  word = g_array_index (word_list->list, gchar *, position);

  return word;
}

gboolean
word_list_get_word_index (WordList    *word_list,
                          guint        position,
                          WordIndex   *word_index)
{
  WordListSection section;
  WordIndex new_word_index;
  const guchar *word;

  g_return_val_if_fail (WORD_IS_LIST (word_list), FALSE);

  new_word_index.length = word_list->filter_len;
  section = word_list_index_get_section (word_list->index, word_list->filter_len);

  if (section.word_len == -1)
    /* We don't have any words of filter_len length */
    return FALSE;
  if (word_list->word_list_only)
    {
      /* The index is just the position we were passed in! */
      new_word_index.index = position;
      *word_index = new_word_index;
      return TRUE;
    }

  /* we need to calculate the index from the offset of word. */
  word = g_array_index (word_list->list, guchar *, position);
  new_word_index.index = (((word - word_list->data)) - (section.offset + 1)) / section.stride;

  *word_index = new_word_index;
  return TRUE;

}

gint
word_list_get_priority (WordList *word_list,
                        guint     position)
{
  const gchar *word;
  g_return_val_if_fail (WORD_IS_LIST (word_list), 0);

  word = word_list_get_word (word_list, position);

  return WORD_LIST_PRIORITY (word);
}

const gchar *
word_list_get_enumeration_src  (WordList *word_list,
                                guint     position)
{
  const gchar *word;
  gushort enumeration_offset;

  g_return_val_if_fail (WORD_IS_LIST (word_list), 0);

  word = word_list_get_word (word_list, position);
  enumeration_offset = ((gushort *) (word - ENUMERATION_SIZE))[0];

  if (enumeration_offset == 0)
    return NULL;

  return (gchar *) (word_list->data + enumeration_offset +
                    word_list->index->enumerations_offset);
}

/**
 * word_list_get_array_list:
 * @word_list: A @WordList
 * @filter: the filter to match
 * @mode: the mode of the operation
 *
 * Returns a list of @WordList of words matching the @mode
 * operation. Currently, only #WORD_LIST_MODE is supported. This
 * function does not alter the WordList.
 *
 * Returns: A newly allocated @WordArray
 **/
WordArray *
word_list_get_array_list (WordList     *word_list,
                          const gchar  *filter,
                          WordListMode  mode)
{
  WordArray *word_array;
  guint filter_len;
  guint n_chars = 0;

  g_return_val_if_fail (WORD_IS_LIST (word_list), NULL);
  /* FIXME(anagram): we only support MATCH for now. */
  g_return_val_if_fail (mode == WORD_LIST_MATCH, NULL);

  word_array = word_array_new ();

  if (filter == NULL)
    return word_array;

  if (! validate_filter (word_list, filter, &n_chars))
    return word_array;

  filter_len = g_utf8_strlen (filter, -1);

  /* The filter is just '???..' We return a consecutive array */
  if (n_chars == 0)
    {
      WordListSection section;

      section = word_list_index_get_section (word_list->index, filter_len);
      if (section.word_len == -1)
        /* We don't have any words of filter_len length */
        return word_array;

      g_array_set_size (word_array, section.count);

      for (gint i = 0; i < (gint) section.count; i++)
        {
          WordIndex *word_index;

          word_index = &(word_array_index (word_array, i));
          word_index->length = filter_len;
          word_index->index = i;
        }
    }
  else
    {
      g_autoptr (GArray) intersect_list = NULL;

      intersect_list = get_filter_intersect_list (word_list, filter);

      if (intersect_list)
        {
          g_array_set_size (word_array, intersect_list->len);

          for (guint i = 0; i < intersect_list->len; i++)
            {
              WordIndex *word_index;

              word_index = &(word_array_index (word_array, i));
              word_index->length = filter_len;
              word_index->index = (int) g_array_index (intersect_list, gushort, i);
            }
        }
    }

  return word_array;
}

/**
 * word_list_lookup_index:
 * @word_list: A @WordList
 * @word: The word to lookup
 * @word_index: Location to fill with a new word_index
 *
 * Looks up the location in the master index of @word. Unlike
 * word_list_get_word(), this function ignores the currently set
 * filter and determines if @word is a possible word.
 *
 * Returns:
 **/
gboolean
word_list_lookup_index (WordList    *word_list,
                        const gchar *word,
                        WordIndex   *word_index)
{
  const gchar *ptr;
  gint pos = 0;
  gint len;
  g_autoptr (GArray) intersect_list = NULL;

  g_return_val_if_fail (WORD_IS_LIST (word_list), FALSE);
  g_return_val_if_fail (word != NULL, FALSE);
  g_return_val_if_fail (word_index != NULL, FALSE);

  if (word == NULL)
    return FALSE;

  len = g_utf8_strlen (word, -1);
  if (len <= word_list->index->min_length ||
      len >= word_list->index->max_length)
    return FALSE;

  for (ptr = word; ptr[0] != '\0'; ptr = g_utf8_next_char (ptr))
    {
      FilterFragment fragment;
      FilterFragmentList fragment_list;
      gunichar c;

      /* FIXME (magichars): We should both catch this at the input
       * time and centralize this */
      c = g_utf8_get_char (ptr);
      fragment.length = len;
      fragment.position = pos;
      fragment.char_index = ipuz_charset_get_char_index (word_list->index->charset, c);
      if (fragment.char_index == -1)
        /* This word contains a character we don't contain */
        return FALSE;

      word_list_lookup_fragment (word_list, fragment, &fragment_list);
      if (fragment_list.len == 0)
        return FALSE;

      if (intersect_list == NULL)
        {
          intersect_list = g_array_new (FALSE, FALSE, sizeof (gushort));
          g_array_set_size (intersect_list, fragment_list.len);
          memcpy (intersect_list->data, fragment_list.data, fragment_list.len * sizeof (gushort));
        }
      else
        {
          fragment_list_intersect (intersect_list, fragment_list);
          if (intersect_list->len == 0)
            return FALSE;
        }
      pos++;
    }

  if (intersect_list == NULL)
    return FALSE;

  g_return_val_if_fail (intersect_list->len == 1, FALSE);
  word_index->length = len;
  word_index->index = (gint) g_array_index (intersect_list, gushort, 0);

  return TRUE;
}

/**
 * word_list_get_indexed_word:
 * @word_list: A @WordList
 * @word_index: Index location of a word to lookup.
 *
 * Looks up the word at @word_index. This ignores the filter and
 * returns the enry from the global Index table.
 *
 * Returns: the word at @word_index
 **/
const gchar *
word_list_get_indexed_word (WordList  *word_list,
                            WordIndex  word_index)
{
  g_return_val_if_fail (WORD_IS_LIST (word_list), NULL);

  return word_list_lookup_word (word_list, NULL, word_index);
}

/**
 * word_list_get_indexed_priority:
 * @word_list: A @WordList
 * @word_index: Index location of a priority to lookup.
 *
 * Looks up the word at @word_index. This ignores the filter and
 * returns the entry from the global Index table.
 *
 * Returns: the priority at @word_index
 **/
gint
word_list_get_indexed_priority (WordList  *word_list,
                                WordIndex  word_index)
{
  g_return_val_if_fail (WORD_IS_LIST (word_list), 0);

  return WORD_LIST_PRIORITY (word_list_get_indexed_word (word_list, word_index));
}

const gchar *
word_list_get_indexed_enumeration_src (WordList  *word_list,
                                       WordIndex  word_index)
{
  const gchar *word;
  gushort enumeration_offset;

  g_return_val_if_fail (WORD_IS_LIST (word_list), NULL);

  word = word_list_get_indexed_word (word_list, word_index);
  enumeration_offset = ((gushort *) (word - ENUMERATION_SIZE))[0];

  if (enumeration_offset == 0)
    return NULL;

  return (gchar *) (word_list->data + enumeration_offset +
                    word_list->index->enumerations_offset);
}

/**
 * word_list_dump:
 * @word_list: A @WordList
 *
 * Dumps the current filtered set of words in @word_list to stdout.
 **/
void
word_list_dump (WordList *word_list)
{
  guint i;

  g_return_if_fail (WORD_IS_LIST (word_list));

  if (word_list->list->len == 0)
    g_print ("<no words>\n");

  for (i = 0; i < word_list->list->len; i++)
    {
      g_print ("%s\n", g_array_index (word_list->list, char *, i));
    }
}


static GArray *
get_filter_intersect_list (WordList    *word_list,
                           const gchar *filter)
{
  const gchar *ptr;
  gint pos = 0;
  GArray *intersect_list = NULL;
  gint filter_len;

  g_assert (filter != NULL);
  filter_len = g_utf8_strlen (filter, -1);

  for (ptr = filter; ptr[0] != '\0'; ptr = g_utf8_next_char (ptr))
    {
      FilterFragment fragment;
      FilterFragmentList fragment_list;
      gunichar c;

      c = g_utf8_get_char (ptr);
      if (g_utf8_strchr (WORD_LIST_CHAR_WILDCARDS, -1, c))
        {
          pos++;
          continue;
        }

      /* This should be fine. We call this after sanitizing the
       * filter. */
      fragment.length = filter_len;
      fragment.position = pos;
      fragment.char_index = ipuz_charset_get_char_index (word_list->index->charset, c);

      word_list_lookup_fragment (word_list, fragment, &fragment_list);
      if (fragment_list.len == 0)
        {
          /* There are no fragments matching this description */
          if (intersect_list)
            g_array_set_size (intersect_list, 0);
          return intersect_list;
        }

      if (intersect_list == NULL)
        {
          intersect_list = g_array_new (FALSE, FALSE, sizeof (gushort));
          g_array_set_size (intersect_list, fragment_list.len);
          memcpy (intersect_list->data, fragment_list.data, fragment_list.len * sizeof (gushort));
        }
      else
        {
          fragment_list_intersect (intersect_list, fragment_list);
          if (intersect_list->len == 0)
            return intersect_list;
        }
      pos++;
    }

  return intersect_list;
}

static gunichar
get_nth_char (const gchar *src,
              guint        n)
{
  const gchar *s = src;
  while (n && *s)
    {
      s = g_utf8_next_char(s);
      n--;
    }

  if (! *s)
    return 0;

  return g_utf8_get_char (s);
}

static IpuzCharset *
calculate_pos_chars (WordList    *word_list,
                     const gchar *filter,
                     guint        pos,
                     IpuzCharset *opposing_pos_chars,
                     WordArray   *word_array)
{
  IpuzCharsetBuilder *builder;
  g_autoptr (GArray) intersect_list = NULL;
  guint len;

  len = g_utf8_strlen (filter, -1);

  intersect_list = get_filter_intersect_list (word_list, filter);

  builder = ipuz_charset_builder_new ();
  if (intersect_list)
    {
      for (guint i = 0; i < intersect_list->len; i++)
        {
          const gchar *word;
          WordIndex word_index = {
            .length = len,
          };
          gunichar c;

          word_index.index = (int) g_array_index (intersect_list, gushort, i);

          word = word_list_lookup_word (word_list, NULL, word_index);
          c = get_nth_char (word, pos);

          if (opposing_pos_chars == NULL ||
              ipuz_charset_get_char_count (opposing_pos_chars, c) > 0)
            {
              ipuz_charset_builder_add_character (builder, c);
              if (word_array)
                g_array_append_val (word_array, word_index);
            }
        }
    }

  /* Return the itersection of the two*/
  if (opposing_pos_chars)
    {
      g_autoptr (IpuzCharset) first_pass = NULL;
      first_pass = ipuz_charset_builder_build (builder);

      builder = ipuz_charset_builder_new ();
      for (size_t index = 0;
           index < ipuz_charset_get_n_values (first_pass);
           index++)
        {
          IpuzCharsetIterValue val;
          guint opposing_pos_chars_count;

          val = ipuz_charset_get_value (first_pass, index);
          opposing_pos_chars_count =
            ipuz_charset_get_char_count (opposing_pos_chars, val.c);

          /* We're going to multiply these, so make sure we have a min
             of 1 */
          opposing_pos_chars_count = MAX (opposing_pos_chars_count, 1);
          val.count = MAX (val.count, 1);


          ipuz_charset_builder_set_char_count (builder,
                                               val.c,
                                               val.count *
                                               opposing_pos_chars_count);
        }
    }

  return ipuz_charset_builder_build (builder);
}

static IpuzCharset *
calculate_empty_filter_pos_chars (WordList    *word_list,
                                  const gchar *filter,
                                  guint        pos,
                                  IpuzCharset *intersect_charset,
                                  WordArray   *word_array)
{
  IpuzCharsetBuilder *builder;
  IpuzCharset *charset;
  IpuzCharsetIter *iter;
  guint filter_len;


  /* If filter is "??????" we just have to go through the charset and
   * each filter fragment to see if it exists. No need for a intersection list */
  builder = ipuz_charset_builder_new ();
  filter_len = g_utf8_strlen (filter, -1);

  charset = intersect_charset ? intersect_charset : word_list->index->charset;

  for (size_t index = 0;
       index < ipuz_charset_get_n_values (charset);
       index++)
    {
      FilterFragment fragment;
      FilterFragmentList fragment_list;
      IpuzCharsetIterValue val;

      val = ipuz_charset_get_value (charset, index);

      /* This should be fine. We call this after sanitizing the
       * filter. */
      fragment.length = filter_len;
      fragment.position = pos;
      fragment.char_index = ipuz_charset_get_char_index (word_list->index->charset, val.c);

      word_list_lookup_fragment (word_list, fragment, &fragment_list);
      if (fragment_list.len > 0)
        {
          ipuz_charset_builder_set_char_count (builder, val.c, fragment_list.len);

          if (word_array)
            {
              WordIndex word_index = {
                .length = filter_len,
              };

              for (guint i = 0; i < fragment_list.len; i++)
                {
                  word_index.index = (gint) fragment_list.data[i];
                  g_array_append_val (word_array, word_index);
                }
            }
        }
    }

  return ipuz_charset_builder_build (builder);
}

static void
calculate_intersect_special (WordList     *word_list,
                             const gchar  *filter1,
                             const gchar  *filter2,
                             gboolean      filter1_valid,
                             gboolean      filter2_valid,
                             gunichar      c,
                             IpuzCharset **intersecting_chars,
                             WordArray   **word_array1,
                             WordArray   **word_array2)
{
  if (word_array1)
    {
      if (filter1_valid)
        *word_array1 = word_list_get_array_list (word_list, filter1, WORD_LIST_MATCH);
      else
        *word_array1 = word_array_new ();
    }
  if (word_array2)
    {
      if (filter2_valid)
        *word_array2 = word_list_get_array_list (word_list, filter2, WORD_LIST_MATCH);
      else
        *word_array2 = word_array_new ();
    }
  /* create a Charset with the intersecting character in it */
  if (intersecting_chars)
    {
      IpuzCharsetBuilder *builder;
      guint filter1_len = 1;
      guint filter2_len = 1;

      if (word_array1)
        filter1_len = MAX ((*word_array1)->len, 1);
      if (word_array2)
        filter2_len = MAX ((*word_array2)->len, 1);

      builder = ipuz_charset_builder_new ();
      /* NOTE: setting the count requires the word_arrays to not
       * be NULL. If anything expects anything different in the
       * future, we'll have to calculate this. */
      /* Also, note it's fine to multiply these two numbers, as no
       * word section is longer than a gushort by definition. */
      ipuz_charset_builder_set_char_count (builder, c, filter1_len * filter2_len);
      *intersecting_chars = ipuz_charset_builder_build (builder);
    }
}


static IpuzCharset *
calculate_intersect_phase1 (WordList    *word_list,
                            const gchar *filter,
                            guint        pos,
                            guint        n_chars)

{
  /* We can bypass the linear search iff we don't count about the
   * intersecting_charset. This is significantly faster */
  if (n_chars == 0)
    return calculate_empty_filter_pos_chars (word_list,
                                             filter, pos,
                                             NULL, NULL);
  else
    return calculate_pos_chars (word_list,
                                filter, pos,
                                NULL, NULL);
}

static IpuzCharset *
calculate_intersect_phase2 (WordList    *word_list,
                            const gchar *filter,
                            guint        pos,
                            guint        n_chars,
                            IpuzCharset *intersecting_charset,
                            WordArray   *word_array)
{
  if (n_chars == 0)
    return calculate_empty_filter_pos_chars (word_list,
                                             filter, pos,
                                             intersecting_charset,
                                             word_array);
  else
    return calculate_pos_chars (word_list,
                                filter, pos,
                                intersecting_charset,
                                word_array);
}

static void
calculate_intersect_phase3 (WordList    *word_list,
                            const gchar *filter,
                            guint        pos,
                            guint        n_chars,
                            IpuzCharset *intersecting_charset,
                            WordArray   *word_array)
{
  /* We don't need this extra calculated charset so can throw it
   * out. */
  IpuzCharset *unused = NULL;

  /* The sold purpose of this phase is to get the word_array from the
   * first filter. if word_array is NULL, we can skip this phase. */
  if (word_array == NULL)
    return;
  if (n_chars == 0)
    unused = calculate_empty_filter_pos_chars (word_list,
                                               filter, pos,
                                               intersecting_charset,
                                               word_array);
  else
    unused = calculate_pos_chars (word_list,
                                  filter, pos,
                                  intersecting_charset,
                                  word_array);

  ipuz_charset_unref (unused);
}

/**
 * word_list_find_intersection:
 * @word_list: A #WordList
 * @filter1: The first filter
 * @pos1: Offset of intersection in @filter1
 * @filter2: The second filter
 * @pos2: Offset of intersection in @filter2
 * @intersecting_chars (nullable)(out): Out paramter for the #IpuzCharset
 * @word_array1 (nullable)(out): Out parameter for the list of possible words for the intersection in the direction of @filter1
 * @word_array2 (nullable)(out): Out parameter for the list of possible words for the intersection in the direction of @filter2
 *
 * Calculates the possible intersections between two filters. If
 * @word_array1 and @word_array2 are NULL, then it avoids calculating
 * them which can save computation time. If intersecting_chars is NULL, then this avoids calculating the number
 *
 * Note: if @filter1 or @filter2 are NULL, we just return the list of
 * words/characters in the other filter.
 **/
void
word_list_find_intersection (WordList     *word_list,
                             const gchar  *filter1,
                             const guint   pos1,
                             const gchar  *filter2,
                             const guint   pos2,
                             IpuzCharset **intersecting_chars,
                             WordArray   **word_array1,
                             WordArray   **word_array2)
{
  guint n_chars1 = 0;
  guint n_chars2 = 0;
  g_autoptr (IpuzCharset) charset1 = NULL;
  g_autoptr (IpuzCharset) charset2 = NULL;
  gboolean filter1_valid = FALSE;
  gboolean filter2_valid = FALSE;
  gunichar c1 = 0;
  gunichar c2 = 0;

  g_return_if_fail (WORD_IS_LIST (word_list));

  /* Sanity check our inputs */
  /* At least one filter has to be set */
  g_return_if_fail (filter1 != NULL || filter2 != NULL);

  /* First make sure pos is within the bounds of the filter in */
  if (filter1)
    {
      c1 = get_nth_char (filter1, pos1);
      g_return_if_fail (c1 != 0);
    }
  if (filter2)
    {
      c2 = get_nth_char (filter2, pos2);
      g_return_if_fail (c2 != 0);
    }

  /* Normalize wildcards */
  if (c1 == ' ') c1 = '?';
  if (c2 == ' ') c2 = '?';

  /* If we have two intersecting characters, they should be the same */
  if (filter1 && filter2)
    g_return_if_fail (c1 == c2);


  /* Make sure our filter makes sense and includes characters from the
   * word list. */
  filter1_valid = validate_filter (word_list, filter1, &n_chars1);
  filter2_valid = validate_filter (word_list, filter2, &n_chars2);

  /* If the two filters intersect on a real letter, then we can avoid
   * a lot of calculations. Example: "??C??" intersecting with "C??"
   * on the 'C' character. In this instance, we just */
  if (!g_utf8_strchr (WORD_LIST_CHAR_WILDCARDS, -1, c1))
    {
      calculate_intersect_special (word_list,
                                   filter1, filter2,
                                   filter1_valid, filter2_valid,
                                   c1,
                                   intersecting_chars,
                                   word_array1, word_array2);
      return;
    }

  /* From here on out, we have valid inputs so we should set our
   * output variables to good defaults */
  if (intersecting_chars)
    *intersecting_chars = NULL;
  if (word_array1)
    *word_array1 = word_array_new ();
  if (word_array2)
    *word_array2 = word_array_new ();


  /* If either/both of the filters isn't valid then we can
   * short-circuit calculating the intersection. Phase2 will calculate
   * both the word_list and charset, so we can reuse it for this */
  if (!filter1_valid || !filter2_valid)
    {
      if (filter1_valid)
        {
          charset1 = calculate_intersect_phase2 (word_list,
                                                 filter1, pos1,
                                                 n_chars1,
                                                 NULL,
                                                 word_array1?*word_array1:NULL);
          if (intersecting_chars)
            *intersecting_chars = ipuz_charset_ref (charset1);
        }
      else if (filter2_valid)
        {
          charset2 = calculate_intersect_phase2 (word_list,
                                                 filter2, pos2,
                                                 n_chars2,
                                                 NULL,
                                                 word_array2?*word_array2:NULL);
          if (intersecting_chars)
            *intersecting_chars = ipuz_charset_ref (charset2);
        }

      return;
    }


  /* Both filter1 and filter2 are valid, so we have to calculate the
   * intersections. This is done in three phases.
   */

  /* Phase 1: Calculate the set of possible characters at pos1 for
   * filter1. We don't need to worry about calculating the word_list
   * yet */
  charset1 = calculate_intersect_phase1 (word_list,
                                         filter1, pos1,
                                         n_chars1);

  /* phase 2: Calculate the overlapping characters and the second
   * word_array. If necessary, calculate the second filters word_array */
  charset2 =
    calculate_intersect_phase2 (word_list,
                                filter2, pos2,
                                n_chars2,
                                charset1,
                                word_array2?*word_array2:NULL);

  /* phase 3: Recalculate the word_array first filter with the
   * overlap */
  calculate_intersect_phase3 (word_list,
                              filter1, pos1,
                              n_chars1,
                              charset2,
                              word_array1?*word_array1:NULL);

  /* copy our charset over to intersecting_chars */
  if (intersecting_chars)
    *intersecting_chars = ipuz_charset_ref (charset2);
}
