/* edit-word-list.h
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>
#include "grid-state.h"


G_BEGIN_DECLS

#define EDIT_TYPE_WORD_LIST (edit_word_list_get_type())
G_DECLARE_FINAL_TYPE (EditWordList, edit_word_list, EDIT, WORD_LIST, GtkWidget);


void edit_word_list_update (EditWordList *self,
                            GridState    *edit_state);


G_END_DECLS
