/* play-cell.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <librsvg/rsvg.h>
#include "play-cell.h"
#include "play-marshaller.h"
#include "crosswords-enums.h"
#include "play-style.h"


#define MIN_ARROWWORD_SCALE 6


enum
{
  PROP_0,
  PROP_ROW,
  PROP_COLUMN,
  N_PROPS
};
static GParamSpec *obj_props[N_PROPS] =  {NULL, };


enum {
  GUESS,
  GUESS_AT_CELL,
  DO_COMMAND,
  CELL_SELECTED,
  N_SIGNALS
};
static guint obj_signals[N_SIGNALS] = { 0 };


struct _PlayCell
{
  GtkWidget parent_instance;

  /* child labels. */
  /* We only support one cluenum for now */
  GtkWidget *cluenum_label;
  GtkWidget *main_label;
  GtkWidget *secondary_label;

  /* style information */
  IpuzStyleMark cluenum_location;

  IpuzCellType cell_type;
  LayoutItemCellStyle css_class;
  LayoutItemCellForegroundStyle foreground_css_class;
  LayoutItemSplitHighlightState split_highlight_state;
  IpuzStyleShape shapebg;
  gboolean bg_color_set;
  GdkRGBA bg_color;
  gboolean text_color_set;
  GdkRGBA text_color;

  /* Cached current label settings */
  gchar *current_main_label_text;
  guint current_font_size;
  gboolean current_text_color_set;
  GdkRGBA current_text_color;

  gchar *current_cluenum_label_text;
  gfloat current_cluenum_xalign;
  gfloat current_cluenum_yalign;
  gboolean current_cluenum_text_color_set;
  GdkRGBA current_cluenum_text_color;

  /* Input */
  gboolean editable;
  gboolean in_multichar_mode;
  gboolean has_secondary_text;
  gchar *multichar_buffer;
  gchar *multichar_buffer_save;
  GtkIMContext *im_context;

  gulong commit_cb;
  gulong preedit_changed_cb;
  gulong preedit_end_cb;

  LayoutConfig config;

  /* Static value for the cell. Does not change */
  guint row;
  guint column;
};


static void     play_cell_init                (PlayCell              *self);
static void     play_cell_class_init          (PlayCellClass         *klass);
static void     play_cell_set_property        (GObject               *object,
                                               guint                  prop_id,
                                               const GValue          *value,
                                               GParamSpec            *pspec);
static void     play_cell_get_property        (GObject               *object,
                                               guint                  prop_id,
                                               GValue                *value,
                                               GParamSpec            *pspec);
static void     play_cell_dispose             (GObject               *object);
static void     play_cell_size_allocate       (GtkWidget             *widget,
                                               int                    width,
                                               int                    height,
                                               int                    baseline);
static void     play_cell_measure             (GtkWidget             *widget,
                                               GtkOrientation         orientation,
                                               int                    for_size,
                                               int                   *minimum,
                                               int                   *natural,
                                               int                   *minimum_baseline,
                                               int                   *natural_baseline);
static void     play_cell_snapshot            (GtkWidget             *widget,
                                               GtkSnapshot           *snapshot);
static void     play_cell_focus_in_cb         (GtkEventController    *controller,
                                               PlayCell              *cell);
static void     play_cell_focus_out_cb        (GtkEventController    *controller,
                                               PlayCell              *cell);
static void     play_cell_commit_cb           (GtkIMContext          *context,
                                               const char            *str,
                                               PlayCell              *cell);
static void     play_cell_preedit_changed_cb  (GtkIMContext          *context,
                                               PlayCell              *cell);
static void     play_cell_preedit_end_cb      (GtkIMContext          *context,
                                               PlayCell              *cell);
static gboolean play_cell_key_pressed_cb      (GtkEventControllerKey *controller,
                                               guint                  keyval,
                                               guint                  keycode,
                                               GdkModifierType        state,
                                               gpointer               user_data);
static void     play_cell_multichar_append    (PlayCell              *self,
                                               const gchar           *text);
static void     play_cell_multichar_backspace (PlayCell              *self);
static void     play_cell_multichar_toggle    (PlayCell              *self);
static void     play_cell_multichar_cancel    (PlayCell              *self);
static void     play_cell_multichar_commit    (PlayCell              *self);
static void     set_label_font_size           (GtkLabel              *label,
                                               guint                  size,
                                               PangoAttrList         *attrs);
static void     set_label_color               (GtkLabel              *label,
                                               GdkRGBA                text_color,
                                               PangoAttrList         *attrs);
static void     update_main_label             (PlayCell              *self,
                                               const gchar           *text,
                                               guint                  size,
                                               gboolean               text_color_set,
                                               GdkRGBA                text_color,
                                               gboolean               overrun,
                                               PangoAttrList         *extra_attrs);
static guint    calc_main_label_size          (PlayCell              *cell,
                                               const gchar           *text,
                                               PangoAttrList         *attrs,
                                               gboolean              *overrun);


G_DEFINE_TYPE (PlayCell, play_cell, GTK_TYPE_WIDGET);


static void
play_cell_init (PlayCell *self)
{
  GtkEventController *controller;

  gtk_widget_set_focusable (GTK_WIDGET (self), TRUE);
  gtk_widget_set_can_focus (GTK_WIDGET (self), TRUE);

  controller = GTK_EVENT_CONTROLLER (gtk_event_controller_focus_new ());
  g_signal_connect (G_OBJECT (controller), "enter", G_CALLBACK (play_cell_focus_in_cb), self);
  g_signal_connect (G_OBJECT (controller), "leave", G_CALLBACK (play_cell_focus_out_cb), self);
  gtk_widget_add_controller (GTK_WIDGET (self), controller);

  /* GtkIMMultiContext is broken with GtkIMContextQuartz at the moment.
   * If that was fixed, we could use the native input methods and UI (including e.g. long-presses).
   * On the other hand, forcing the simple IM gives us consistency with the Linux behavior with
   * regards to Unicode inputs using Ctrl+Shift+u. Let's use that for now.
   */
#ifdef GDK_WINDOWING_MACOS
  self->im_context = gtk_im_context_simple_new ();
#else
  self->im_context = gtk_im_multicontext_new ();
#endif /* GDK_WINDOWING_MACOS */

  gtk_im_context_set_client_widget (self->im_context, GTK_WIDGET (self));
  self->commit_cb = 0;
  self->preedit_changed_cb = 0;
  self->preedit_end_cb = 0;

  controller = GTK_EVENT_CONTROLLER (gtk_event_controller_key_new ());
  gtk_event_controller_set_propagation_phase (controller, GTK_PHASE_TARGET);
  gtk_event_controller_key_set_im_context (GTK_EVENT_CONTROLLER_KEY (controller),
                                           self->im_context);
  g_signal_connect (G_OBJECT (controller), "key-pressed", G_CALLBACK (play_cell_key_pressed_cb), NULL);
  gtk_widget_add_controller (GTK_WIDGET (self), controller);

  self->cluenum_label = gtk_label_new ("");
  gtk_widget_add_css_class (self->cluenum_label, "cluenum");
  self->cluenum_location = IPUZ_STYLE_MARK_TL;
  gtk_label_set_xalign (GTK_LABEL (self->cluenum_label), 0.0);
  gtk_widget_set_child_visible (self->cluenum_label, FALSE);
  gtk_widget_set_parent (self->cluenum_label, GTK_WIDGET (self));

  self->main_label = gtk_label_new (NULL);
  gtk_widget_add_css_class (self->main_label, "main");
  gtk_label_set_xalign (GTK_LABEL (self->main_label), 0.5);
  gtk_widget_set_child_visible (self->main_label, FALSE);
  gtk_widget_set_parent (self->main_label, GTK_WIDGET (self));
  gtk_label_set_justify (GTK_LABEL (self->main_label), GTK_JUSTIFY_CENTER);
  gtk_label_set_wrap_mode (GTK_LABEL (self->main_label), PANGO_WRAP_WORD_CHAR);

  self->secondary_label = gtk_label_new (NULL);
  gtk_widget_add_css_class (self->secondary_label, "main");
  gtk_label_set_xalign (GTK_LABEL (self->secondary_label), 0.5);
  gtk_widget_set_child_visible (self->secondary_label, FALSE);
  gtk_widget_set_parent (self->secondary_label, GTK_WIDGET (self));
  gtk_label_set_justify (GTK_LABEL (self->secondary_label), GTK_JUSTIFY_CENTER);
  gtk_label_set_wrap (GTK_LABEL (self->secondary_label), TRUE); /* This is always true for a secondary label */
  gtk_label_set_wrap_mode (GTK_LABEL (self->secondary_label), PANGO_WRAP_WORD_CHAR);
}

static void
play_cell_class_init (PlayCellClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->set_property = play_cell_set_property;
  object_class->get_property = play_cell_get_property;
  object_class->dispose = play_cell_dispose;

  widget_class->size_allocate = play_cell_size_allocate;
  widget_class->measure = play_cell_measure;
  widget_class->snapshot = play_cell_snapshot;

  gtk_widget_class_set_css_name (widget_class, "play-cell");
  gtk_widget_class_set_accessible_role (widget_class, GTK_ACCESSIBLE_ROLE_TEXT_BOX);

  obj_props[PROP_ROW] = g_param_spec_uint ("row",
                                           "Row",
                                           "Row coordinate of the cell in the crossword",
                                           0, G_MAXUINT8, 0,
                                           G_PARAM_READWRITE|G_PARAM_CONSTRUCT_ONLY);
  obj_props[PROP_COLUMN] = g_param_spec_uint ("column",
                                              "column",
                                              "Column coordinate of the cell in the crossword",
                                              0, G_MAXUINT8, 0,
                                              G_PARAM_READWRITE|G_PARAM_CONSTRUCT_ONLY);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);

  obj_signals [GUESS] =
    g_signal_new ("guess",
                  PLAY_TYPE_CELL,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  G_TYPE_STRING);

  obj_signals [GUESS_AT_CELL] =
    g_signal_new ("guess-at-cell",
                  PLAY_TYPE_CELL,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 3,
                  G_TYPE_STRING,
                  G_TYPE_UINT, G_TYPE_UINT);

  obj_signals [DO_COMMAND] =
    g_signal_new ("do-command",
                  PLAY_TYPE_CELL,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  GRID_TYPE_CMD_KIND);

  obj_signals [CELL_SELECTED] =
    g_signal_new ("cell-selected",
                  PLAY_TYPE_CELL,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 2,
                  G_TYPE_UINT, G_TYPE_UINT);
}

static void
play_cell_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  PlayCell *self;

  g_return_if_fail (PLAY_IS_CELL (object));

  self = PLAY_CELL (object);

  switch (prop_id)
    {
    case PROP_ROW:
      self->row = g_value_get_uint (value);
      break;
    case PROP_COLUMN:
      self->column = g_value_get_uint (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
play_cell_get_property (GObject     *object,
                         guint       prop_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  PlayCell *self;

  g_return_if_fail (PLAY_IS_CELL (object));

  self = PLAY_CELL (object);

  switch (prop_id)
    {
    case PROP_ROW:
      g_value_set_uint (value, self->row);
      break;
    case PROP_COLUMN:
      g_value_set_uint (value, self->column);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
play_cell_dispose (GObject *object)
{
  PlayCell *self = PLAY_CELL (object);
  GtkWidget *child;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);
  self->cluenum_label = NULL;
  self->main_label = NULL;
  self->secondary_label = NULL;

  g_clear_object (&self->im_context);
  g_clear_pointer (&self->multichar_buffer, g_free);
  g_clear_pointer (&self->multichar_buffer_save, g_free);
  g_clear_pointer (&self->current_main_label_text, g_free);

  G_OBJECT_CLASS (play_cell_parent_class)->dispose (object);
}

static void
size_allocate_clue_block (GtkWidget *widget,
                          int        width,
                          int        height,
                          int        baseline)
{
  PlayCell *self;
  GtkAllocation allocation;
  guint scale;
  gint label_minimum, label_natural, label_minimum_baseline, label_natural_baseline;

  g_return_if_fail (PLAY_IS_CELL (widget));

  self = PLAY_CELL (widget);

  /* Magic */
  scale = (self->config.base_size/2) - self->config.base_size/8;
  if (scale < MIN_ARROWWORD_SCALE)
    {
      gtk_widget_set_child_visible (self->main_label, FALSE);
      gtk_widget_set_child_visible (self->secondary_label, FALSE);
      return;
    }

  gtk_widget_set_child_visible (self->main_label, TRUE);
  gtk_widget_set_child_visible (self->secondary_label, self->has_secondary_text);

  allocation.x = 1;
  allocation.y = 1;
  allocation.width = width - 2;
  if (self->has_secondary_text)
    allocation.height = (height - 2)/2;
  else
    allocation.height = height -2;

  set_label_font_size (GTK_LABEL (self->main_label), scale, NULL);
  gtk_widget_measure (self->main_label,
                      GTK_ORIENTATION_VERTICAL, allocation.width,
                      &label_minimum, &label_natural,
                      &label_minimum_baseline, &label_natural_baseline);
  allocation.height = MAX (allocation.height, label_minimum);
  gtk_widget_size_allocate (self->main_label,
                            &allocation,
                            baseline);

  if (self->has_secondary_text)
    {
      set_label_font_size (GTK_LABEL (self->secondary_label), scale, NULL);
      allocation.y = (height - 2)/2 + 1;
      allocation.height = (height - 2)/2;
      gtk_widget_measure (self->secondary_label,
                          GTK_ORIENTATION_VERTICAL, allocation.width,
                          &label_minimum, &label_natural,
                          &label_minimum_baseline, &label_natural_baseline);
      allocation.height = MAX (allocation.height, label_minimum);
      gtk_widget_size_allocate (self->secondary_label,
                                &allocation,
                                baseline);
    }
}

static void
play_cell_size_allocate (GtkWidget *widget,
                         int        width,
                         int        height,
                         int        baseline)
{
  PlayCell *self;
  GtkAllocation allocation;
  gint label_minimum, label_natural, label_minimum_baseline, label_natural_baseline;

  g_return_if_fail (PLAY_IS_CELL (widget));

  self = PLAY_CELL (widget);

  /* Special-case arrowword blocks as they're different enough. */
  if (self->css_class == LAYOUT_ITEM_STYLE_CLUE_BLOCK)
    {
      size_allocate_clue_block (widget, width, height, baseline);
      return;
    }

  gtk_widget_set_child_visible (self->secondary_label, FALSE);
  gtk_widget_measure (self->cluenum_label,
                      GTK_ORIENTATION_HORIZONTAL, height -2,
                      &label_minimum, &label_natural,
                      &label_minimum_baseline, &label_natural_baseline);
  allocation.x = 1;
  allocation.y = 1;
  allocation.width = MAX (width - 2, label_minimum);
  allocation.height = height - 2;
  gtk_widget_size_allocate (self->cluenum_label,
                            &allocation,
                            baseline);

  gtk_widget_measure (self->main_label,
                      GTK_ORIENTATION_VERTICAL, allocation.width,
                      &label_minimum, &label_natural,
                      &label_minimum_baseline, &label_natural_baseline);
  allocation.height = MAX (allocation.height, label_minimum);
  gtk_widget_size_allocate (self->main_label,
                            &allocation,
                            baseline);
}

static void
play_cell_measure (GtkWidget      *widget,
                   GtkOrientation  orientation,
                   int             for_size,
                   int            *minimum,
                   int            *natural,
                   int            *minimum_baseline,
                   int            *natural_baseline)
{
  PlayCell *self;

  g_return_if_fail (PLAY_IS_CELL (widget));

  self = PLAY_CELL (widget);

  *minimum = 3 * self->config.base_size;
  *natural = 3 * self->config.base_size;

  /* We now need to force measure child widgets. This makes a
   * g_warning go away but otherwise does nothing, as the size comes
   * from the base_size */
  for (GtkWidget *child = gtk_widget_get_first_child (widget);
       child != NULL;
       child = gtk_widget_get_next_sibling (child))
    {
      gint child_min, child_nat, child_baseline, child_nat_baseline;
      if (gtk_widget_should_layout (child) && gtk_widget_get_child_visible (child))
        {
          gtk_widget_measure (child, orientation, *minimum,
                              &child_min, &child_nat,
                              &child_baseline, &child_nat_baseline);
        }
    }
}

struct shapebg_asset {
  IpuzStyleShape value;
  const char *name;
  gboolean hollow;
};

static const struct shapebg_asset shapebg_map[] = {
  { IPUZ_STYLE_SHAPE_CIRCLE,         "circle.svg",         TRUE},
  { IPUZ_STYLE_SHAPE_ARROW_LEFT,     "arrow-left.svg",     FALSE },
  { IPUZ_STYLE_SHAPE_ARROW_RIGHT,    "arrow-right.svg",    FALSE },
  { IPUZ_STYLE_SHAPE_ARROW_UP,       "arrow-up.svg",       FALSE },
  { IPUZ_STYLE_SHAPE_ARROW_DOWN,     "arrow-down.svg",     FALSE },
  { IPUZ_STYLE_SHAPE_TRIANGLE_LEFT,  "triangle-left.svg",  FALSE },
  { IPUZ_STYLE_SHAPE_TRIANGLE_RIGHT, "triangle-right.svg", FALSE },
  { IPUZ_STYLE_SHAPE_TRIANGLE_UP,    "triangle-up.svg",    FALSE },
  { IPUZ_STYLE_SHAPE_TRIANGLE_DOWN,  "triangle-down.svg",  FALSE },
  { IPUZ_STYLE_SHAPE_DIAMOND,        "diamond.svg",        FALSE },
  { IPUZ_STYLE_SHAPE_CLUB,           "club.svg",           FALSE },
  { IPUZ_STYLE_SHAPE_HEART,          "heart.svg",          FALSE },
  { IPUZ_STYLE_SHAPE_SPADE,          "spade.svg",          FALSE },
  { IPUZ_STYLE_SHAPE_STAR,           "star.svg",           FALSE },
  { IPUZ_STYLE_SHAPE_SQUARE,         "square.svg",         TRUE },
  { IPUZ_STYLE_SHAPE_RHOMBUS,        "rhombus.svg",        TRUE },
  { IPUZ_STYLE_SHAPE_SLASH,          "slash.svg",          FALSE },
  { IPUZ_STYLE_SHAPE_BACKSLASH,      "backslash.svg",      FALSE },
  { IPUZ_STYLE_SHAPE_X,              "x.svg",              FALSE },
};

static char *
shapebg_asset_name (IpuzStyleShape shapebg)
{
  guint i;

  for (i = 0; i < G_N_ELEMENTS (shapebg_map); i++)
    {
      if (shapebg_map[i].value == shapebg)
        return g_strdup (shapebg_map[i].name);
    }

  return NULL;
}

static gboolean
shapebg_asset_hollow (IpuzStyleShape shapebg)
{
  guint i;

  for (i = 0; i < G_N_ELEMENTS (shapebg_map); i++)
    {
      if (shapebg_map[i].value == shapebg)
        return shapebg_map[i].hollow;
    }

  return FALSE;
}

static char *
make_stylesheet (const GdkRGBA *color)
{
  g_autofree char *css_color = gdk_rgba_to_string (color);

  return g_strdup_printf (".shapebg-stroke { stroke-width: 5%%; stroke: %s; fill: none; }\n"
                          ".shapebg-fill { fill: %s; stroke: none;}\n",
                          css_color,
                          css_color);
}

static void
play_cell_draw_shapebg (cairo_t        *cr,
                        IpuzStyleShape  shape,
                        const GdkRGBA  *color,
                        double          x,
                        double          y,
                        double          width,
                        double          height)
{
  if (shape == IPUZ_STYLE_SHAPE_NONE)
    return;

  g_autofree char *asset_name = shapebg_asset_name (shape);
  if (asset_name == NULL)
    return;

  g_autofree char *path = g_strdup_printf ("/org/gnome/Crosswords/icons/shapebg/%s", asset_name);

  GError *error = NULL;
  g_autoptr (GInputStream) stream = g_resources_open_stream (path, G_RESOURCE_LOOKUP_FLAGS_NONE, &error);
  if (!stream) {
    g_error ("Bug!!! - cannot locate resource %s for %u: %s\n", path, (guint) shape, error->message);
  }

  RsvgHandle *handle = rsvg_handle_new_from_stream_sync (stream, NULL, RSVG_HANDLE_FLAGS_NONE, NULL, &error);
  if (!handle) {
    g_error ("Bug!!! - invalid SVG in resource %s: %s\n", path, error->message);
  }

  g_autofree char *stylesheet = make_stylesheet (color);
  if (!rsvg_handle_set_stylesheet (handle, (const guint8 *) stylesheet, strlen (stylesheet), &error))
    {
      g_error ("Bug!!! - invalid CSS for shapebg: %s\n", error->message);
    }

  RsvgRectangle viewport = {
    .x = x,
    .y = y,
    .width = width,
    .height = height,
  };
  if (!rsvg_handle_render_document (handle, cr, &viewport, &error))
    {
      g_error ("Bug?  Could not render shapebg: %s", error->message);
    }

  g_object_unref (handle);
}


/* FIXME(highcontrast): We should make these more extreme in high-contrast mode */
#define LIGHTEN_MASK "#FFFFFF20"
#define DARKEN_MASK  "#00000020"

static void
play_cell_snapshot (GtkWidget   *widget,
                    GtkSnapshot *snapshot)
{
  PlayCell *cell;
  gint size;

  g_return_if_fail (PLAY_IS_CELL (widget));

  cell = PLAY_CELL (widget);
  size = cell->config.base_size * 3;

  /* Draw the background. Either from an explicit color in the puzzle,
   * or calculated from CSS */
  if (cell->bg_color_set &&
      (cell->css_class == LAYOUT_ITEM_STYLE_NORMAL ||
       cell->css_class == LAYOUT_ITEM_STYLE_BLOCK))
    {
      gtk_snapshot_append_color (snapshot, &cell->bg_color,
                                 &GRAPHENE_RECT_INIT (0, 0, size, size));
    }

  /* For blocks, draw a small repeated dot pattern over it to add a texture */
  if (cell->cell_type == IPUZ_CELL_BLOCK)
    {
      GskRoundedRect dot;
      GdkRGBA pattern_color;

      if (cell->bg_color_set)
        {
          gfloat luminance;
          luminance = play_style_color_luminance (&cell->bg_color);

          if (luminance > 0.5)
            gdk_rgba_parse (&pattern_color, DARKEN_MASK);
          else
            gdk_rgba_parse (&pattern_color, LIGHTEN_MASK);
        }
      else
        {
          /* FIXME(CSS): There's no obvious way to get the background
           * color from CSS (as it could be a pattern). We assume it's
           * overall luminance is dark unless otherwise set and do a
           * lightening pattern
           */
          gdk_rgba_parse (&pattern_color, LIGHTEN_MASK);
        }

      gtk_snapshot_push_repeat (snapshot,
                                &GRAPHENE_RECT_INIT (0, 0, size, size),
                                NULL);
      gsk_rounded_rect_init_from_rect (&dot,
                                       &GRAPHENE_RECT_INIT (0, 0, 4, 4), 3);
      gtk_snapshot_push_rounded_clip (snapshot, &dot);
      gtk_snapshot_append_color (snapshot, &pattern_color, &dot.bounds);
      gtk_snapshot_pop (snapshot);
      gtk_snapshot_pop (snapshot);
    }

  /* Draw the shapebg, if it exists */
  if (cell->shapebg != IPUZ_STYLE_SHAPE_NONE)
    {
      /* FIXME(CSS): get the color for the shapebg from the CSS file
       */
      GdkRGBA color;
      cairo_t *cr;

      if (shapebg_asset_hollow (cell->shapebg))
        gdk_rgba_parse (&color, "rgba(0.0,0.0,0.0,0.35)");
      else
        gdk_rgba_parse (&color, "rgba(0.0,0.0,0.0,0.15)");

      cr = gtk_snapshot_append_cairo (snapshot,
                                      &GRAPHENE_RECT_INIT (0, 0, size, size));
      play_cell_draw_shapebg (cr, cell->shapebg,
                              &color,
                              0, 0, size, size);
    }
  if (cell->in_multichar_mode)
   {
     GdkRGBA color;

     if (cell->text_color_set)
       color = cell->text_color;
     else
       gdk_rgba_parse (&color, "rgba(0.0,0.0,0.0,0.5)");

     gtk_snapshot_append_color (snapshot, &color,
                                &GRAPHENE_RECT_INIT (0, 0, size, cell->config.border_size));
     gtk_snapshot_append_color (snapshot, &color,
                                &GRAPHENE_RECT_INIT (0, cell->config.border_size, cell->config.border_size, size - 2 * cell->config.border_size));
     gtk_snapshot_append_color (snapshot, &color,
                                &GRAPHENE_RECT_INIT (0, size - cell->config.border_size, size, cell->config.border_size));
     gtk_snapshot_append_color (snapshot, &color,
                                &GRAPHENE_RECT_INIT (size - cell->config.border_size, cell->config.border_size, cell->config.border_size, size - 2 * cell->config.border_size));
   }

  if (cell->split_highlight_state != LAYOUT_SPLIT_HIGHLIGHT_NONE)
    {
      GdkRGBA highlight_color;
      gint y = 0;
      gint height = size;
      gdk_rgba_parse (&highlight_color, "rgba(1.0,1.0,1.0,0.20)");
      if (cell->split_highlight_state == LAYOUT_SPLIT_HIGHLIGHT_PRIMARY)
        {
          height = size / 2;
        }
      else if (cell->split_highlight_state == LAYOUT_SPLIT_HIGHLIGHT_SECONDARY)
        {
          height = size / 2;
          y = height + 1;
        }

      gtk_snapshot_append_color (snapshot, &highlight_color,
                                 &GRAPHENE_RECT_INIT (0, y, size, height));
    }

  GTK_WIDGET_CLASS (play_cell_parent_class)->snapshot (widget, snapshot);
}

static void
play_cell_focus_in_cb (GtkEventController *controller,
                       PlayCell           *cell)
{
  gtk_widget_queue_draw (GTK_WIDGET (cell));
  gtk_im_context_focus_in (cell->im_context);
}

static void
play_cell_focus_out_cb (GtkEventController *controller,
                        PlayCell           *cell)
{
  if (cell->in_multichar_mode)
    {
      play_cell_multichar_commit (cell);
      play_cell_multichar_cancel (cell);
    }
  gtk_im_context_focus_out (cell->im_context);
}

/*
 * Text Handling
 */

static void
play_cell_commit_cb (GtkIMContext *context,
                     const char   *str,
                     PlayCell     *cell)
{
  g_autofree gchar *upper = NULL;

  if (g_strcmp0 (str, " ") == 0)
    {
      g_signal_emit (cell, obj_signals[GUESS], 0, NULL);
      return;
    }

  /* Always normalize to uppercase */
  upper = g_utf8_strup (str, -1);
  if (cell->in_multichar_mode)
    {
      guint font_size;
      gboolean overrun;

      play_cell_multichar_append (cell, upper);
      /* We don't commit the intermediate editing characters, so we
       * need to update the label right away */
      font_size = calc_main_label_size (cell, cell->multichar_buffer, NULL, &overrun);
      update_main_label (cell, cell->multichar_buffer, font_size, cell->text_color_set, cell->text_color, overrun, NULL);
    }
  else
    {
      g_signal_emit (cell, obj_signals[GUESS], 0, upper);
    }
}

static void
play_cell_preedit_changed_cb (GtkIMContext *context,
                              PlayCell     *cell)
{
  g_autofree char *preedit_str = NULL;
  g_autofree char *str = NULL;
  PangoAttrList *attrs = NULL;
  int cursor_pos;
  guint font_size;
  gboolean overrun = FALSE;

  if (cell->css_class == LAYOUT_ITEM_STYLE_INITIAL_VAL)
    return;

  gtk_im_context_get_preedit_string (context, &preedit_str, &attrs, &cursor_pos);
  if (!preedit_str || preedit_str[0] == '\0')
    return;

  if (cell->in_multichar_mode)
    {
      if (cell->multichar_buffer)
        {
          PangoAttrList *other_attrs;

          other_attrs = pango_attr_list_new ();

          /* FIXME: I don't think we have this quite correct. I think
           * the only way to fix it is to */
          pango_attr_list_splice (other_attrs, attrs,
                                  g_utf8_strlen (cell->multichar_buffer, -1),
                                  g_utf8_strlen (preedit_str, -1) + 1);
          str = g_strconcat (cell->multichar_buffer, preedit_str, NULL);
          pango_attr_list_unref (attrs);
          attrs = other_attrs;
        }
      else
        {
          str = g_strdup (preedit_str);
        }
    }
  else
    str = g_strdup (preedit_str);

  font_size = calc_main_label_size (cell, str, attrs, &overrun);
  update_main_label (cell, str, font_size, cell->text_color_set, cell->text_color, overrun, attrs);

  pango_attr_list_unref (attrs);
}

static void
play_cell_preedit_end_cb (GtkIMContext *context,
                          PlayCell   *cell)
{
  // FIXME(input): If the user started editing the cell with an
  // invalid IM sequence, we are currently in a broken state. We need
  // to reset the label to be the correct value However, we don't
  // store that info. We need to save it and restore it here.

  // Also, restore the multichar state
}


static gboolean
play_cell_key_pressed_cb (GtkEventControllerKey *controller,
                          guint                  keyval,
                          guint                  keycode,
                          GdkModifierType        state,
                          gpointer               user_data)
{
  PlayCell *cell = PLAY_CELL (gtk_event_controller_get_widget (GTK_EVENT_CONTROLLER (controller)));
  GridCmdKind kind = GRID_CMD_KIND_NONE;

  /* Remove the default mod_mask */
  state &= gtk_accelerator_get_default_mod_mask ();

  /* We don't want to catch anythint with Alt */
  if (state & GDK_ALT_MASK)
    return FALSE;

  switch (keyval)
    {
    case GDK_KEY_Escape:
      if (cell->in_multichar_mode)
        {
          play_cell_multichar_cancel (cell);
          return TRUE;
        }
      else if (cell->editable)
        {
          play_cell_multichar_toggle (cell);
          return TRUE;
        }
      break;
    case GDK_KEY_period:
      if (state & GDK_CONTROL_MASK &&
          cell->editable)
        {
          play_cell_multichar_toggle (cell);
          return TRUE;
        }
      break;
    case GDK_KEY_Delete:
      kind = GRID_CMD_KIND_DELETE;
      break;
    case GDK_KEY_BackSpace:
      if (cell->in_multichar_mode)
        {
          play_cell_multichar_backspace (cell);
          return TRUE;
        }
      kind = GRID_CMD_KIND_BACKSPACE;
      break;
    case GDK_KEY_Tab:
      if (state & GDK_SHIFT_MASK)
        kind = GRID_CMD_KIND_BACK_EMPTYCELL;
      else
        kind = GRID_CMD_KIND_FORWARD_EMPTYCELL;
      break;
    case GDK_KEY_ISO_Left_Tab:
      kind = GRID_CMD_KIND_BACK_EMPTYCELL;
      break;
    case GDK_KEY_n:
      if (state & GDK_CONTROL_MASK)
        kind = GRID_CMD_KIND_FORWARD_CLUE;
      break;
    case GDK_KEY_p:
      if (state & GDK_CONTROL_MASK)
        kind = GRID_CMD_KIND_BACK_CLUE;
      break;
    case GDK_KEY_Up:
      kind = GRID_CMD_KIND_UP;
      break;
    case GDK_KEY_Down:
      kind = GRID_CMD_KIND_DOWN;
      break;
    case GDK_KEY_Left:
      kind = GRID_CMD_KIND_LEFT;
      break;
    case GDK_KEY_Right:
      kind = GRID_CMD_KIND_RIGHT;
      break;
    case GDK_KEY_Return:
      if (cell->in_multichar_mode)
        {
          play_cell_multichar_commit (cell);
          return TRUE;
        }
      kind = GRID_CMD_KIND_SWITCH;
      break;
    case GDK_KEY_t:
      if (state & GDK_CONTROL_MASK)
        kind = GRID_CMD_KIND_SWITCH;
      break;
    default:
      return FALSE;
    }
  if (kind == GRID_CMD_KIND_NONE)
    return FALSE;

  g_signal_emit (cell, obj_signals[DO_COMMAND], 0, kind);
  return TRUE;
}

/*
 * Label manipulation functions
 */


static void
set_label_font_size (GtkLabel      *label,
                     guint          font_size,
                     PangoAttrList *attrs)
{
  PangoFontDescription *font_desc;
  PangoAttribute *attr;
  gboolean free_attrs = FALSE;

  if (attrs == NULL)
    attrs = gtk_label_get_attributes (label);

  /* I don't know why this is sometimes null */
  if (attrs == NULL)
    {
      attrs = pango_attr_list_new ();
      free_attrs = TRUE;
    }

  font_desc = pango_font_description_new ();
  pango_font_description_set_size (font_desc, font_size * PANGO_SCALE);
  attr = pango_attr_font_desc_new (font_desc);
  pango_font_description_free (font_desc);

  pango_attr_list_change (attrs, attr);
  gtk_label_set_attributes(label, attrs);

  if (free_attrs)
    {
      pango_attr_list_unref (attrs);
    }
}

static void
set_label_color (GtkLabel      *label,
                 GdkRGBA        text_color,
                 PangoAttrList *attrs)
{
  PangoAttribute *attr;

  if (attrs == NULL)
    attrs = pango_attr_list_new();
  else
    pango_attr_list_ref (attrs);

  attr = pango_attr_foreground_new ((gint) (text_color.red*65535), (gint) (text_color.green * 65535), (gint)(text_color.blue *65535));
  pango_attr_list_insert (attrs, attr);
  gtk_label_set_attributes(label, attrs);

  pango_attr_list_unref (attrs);
}

static guint
measure_text_width (PlayCell      *cell,
                    const gchar   *str,
                    PangoAttrList *attrs,
                    gint           size)
{
  PangoLayout *layout;
  PangoAttrList *test_attrs;
  PangoFontDescription *font_desc;
  PangoAttribute *attr;
  PangoRectangle ink_rect;

  g_assert (attrs);

  layout = gtk_widget_create_pango_layout (GTK_WIDGET (cell->main_label), str);
  test_attrs = pango_attr_list_copy (attrs);

  font_desc = pango_font_description_new ();
  pango_font_description_set_size (font_desc, size * PANGO_SCALE);
  attr = pango_attr_font_desc_new (font_desc);
  pango_font_description_free (font_desc);

  pango_attr_list_insert (test_attrs, attr);
  pango_layout_set_attributes (layout, test_attrs);

  pango_layout_get_extents (layout, &ink_rect, NULL);

  pango_attr_list_unref (test_attrs);
  g_object_unref (layout);

  return ink_rect.width / PANGO_SCALE;
}

/* Calculate what size is needed to have text fit within the
 * main_label within a cell. If we can't find a valid size, we return
 * the standard size and set overrun to TRUE */
static guint
calc_main_label_size (PlayCell      *cell,
                      const gchar   *text,
                      PangoAttrList *attrs,
                      gboolean      *overrun)
{
  guint i;
  guint font_size;

  if (overrun)
    *overrun = FALSE;

  if (attrs == NULL)
    attrs = pango_attr_list_new();
  else
    pango_attr_list_ref (attrs);

  for (i = MAX (2*(cell->config.base_size) - 6, 0); i > 0; i--)
    {
      guint width;
      width = measure_text_width (cell, text, attrs, i);
      if (width < 3 * cell->config.base_size - 4)
        break;
    }
  if (i == 0)
    {
      if (overrun)
        *overrun = TRUE;
      font_size = MAX (2*(cell->config.base_size) - 6, 0);
    }
  else
    {
      font_size = i;
    }

  pango_attr_list_unref (attrs);

  return font_size;
}

/* Multichar functions */
static void
play_cell_multichar_append (PlayCell    *self,
                            const gchar *text)
{
  g_assert (PLAY_IS_CELL (self));
  g_return_if_fail (self->in_multichar_mode);

  if (self->multichar_buffer)
    self->multichar_buffer = g_strconcat (self->multichar_buffer, text, NULL);
  else
    self->multichar_buffer = g_strdup (text);
}

static void
play_cell_multichar_backspace (PlayCell *self)
{
  g_assert (PLAY_IS_CELL (self));
  g_return_if_fail (self->in_multichar_mode);

  if (self->multichar_buffer)
    {
      guint font_size;
      gboolean overrun = FALSE;
      gsize len;

      len = g_utf8_strlen (self->multichar_buffer, -1);
      g_utf8_strncpy (self->multichar_buffer, self->multichar_buffer,
                      len > 0 ? len - 1 : 0);
      font_size = calc_main_label_size (self, self->multichar_buffer, NULL, &overrun);
      update_main_label (self, self->multichar_buffer, font_size, self->text_color_set, self->text_color, overrun, NULL);
    }
}

static void
play_cell_multichar_toggle (PlayCell *self)
{
  g_assert (PLAY_IS_CELL (self));

  if (self->in_multichar_mode)
    {
      play_cell_multichar_commit (self);
      play_cell_multichar_cancel (self);
    }
  else
    {
      guint font_size;
      gboolean overrun = FALSE;

      self->in_multichar_mode = TRUE;
      g_clear_pointer (&self->multichar_buffer_save, g_free);
      /* We have to keep an old copy of the text around in case the user cancels things.
       */
      self->multichar_buffer_save = g_strdup (self->current_main_label_text);
      self->multichar_buffer = g_strdup (self->current_main_label_text);

      font_size = calc_main_label_size (self, self->multichar_buffer, NULL, &overrun);
      update_main_label (self, self->multichar_buffer, font_size, self->text_color_set, self->text_color, overrun, NULL);
    }

  gtk_widget_queue_draw (GTK_WIDGET (self));
}

static void
play_cell_multichar_commit (PlayCell *self)
{
  g_assert (PLAY_IS_CELL (self));
  g_return_if_fail (self->in_multichar_mode);

  self->in_multichar_mode = FALSE;
  g_signal_emit (self, obj_signals[GUESS_AT_CELL], 0, self->multichar_buffer, self->row, self->column);
  g_clear_pointer (&self->multichar_buffer, g_free);
  gtk_widget_queue_draw (GTK_WIDGET (self));

}

static void
play_cell_multichar_cancel (PlayCell *self)
{
  g_assert (PLAY_IS_CELL (self));

  if (self->in_multichar_mode)
    {
      guint font_size;
      gboolean overrun = FALSE;

      self->in_multichar_mode = FALSE;
      g_clear_pointer (&self->multichar_buffer, g_free);

      font_size = calc_main_label_size (self, self->multichar_buffer_save, NULL, &overrun);
      update_main_label (self, self->multichar_buffer_save, font_size, self->text_color_set, self->text_color, overrun, NULL);
      gtk_widget_queue_draw (GTK_WIDGET (self));
    }
}

/*
 * Public Methods
 */
GtkWidget *
play_cell_new (IpuzCellCoord coord,
               LayoutConfig  config)
{
  PlayCell *cell;

  cell = PLAY_CELL (g_object_new (PLAY_TYPE_CELL,
                                  "row", coord.row,
                                  "column", coord.column,
                                  NULL));

  cell->config = config;

  return GTK_WIDGET (cell);
}

static const gchar *
css_class_to_string (LayoutItemCellStyle css_class)
{
  switch (css_class)
    {
    case LAYOUT_ITEM_STYLE_UNSET:
      return "unset";
    case LAYOUT_ITEM_STYLE_NULL:
      return "null";
    case LAYOUT_ITEM_STYLE_VISIBLE_NULL:
      return "visible-null";
    case LAYOUT_ITEM_STYLE_NORMAL:
      return "normal";
    case LAYOUT_ITEM_STYLE_FOCUSED:
      return "focused";
    case LAYOUT_ITEM_STYLE_BLOCK:
      return "block";
    case LAYOUT_ITEM_STYLE_FOCUSED_BLOCK:
      return "focused-block";
    case LAYOUT_ITEM_STYLE_ERROR:
      return "error";
    case LAYOUT_ITEM_STYLE_SELECTED:
      return "selected";
    case LAYOUT_ITEM_STYLE_HIGHLIGHTED:
      return "highlighted";
    case LAYOUT_ITEM_STYLE_HIGHLIGHTED_SELECTED:
      return "highlighted-selected";
    case LAYOUT_ITEM_STYLE_INITIAL_VAL:
      return "initial-val";
    case LAYOUT_ITEM_STYLE_CLUE_BLOCK:
      return "initial-val";
    default:
      g_assert_not_reached ();
    }
  return NULL;
}

static const gchar *
foreground_css_class_to_string (LayoutItemCellForegroundStyle css_class)
{
  switch (css_class)
    {
    case LAYOUT_ITEM_FOREGROUND_STYLE_UNSET:
      return "unset";
    case LAYOUT_ITEM_FOREGROUND_STYLE_NORMAL:
      return "normal";
    case LAYOUT_ITEM_FOREGROUND_STYLE_PENCIL:
      return "pencil";
    case LAYOUT_ITEM_FOREGROUND_STYLE_INITIAL_VAL:
      return "initial-val";
    default:
      g_assert_not_reached ();
    }
  return NULL;
}

static void
play_cell_set_css_class (GtkWidget           *widget,
                         LayoutItemCellStyle  old_css_class,
                         LayoutItemCellStyle  new_css_class)
{
  if (old_css_class == new_css_class)
    return;

  /* Set the css classes on the widget */
  gtk_widget_remove_css_class (GTK_WIDGET (widget), css_class_to_string (old_css_class));
  gtk_widget_add_css_class (GTK_WIDGET (widget), css_class_to_string (new_css_class));
}

static void
play_cell_set_foreground_css_class (GtkWidget                     *widget,
                                    LayoutItemCellForegroundStyle  old_css_class,
                                    LayoutItemCellForegroundStyle  new_css_class)
{
  if (old_css_class == new_css_class)
    return;

  /* Set the css classes on the widget */
  gtk_widget_remove_css_class (GTK_WIDGET (widget), foreground_css_class_to_string (old_css_class));
  gtk_widget_add_css_class (GTK_WIDGET (widget), foreground_css_class_to_string (new_css_class));
}

static void
connect_im_signals (PlayCell *cell)
{
  if (cell->commit_cb == 0)
    {
      cell->commit_cb = g_signal_connect (cell->im_context, "commit", G_CALLBACK (play_cell_commit_cb), cell);
      cell->preedit_changed_cb = g_signal_connect (cell->im_context, "preedit-changed", G_CALLBACK (play_cell_preedit_changed_cb), cell);
      cell->preedit_end_cb = g_signal_connect (cell->im_context, "preedit-end", G_CALLBACK (play_cell_preedit_end_cb), cell);
    }
}


static void
disconnect_im_signals (PlayCell *cell)
{
  if (cell->commit_cb != 0)
    {
      g_signal_handler_disconnect (cell->im_context, cell->commit_cb);
      g_signal_handler_disconnect (cell->im_context, cell->preedit_changed_cb);
      g_signal_handler_disconnect (cell->im_context, cell->preedit_end_cb);
      cell->commit_cb = 0;
      cell->preedit_changed_cb = 0;
      cell->preedit_end_cb = 0;
    }
}


void
play_cell_select (PlayCell *cell)
{
  g_return_if_fail (PLAY_IS_CELL (cell));

  gtk_im_context_focus_in (cell->im_context);
  gtk_widget_queue_draw (GTK_WIDGET (cell));

  g_signal_emit (cell, obj_signals[CELL_SELECTED], 0, cell->row, cell->column);
}

static void
update_cluenum_label (PlayCell      *self,
                      const gchar   *text,
                      gboolean       text_color_set,
                      GdkRGBA        text_color,
                      PangoAttrList *extra_attrs)
{
  gboolean update_label = FALSE;
  gfloat xalign = 0.0;
  gfloat yalign = 0.0;

  if (g_strcmp0 (text, self->current_cluenum_label_text))
    {
      g_clear_pointer (&self->current_cluenum_label_text, g_free);
      self->current_cluenum_label_text = g_strdup (text);
      update_label = TRUE;
    }

  if (self->cluenum_location & IPUZ_STYLE_MARK_CENTER_COL)
    xalign = 0.5;
  if (self->cluenum_location & IPUZ_STYLE_MARK_RIGHT)
    xalign = 1.0;

  if (self->cluenum_location & IPUZ_STYLE_MARK_CENTER_ROW)
    yalign = 0.5;
  if (self->cluenum_location & IPUZ_STYLE_MARK_BOTTOM)
    yalign = 1.0;

  if (xalign != self->current_cluenum_xalign ||
      yalign != self->current_cluenum_xalign)
    {
      self->current_cluenum_xalign = xalign;
      self->current_cluenum_xalign = yalign;
      update_label = TRUE;
    }

  if (text_color_set != self->current_cluenum_text_color_set)
    {
      self->current_text_color_set = text_color_set;
      update_label = TRUE;
    }

  if (self->text_color_set &&
      !gdk_rgba_equal (&text_color, &self->current_text_color))
    {
      self->current_cluenum_text_color = text_color;
      update_label = TRUE;
    }

  if (update_label)
    {
      gtk_label_set_xalign (GTK_LABEL (self->cluenum_label), xalign);
      gtk_label_set_yalign (GTK_LABEL (self->cluenum_label), yalign);
      gtk_label_set_text (GTK_LABEL (self->cluenum_label), self->current_cluenum_label_text);

      set_label_font_size (GTK_LABEL (self->cluenum_label),
                           (self->config.base_size - 2)*2/3,
                           extra_attrs);
      if (self->text_color_set)
        set_label_color (GTK_LABEL (self->cluenum_label), self->text_color, extra_attrs);
    }

}

/* Centralize the place where the main label is updated. This will
 * aggressively cache the values and only update it if needed. If
 * extra_attrs is set, then those will be added to the final
 * rendering. extra_attrs isn't cached, so it will disappear next time
 * this function is called.
 */
static void
update_main_label (PlayCell      *self,
                   const gchar   *text,
                   guint          size,
                   gboolean       text_color_set,
                   GdkRGBA        text_color,
                   gboolean       overrun,
                   PangoAttrList *extra_attrs)
{
  gboolean update_label = FALSE;

  if (overrun)
    text = "�";

  if (text_color_set != self->current_text_color_set)
    {
      self->current_text_color_set = text_color_set;
      update_label = TRUE;
    }

  if (self->text_color_set &&
      !gdk_rgba_equal (&text_color, &self->current_text_color))
    {
      self->current_text_color = text_color;
      update_label = TRUE;
    }

  if (g_strcmp0 (text, self->current_main_label_text) && text != self->current_main_label_text)
    {
      g_clear_pointer (&self->current_main_label_text, g_free);
      self->current_main_label_text = g_strdup (text);
      update_label = TRUE;
    }

  if (size != self->current_font_size)
    {
      self->current_font_size = size;
      update_label = TRUE;
    }

  if (update_label)
    {
      g_autoptr (PangoAttrList) attrs = NULL;
      if (extra_attrs)
        attrs = pango_attr_list_ref (extra_attrs);
      else
        attrs = pango_attr_list_new ();

      //      g_print ("updating label %s\n", self->current_main_label_text);
      gtk_label_set_text (GTK_LABEL (self->main_label), self->current_main_label_text);
      set_label_font_size (GTK_LABEL (self->main_label), self->current_font_size, attrs);
      if (self->current_text_color_set)
        set_label_color (GTK_LABEL (self->main_label), self->current_text_color, attrs);
    }
}

/* Super complex function. Handle with care.
 */
void
play_cell_update (PlayCell   *self,
                  LayoutCell *layout)
{
  g_autoptr (PangoAttrList) cluenum_attrs = NULL;
  guint font_size;
  gboolean overrun = FALSE;
  gboolean redraw_needed = FALSE;

  g_return_if_fail (PLAY_IS_CELL (self));

  play_cell_set_css_class (GTK_WIDGET (self),
                           self->css_class, layout->css_class);
  play_cell_set_foreground_css_class (self->main_label,
                                      self->foreground_css_class,
                                      layout->foreground_css_class);
  self->editable = layout->editable;
  if (self->editable)
    connect_im_signals (self);
  else
    disconnect_im_signals (self);

  if (self->has_secondary_text)
    {
      self->has_secondary_text = FALSE;
      redraw_needed = TRUE;
    }

  if (self->split_highlight_state != LAYOUT_SPLIT_HIGHLIGHT_NONE)
    redraw_needed = TRUE;
  self->split_highlight_state = LAYOUT_SPLIT_HIGHLIGHT_NONE;

  if (self->css_class != layout->css_class)
    {
      self->css_class = layout->css_class;
      redraw_needed = TRUE;
    }

  if (self->foreground_css_class != layout->foreground_css_class)
    {
      self->foreground_css_class = layout->foreground_css_class;
      redraw_needed = TRUE;
    }

  if (self->shapebg != layout->shapebg)
    {
      self->shapebg = layout->shapebg;
      redraw_needed = TRUE;
    }

  if (self->cell_type != layout->cell_type)
    {
      self->cell_type = layout->cell_type;
      redraw_needed = TRUE;
    }

  if (self->bg_color_set != layout->bg_color_set)
    {
      self->bg_color_set = layout->bg_color_set;
      redraw_needed = TRUE;
    }

  if (!gdk_rgba_equal (&self->bg_color, &layout->bg_color))
    {
      self->bg_color = layout->bg_color;
      if (self->bg_color_set)
        redraw_needed = TRUE;
    }

  if (self->text_color_set != layout->text_color_set)
    {
      self->text_color_set = layout->text_color_set;
      redraw_needed = TRUE;
    }

  if (!gdk_rgba_equal (&self->text_color, &layout->text_color))
    {
      self->text_color_set = layout->text_color_set;
      if (self->bg_color_set)
        redraw_needed = TRUE;
    }

  if (self->cluenum_location != layout->cluenum_location)
    {
      self->cluenum_location = layout->cluenum_location;
      redraw_needed = TRUE;
    }

  /* Set up the state of the cell */
  gtk_widget_set_can_focus (GTK_WIDGET (self), self->editable);
  gtk_label_set_wrap (GTK_LABEL (self->main_label), FALSE);
  gtk_widget_set_child_visible (self->cluenum_label, TRUE);
  gtk_widget_set_child_visible (self->main_label, TRUE);
  gtk_widget_set_child_visible (self->secondary_label, FALSE);


  /* Setup the labels */
  update_cluenum_label (self, layout->cluenum_text, self->text_color_set, self->text_color, NULL);
  font_size = calc_main_label_size (self, layout->main_text, NULL, &overrun);
  update_main_label (self, layout->main_text, font_size, self->text_color_set, self->text_color, overrun, NULL);

  if (redraw_needed)
    gtk_widget_queue_draw (GTK_WIDGET (self));
}

void
play_cell_load_clue_block_state (PlayCell            *cell,
                                 IpuzCell            *puz_cell,
                                 LayoutClueBlockCell *layout)
{
  g_return_if_fail (PLAY_IS_CELL (cell));
  g_return_if_fail (puz_cell != NULL);

  play_cell_set_css_class (GTK_WIDGET (cell), cell->css_class, layout->css_class);
  cell->editable = FALSE;

  /* Copy over relevant style information for ::snapshot() */
  cell->cell_type = layout->cell_type;
  cell->css_class = layout->css_class;
  cell->bg_color_set = FALSE;
  cell->text_color_set = FALSE;
  cell->split_highlight_state = layout->split_highlight_state;

  /* Set up basic attributes */
  gtk_widget_set_can_focus (GTK_WIDGET (cell), FALSE);
  disconnect_im_signals (cell);


  /* Setup the labels */
  gtk_widget_set_child_visible (cell->cluenum_label, FALSE);
  gtk_widget_set_child_visible (cell->main_label, TRUE);
  gtk_label_set_wrap (GTK_LABEL (cell->main_label), TRUE);


  /*set up the labels */
  gtk_label_set_text (GTK_LABEL (cell->main_label), layout->main_text);
  if (layout->secondary_text != NULL)
    {
      cell->has_secondary_text = TRUE;
      gtk_label_set_text (GTK_LABEL (cell->secondary_label), layout->secondary_text);
    }
  else
    cell->has_secondary_text = FALSE;
}

void
play_cell_update_config (PlayCell     *cell,
                         LayoutConfig  config)
{
  g_return_if_fail (PLAY_IS_CELL (cell));

  if (! layout_config_equal (&config, &cell->config))
    {
      guint font_size;
      gboolean overrun = FALSE;

      cell->config = config;

      set_label_font_size (GTK_LABEL (cell->cluenum_label),
                           (cell->config.base_size - 2)*2/3,
                           NULL);/* FIXME: centralize this */
      font_size = calc_main_label_size (cell, cell->current_main_label_text, NULL, &overrun);
      update_main_label (cell, cell->current_main_label_text, font_size, cell->text_color_set, cell->text_color, overrun, NULL);

      gtk_widget_queue_resize (GTK_WIDGET (cell));
    }
}

/* Helper functions */

guint
get_play_cell_size (guint base_size)
{
  return base_size * 3;
}
