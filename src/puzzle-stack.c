/* puzzle-stack.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include "puzzle-stack.h"
#include "crosswords-enums.h"


/* Set this up to 3 to enable more debugging output */
#define PUZZLE_STACK_DEBUG 0

#ifdef DEVELOPMENT_BUILD
#define VALIDATE_PUZZLE_STACK(puzzle_stack) puzzle_stack_validate(__FUNCTION__,puzzle_stack)
#else
#define VALIDATE_PUZZLE_STACK(puzzle_stack)
#endif


enum {
  CHANGED,
  N_SIGNALS
};

enum {
  PROP_0,
  PROP_TYPE,
  N_PROPS
};

static guint obj_signals[N_SIGNALS] = { 0 };
static GParamSpec *obj_props[N_PROPS] = {NULL, };


typedef struct _PuzzleStep
{
  IpuzPuzzle *puzzle;
  PuzzleStackChangeType change_type;
  GObject *data_obj;
} PuzzleStep;

#define PUZZLE_STEP(step) ((PuzzleStep *) step)
static PuzzleStep *puzzle_step_new  (void);
static void        puzzle_step_free (PuzzleStep *step);


struct _PuzzleStack
{
  GObject parent_object;

  PuzzleStackType type;
  GList *stack;
  GList *current;
  IpuzPuzzle *copy;
  GList *saved;
  gint64 saved_time;
};

static void puzzle_stack_init         (PuzzleStack      *self);
static void puzzle_stack_class_init   (PuzzleStackClass *klass);
static void puzzle_stack_set_property (GObject          *object,
                                       guint             prop_id,
                                       const GValue     *value,
                                       GParamSpec       *pspec);
static void puzzle_stack_get_property (GObject          *object,
                                       guint             prop_id,
                                       GValue           *value,
                                       GParamSpec       *pspec);
static void puzzle_stack_finalize     (GObject          *object);


G_DEFINE_TYPE (PuzzleStack, puzzle_stack, G_TYPE_OBJECT);


static PuzzleStep *
puzzle_step_new (void)
{
  PuzzleStep *step;

  step = g_new0 (PuzzleStep, 1);
  /* Quick hack to store data. See puzzle-stack.md for more information */
  step->data_obj = g_object_new (G_TYPE_OBJECT, NULL);

  return step;
}

static void
puzzle_step_free (PuzzleStep *step)
{
  if (step == NULL)
    return;

  if (step->puzzle)
    g_object_unref (step->puzzle);
  g_object_unref (step->data_obj);
  g_free (step);
}

static void
puzzle_stack_init (PuzzleStack *self)
{
  PuzzleStep *step;

  step = puzzle_step_new ();
  step->change_type = STACK_CHANGE_PUZZLE;
  self->stack = g_list_prepend (self->stack, step);
  self->current = self->stack;
  self->saved = NULL;
  self->saved_time = 0;
}

static void
puzzle_stack_class_init (PuzzleStackClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->set_property = puzzle_stack_set_property;
  object_class->get_property = puzzle_stack_get_property;
  object_class->finalize = puzzle_stack_finalize;

  obj_signals [CHANGED] =
    g_signal_new ("changed",
                  PUZZLE_TYPE_STACK,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 2,
                  PUZZLE_TYPE_STACK_OPERATION,
                  PUZZLE_TYPE_STACK_CHANGE_TYPE);

  obj_props[PROP_TYPE] =
    g_param_spec_enum ("type",
                       "Type",
                       "Puzzle Stack Type",
                       PUZZLE_TYPE_STACK_TYPE,
                       PUZZLE_STACK_PLAY,
                       G_PARAM_READWRITE);

  g_object_class_install_properties (object_class, N_PROPS, obj_props);
}

static void
puzzle_stack_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  PuzzleStack *self = (PuzzleStack *) object;

  switch (prop_id)
    {
    case PROP_TYPE:
      self->type = g_value_get_enum (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
puzzle_stack_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  PuzzleStack *self = (PuzzleStack *) object;

  switch (prop_id)
    {
    case PROP_TYPE:
      g_value_set_enum (value, self->type);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
puzzle_stack_clear (PuzzleStack *self)
{
  g_list_free_full (self->stack, (GDestroyNotify) puzzle_step_free);
  g_clear_object (&self->copy);
  self->stack = NULL;
  self->current = NULL;
  self->saved = NULL;
  self->saved_time = 0;
}

static void
puzzle_stack_finalize (GObject *object)
{
  puzzle_stack_clear (PUZZLE_STACK (object));

  G_OBJECT_CLASS (puzzle_stack_parent_class)->finalize (object);
}

/* Public methods */



static void
puzzle_stack_set_new_puzzle (PuzzleStack *puzzle_stack,
                             IpuzPuzzle  *puzzle)
{
  PuzzleStep *step;

  puzzle_stack_clear (puzzle_stack);
  /* Is this kosher? I've never seen it before, but it's exactly what
   * I want. I need to reset this back to a base state. */
  puzzle_stack_init (puzzle_stack);

  if (puzzle)
    {
      step = (PuzzleStep *) puzzle_stack->current->data;
      step->puzzle = g_object_ref (puzzle);
      puzzle_stack->copy = ipuz_puzzle_deep_copy (puzzle);
      /* We never store the guesses when we're in EDIT.
       * Clear it out if it happens to exist */
      if (puzzle_stack->type == PUZZLE_STACK_EDIT)
        ipuz_crossword_set_guesses (IPUZ_CROSSWORD (puzzle_stack->copy), NULL);

      puzzle_stack_set_saved (puzzle_stack);

      g_signal_emit (puzzle_stack, obj_signals [CHANGED], 0, PUZZLE_STACK_OPERATION_NEW_FRAME, STACK_CHANGE_PUZZLE);
    }
}


PuzzleStack *
puzzle_stack_new (PuzzleStackType  type,
                  IpuzPuzzle      *initial_puzzle)
{
  PuzzleStack *puzzle_stack;

  puzzle_stack = (PuzzleStack *) g_object_new (PUZZLE_TYPE_STACK,
                                               "type", type,
                                               NULL);

  puzzle_stack_set_new_puzzle (puzzle_stack, initial_puzzle);

  return puzzle_stack;
}

void
puzzle_stack_push_change (PuzzleStack           *puzzle_stack,
                          PuzzleStackChangeType  change_type,
                          IpuzPuzzle            *puzzle)
{
  PuzzleStep *new_step;
  PuzzleStep *current_step;
  IpuzPuzzle *copy = NULL;

  g_return_if_fail (PUZZLE_IS_STACK (puzzle_stack));
  VALIDATE_PUZZLE_STACK (puzzle_stack);
  if (puzzle)
    g_return_if_fail (IPUZ_IS_CROSSWORD (puzzle));


  /* if we are setting a new puzzle, we take a different path. */
  if (change_type == STACK_CHANGE_PUZZLE)
    {
      puzzle_stack_set_new_puzzle (puzzle_stack, puzzle);
      return;
    }


  new_step = puzzle_step_new ();
  current_step = (PuzzleStep *) puzzle_stack->current->data;

  /* Get rid of the redo stack ahead of us, if it exists */
  if (puzzle_stack_can_redo (puzzle_stack))
    {
      /* Remove the chain in front */
      puzzle_stack->current->prev->next = NULL;
      puzzle_stack->current->prev = NULL;

      /* delete it */
      g_list_free_full (puzzle_stack->stack, (GDestroyNotify) puzzle_step_free);
      puzzle_stack->stack = NULL;
    }

  if (puzzle)
    {
      g_object_ref (puzzle);
      copy = ipuz_puzzle_deep_copy (puzzle);
      if (puzzle_stack->type == PUZZLE_STACK_EDIT)
        ipuz_crossword_set_guesses (IPUZ_CROSSWORD (copy), NULL);
    }

  /* Swap all the values. This is pretty complex, so see
   * puzzle-stack.md for more information on what's happening here */
  new_step->puzzle = puzzle;
  new_step->change_type = change_type;

  if (current_step->puzzle)
    g_object_unref (current_step->puzzle);
  current_step->puzzle = puzzle_stack->copy;

  puzzle_stack->stack = g_list_prepend (puzzle_stack->current, new_step);
  puzzle_stack->current = puzzle_stack->stack;
  puzzle_stack->copy = copy;

  g_signal_emit (puzzle_stack, obj_signals [CHANGED], 0, PUZZLE_STACK_OPERATION_NEW_FRAME, change_type);

  VALIDATE_PUZZLE_STACK (puzzle_stack);
}

void
puzzle_stack_set_data (PuzzleStack    *puzzle_stack,
                       const gchar    *data_hint,
                       gpointer        data,
                       GDestroyNotify  clear_func)
{
  PuzzleStep *step;

  g_return_if_fail (PUZZLE_IS_STACK (puzzle_stack));
  VALIDATE_PUZZLE_STACK (puzzle_stack);

  step = PUZZLE_STEP (puzzle_stack->current->data);

  return g_object_set_data_full (step->data_obj,
                                 data_hint,
                                 data,
                                 clear_func);
}

void
puzzle_stack_set_saved (PuzzleStack *puzzle_stack)
{
  g_return_if_fail (PUZZLE_IS_STACK (puzzle_stack));
  VALIDATE_PUZZLE_STACK (puzzle_stack);

  puzzle_stack->saved = puzzle_stack->current;
  puzzle_stack->saved_time = g_get_monotonic_time ();
}

gpointer
puzzle_stack_get_data (PuzzleStack *puzzle_stack,
                       const gchar *data_hint)
{
  PuzzleStep *step;

  g_return_val_if_fail (PUZZLE_IS_STACK (puzzle_stack), NULL);
  VALIDATE_PUZZLE_STACK (puzzle_stack);

  step = PUZZLE_STEP (puzzle_stack->current->data);

  return g_object_get_data (step->data_obj, data_hint);
}

gpointer
puzzle_stack_peek_next_data (PuzzleStack *puzzle_stack,
                             const gchar *data_hint)
{
  PuzzleStep *step;

  g_return_val_if_fail (PUZZLE_IS_STACK (puzzle_stack), NULL);
  VALIDATE_PUZZLE_STACK (puzzle_stack);

  if (puzzle_stack->current->prev == NULL)
    return NULL;

  step = PUZZLE_STEP (puzzle_stack->current->prev->data);

  return g_object_get_data (step->data_obj, data_hint);
}

IpuzPuzzle *
puzzle_stack_get_puzzle (PuzzleStack *puzzle_stack)
{
  PuzzleStep *step;

  g_return_val_if_fail (PUZZLE_IS_STACK (puzzle_stack), NULL);
  VALIDATE_PUZZLE_STACK (puzzle_stack);

  step = PUZZLE_STEP (puzzle_stack->current->data);

  return step->puzzle;
}

/* Convenience to get the current guesses */
IpuzGuesses *
puzzle_stack_get_guesses (PuzzleStack *puzzle_stack)
{
  PuzzleStep *step;

  g_return_val_if_fail (PUZZLE_IS_STACK (puzzle_stack), NULL);
  VALIDATE_PUZZLE_STACK (puzzle_stack);

  step = PUZZLE_STEP (puzzle_stack->current->data);

  if (step->puzzle)
    return ipuz_crossword_get_guesses (IPUZ_CROSSWORD (step->puzzle));

  return NULL;
}

PuzzleStackChangeType
puzzle_stack_get_change_type (PuzzleStack *puzzle_stack)
{
  PuzzleStep *step;

  g_return_val_if_fail (PUZZLE_IS_STACK (puzzle_stack), STACK_CHANGE_PUZZLE);
  VALIDATE_PUZZLE_STACK (puzzle_stack);

  step = PUZZLE_STEP (puzzle_stack->current->data);

  return step->change_type;
}


gboolean
puzzle_stack_can_undo (PuzzleStack *puzzle_stack)
{
  g_return_val_if_fail (PUZZLE_IS_STACK (puzzle_stack), FALSE);
  VALIDATE_PUZZLE_STACK (puzzle_stack);

  if (puzzle_stack->current->next == NULL)
    return FALSE;

  return TRUE;
}

gboolean
puzzle_stack_can_redo (PuzzleStack *puzzle_stack)
{
  g_return_val_if_fail (PUZZLE_IS_STACK (puzzle_stack), FALSE);
  VALIDATE_PUZZLE_STACK (puzzle_stack);

  if (puzzle_stack->current->prev == NULL)
    return FALSE;

  return TRUE;
}

gboolean
puzzle_stack_get_unsaved_changes (PuzzleStack *puzzle_stack)
{
  g_return_val_if_fail (PUZZLE_IS_STACK (puzzle_stack), FALSE);
  VALIDATE_PUZZLE_STACK (puzzle_stack);

  if (puzzle_stack_get_puzzle (puzzle_stack) == NULL)
    return FALSE;

  return puzzle_stack->current != puzzle_stack->saved;
}

gint64
puzzle_stack_get_last_save_time (PuzzleStack *puzzle_stack)
{
  g_return_val_if_fail (PUZZLE_IS_STACK (puzzle_stack), FALSE);
  VALIDATE_PUZZLE_STACK (puzzle_stack);

  return puzzle_stack->saved_time;
}


void
puzzle_stack_undo (PuzzleStack *puzzle_stack)
{
  PuzzleStackChangeType change_type;
  PuzzleStep *step;

  g_return_if_fail (PUZZLE_IS_STACK (puzzle_stack));
  VALIDATE_PUZZLE_STACK (puzzle_stack);

  if (! puzzle_stack_can_undo (puzzle_stack))
    return;

  /* When we undo, the type change the one we left, not
   * the one we are moving to */
  change_type = puzzle_stack_get_change_type (puzzle_stack);
  puzzle_stack->current = puzzle_stack->current->next;


  step = (PuzzleStep *) puzzle_stack->current->data;
  g_object_unref (puzzle_stack->copy);
  puzzle_stack->copy = ipuz_puzzle_deep_copy (step->puzzle);

  g_signal_emit (puzzle_stack, obj_signals [CHANGED], 0, PUZZLE_STACK_OPERATION_UNDO, change_type);

  VALIDATE_PUZZLE_STACK (puzzle_stack);
}

void
puzzle_stack_redo (PuzzleStack *puzzle_stack)
{
  PuzzleStep *step;

  g_return_if_fail (PUZZLE_IS_STACK (puzzle_stack));
  VALIDATE_PUZZLE_STACK (puzzle_stack);

  if (! puzzle_stack_can_redo (puzzle_stack))
    return;

  puzzle_stack->current = puzzle_stack->current->prev;
  step = (PuzzleStep *) puzzle_stack->current->data;
  g_object_unref (puzzle_stack->copy);
  puzzle_stack->copy = ipuz_puzzle_deep_copy (step->puzzle);

  g_signal_emit (puzzle_stack, obj_signals [CHANGED], 0, PUZZLE_STACK_OPERATION_REDO, step->change_type);

  VALIDATE_PUZZLE_STACK (puzzle_stack);
}

void
puzzle_stack_dump (PuzzleStack *puzzle_stack)
{
  GList *l;

  g_print ("dumping PuzzleStack (%p)\n", puzzle_stack);
  for (l = puzzle_stack->stack; l; l = l->next)
    {
      PuzzleStep *step;

      step = l->data;
      g_assert (l != NULL);
      if (l == puzzle_stack->current)
        g_print ("\t** Node (%p)\n", l);
      else
        g_print ("\tNode (%p)\n", l);

      if (step->puzzle)
        {
          IpuzGuesses *guesses = NULL;

          guesses = ipuz_crossword_get_guesses (IPUZ_CROSSWORD (step->puzzle));
          g_print ("\tCrossword (%p) – Ref count (%d)\n",
                   step->puzzle,
                   G_OBJECT (step->puzzle)->ref_count);
          if (guesses)
            g_print ("\t\tGuesses (%p)\n", guesses);
          else
            g_print ("\t\tGuesses - NULL\n");
        }
      else
        {
          g_print ("\tCrossword – NULL\n");
        }

      switch (step->change_type)
        {
        case STACK_CHANGE_PUZZLE:
          g_print ("\tChange Type: PUZZLE\n");
          break;
        case STACK_CHANGE_GRID:
          g_print ("\tChange Type: GRID\n");
          break;
        case STACK_CHANGE_STATE:
          g_print ("\tChange Type: STATE\n");
          break;
        case STACK_CHANGE_METADATA:
          g_print ("\tChange Type: METADATA\n");
          break;
        case STACK_CHANGE_GUESS:
          g_print ("\tChange Type: GUESS\n");
          break;
        default:
          g_assert_not_reached ();
        }

#if PUZZLE_STACK_DEBUG > 1
      if (l == puzzle_stack->current && step->puzzle != NULL)
        {
          IpuzGuesses *guesses = ipuz_crossword_get_guesses (IPUZ_CROSSWORD (step->puzzle));
          if (guesses)
            ipuz_guesses_print (guesses);
        }
#elif PUZZLE_STACK_DEBUG > 2
      IpuzGuesses *guesses = ipuz_crossword_get_guesses (IPUZ_CROSSWORD (step->puzzle));
      if (guesses)
        ipuz_guesses_print (guesses);
#endif
      g_print ("\n\n");
    }

  g_print ("\n\n\n");
}


void
puzzle_stack_validate (const char  *curframe,
                       PuzzleStack *puzzle_stack)
{
  GList *l;
  gboolean current_in_stack = FALSE;
  gboolean saved_in_stack = FALSE;

  g_assert (PUZZLE_IS_STACK (puzzle_stack));
  g_assert (puzzle_stack->stack != NULL);
  g_assert (puzzle_stack->current != NULL);

  for (l = puzzle_stack->stack; l; l = l->next)
    {
      if (l->next != NULL)
        g_assert (l->next->prev == l);
      if (l->prev != NULL)
        g_assert (l->prev->next == l);
      if (l == puzzle_stack->current)
        current_in_stack = TRUE;
      if (l == puzzle_stack->saved)
        saved_in_stack = TRUE;
    }

  g_assert (current_in_stack);
  if (puzzle_stack->saved)
    g_assert (saved_in_stack);

#if PUZZLE_STACK_DEBUG > 0
  g_print ("FRAME: \e[1m%s\e[m\t", curframe);
  puzzle_stack_dump (puzzle_stack);
#endif
}
