/* edit-save-changes-dialog.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS


GList    *edit_save_info_list_add      (GList        *save_info_list,
                                        GtkWidget    *edit_window,
                                        const gchar  *title,
                                        const gchar  *path);
void      edit_save_changes_dialog_run (GtkWindow    *parent,
                                        GList        *save_info_list,
                                        GApplication *app);


G_END_DECLS
