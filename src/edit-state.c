/* edit-state.c
 *
 * Copyright 2023 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include "edit-state.h"
#include "edit-window.h"


#define EDIT_STATE_STAGE_PRE_HINT        "edit-state-stage-pre-hint"
#define EDIT_STATE_FOCUS_PRE_HINT        "edit-state-focus-pre-hint"
#define EDIT_STATE_GRID_PRE_STATE_HINT   "edit-state-grid-pre-state-hint"
#define EDIT_STATE_CLUES_PRE_STATE_HINT  "edit-state-clues-pre-state-hint"
#define EDIT_STATE_STYLE_PRE_STATE_HINT  "edit-state-style-pre-state-hint"

#define EDIT_STATE_STAGE_POST_HINT       "edit-state-stage-post-hint"
#define EDIT_STATE_FOCUS_POST_HINT       "edit-state-focus-post-hint"
#define EDIT_STATE_GRID_POST_STATE_HINT  "edit-state-grid-post-state-hint"
#define EDIT_STATE_CLUES_POST_STATE_HINT "edit-state-clues-post-state-hint"
#define EDIT_STATE_STYLE_POST_STATE_HINT "edit-state-style-post-state-hint"

#define DEFAULT_SIDEBAR_BASE_SIZE   20


EditState *
edit_state_new (IpuzPuzzle *puzzle)
{
  IpuzSymmetry symmetry = IPUZ_SYMMETRY_ROTATIONAL_QUARTER;
  EditState *edit_state;

  if (puzzle)
    {
      g_assert (IPUZ_IS_CROSSWORD (puzzle));
      symmetry = ipuz_crossword_get_symmetry (IPUZ_CROSSWORD (puzzle));
    }

  edit_state = g_new0 (EditState, 1);
  edit_state->stage = EDIT_STAGE_GRID;
  edit_state->solver = word_solver_new ();
  edit_state->quirks = crosswords_quirks_new (puzzle);
  crosswords_quirks_set_symmetry (edit_state->quirks, symmetry);
  edit_state->sidebar_logo_config =
    layout_config_at_base_size (DEFAULT_SIDEBAR_BASE_SIZE);

  if (puzzle)
    {
      edit_state->puzzle_kind = ipuz_puzzle_get_puzzle_kind (puzzle);
      edit_state->grid_state =
        grid_state_new (IPUZ_CROSSWORD (puzzle),
                        edit_state->quirks,
                        GRID_STATE_EDIT);
      edit_state->clues_state =
        grid_state_new (IPUZ_CROSSWORD (puzzle),
                        edit_state->quirks,
                        GRID_STATE_EDIT_BROWSE);
      edit_state->style_state =
        grid_state_new (IPUZ_CROSSWORD (puzzle),
                        edit_state->quirks,
                        GRID_STATE_EDIT);
      edit_state->preview_state =
        grid_state_new (IPUZ_CROSSWORD (puzzle),
                        edit_state->quirks,
                        GRID_STATE_VIEW);
      if (IPUZ_IS_BARRED (puzzle))
        edit_state->sidebar_logo_config.border_size *= 2;
    }

  return edit_state;
}

void
edit_state_validate (const char *curframe,
                     EditState  *edit_state)
{
  g_assert (edit_state != NULL);
  g_assert (edit_state->solver != NULL);
  g_assert (edit_state->grid_state != NULL);
  g_assert (edit_state->clues_state != NULL);
  g_assert (edit_state->style_state != NULL);
  g_assert (edit_state->preview_state != NULL);
  g_assert (edit_state->quirks == edit_state->grid_state->quirks);
  g_assert (edit_state->quirks == edit_state->clues_state->quirks);
  g_assert (edit_state->quirks == edit_state->style_state->quirks);
}

void
edit_state_free (EditState *edit_state)
{
  g_clear_object (&edit_state->solver);
  g_clear_object (&edit_state->quirks);
  g_clear_object (&edit_state->info);
  g_clear_object (&edit_state->preview_window);
  g_clear_pointer (&edit_state->grid_state, grid_state_free);
  g_clear_pointer (&edit_state->clues_state, grid_state_free);
  g_clear_pointer (&edit_state->style_state, grid_state_free);
  g_clear_pointer (&edit_state->preview_state, grid_state_free);
  g_free (edit_state->clue_selection_text);

  g_free (edit_state);
}

void
edit_state_save_to_stack (GtkWidget   *edit_window,
                          EditState   *edit_state,
                          PuzzleStack *puzzle_stack,
                          gboolean     pre_state)
{
  GtkWidget *focused_widget;
  GDestroyNotify clear_func = NULL;
  GridState *dehydrated_state;

  g_assert (EDIT_IS_WINDOW (edit_window));
  focused_widget = gtk_root_get_focus (GTK_ROOT (edit_window));

  if (focused_widget)
    {
      g_object_ref (focused_widget);
      clear_func = g_object_unref;
    }
  puzzle_stack_set_data (puzzle_stack,
                         pre_state?EDIT_STATE_FOCUS_PRE_HINT:EDIT_STATE_FOCUS_POST_HINT,
                         focused_widget,
                         clear_func);

  puzzle_stack_set_data (puzzle_stack,
                         pre_state?EDIT_STATE_STAGE_PRE_HINT:EDIT_STATE_STAGE_POST_HINT,
                         GINT_TO_POINTER (edit_state->stage),
                         NULL);

  dehydrated_state = grid_state_dehydrate (edit_state->grid_state);
  puzzle_stack_set_data (puzzle_stack,
                         pre_state?EDIT_STATE_GRID_PRE_STATE_HINT:EDIT_STATE_GRID_POST_STATE_HINT,
                         dehydrated_state,
                         (GDestroyNotify) grid_state_free);

  dehydrated_state = grid_state_dehydrate (edit_state->clues_state);
  puzzle_stack_set_data (puzzle_stack,
                         pre_state?EDIT_STATE_CLUES_PRE_STATE_HINT:EDIT_STATE_CLUES_POST_STATE_HINT,
                         dehydrated_state,
                         (GDestroyNotify) grid_state_free);

  dehydrated_state = grid_state_dehydrate (edit_state->style_state);
  puzzle_stack_set_data (puzzle_stack,
                         pre_state?EDIT_STATE_STYLE_PRE_STATE_HINT:EDIT_STATE_STYLE_POST_STATE_HINT,
                         dehydrated_state,
                         (GDestroyNotify) grid_state_free);
}

/* Undoes setting the state */
void
edit_state_clear_current_stack (GtkWidget   *edit_window,
                                PuzzleStack *puzzle_stack,
                                gboolean     pre_state)
{
  g_assert (EDIT_IS_WINDOW (edit_window));

  puzzle_stack_set_data (puzzle_stack,
                         pre_state?EDIT_STATE_FOCUS_PRE_HINT:EDIT_STATE_FOCUS_POST_HINT,
                         NULL, NULL);

  puzzle_stack_set_data (puzzle_stack,
                         pre_state?EDIT_STATE_STAGE_PRE_HINT:EDIT_STATE_STAGE_POST_HINT,
                         NULL, NULL);

  puzzle_stack_set_data (puzzle_stack,
                         pre_state?EDIT_STATE_GRID_PRE_STATE_HINT:EDIT_STATE_GRID_POST_STATE_HINT,
                         NULL, NULL);

  puzzle_stack_set_data (puzzle_stack,
                         pre_state?EDIT_STATE_CLUES_PRE_STATE_HINT:EDIT_STATE_CLUES_POST_STATE_HINT,
                         NULL, NULL);

  puzzle_stack_set_data (puzzle_stack,
                         pre_state?EDIT_STATE_STYLE_PRE_STATE_HINT:EDIT_STATE_STYLE_POST_STATE_HINT,
                         NULL, NULL);
}

void
edit_state_restore_from_stack (EditState   *edit_state,
                               PuzzleStack *puzzle_stack,
                               gboolean     pre_state)
{
  IpuzPuzzle *puzzle;
  GridState *dehydrated_state;
  GtkWidget *focused_widget;

  puzzle = puzzle_stack_get_puzzle (puzzle_stack);

  focused_widget =
    puzzle_stack_get_data (puzzle_stack,
                           pre_state?EDIT_STATE_FOCUS_PRE_HINT:EDIT_STATE_FOCUS_POST_HINT);
  if (focused_widget && gtk_widget_get_realized (focused_widget))
    gtk_widget_grab_focus (focused_widget);

  edit_state->stage =
    GPOINTER_TO_INT (puzzle_stack_get_data (puzzle_stack,
                                            pre_state?EDIT_STATE_STAGE_PRE_HINT:EDIT_STATE_STAGE_POST_HINT));

  dehydrated_state =
    puzzle_stack_get_data (puzzle_stack,
                           pre_state?EDIT_STATE_GRID_PRE_STATE_HINT:EDIT_STATE_GRID_POST_STATE_HINT);
  g_assert (dehydrated_state);
  edit_state->grid_state =
    grid_state_replace (edit_state->grid_state,
                        grid_state_hydrate (dehydrated_state,
                                            IPUZ_CROSSWORD (puzzle),
                                            edit_state->quirks));

  dehydrated_state =
    puzzle_stack_get_data (puzzle_stack,
                           pre_state?EDIT_STATE_CLUES_PRE_STATE_HINT:EDIT_STATE_CLUES_POST_STATE_HINT);
  g_assert (dehydrated_state);
  edit_state->clues_state =
    grid_state_replace (edit_state->clues_state,
                        grid_state_hydrate (dehydrated_state,
                                            IPUZ_CROSSWORD (puzzle),
                                            edit_state->quirks));

  dehydrated_state =
    puzzle_stack_get_data (puzzle_stack,
                           pre_state?EDIT_STATE_STYLE_PRE_STATE_HINT:EDIT_STATE_STYLE_POST_STATE_HINT);
  g_assert (dehydrated_state);
  edit_state->style_state =
    grid_state_replace (edit_state->style_state,
                        grid_state_hydrate (dehydrated_state,
                                            IPUZ_CROSSWORD (puzzle),
                                            edit_state->quirks));
}

struct stage_name_table {
  const char *stage_name;
  EditGridStage stage;
};

static const struct stage_name_table stage_names[] = {
  { "cell",      EDIT_GRID_STAGE_CELL },
  { "word-list", EDIT_GRID_STAGE_WORD_LIST },
  { "autofill",  EDIT_GRID_STAGE_AUTOFILL },
  { "acrostic",  EDIT_GRID_STAGE_ACROSTIC },
};

const char *
edit_grid_stage_to_name (EditGridStage stage)
{
  for (guint i = 0; i < G_N_ELEMENTS (stage_names); i++)
    {
      if (stage == stage_names[i].stage)
        return stage_names[i].stage_name;
    }

  g_assert_not_reached ();
}

EditGridStage
name_to_edit_grid_stage (const gchar *name)
{
  for (guint i = 0; i < G_N_ELEMENTS (stage_names); i++)
    {
      if (! g_strcmp0 (name, stage_names[i].stage_name))
        return stage_names[i].stage;
    }

  g_assert_not_reached ();
}
