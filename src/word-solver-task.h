/* word-solver.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <libipuz/libipuz.h>
#include "cell-array.h"
#include "word-list-misc.h"
#include "word-solver.h"

G_BEGIN_DECLS


#define WORD_TYPE_SOLVER_TASK (word_solver_task_get_type())
G_DECLARE_FINAL_TYPE (WordSolverTask, word_solver_task, WORD, SOLVER_TASK, GObject);


WordSolverTask *word_solver_task_new (gint           *count,
                                      GMutex         *solutions_mutex,
                                      GArray         *solutions,
                                      IpuzCrossword  *xword,
                                      CellArray      *selected_cells,
                                      GHashTable     *initial_word_table,
                                      WordArray      *skip_list,
                                      gint            max_solutions,
                                      gint            min_priority);
void            word_solver_task_run (GTask          *task,
                                      WordSolver     *solver,
                                      WordSolverTask *self,
                                      GCancellable   *cancellable);


G_END_DECLS
