/* edit-color-row.c
 *
 * Copyright 2024 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/* This is a cut-and-paste of edit-shapebg-row.c
 * If we use this pattern another time, we should make a base class
 * for it.
 */


#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include "edit-color-row.h"
#include "edit-color-swatch.h"
#include "play-style.h"


enum
{
  PROP_0,
  PROP_CURRENT_COLOR,
  N_PROPS
};

enum
{
  CHANGED,
  N_SIGNALS
};

static GParamSpec *obj_props[N_PROPS] = {NULL, };
static guint obj_signals[N_SIGNALS] = { 0, };


struct _EditColorRow
{
  AdwActionRow parent_instance;

  GtkWidget *color_popover;
  GtkWidget *popover_grid;
  GtkWidget *clear_button;
  GArray *color_buttons;

  gboolean color_set;
  GdkRGBA current_color;
};


G_DEFINE_FINAL_TYPE (EditColorRow, edit_color_row, ADW_TYPE_ACTION_ROW);


static void edit_color_row_init         (EditColorRow      *self);
static void edit_color_row_class_init   (EditColorRowClass *klass);
static void edit_color_row_set_property (GObject           *object,
                                         guint              prop_id,
                                         const GValue      *value,
                                         GParamSpec        *psec);
static void edit_color_row_get_property (GObject           *object,
                                         guint              prop_id,
                                         GValue            *value,
                                         GParamSpec        *psec);
static void edit_color_row_activate     (AdwActionRow      *action_row);
static void notify_popover_visible_cb   (EditColorRow      *self);
static void update_buttons              (EditColorRow      *self);

static void
color_button_clicked (GtkWidget    *button,
                        EditColorRow *self)
{
  GdkRGBA *color;

  color = g_object_get_data (G_OBJECT (button), "color");
  gtk_popover_popdown (GTK_POPOVER (self->color_popover));

  g_signal_emit (self, obj_signals[CHANGED], 0, color);
}

static GtkToggleButton *
set_up_color_button (EditColorRow    *self,
                     GtkToggleButton *group,
                     PlayColor        color)
{
  GtkWidget *button;
  GtkWidget *swatch;
  const GdkRGBA *rgba;

  button = gtk_toggle_button_new ();
  rgba = play_style_get_color (play_style_get (), color);

  gtk_toggle_button_set_group (GTK_TOGGLE_BUTTON (button), group);
  g_object_set_data (G_OBJECT (button), "color", (gpointer) rgba);

  swatch = g_object_new (EDIT_TYPE_COLOR_SWATCH,
                         "color", rgba,
                         NULL);
  gtk_widget_set_size_request (swatch, 24, 24);
  gtk_button_set_child (GTK_BUTTON (button), swatch);
  gtk_grid_attach (GTK_GRID (self->popover_grid),
                   button,
                   (guint) color % 4, (guint) color / 4,
                   1, 1);
  g_signal_connect (button, "clicked", G_CALLBACK (color_button_clicked), self);

  return GTK_TOGGLE_BUTTON (button);
}

static void
edit_color_row_init (EditColorRow *self)
{
  GtkToggleButton *group = NULL;
  gtk_widget_init_template (GTK_WIDGET (self));

  for (guint i = 0; i < PLAY_COLOR_PALLETTE_SIZE ; i ++)
    group = set_up_color_button (self, group, i);

  self->color_set = FALSE;
}

static void
edit_color_row_class_init (EditColorRowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  AdwActionRowClass *action_row_class = ADW_ACTION_ROW_CLASS (klass);

  object_class->set_property = edit_color_row_set_property;
  object_class->get_property = edit_color_row_get_property;

  action_row_class->activate = edit_color_row_activate;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-color-row.ui");

  gtk_widget_class_bind_template_child (widget_class, EditColorRow, color_popover);
  gtk_widget_class_bind_template_child (widget_class, EditColorRow, popover_grid);
  gtk_widget_class_bind_template_child (widget_class, EditColorRow, clear_button);
  gtk_widget_class_bind_template_callback (widget_class, notify_popover_visible_cb);

  obj_signals[CHANGED] =
    g_signal_new ("changed",
                  EDIT_TYPE_COLOR_ROW,
                  G_SIGNAL_RUN_FIRST,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  GDK_TYPE_RGBA);
  gtk_widget_class_set_css_name (widget_class, "row");

  obj_props [PROP_CURRENT_COLOR] = g_param_spec_boxed ("current-color",
                                                       NULL, NULL,
                                                       GDK_TYPE_RGBA,
                                                       G_PARAM_READWRITE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);
}

static void
edit_color_row_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *psec)
{
  EditColorRow *self;
  
  g_return_if_fail (EDIT_IS_COLOR_ROW (object));

  self = EDIT_COLOR_ROW (object);

  switch (prop_id)
    {
      case PROP_CURRENT_COLOR:
        if (g_value_get_boxed (value))
          {
            self->current_color = *(GdkRGBA *) g_value_get_boxed (value);
            self->color_set = TRUE;
          }
        else
          {
            self->color_set = FALSE;
          }
        update_buttons (self);
	break;
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, psec);
	break;
    }
}

static void
edit_color_row_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *psec)
{
  EditColorRow *self;
  
  g_return_if_fail (EDIT_IS_COLOR_ROW (object));

  self = EDIT_COLOR_ROW (object);

  switch (prop_id)
    {
      case PROP_CURRENT_COLOR:
        g_value_set_boxed (value, &self->current_color);
	break;
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, psec);
	break;
    }
}

static void
edit_color_row_activate (AdwActionRow *action_row)
{
  EditColorRow *self;

  self = EDIT_COLOR_ROW (action_row);

  gtk_popover_popup (GTK_POPOVER (self->color_popover));
}

static void
notify_popover_visible_cb (EditColorRow *self)
{
  /* this prevents clicks on the popover widget also appearing visually on the row */
  if (gtk_widget_get_visible (GTK_WIDGET (self->color_popover)))
    gtk_widget_add_css_class (GTK_WIDGET (self), "has-open-popup");
  else
    gtk_widget_remove_css_class (GTK_WIDGET (self), "has-open-popup");
}

static void
update_buttons (EditColorRow *self)
{
  for (guint i = 0; i < PLAY_COLOR_PALLETTE_SIZE ; i ++)
    {
      const GdkRGBA *rgba;
      GtkWidget *button;

      button = gtk_grid_get_child_at (GTK_GRID (self->popover_grid),
                                      i % 4, i / 4);
      g_assert (button);
      g_signal_handlers_block_by_func (button, color_button_clicked, self);


      rgba = play_style_get_color (play_style_get (), (PlayColor) i);
      if (self->color_set && gdk_rgba_equal (rgba, &self->current_color))
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), TRUE);
      else
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), FALSE);
          
      g_signal_handlers_unblock_by_func (button, color_button_clicked, self);
    }
}
