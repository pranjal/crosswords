/* picker-list-row.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include <libipuz/libipuz.h>
#include <glib/gi18n-lib.h>

#include "picker-list-row.h"
#include "puzzle-set-model.h"


enum
{
  PROP_0,
  PROP_MODEL_ROW,
  N_PROPS
};

enum {
  CHANGED,
  CLEAR_CLICKED,
  DELETE_CLICKED,
  N_SIGNALS
};

static GParamSpec *obj_props[N_PROPS] = {NULL, };
static guint obj_signals[N_SIGNALS] = { 0 };


struct _PickerListRow
{
  AdwActionRow parent_object;

  PuzzleSetModelRow *model_row;
  guint changed_handler;

  /* internal widgets */
  GtkWidget *delete_button;
};


static void picker_list_row_init         (PickerListRow      *self);
static void picker_list_row_class_init   (PickerListRowClass *klass);
static void picker_list_row_get_property (GObject            *object,
                                          guint               prop_id,
                                          GValue             *value,
                                          GParamSpec         *pspec);
static void picker_list_row_set_property (GObject            *object,
                                          guint               prop_id,
                                          const GValue       *value,
                                          GParamSpec         *pspec);
static void picker_list_row_dispose      (GObject            *object);
static void clear_button_clicked_cb      (PickerListRow      *self);
static void delete_button_clicked_cb     (PickerListRow      *self);
static void model_row_changed            (PickerListRow      *self);


G_DEFINE_TYPE (PickerListRow, picker_list_row, ADW_TYPE_ACTION_ROW);


static void
picker_list_row_init (PickerListRow *self)
{
  GtkWidget *box;
  GtkWidget *button;

  box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
  gtk_widget_add_css_class (box, "linked");

  button = gtk_button_new ();
  gtk_button_set_icon_name (GTK_BUTTON (button), "view-refresh-symbolic");
  gtk_box_append (GTK_BOX (box), button);
  gtk_widget_set_valign (box, GTK_ALIGN_CENTER);
  g_signal_connect_swapped (button, "clicked",
                            G_CALLBACK (clear_button_clicked_cb),
                            self);

  self->delete_button = gtk_button_new ();
  gtk_button_set_icon_name (GTK_BUTTON (self->delete_button), "edit-delete-symbolic");
  gtk_box_append (GTK_BOX (box), self->delete_button);
  g_signal_connect_swapped (self->delete_button, "clicked",
                            G_CALLBACK (delete_button_clicked_cb),
                            self);
  adw_action_row_add_suffix (ADW_ACTION_ROW (self), box);
}

static void
picker_list_row_class_init (PickerListRowClass  *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = picker_list_row_get_property;
  object_class->set_property = picker_list_row_set_property;
  object_class->dispose = picker_list_row_dispose;

  obj_props[PROP_MODEL_ROW] =
    g_param_spec_object ("model-row",
                         "Model Row",
                         "Model Row",
                         PUZZLE_TYPE_SET_MODEL_ROW,
                         G_PARAM_READWRITE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);

  obj_signals[CHANGED] =
    g_signal_new ("changed",
                  PICKER_TYPE_LIST_ROW,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);

  obj_signals[CLEAR_CLICKED] =
    g_signal_new ("clear-clicked",
                  PICKER_TYPE_LIST_ROW,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);

  obj_signals[DELETE_CLICKED] =
    g_signal_new ("delete-clicked",
                  PICKER_TYPE_LIST_ROW,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);
}

static void
picker_list_row_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  PickerListRow *self;

  self = PICKER_LIST_ROW (object);

  switch (prop_id)
    {
    case PROP_MODEL_ROW:
      g_value_set_object (value, self->model_row);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
picker_list_row_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  PickerListRow *self;

  self = PICKER_LIST_ROW (object);

  switch (prop_id)
    {
    case PROP_MODEL_ROW:
      g_clear_object (&self->model_row);
      self->model_row = (PuzzleSetModelRow *) g_value_dup_object (value);
      model_row_changed (self);
      if (self->changed_handler)
        g_signal_handler_disconnect (self->model_row, self->changed_handler);
      self->changed_handler =
        g_signal_connect_swapped (self->model_row, "changed",
                                  G_CALLBACK (model_row_changed), self);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
picker_list_row_dispose (GObject *object)
{
  PickerListRow *self;

  self = PICKER_LIST_ROW (object);

  if (self->changed_handler)
    {
      g_signal_handler_disconnect (self->model_row, self->changed_handler);
      self->changed_handler = 0;
    }
  g_clear_object (&self->model_row);

  G_OBJECT_CLASS (picker_list_row_parent_class)->dispose (object);
}

static void
clear_button_clicked_cb (PickerListRow *self)
{
  g_signal_emit (self, obj_signals [CLEAR_CLICKED], 0);
}

static void
delete_button_clicked_cb (PickerListRow *self)
{
  g_signal_emit (self, obj_signals [DELETE_CLICKED], 0);
}

static void
model_row_changed (PickerListRow *self)
{
  gboolean readonly;
  IpuzPuzzle *puzzle;
  const gchar *puzzle_name;
  g_autofree gchar *title = NULL;
  IpuzGuesses *guesses;
  gfloat percent = 0.0;
  gboolean won;
  g_autofree gchar *subtitle = NULL;

  readonly = puzzle_set_model_row_get_readonly (self->model_row);
  gtk_widget_set_visible (self->delete_button, !readonly);

  puzzle = puzzle_set_model_row_get_puzzle (self->model_row);
  puzzle_name = puzzle_set_model_row_get_puzzle_name (self->model_row);
  g_object_get (puzzle,
                "title", &title,
                NULL);

  guesses = ipuz_crossword_get_guesses (IPUZ_CROSSWORD (puzzle));
  if (guesses)
    percent = ipuz_guesses_get_percent (guesses);
  won = ipuz_crossword_game_won (IPUZ_CROSSWORD (puzzle));

  if (won)
    {
      gtk_widget_add_css_class (GTK_WIDGET (self), "puzzle-won");
      subtitle = g_strdup (_("<b>Puzzle won</b>"));
    }
  else
    {
      gtk_widget_remove_css_class (GTK_WIDGET (self), "puzzle-won");
      if (percent == 0.0)
        subtitle = NULL;
      else
        subtitle = g_strdup_printf (_("%d%% filled in"), (gint)(percent * 100));
    }

  g_object_set (self,
                "title", title?title:puzzle_name,
                "subtitle", subtitle,
                NULL);

  g_signal_emit (self, obj_signals [CHANGED], 0);
}
