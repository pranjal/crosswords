/* edit-save-changes-dialog.c
 *
 * Copyright 2022 Jonathan Blandford
 * Based on code by Christian Hergert from gnome-text-editor
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include "edit-save-changes-dialog.h"
#include "edit-window.h"


#define SAVE_DIALOG_APP "save-dialog-app"
typedef struct
{
  GtkWidget *edit_window;
  GtkWidget *check;
  char *title;
  char *path;
} EditSaveInfo;


static void
edit_save_info_free (EditSaveInfo *info)
{
  g_free (info->title);
  g_free (info->path);
  g_object_unref (info->edit_window);
  g_free (info);
}

static void
edit_save_info_list_free (GList    *list,
                          GClosure *closure)
{
  g_list_free_full (list, (GDestroyNotify) edit_save_info_free);
}


static void
edit_save_changes_close_windows (GList *save_info_list)
{
  for (GList *list = save_info_list; list; list = list->next)
    {
      EditSaveInfo *save_info = list->data;

      if (gtk_check_button_get_active (GTK_CHECK_BUTTON (save_info->check)))
        {
          gtk_window_destroy (GTK_WINDOW (save_info->edit_window));
        }
    }
}

static void
edit_save_changes_save_files (GList *save_info_list)
{
  GList *list;

  for (list = save_info_list; list; list = list->next)
    {
      EditSaveInfo *save_info = list->data;

      if (gtk_check_button_get_active (GTK_CHECK_BUTTON (save_info->check)))
        {
          g_autofree gchar *uri = NULL;

          uri = g_filename_to_uri (save_info->path, NULL, NULL);
          /* FIXME(error): We need to go back and catch save errors */
          edit_window_save (EDIT_WINDOW (save_info->edit_window), uri);
        }
    }
}

static void
edit_save_changes_dialog_response (GtkMessageDialog *dialog,
                                   const gchar      *response,
                                   GList            *save_info_list)
{
  GApplication *app;

  app = G_APPLICATION (g_object_get_data (G_OBJECT (dialog), SAVE_DIALOG_APP));

  if (g_strcmp0 (response, "discard") == 0)
    {
      if (app)
        g_application_quit (app);
      else
        edit_save_changes_close_windows (save_info_list);
    }
  else if (g_strcmp0 (response, "save") == 0)
    {
      edit_save_changes_save_files (save_info_list);
      if (app)
        g_application_quit (app);
      else
        edit_save_changes_close_windows (save_info_list);
    }
}

static GtkWidget *
edit_save_changes_dialog_new (GtkWindow    *parent,
                              GList        *save_info_list)
{
  const char *discard_label;
  GtkWidget *dialog;
  GtkWidget *group;

  g_return_val_if_fail (!parent || GTK_IS_WINDOW (parent), NULL);
  g_return_val_if_fail (save_info_list != NULL, NULL);

  discard_label = g_dngettext (GETTEXT_PACKAGE, _("_Discard"), _("_Discard All"), (save_info_list->next == NULL));

  dialog = adw_message_dialog_new (parent,
                                   _("Save Changes?"),
                                   _("Open puzzles contain unsaved changes. Changes which are not saved will be permanently lost."));

  adw_message_dialog_add_responses (ADW_MESSAGE_DIALOG (dialog),
                                    "cancel",  _("_Cancel"),
                                    "discard", discard_label,
                                    "save", _("_Save"),
                                    NULL);
  adw_message_dialog_set_response_appearance (ADW_MESSAGE_DIALOG (dialog),
                                              "discard",
                                              ADW_RESPONSE_DESTRUCTIVE);
  adw_message_dialog_set_default_response (ADW_MESSAGE_DIALOG (dialog), "cancel");
  adw_message_dialog_set_close_response (ADW_MESSAGE_DIALOG (dialog), "cancel");

  group = adw_preferences_group_new ();
  adw_message_dialog_set_extra_child (ADW_MESSAGE_DIALOG (dialog), group);

  for (GList *l = save_info_list; l; l = l->next)
    {
      EditSaveInfo *save_info = (EditSaveInfo *) l->data;
      GtkWidget *row;

      row = g_object_new (ADW_TYPE_ACTION_ROW,
                          "title", save_info->title,
                          "subtitle", save_info->path,
                          NULL);
      save_info->check = g_object_new (GTK_TYPE_CHECK_BUTTON,
                                       "active", TRUE,
                                       NULL);
      gtk_accessible_update_property (GTK_ACCESSIBLE (save_info->check),
                                      GTK_ACCESSIBLE_PROPERTY_LABEL,
                                      _("Save changes for this puzzle"), -1);
      adw_action_row_add_prefix (ADW_ACTION_ROW (row), save_info->check);
      adw_action_row_set_activatable_widget (ADW_ACTION_ROW (row), save_info->check);
      adw_preferences_group_add (ADW_PREFERENCES_GROUP (group), row);
    }

  return dialog;
}

GList *
edit_save_info_list_add (GList       *save_info_list,
                         GtkWidget   *edit_window,
                         const gchar *title,
                         const gchar *path)
{
  EditSaveInfo *save_info;

  g_return_val_if_fail (EDIT_IS_WINDOW (edit_window), NULL);
  g_return_val_if_fail (title != NULL, NULL);
  g_return_val_if_fail (path != NULL, NULL);

  save_info = g_new0 (EditSaveInfo, 1);
  save_info->edit_window = g_object_ref (edit_window);
  save_info->title = g_strdup (title);
  save_info->path = g_strdup (path);

  /* Append to keep the order the same as the MRU list */
  return g_list_append (save_info_list, save_info);
}

void
edit_save_changes_dialog_run (GtkWindow    *parent,
                              GList        *save_info_list,
                              GApplication *app)
{
  GtkWidget *dialog;

  dialog = edit_save_changes_dialog_new (parent, save_info_list);
  g_object_set_data (G_OBJECT (dialog), SAVE_DIALOG_APP, app);
  g_signal_connect_data (dialog,
                         "response",
                         G_CALLBACK (edit_save_changes_dialog_response),
                         save_info_list,
                         (GClosureNotify) edit_save_info_list_free, 0);

  gtk_window_present (GTK_WINDOW (dialog));
}
