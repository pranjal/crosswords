/* edit-window-controls.c
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* This file is a part of the EditWindow widget and is not a
 * standalone object. Instead, it is meant to encapsulate all code
 * related to PanelWidget flows in one separate file for convenience.
 *
 * BE CAREFUL TOUCING THIS FILE. IT IS A COMPLEXITY SYNC
 */



#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include "cell-preview.h"
#include "edit-autofill-details.h"
#include "edit-bars.h"
#include "edit-cell.h"
#include "edit-clue-details.h"
#include "edit-clue-list.h"
#include "edit-clue-info.h"
#include "edit-grid.h"
#include "edit-grid-info.h"
#include "edit-histogram.h"
#include "edit-metadata.h"
#include "edit-preview-window.h"
#include "edit-shapebg-row.h"
#include "edit-state.h"
#include "edit-symmetry.h"
#include "edit-window.h"
#include "edit-window-private.h"
#include "edit-word-list.h"
#include "word-array-model.h"
#include "word-list.h"


static void     load_puzzle_kind                          (EditWindow        *self);

/* update_* functions. These are for propagating the state from a new
   state to all the widgets in the editor. They don't touch the
   current state . */
static void     update_all                                (EditWindow        *self);
static void     update_grid_cursor_moved                  (EditWindow        *self);
static void     update_clues_cursor_moved                 (EditWindow        *self);
static void     update_clues_selection_changed            (EditWindow        *self);
static void     update_style_cursor_moved                 (EditWindow        *self);
static gboolean update_solver_timeout                     (EditWindow        *self);

/* State change functions. These are called after a change to the
 * puzzle. They modify the internal state to represent the new
 * state. */
static void     pushed_puzzle_new                         (EditWindow        *self);
static void     pushed_grid_changed                       (EditWindow        *self);
static void     pushed_state_changed                      (EditWindow        *self);
static void     pushed_metadata_changed                   (EditWindow        *self);

/* Callbacks from widget. These handle individual change requests from
 * a widget. They are responsible for pushing the change (if
 * necessary, and calling appropriate update functions. */
static void     symmetry_widget_symmetry_changed_cb       (EditWindow        *self,
                                                           IpuzSymmetry       symmetry);
static void     main_grid_guess_cb                        (EditWindow        *self,
                                                           const gchar       *guess);
static void     main_grid_guess_at_cell_cb                (EditWindow        *self,
                                                           const gchar       *guess,
                                                           IpuzCellCoord     *coord);
static void     main_grid_do_command_cb                   (EditWindow        *self,
                                                           GridCmdKind        kind);
static void     main_grid_copy_cb                         (EditWindow        *self);
static void     main_grid_paste_cb                        (EditWindow        *self,
                                                           const gchar       *paste);
static void     main_grid_cell_selected_cb                (EditWindow        *self,
                                                           IpuzCellCoord     *coord);
static void     main_grid_select_drag_start_cb            (EditWindow        *self,
                                                           IpuzCellCoord     *anchor_coord,
                                                           IpuzCellCoord     *new_coord,
                                                           GridSelectionMode  mode);
static void     main_grid_select_drag_update_cb           (EditWindow        *self,
                                                           IpuzCellCoord     *anchor_coord,
                                                           IpuzCellCoord     *new_coord,
                                                           GridSelectionMode  mode);
static void     main_grid_select_drag_end_cb              (EditWindow        *self);
static void     word_list_widget_word_activated_cb        (EditWindow        *self,
                                                           IpuzClueDirection  direction,
                                                           const gchar       *word,
                                                           const gchar       *enumeration_src);
static void     edit_autofill_details_grid_selected_cb    (EditWindow        *self,
                                                           guint              index);
static void     edit_autofill_details_start_solve_cb      (EditWindow        *self,
                                                           WordArrayModel    *skip_list,
                                                           gint               max_count);
static void     edit_autofill_details_stop_solve_cb       (EditWindow        *self);
static void     edit_autofill_details_reset_cb            (EditWindow        *self);
static void     clue_list_widget_clue_selected_cb         (EditWindow        *self,
                                                           IpuzClueId        *clue_id);
static void     clue_details_widget_clue_changed_cb       (EditWindow        *self,
                                                           const gchar       *new_clue,
                                                           IpuzEnumeration   *new_enumeration);
static void     clue_details_widget_selection_changed_cb  (EditWindow        *self,
                                                           const gchar       *selection_text);
static void     edit_metadata_metadata_changed_cb         (EditWindow        *self,
                                                           gchar             *property,
                                                           gchar             *value);
static void     edit_metadata_showenumerations_changed_cb (EditWindow        *self,
                                                           gboolean           showenumerations);


static void
update_color_rows (EditWindow *self)
{
  IpuzCell *cell;
  IpuzStyle *style;
  const gchar *color_str = NULL;

  cell = ipuz_crossword_get_cell (self->edit_state->style_state->xword,
                                  &self->edit_state->style_state->cursor);
  g_assert (cell);

  style = ipuz_cell_get_style (cell);

  if (style)
    color_str = ipuz_style_get_text_color (style);
  if (color_str)
    {
      GdkRGBA color;

      if (gdk_rgba_parse (&color, color_str))
        g_object_set (self->style_foreground_color_row,
                      "current-color", &color,
                      NULL);
    }
  else
    {
      g_object_set (self->style_foreground_color_row,
                    "current-color", NULL,
                    NULL);
    }

  if (style)
    color_str = ipuz_style_get_bg_color (style);
  else
    color_str = NULL;
  if (color_str)
    {
      GdkRGBA color;

      if (gdk_rgba_parse (&color, color_str))
        g_object_set (self->style_background_color_row,
                      "current-color", &color,
                      NULL);
    }
  else
    g_object_set (self->style_background_color_row,
                  "current-color", NULL,
                  NULL);
}

static void
update_all (EditWindow *self)
{
  IpuzPuzzle *puzzle;
  //GTimer *timer;

  //timer = g_timer_new ();
  //g_timer_start (timer);

  VALIDATE_EDIT_STATE (self->edit_state);
  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);

  /* recalculate the info for the puzzle */
  g_clear_object (&self->edit_state->info);
  self->edit_state->info = ipuz_puzzle_get_puzzle_info (puzzle);

  switch (self->edit_state->stage)
    {
    case EDIT_STAGE_GRID:
      edit_grid_update (EDIT_GRID (self->main_grid), self->edit_state->grid_state);
      edit_word_list_update (EDIT_WORD_LIST (self->word_list_widget), self->edit_state->grid_state);
      cell_preview_update_from_cursor (CELL_PREVIEW (self->grid_preview),
                                       self->edit_state->grid_state,
                                       self->edit_state->sidebar_logo_config);
      edit_cell_update (EDIT_CELL (self->cell_widget), self->edit_state->grid_state, self->edit_state->sidebar_logo_config);
      edit_grid_info_update (EDIT_GRID_INFO (self->edit_grid_widget), self->edit_state->grid_state, self->edit_state->info);
      edit_histogram_update (EDIT_HISTOGRAM (self->letter_histogram_widget), self->edit_state->info);
      edit_histogram_update (EDIT_HISTOGRAM (self->cluelen_histogram_widget), self->edit_state->info);
      edit_autofill_details_update (EDIT_AUTOFILL_DETAILS (self->autofill_details),
                                    self->edit_state->grid_state,
                                    self->edit_state->solver);
      break;
    case EDIT_STAGE_CLUES:
      edit_grid_update (EDIT_GRID (self->main_grid), self->edit_state->clues_state);
      edit_clue_details_update (EDIT_CLUE_DETAILS (self->clue_details_widget),
                                self->edit_state->clues_state,
                                self->edit_state->sidebar_logo_config);
      edit_clue_list_update (EDIT_CLUE_LIST (self->clue_list_widget), self->edit_state->clues_state);
      break;
    case EDIT_STAGE_STYLE:
      edit_grid_update (EDIT_GRID (self->main_grid), self->edit_state->style_state);
      edit_shapebg_row_update (EDIT_SHAPEBG_ROW (self->style_shapebg_row), self->edit_state->style_state);
      update_color_rows (self);
      cell_preview_update_from_cursor (CELL_PREVIEW (self->style_preview),
                                       self->edit_state->style_state,
                                       self->edit_state->sidebar_logo_config);

      break;
    case EDIT_STAGE_METADATA:
      edit_metadata_update (EDIT_METADATA (self->edit_metadata), puzzle);
      break;
    default:
      g_assert_not_reached ();
    }

  /* Preview window updates on every change */
  if (self->edit_state->preview_window)
    edit_preview_window_update (EDIT_PREVIEW_WINDOW (self->edit_state->preview_window),
                                self->edit_state->preview_state);

  //g_timer_stop (timer);
  //g_print ("update_all: Total time %f\n", g_timer_elapsed (timer, NULL));
  //g_timer_destroy (timer);
}


/* Sets the initial state of a window for the current puzzle
 * kind. This is called once before any user interaction. */
static void
load_puzzle_kind (EditWindow *self)
{
  IpuzPuzzle *puzzle;
  IpuzPuzzleKind kind;
  const gchar *grid_stage = "cell";

  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);
  kind = ipuz_puzzle_get_puzzle_kind (puzzle);

  self->edit_state->symmetry_widget_enabled = FALSE;
  self->edit_state->grid_switcher_bar_enabled = FALSE;
  self->edit_state->letter_histogram_widget_enabled = FALSE;
  self->edit_state->cluelen_histogram_widget_enabled = FALSE;

  /* Figure out the default setup */
  switch (kind)
    {
    case IPUZ_PUZZLE_CROSSWORD:
    case IPUZ_PUZZLE_CRYPTIC:
    case IPUZ_PUZZLE_BARRED:
      self->edit_state->symmetry_widget_enabled = TRUE;
      self->edit_state->grid_switcher_bar_enabled = TRUE;
      self->edit_state->letter_histogram_widget_enabled = TRUE;
      self->edit_state->cluelen_histogram_widget_enabled = TRUE;

      break;
    case IPUZ_PUZZLE_ACROSTIC:
      grid_stage = "acrostic";
      break;
    default:
      g_assert_not_reached ();
    }

  /* set the initial stage and page */
  _edit_window_set_stage (self, EDIT_STAGE_GRID);
  adw_view_stack_set_visible_child_name (ADW_VIEW_STACK (self->grid_stack),
                                         grid_stage);

  if (self->edit_state->symmetry_widget_enabled)
    {
      IpuzSymmetry symmetry;

      symmetry = crosswords_quirks_get_symmetry (self->edit_state->quirks);
      edit_symmetry_set_symmetry (EDIT_SYMMETRY (self->symmetry_widget), symmetry, self->edit_state->sidebar_logo_config);
    }
  gtk_widget_set_child_visible (self->grid_switcher_bar, self->edit_state->grid_switcher_bar_enabled);
  gtk_widget_action_set_enabled (GTK_WIDGET (self), "win.goto-grid-stage", self->edit_state->grid_switcher_bar_enabled);

  gtk_widget_set_visible (self->letter_histogram_group, self->edit_state->letter_histogram_widget_enabled);
  gtk_widget_set_visible (self->cluelen_histogram_group, self->edit_state->cluelen_histogram_widget_enabled);
}

static void
update_grid_cursor_moved (EditWindow *self)
{
  if (self->edit_state->stage != EDIT_STAGE_GRID)
    return;

  cell_preview_update_from_cursor (CELL_PREVIEW (self->grid_preview),
                                   self->edit_state->grid_state,
                                   self->edit_state->sidebar_logo_config);
  edit_grid_update (EDIT_GRID (self->main_grid), self->edit_state->grid_state);
  edit_word_list_update (EDIT_WORD_LIST (self->word_list_widget), self->edit_state->grid_state);
  edit_cell_update (EDIT_CELL (self->cell_widget), self->edit_state->grid_state, self->edit_state->sidebar_logo_config);
}

static void
update_clues_cursor_moved (EditWindow *self)
{
  if (self->edit_state->stage != EDIT_STAGE_CLUES)
    return;

  edit_grid_update (EDIT_GRID (self->main_grid), self->edit_state->clues_state);
  edit_clue_details_update (EDIT_CLUE_DETAILS (self->clue_details_widget),
                            self->edit_state->clues_state,
                            self->edit_state->sidebar_logo_config);
  edit_clue_list_update (EDIT_CLUE_LIST (self->clue_list_widget), self->edit_state->clues_state);
}

static void
update_clues_selection_changed (EditWindow *self)
{
  edit_clue_info_update (EDIT_CLUE_INFO (self->clue_info_anagram),
                         self->edit_state->clue_selection_text);
  edit_clue_info_update (EDIT_CLUE_INFO (self->clue_info_odd),
                         self->edit_state->clue_selection_text);
  edit_clue_info_update (EDIT_CLUE_INFO (self->clue_info_even),
                         self->edit_state->clue_selection_text);
}


static void
update_style_cursor_moved (EditWindow *self)
{
  if (self->edit_state->stage != EDIT_STAGE_STYLE)
    return;

  cell_preview_update_from_cursor (CELL_PREVIEW (self->style_preview),
                                   self->edit_state->style_state,
                                   self->edit_state->sidebar_logo_config);
  edit_shapebg_row_update (EDIT_SHAPEBG_ROW (self->style_shapebg_row), self->edit_state->style_state);
  update_color_rows (self);
  edit_grid_update (EDIT_GRID (self->main_grid), self->edit_state->style_state);
}

static gboolean
update_solver_timeout (EditWindow *self)
{
  update_all (self);

  if (word_solver_get_state (self->edit_state->solver) == WORD_SOLVER_RUNNING)
    return TRUE;

  self->edit_state->solver_timeout = 0;
  return FALSE;
}

/* This is called by the pushed_* class of functions. At this point,
 * all autofill changes should be committed, and we are guaranteed
 * that an update will happen later. It just has to clear the autofill
 * machinery.
 *
 * We don't support autofill to keep running past any puzzle changes
 * at this time.
 */
static void
cancel_autofill (EditWindow *self)
{
  if (self->edit_state == NULL)
    return;

  g_clear_handle_id (&self->edit_state->solver_timeout, g_source_remove);
  word_solver_reset (self->edit_state->solver);
  self->edit_state->grid_state =
    grid_state_replace (self->edit_state->grid_state,
                        grid_state_set_guesses (self->edit_state->grid_state, NULL));
}

/* This is called when we clear the stack and push a new puzzle onto
 * it. */
static void
pushed_puzzle_new (EditWindow *self)
{
  IpuzPuzzle *puzzle;

  cancel_autofill (self);

  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);

  g_clear_pointer (&self->edit_state, edit_state_free);
  self->edit_state = edit_state_new (puzzle);

  /* Update widgets */
  load_puzzle_kind (self);
  update_all (self);
}

static void
pushed_grid_changed (EditWindow *self)
{
  IpuzPuzzle *puzzle;

  cancel_autofill (self);

  /* Make sure both states reflect the new puzzle */
  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);
  self->edit_state->grid_state =
    grid_state_replace (self->edit_state->grid_state,
                        grid_state_swap_xword (self->edit_state->grid_state,
                                               IPUZ_CROSSWORD (puzzle)));
  self->edit_state->clues_state =
    grid_state_replace (self->edit_state->clues_state,
                        grid_state_swap_xword (self->edit_state->clues_state,
                                               IPUZ_CROSSWORD (puzzle)));
  self->edit_state->style_state =
    grid_state_replace (self->edit_state->style_state,
                        grid_state_swap_xword (self->edit_state->style_state,
                                               IPUZ_CROSSWORD (puzzle)));

  self->edit_state->style_state =
    grid_state_replace (self->edit_state->style_state,
                        grid_state_swap_xword (self->edit_state->style_state,
                                               IPUZ_CROSSWORD (puzzle)));

  update_all (self);
}

static void
pushed_state_changed (EditWindow *self)
{
  cancel_autofill (self);

  edit_word_list_update (EDIT_WORD_LIST (self->word_list_widget), self->edit_state->grid_state);
}

static void
pushed_metadata_changed (EditWindow *self)
{
  IpuzPuzzle *puzzle;

  cancel_autofill (self);

  /* Make sure both states reflect the new puzzle */
  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);
  edit_metadata_update (EDIT_METADATA (self->edit_metadata), puzzle);
}

/*
 * Callbacks
 */

static void
symmetry_widget_symmetry_changed_cb (EditWindow   *self,
                                     IpuzSymmetry  symmetry)
{
  crosswords_quirks_set_symmetry (self->edit_state->quirks,
                                  symmetry);
}

static void
edit_grid_guess_cb (EditWindow  *self,
                    const gchar *guess)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  self->edit_state->grid_state =
    grid_state_replace (self->edit_state->grid_state,
                        grid_state_guess (self->edit_state->grid_state, guess));

  puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (self->edit_state->grid_state->xword));
}

static void
edit_grid_guess_at_cell_cb (EditWindow    *self,
                            const gchar   *guess,
                            IpuzCellCoord *coord)
{
  g_assert (coord != NULL);
  VALIDATE_EDIT_STATE (self->edit_state);

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  self->edit_state->grid_state =
    grid_state_replace (self->edit_state->grid_state,
                        grid_state_guess_at_cell (self->edit_state->grid_state, guess, *coord));

  puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (self->edit_state->grid_state->xword));
}

static void
edit_grid_do_command_cb (EditWindow   *self,
                         GridCmdKind  kind)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  if (CMD_KIND_DESTRUCTIVE (kind))
    edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                              self->puzzle_stack,
                              TRUE);

  self->edit_state->grid_state =
    grid_state_replace (self->edit_state->grid_state,
                        grid_state_do_command (self->edit_state->grid_state, kind));

  /* FIXME(undo): this will trigger even if nothing has changed, such
   * as backspace of an empty cell. We should check first*/
  if (CMD_KIND_DESTRUCTIVE (kind))
    puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_GRID,
                              IPUZ_PUZZLE (self->edit_state->grid_state->xword));
  else
    update_grid_cursor_moved (self);
}

static void
edit_grid_cell_selected_cb (EditWindow    *self,
                            IpuzCellCoord *coord)
{
  g_assert (coord != NULL);
  VALIDATE_EDIT_STATE (self->edit_state);

  self->edit_state->grid_state =
    grid_state_replace (self->edit_state->grid_state,
                        grid_state_cell_selected (self->edit_state->grid_state, *coord));

  update_grid_cursor_moved (self);
}

static void
edit_grid_copy_cb (EditWindow *self)
{
  GdkClipboard *clipboard;
  g_autofree gchar *str = NULL;

  /* If direction is NONE, we're probably on a block */
  if (self->edit_state->grid_state->clue.direction == IPUZ_CLUE_DIRECTION_NONE)
    return;

  str = ipuz_crossword_get_clue_string_by_id (self->edit_state->grid_state->xword,
                                              &self->edit_state->grid_state->clue);
  clipboard = gtk_widget_get_clipboard (GTK_WIDGET (self));
  gdk_clipboard_set_text (clipboard, str);
}

static void
edit_grid_paste_cb (EditWindow  *self,
                    const gchar *str)
{
  IpuzClueDirection direction;

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  direction = self->edit_state->grid_state->clue.direction;
  if (direction == IPUZ_CLUE_DIRECTION_NONE)
    direction = IPUZ_CLUE_DIRECTION_ACROSS;

  self->edit_state->grid_state = grid_state_replace (self->edit_state->grid_state,
                                                     grid_state_guess_word (self->edit_state->grid_state,
                                                                            self->edit_state->grid_state->cursor,
                                                                            direction, str));
  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (self->edit_state->grid_state->xword));
}

static void
edit_clues_cell_selected_cb (EditWindow    *self,
                             IpuzCellCoord *coord)
{
  g_assert (coord != NULL);
  VALIDATE_EDIT_STATE (self->edit_state);

  edit_clue_details_commit_changes (EDIT_CLUE_DETAILS (self->clue_details_widget));

  self->edit_state->clues_state =
    grid_state_replace (self->edit_state->clues_state,
                        grid_state_cell_selected (self->edit_state->clues_state, *coord));

  update_clues_cursor_moved (self);
}

static void
edit_clues_copy_cb (EditWindow *self)
{
  GdkClipboard *clipboard;
  g_autofree gchar *str = NULL;

  /* If direction is NONE, we're probably on a block */
  if (self->edit_state->clues_state->clue.direction == IPUZ_CLUE_DIRECTION_NONE)
    return;

  str = ipuz_crossword_get_clue_string_by_id (self->edit_state->clues_state->xword,
                                              &self->edit_state->clues_state->clue);
  clipboard = gtk_widget_get_clipboard (GTK_WIDGET (self));
  gdk_clipboard_set_text (clipboard, str);
}


static void
edit_clues_do_command_cb (EditWindow  *self,
                          GridCmdKind  kind)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  edit_clue_details_commit_changes (EDIT_CLUE_DETAILS (self->clue_details_widget));

  self->edit_state->clues_state =
    grid_state_replace (self->edit_state->clues_state,
                        grid_state_do_command (self->edit_state->clues_state, kind));

  update_clues_cursor_moved (self);
}

static void
edit_style_cell_selected_cb (EditWindow    *self,
                             IpuzCellCoord *coord)
{
  g_assert (coord != NULL);
  VALIDATE_EDIT_STATE (self->edit_state);

  self->edit_state->style_state =
    grid_state_replace (self->edit_state->style_state,
                        grid_state_cell_selected (self->edit_state->style_state, *coord));

  update_style_cursor_moved (self);
}

static void
edit_style_copy_cb (EditWindow *self)
{
  GdkClipboard *clipboard;
  g_autofree gchar *str = NULL;

  /* If direction is NONE, we're probably on a block */
  if (self->edit_state->style_state->clue.direction == IPUZ_CLUE_DIRECTION_NONE)
    return;

  str = ipuz_crossword_get_clue_string_by_id (self->edit_state->style_state->xword,
                                              &self->edit_state->style_state->clue);
  clipboard = gtk_widget_get_clipboard (GTK_WIDGET (self));
  gdk_clipboard_set_text (clipboard, str);
}

static void
edit_style_do_command_cb (EditWindow  *self,
                          GridCmdKind  kind)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  self->edit_state->style_state =
    grid_state_replace (self->edit_state->style_state,
                        grid_state_do_command (self->edit_state->style_state, kind));

  update_style_cursor_moved (self);
}

static void
main_grid_guess_cb (EditWindow  *self,
                    const gchar *guess)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  if (self->edit_state->stage == EDIT_STAGE_GRID)
    edit_grid_guess_cb (self, guess);
}

static void
main_grid_guess_at_cell_cb (EditWindow    *self,
                            const gchar   *guess,
                            IpuzCellCoord *coord)
{
  g_assert (coord != NULL);
  VALIDATE_EDIT_STATE (self->edit_state);

  if (self->edit_state->stage == EDIT_STAGE_GRID)
    edit_grid_guess_at_cell_cb (self, guess, coord);
}

static void
main_grid_do_command_cb (EditWindow  *self,
                         GridCmdKind  kind)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  if (self->edit_state->stage == EDIT_STAGE_GRID)
    edit_grid_do_command_cb (self, kind);
  else if (self->edit_state->stage == EDIT_STAGE_CLUES)
    edit_clues_do_command_cb (self, kind);
  else if (self->edit_state->stage == EDIT_STAGE_STYLE)
    edit_style_do_command_cb (self, kind);
}

static void
main_grid_cell_selected_cb (EditWindow    *self,
                            IpuzCellCoord *coord)
{
  g_assert (coord != NULL);
  VALIDATE_EDIT_STATE (self->edit_state);

  if (self->edit_state->stage == EDIT_STAGE_GRID)
    edit_grid_cell_selected_cb (self, coord);
  else if (self->edit_state->stage == EDIT_STAGE_CLUES)
    edit_clues_cell_selected_cb (self, coord);
  else if (self->edit_state->stage == EDIT_STAGE_STYLE)
    edit_style_cell_selected_cb (self, coord);
}

static void
main_grid_select_drag_start_cb (EditWindow        *self,
                                IpuzCellCoord     *anchor_coord,
                                IpuzCellCoord     *new_coord,
                                GridSelectionMode  mode)
{
  self->edit_state->grid_state =
    grid_state_replace (self->edit_state->grid_state,
                        grid_state_select_drag_start (self->edit_state->grid_state,
                                                      *anchor_coord,
                                                      *new_coord,
                                                      mode));
  update_all (self);
}

static void
main_grid_select_drag_update_cb (EditWindow        *self,
                                 IpuzCellCoord     *anchor_coord,
                                 IpuzCellCoord     *new_coord,
                                 GridSelectionMode  mode)
{
  self->edit_state->grid_state =
    grid_state_replace (self->edit_state->grid_state,
                        grid_state_select_drag_update (self->edit_state->grid_state,
                                                       *anchor_coord,
                                                       *new_coord,
                                                       mode));
  update_all (self);
}

static void
main_grid_select_drag_end_cb (EditWindow *self)
{
  self->edit_state->grid_state =
    grid_state_replace (self->edit_state->grid_state,
                        grid_state_select_drag_end (self->edit_state->grid_state));
  update_all (self);
}

static void
main_grid_copy_cb (EditWindow *self)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  if (self->edit_state->stage == EDIT_STAGE_GRID)
    edit_grid_copy_cb (self);
  else if (self->edit_state->stage == EDIT_STAGE_CLUES)
    edit_clues_copy_cb (self);
  else if (self->edit_state->stage == EDIT_STAGE_STYLE)
    edit_style_copy_cb (self);
}

static void
main_grid_paste_cb (EditWindow  *self,
                    const gchar *str)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  if (self->edit_state->stage == EDIT_STAGE_GRID)
    edit_grid_paste_cb (self, str);
}

static void
word_list_widget_word_activated_cb (EditWindow        *self,
                                    IpuzClueDirection  direction,
                                    const gchar       *word,
                                    const gchar       *enumeration_src)
{
  IpuzClue *clue;
  const GArray *coords;
  IpuzCellCoord coord;
  g_autoptr (IpuzEnumeration) enumeration = NULL;

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  clue = ipuz_crossword_find_clue_by_coord (self->edit_state->grid_state->xword,
                                            direction,
                                            &self->edit_state->grid_state->cursor);

  /* Some sanity checking. May fail in raw mode in the future */
  g_assert (clue != NULL);

  /* Update the enumeration */
  if (enumeration_src)
    {
      enumeration = ipuz_enumeration_new (enumeration_src, IPUZ_VERBOSITY_STANDARD);
    }
  ipuz_clue_set_enumeration (clue, enumeration);
  ipuz_crossword_fix_enumerations (self->edit_state->grid_state->xword);

  coords = ipuz_clue_get_cells (clue);
  g_assert (coords != NULL && coords->len > 0);

  coord = g_array_index (coords, IpuzCellCoord, 0);
  self->edit_state->grid_state =
    grid_state_replace (self->edit_state->grid_state,
                        grid_state_guess_word (self->edit_state->grid_state,
                                               coord,
                                               direction, word));
  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (self->edit_state->grid_state->xword));
}

static void
solver_finished_cb (EditWindow *self)
{
  update_all (self);
}

static void
edit_autofill_details_grid_selected_cb (EditWindow *self,
                                        guint       index)
{
  IpuzGuesses *solution;
  g_autoptr (IpuzGuesses) solution_copy = NULL;

  /* Make a copy of the solution */
  solution = word_solver_get_solution (self->edit_state->solver, index);
  if (solution)
    solution_copy = ipuz_guesses_copy (solution);
  self->edit_state->grid_state =
    grid_state_replace (self->edit_state->grid_state,
                        grid_state_set_guesses (self->edit_state->grid_state, solution_copy));

  update_grid_cursor_moved (self);
}

static void
edit_autofill_details_start_solve_cb (EditWindow     *self,
                                      WordArrayModel *skip_list,
                                      gint            max_count)
{
  if (self->edit_state->solver_timeout != 0)
    {
      /* We didn't stop the timer... ./~ */
      g_warning ("Failed to clear the previous timeout");
      g_clear_handle_id (&self->edit_state->solver_timeout, g_source_remove);
    }

  if (self->edit_state->solver_finished_handler == 0)
    self->edit_state->solver_finished_handler =
      g_signal_connect_swapped (self->edit_state->solver,
                                "finished",
                                G_CALLBACK (solver_finished_cb), self);


  word_solver_run (self->edit_state->solver,
                   self->edit_state->grid_state->xword,
                   self->edit_state->grid_state->selected_cells,
                   word_array_model_get_array (skip_list),
                   max_count, 50);
  self->edit_state->solver_timeout =
    g_timeout_add (1000, G_SOURCE_FUNC (update_solver_timeout), self);
  update_all (self);
}

static void
edit_autofill_details_stop_solve_cb (EditWindow *self)
{
  g_clear_handle_id (&self->edit_state->solver_timeout, g_source_remove);
  word_solver_cancel (self->edit_state->solver);
  update_all (self);
}

static void
edit_autofill_details_reset_cb (EditWindow *self)
{
  cancel_autofill (self);
  update_all (self);
}

static void
cell_widget_side_changed_cb (EditWindow     *self,
                             IpuzStyleSides  side)
{
  IpuzSymmetry symmetry;
  IpuzStyleSides new_sides = 0;

  g_assert (IPUZ_IS_BARRED (self->edit_state->grid_state->xword));

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  symmetry = crosswords_quirks_get_symmetry (self->edit_state->grid_state->quirks);

  new_sides = ipuz_barred_calculate_side_toggle (IPUZ_BARRED (self->edit_state->grid_state->xword),
                                                 &self->edit_state->grid_state->cursor,
                                                 side,
                                                 symmetry);
  self->edit_state->grid_state =
    grid_state_replace (self->edit_state->grid_state,
                        grid_state_set_bars (self->edit_state->grid_state,
                                             self->edit_state->grid_state->cursor,
                                             new_sides, TRUE));

  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (self->edit_state->grid_state->xword));
}

static void
cell_widget_cell_type_changed_cb (EditWindow   *self,
                                  IpuzCellType  cell_type)
{
  VALIDATE_EDIT_STATE (self->edit_state);

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  self->edit_state->grid_state =
    grid_state_replace (self->edit_state->grid_state,
                        grid_state_set_cell_type (self->edit_state->grid_state,
                                                    self->edit_state->grid_state->cursor,
                                                    cell_type));

  puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (self->edit_state->grid_state->xword));
}

static void
cell_widget_initial_val_changed_cb (EditWindow *self,
                                    gboolean    use_initial_val)
{
  IpuzCell *cell;
  const gchar *str;

  VALIDATE_EDIT_STATE (self->edit_state);

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  cell = ipuz_crossword_get_cell (self->edit_state->grid_state->xword,
                                  &self->edit_state->grid_state->cursor);
  g_assert (cell != NULL);

  if (use_initial_val)
    {
      str = ipuz_cell_get_solution (cell);
      g_return_if_fail (str != NULL);
      ipuz_cell_set_initial_val (cell, str);
      ipuz_cell_set_solution (cell, NULL);
    }
  else
    {
      str = ipuz_cell_get_initial_val (cell);
      g_return_if_fail (str != NULL);
      ipuz_cell_set_solution (cell, str);
      ipuz_cell_set_initial_val (cell, NULL);
    }

  puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (self->edit_state->grid_state->xword));
}

static void
clue_details_widget_clue_changed_cb (EditWindow      *self,
                                     const gchar     *new_clue_text,
                                     IpuzEnumeration *new_enumeration)
{
  IpuzClue *clue;

  clue = ipuz_crossword_get_clue_by_id (self->edit_state->clues_state->xword,
                                        &self->edit_state->clues_state->clue);

  g_return_if_fail (clue != NULL);

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  ipuz_clue_set_clue_text (clue, new_clue_text);
  ipuz_clue_set_enumeration (clue, new_enumeration);

  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (self->edit_state->clues_state->xword));
}

static void
clue_details_widget_selection_changed_cb (EditWindow  *self,
                                          const gchar *selection_text)
{
  g_clear_pointer (&self->edit_state->clue_selection_text, g_free);
  self->edit_state->clue_selection_text = g_strdup (selection_text);
  update_clues_selection_changed (self);
}

static void
clue_list_widget_clue_selected_cb (EditWindow *self,
                                   IpuzClueId *clue_id)
{
  IpuzClue *clue;

  VALIDATE_EDIT_STATE (self->edit_state);
  g_assert (clue_id);

  edit_clue_details_commit_changes (EDIT_CLUE_DETAILS (self->clue_details_widget));

  clue = ipuz_crossword_get_clue_by_id (self->edit_state->clues_state->xword, clue_id);
  self->edit_state->clues_state =
    grid_state_replace (self->edit_state->clues_state,
                        grid_state_clue_selected (self->edit_state->clues_state, clue));

  update_clues_cursor_moved (self);
}

static void
style_shapebg_changed_cb (EditWindow     *self,
                          IpuzStyleShape  shapebg)
{
  IpuzCell *cell;
  IpuzStyle *style;

  VALIDATE_EDIT_STATE (self->edit_state);

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  self->edit_state->style_state =
    grid_state_replace (self->edit_state->style_state,
                        grid_state_ensure_lone_style (self->edit_state->style_state,
                                                      self->edit_state->style_state->cursor));

  cell = ipuz_crossword_get_cell (self->edit_state->style_state->xword,
                                  &self->edit_state->style_state->cursor);
  g_assert (cell);
  style = ipuz_cell_get_style (cell);
  ipuz_style_set_shapebg (style, shapebg);

  ipuz_crossword_fix_styles (self->edit_state->style_state->xword);

  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (self->edit_state->style_state->xword));
}

static void
style_foreground_color_changed_cb (EditWindow *self,
                                   GdkRGBA    *color)
{
  IpuzCell *cell;
  IpuzStyle *style;
  g_autofree gchar *color_str = NULL;

  VALIDATE_EDIT_STATE (self->edit_state);

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  color_str = gdk_rgba_to_string (color);
  self->edit_state->style_state =
    grid_state_replace (self->edit_state->style_state,
                        grid_state_ensure_lone_style (self->edit_state->style_state,
                                                      self->edit_state->style_state->cursor));

  cell = ipuz_crossword_get_cell (self->edit_state->style_state->xword,
                                  &self->edit_state->style_state->cursor);
  g_assert (cell);
  style = ipuz_cell_get_style (cell);
  ipuz_style_set_text_color (style, color_str);

  ipuz_crossword_fix_styles (self->edit_state->style_state->xword);

  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (self->edit_state->style_state->xword));
}

static void
style_background_color_changed_cb (EditWindow *self,
                                   GdkRGBA    *color)
{
  IpuzCell *cell;
  IpuzStyle *style;
  g_autofree gchar *color_str = NULL;

  VALIDATE_EDIT_STATE (self->edit_state);

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  color_str = gdk_rgba_to_string (color);
  self->edit_state->style_state =
    grid_state_replace (self->edit_state->style_state,
                        grid_state_ensure_lone_style (self->edit_state->style_state,
                                                      self->edit_state->style_state->cursor));

  cell = ipuz_crossword_get_cell (self->edit_state->style_state->xword,
                                  &self->edit_state->style_state->cursor);
  g_assert (cell);
  style = ipuz_cell_get_style (cell);
  ipuz_style_set_bg_color (style, color_str);

  ipuz_crossword_fix_styles (self->edit_state->style_state->xword);

  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_GRID,
                            IPUZ_PUZZLE (self->edit_state->style_state->xword));
}

static void
edit_metadata_metadata_changed_cb (EditWindow *self,
                                   gchar      *property,
                                   gchar      *value)
{
  IpuzPuzzle *puzzle;

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);
  g_object_set (puzzle,
                property, value,
                NULL);
  puzzle_stack_push_change (self->puzzle_stack,
                            STACK_CHANGE_METADATA,
                            puzzle);
}

static void
edit_metadata_showenumerations_changed_cb (EditWindow *self,
                                           gboolean    showenumerations)
{
  IpuzPuzzle *puzzle;
  gboolean old_showenumerations = FALSE;

  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);
  g_object_get (puzzle,
                "showenumerations", &showenumerations,
                NULL);

  if (old_showenumerations ^ showenumerations)
    {
      edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                                self->puzzle_stack,
                                TRUE);
      g_object_set (puzzle,
                    "showenumerations", showenumerations,
                    NULL);
      ipuz_crossword_fix_enumerations (IPUZ_CROSSWORD (puzzle));
      /* We need force the grid to redraw itself, as enumerations show
       * up visibly there. */
      puzzle_stack_push_change (self->puzzle_stack,
                                STACK_CHANGE_GRID,
                                puzzle);
    }
}

/* Actions */
/* Some actions are better done from this file rather than the ones in
   edit-window-controls.c. They can call the update functions from
   here. */
void
_win_grid_actions_commit_pencil (EditWindow  *self,
                                 const gchar *action_name,
                                 GVariant    *param)
{
  gboolean state_changed = FALSE;

  g_assert (EDIT_IS_WINDOW (self));

  edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state,
                            self->puzzle_stack,
                            TRUE);

  self->edit_state->grid_state =
    grid_state_replace (self->edit_state->grid_state,
                        grid_state_commit_pencil (self->edit_state->grid_state,
                                                  &state_changed));

  if (state_changed)
    puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_GRID,
                              IPUZ_PUZZLE (self->edit_state->grid_state->xword));
  else
    edit_state_clear_current_stack (GTK_WIDGET (self), self->puzzle_stack, TRUE);
}

void
_edit_window_puzzle_changed_cb (EditWindow            *self,
                                PuzzleStackOperation   operation,
                                PuzzleStackChangeType  change_type,
                                PuzzleStack           *puzzle_stack)
{
  IpuzPuzzle *puzzle;

  puzzle = puzzle_stack_get_puzzle (puzzle_stack);

  if (puzzle == NULL)
    return;

  switch (operation)
    {
    case PUZZLE_STACK_OPERATION_NEW_FRAME:
      if (change_type == STACK_CHANGE_PUZZLE)
        pushed_puzzle_new (self);
      else if (change_type == STACK_CHANGE_GRID)
        pushed_grid_changed (self);
      else if (change_type == STACK_CHANGE_STATE)
        pushed_state_changed (self);
      else if (change_type == STACK_CHANGE_METADATA)
        pushed_metadata_changed (self);
      edit_state_save_to_stack (GTK_WIDGET (self), self->edit_state, self->puzzle_stack, FALSE);
      break;
    case PUZZLE_STACK_OPERATION_UNDO:
      edit_state_restore_from_stack (self->edit_state, self->puzzle_stack, TRUE);
      update_all (self);
      break;
    case PUZZLE_STACK_OPERATION_REDO:
      edit_state_restore_from_stack (self->edit_state, self->puzzle_stack, FALSE);
      update_all (self);
      break;
    }
}

void
_edit_window_class_controls_init (GtkWidgetClass *widget_class)
{
  g_assert (EDIT_IS_WINDOW_CLASS (widget_class));

  gtk_widget_class_bind_template_callback (widget_class, main_grid_guess_cb);
  gtk_widget_class_bind_template_callback (widget_class, main_grid_guess_at_cell_cb);
  gtk_widget_class_bind_template_callback (widget_class, main_grid_do_command_cb);
  gtk_widget_class_bind_template_callback (widget_class, main_grid_cell_selected_cb);
  gtk_widget_class_bind_template_callback (widget_class, main_grid_select_drag_start_cb);
  gtk_widget_class_bind_template_callback (widget_class, main_grid_select_drag_update_cb);
  gtk_widget_class_bind_template_callback (widget_class, main_grid_select_drag_end_cb);
  gtk_widget_class_bind_template_callback (widget_class, main_grid_copy_cb);
  gtk_widget_class_bind_template_callback (widget_class, main_grid_paste_cb);
  gtk_widget_class_bind_template_callback (widget_class, word_list_widget_word_activated_cb);
  gtk_widget_class_bind_template_callback (widget_class, edit_autofill_details_grid_selected_cb);
  gtk_widget_class_bind_template_callback (widget_class, edit_autofill_details_start_solve_cb);
  gtk_widget_class_bind_template_callback (widget_class, edit_autofill_details_stop_solve_cb);
  gtk_widget_class_bind_template_callback (widget_class, edit_autofill_details_reset_cb);
  gtk_widget_class_bind_template_callback (widget_class, cell_widget_side_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, cell_widget_cell_type_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, cell_widget_initial_val_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, symmetry_widget_symmetry_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, clue_details_widget_clue_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, clue_details_widget_selection_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, clue_list_widget_clue_selected_cb);
  gtk_widget_class_bind_template_callback (widget_class, style_shapebg_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, style_foreground_color_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, style_background_color_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, edit_metadata_metadata_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, edit_metadata_showenumerations_changed_cb);
}

void
_edit_window_control_update_stage (EditWindow *self)
{
  GridStateBehavior behavior = GRID_STATE_EDIT_FLAGS;
  g_assert (EDIT_IS_WINDOW (self));

  g_object_set (self->main_grid,
                "stage", self->edit_state->stage,
                NULL);

  if (self->edit_state->grid_stage == EDIT_GRID_STAGE_AUTOFILL)
    behavior = GRID_STATE_SELECT_FLAGS;
  if (self->edit_state->grid_state->behavior != behavior)
    {
      self->edit_state->grid_state =
        grid_state_replace (self->edit_state->grid_state,
                            grid_state_change_behavior (self->edit_state->grid_state, behavior));
    }

  update_all (self);
}
