/* edit-greeter-acrostic.c
 *
 * Copyright 2024 Tanmay Patil <tanmaynpatil105@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include "basic-templates.h"
#include "crosswords-misc.h"
#include "edit-greeter-acrostic.h"
#include "edit-greeter-details.h"
#include "play-grid.h"
#include "edit-entry-row.h"
#include "acrostic-generator.h"


/* This is used to increment the "My Acrostic-%u" title string */
static guint acrostic_instance_count = 0;


enum
{
  PROP_0,
  PROP_VALID,
  PROP_SPACING,
  PROP_ORIENTATION,
  N_PROPS = PROP_ORIENTATION
};

enum
{
  ACROSTIC_VALID,
  N_SIGNALS
};

static guint obj_signals[N_SIGNALS] = { 0 };
static GParamSpec *obj_props[N_PROPS] = { NULL, };


static void        edit_greeter_acrostic_init          (EditGreeterAcrostic         *self);
static void        edit_greeter_details_interface_init (EditGreeterDetailsInterface *iface);
static void        edit_greeter_acrostic_class_init    (EditGreeterAcrosticClass    *klass);
static void        edit_greeter_acrostic_set_property  (GObject                     *object,
                                                        guint                        prop_id,
                                                        const GValue                *value,
                                                        GParamSpec                  *pspec);
static void        edit_greeter_acrostic_get_property  (GObject                     *object,
                                                        guint                        prop_id,
                                                        GValue                      *value,
                                                        GParamSpec                  *pspec);
static void        edit_greeter_acrostic_dispose       (GObject                     *object);
static IpuzPuzzle *edit_greeter_acrostic_get_puzzle    (EditGreeterDetails          *details);
static void        quote_row_applied_cb                (EditGreeterAcrostic         *self,
                                                        EditEntryRow                *entry_row);
static void        source_row_applied_cb               (EditGreeterAcrostic         *self,
                                                        EditEntryRow                *entry_row);



struct _EditGreeterAcrostic
{
  GtkWidget parent_instance;

  AcrosticGenerator *acrostic_generator;
  gboolean valid;

  GtkWidget *title_row;
  GtkWidget *quote_row;
  GtkWidget *source_row;

  GtkWidget *play_grid;
  IpuzPuzzle *puzzle;
};


G_DEFINE_TYPE_WITH_CODE (EditGreeterAcrostic, edit_greeter_acrostic, GTK_TYPE_WIDGET,
                         G_IMPLEMENT_INTERFACE (GTK_TYPE_ORIENTABLE, NULL)
                         G_IMPLEMENT_INTERFACE (EDIT_TYPE_GREETER_DETAILS, edit_greeter_details_interface_init));


static void
edit_greeter_acrostic_init (EditGreeterAcrostic *self)
{
  g_autofree gchar *title_text = NULL;

  gtk_widget_init_template (GTK_WIDGET (self));

  self->acrostic_generator = acrostic_generator_new ();
  self->puzzle = ipuz_acrostic_new ();

  if (acrostic_instance_count == 0)
    /* translator: this is the default title of crosswords */
    title_text = g_strdup (_("My Acrostic"));
  else
    title_text = g_strdup_printf (_("My Acrostic-%u"), acrostic_instance_count);

  gtk_editable_set_text (GTK_EDITABLE (self->title_row),
                         title_text);

  gtk_widget_set_visible (self->play_grid, false);
}

static void
edit_greeter_acrostic_class_init (EditGreeterAcrosticClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->set_property = edit_greeter_acrostic_set_property;
  object_class->get_property = edit_greeter_acrostic_get_property;
  object_class->dispose = edit_greeter_acrostic_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-greeter-acrostic.ui");

  gtk_widget_class_bind_template_child (widget_class, EditGreeterAcrostic, title_row);
  gtk_widget_class_bind_template_child (widget_class, EditGreeterAcrostic, quote_row);
  gtk_widget_class_bind_template_child (widget_class, EditGreeterAcrostic, source_row);
  gtk_widget_class_bind_template_child (widget_class, EditGreeterAcrostic, play_grid);

  gtk_widget_class_bind_template_callback (widget_class, quote_row_applied_cb);
  gtk_widget_class_bind_template_callback (widget_class, source_row_applied_cb);

  g_object_class_override_property (object_class,
                                    PROP_ORIENTATION,
                                    "orientation");

  obj_signals [ACROSTIC_VALID] =
    g_signal_new ("acrostic-valid",
                  EDIT_TYPE_GREETER_ACROSTIC,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  G_TYPE_BOOLEAN);

  obj_props[PROP_VALID] = g_param_spec_boolean ("valid", NULL, NULL,
                                                FALSE,
                                                G_PARAM_READWRITE|G_PARAM_EXPLICIT_NOTIFY);
  obj_props[PROP_SPACING] = g_param_spec_uint ("spacing", NULL, NULL,
                                               0, G_MAXUINT,
                                               0,
                                               G_PARAM_READWRITE|G_PARAM_EXPLICIT_NOTIFY);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BOX_LAYOUT);
  gtk_widget_class_set_accessible_role (widget_class, GTK_ACCESSIBLE_ROLE_GROUP);
}

static void
edit_greeter_details_interface_init (EditGreeterDetailsInterface *iface)
{
  iface->get_puzzle = edit_greeter_acrostic_get_puzzle;
}

static void
edit_greeter_acrostic_set_property (GObject      *object,
                                    guint         prop_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
  EditGreeterAcrostic *self;
  GtkBoxLayout *box_layout;
  guint spacing;
  gboolean valid;

  self = EDIT_GREETER_ACROSTIC (object);
  box_layout = GTK_BOX_LAYOUT (gtk_widget_get_layout_manager (GTK_WIDGET (object)));

  switch (prop_id)
    {
    case PROP_VALID:
      valid = !!g_value_get_boolean (value);
      if (self->valid != valid)
        {
          self->valid = valid;
          g_object_notify_by_pspec (object, pspec);
        }
      break;
    case PROP_ORIENTATION:
      {
        GtkOrientation orientation = g_value_get_enum (value);
        if (gtk_orientable_get_orientation (GTK_ORIENTABLE (box_layout)) != orientation)
          {
            gtk_orientable_set_orientation (GTK_ORIENTABLE (box_layout), orientation);
            g_object_notify_by_pspec (object, pspec);
          }
      }
      break;
    case PROP_SPACING:
      spacing = g_value_get_uint (value);
      if (spacing != gtk_box_layout_get_spacing (box_layout))
        {
          gtk_box_layout_set_spacing (box_layout, spacing);
          g_object_notify_by_pspec (object, pspec);
        }
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
edit_greeter_acrostic_get_property (GObject    *object,
                                    guint       prop_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
  EditGreeterAcrostic *self;
  GtkBoxLayout *box_layout;

  self = EDIT_GREETER_ACROSTIC (object);
  box_layout = GTK_BOX_LAYOUT (gtk_widget_get_layout_manager (GTK_WIDGET (object)));

  switch (prop_id)
    {
    case PROP_VALID:
      g_value_set_boolean (value, self->valid);
      break;
    case PROP_ORIENTATION:
      g_value_set_enum (value, gtk_orientable_get_orientation (GTK_ORIENTABLE (box_layout)));
      break;
    case PROP_SPACING:
      g_value_set_uint (value, gtk_box_layout_get_spacing (box_layout));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
edit_greeter_acrostic_dispose (GObject *object)
{
  EditGreeterAcrostic *self;
  GtkWidget *child;

  self = EDIT_GREETER_ACROSTIC (object);

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  g_clear_object (&self->puzzle);
  g_clear_object (&self->acrostic_generator);
  g_clear_object (&self->puzzle);

  G_OBJECT_CLASS (edit_greeter_acrostic_parent_class)->dispose (object);
}

static IpuzPuzzle *
edit_greeter_acrostic_get_puzzle (EditGreeterDetails *details)
{
  EditGreeterAcrostic *self;
  const gchar *title_text;

  self = EDIT_GREETER_ACROSTIC (details);

  acrostic_instance_count++;
  title_text = gtk_editable_get_text (GTK_EDITABLE (self->title_row));
  g_object_set (self->puzzle,
                "title", title_text,
                NULL);

  return g_object_ref (self->puzzle);
}

static void
update_example (EditGreeterAcrostic *self)
{
  gboolean show_example = FALSE;
  const gchar *quote_str;
  GridState *state = NULL;

  quote_str = gtk_editable_get_text (GTK_EDITABLE (self->quote_row));
  ipuz_acrostic_set_quote (IPUZ_ACROSTIC (self->puzzle), quote_str);
  if (quote_str)
    show_example = TRUE;

  ipuz_crossword_fix_all (IPUZ_CROSSWORD (self->puzzle),
                          "sync-direction", IPUZ_ACROSTIC_SYNC_QUOTE_STR_TO_GRID,
                          NULL);

  state = grid_state_new (IPUZ_CROSSWORD (self->puzzle), NULL, GRID_STATE_VIEW);
  play_grid_update_state (PLAY_GRID (self->play_grid), state, layout_config_default (IPUZ_PUZZLE_ACROSTIC));
  grid_state_free (state);

  gtk_widget_set_visible (self->play_grid, show_example);
}

static void
update_warnings (EditGreeterAcrostic *self)
{
  g_autoptr (GError) error = NULL;
  gboolean valid = FALSE;
  const gchar *quote_str, *source_str;

  quote_str = gtk_editable_get_text (GTK_EDITABLE (self->quote_row));
  source_str = gtk_editable_get_text (GTK_EDITABLE (self->source_row));

  if (acrostic_generator_set_text (self->acrostic_generator, quote_str, source_str, &error))
    valid = TRUE;
  else
    edit_entry_row_set_error (EDIT_ENTRY_ROW (self->source_row), error->message);

  edit_greeter_details_set_valid (EDIT_GREETER_DETAILS (self), valid);
}

static void
quote_row_applied_cb (EditGreeterAcrostic *self,
                      EditEntryRow        *entry_row)
{
  update_example (self);
  update_warnings (self);
}

static void
source_row_applied_cb (EditGreeterAcrostic *self,
                       EditEntryRow        *entry_row)
{
  update_warnings (self);
}
