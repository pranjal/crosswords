/* edit-shapebg-row.c
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include "edit-shapebg-row.h"


enum
{
  CHANGED,
  N_SIGNALS
};

static guint obj_signals[N_SIGNALS] = { 0 };


struct _EditShapebgRow
{
  AdwActionRow parent_instance;

  GtkWidget *shapebg_popover;
  GtkWidget *popover_grid;
  GtkWidget *clear_button;
  GArray *shapebg_buttons;
};


G_DEFINE_FINAL_TYPE (EditShapebgRow, edit_shapebg_row, ADW_TYPE_ACTION_ROW);


static void edit_shapebg_row_init       (EditShapebgRow      *self);
static void edit_shapebg_row_class_init (EditShapebgRowClass *klass);
static void edit_shapebg_row_activate   (AdwActionRow        *action_row);
static void notify_popover_visible_cb   (EditShapebgRow      *self);
static void update_toggles              (EditShapebgRow      *self,
                                         IpuzStyleShape       shapebg);


typedef struct _ShapebgMapping
{
  const gchar *shapebg_str;
  IpuzStyleShape shapebg;
  guint column;
  guint row;
} ShapebgMapping;

static ShapebgMapping shapebg_mapping[] = {
  { "circle", IPUZ_STYLE_SHAPE_CIRCLE, 0, 0 },
  { "star", IPUZ_STYLE_SHAPE_STAR, 1, 0 },
  { "square", IPUZ_STYLE_SHAPE_SQUARE, 2, 0 },
  { "rhombus", IPUZ_STYLE_SHAPE_RHOMBUS, 3, 0 },
  { "club", IPUZ_STYLE_SHAPE_CLUB, 0, 1 },
  { "diamond", IPUZ_STYLE_SHAPE_DIAMOND, 1, 1 },
  { "heart", IPUZ_STYLE_SHAPE_HEART, 2, 1 },
  { "spade", IPUZ_STYLE_SHAPE_SPADE, 3, 1 },
  { "arrow-left", IPUZ_STYLE_SHAPE_ARROW_LEFT, 0, 2 },
  { "arrow-right", IPUZ_STYLE_SHAPE_ARROW_RIGHT, 1, 2 },
  { "arrow-up", IPUZ_STYLE_SHAPE_ARROW_UP, 2, 2 },
  { "arrow-down", IPUZ_STYLE_SHAPE_ARROW_DOWN, 3, 2 },
  { "triangle-left", IPUZ_STYLE_SHAPE_TRIANGLE_LEFT, 0, 3 },
  { "triangle-right", IPUZ_STYLE_SHAPE_TRIANGLE_RIGHT, 1, 3 },
  { "triangle-up", IPUZ_STYLE_SHAPE_TRIANGLE_UP, 2, 3 },
  { "triangle-down", IPUZ_STYLE_SHAPE_TRIANGLE_DOWN, 3, 3 },
  { "slash", IPUZ_STYLE_SHAPE_SLASH, 0, 4 },
  { "backslash", IPUZ_STYLE_SHAPE_BACKSLASH, 1, 4 },
  { "x", IPUZ_STYLE_SHAPE_X, 2, 4 },
  { NULL, IPUZ_STYLE_SHAPE_NONE, 0, 0 }
};


static void
shapebg_button_clicked (GtkWidget      *button,
                        EditShapebgRow *self)
{
  const gchar *shapebg;

  shapebg = g_object_get_data (G_OBJECT (button), "shapebg");
  gtk_popover_popdown (GTK_POPOVER (self->shapebg_popover));
  g_signal_emit (self, obj_signals[CHANGED], 0, shapebg);
}

static GtkToggleButton *
set_up_shapebg_button (EditShapebgRow  *self,
                       GtkToggleButton *group,
                       ShapebgMapping   map)
{
  GtkWidget *button;
  GtkWidget *image;
  g_autofree const gchar *resource_path = NULL;

  resource_path = g_strdup_printf ("/org/gnome/Crosswords/icons/shapebg/%s.svg",
                                   map.shapebg_str);
  button = gtk_toggle_button_new ();
  gtk_toggle_button_set_group (GTK_TOGGLE_BUTTON (button), group);
  g_object_set_data (G_OBJECT (button), "shapebg", GINT_TO_POINTER (map.shapebg));
  image = gtk_image_new_from_resource (resource_path);
  gtk_button_set_child (GTK_BUTTON (button), image);
  gtk_grid_attach (GTK_GRID (self->popover_grid),
                   button,
                   map.column, map.row,
                   1, 1);
  g_signal_connect (button, "clicked", G_CALLBACK (shapebg_button_clicked), self);

  return GTK_TOGGLE_BUTTON (button);
}

static void
edit_shapebg_row_init (EditShapebgRow *self)
{
  GtkWidget *child;
  GtkToggleButton *group = NULL;

  gtk_widget_init_template (GTK_WIDGET (self));

  for (guint i = 0; shapebg_mapping[i].shapebg_str ; i++)
    group = set_up_shapebg_button (self, GTK_TOGGLE_BUTTON (group), shapebg_mapping[i]);

  /* FIXME(hack): This is a pretty tough hack to get the title_box of
   * the ActionRow to expand horizontally. It's a private widget, as
   * is adw_action_row_set_expand_suffixes() which is how libadwaita
   * handles it for its widgets. */
  child = gtk_widget_get_first_child (GTK_WIDGET (self));
  if (!GTK_IS_BOX (child) ||
      g_strcmp0 ("header", gtk_buildable_get_buildable_id (GTK_BUILDABLE (child))))
    {
      g_warning ("AdwActionRow's internal structure has changed");
      return;
    }
  gtk_widget_set_hexpand (child, TRUE);

  g_object_set_data (G_OBJECT (self->clear_button),
                     "shapebg", GINT_TO_POINTER (IPUZ_STYLE_SHAPE_NONE));
  g_signal_connect (self->clear_button, "clicked", G_CALLBACK (shapebg_button_clicked), self);
}
                                         
static void
edit_shapebg_row_class_init (EditShapebgRowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  AdwActionRowClass *action_row_class = ADW_ACTION_ROW_CLASS (klass);

  action_row_class->activate = edit_shapebg_row_activate;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-shapebg-row.ui");

  gtk_widget_class_bind_template_child (widget_class, EditShapebgRow, shapebg_popover);
  gtk_widget_class_bind_template_child (widget_class, EditShapebgRow, popover_grid);
  gtk_widget_class_bind_template_child (widget_class, EditShapebgRow, clear_button);
  gtk_widget_class_bind_template_callback (widget_class, notify_popover_visible_cb);

  obj_signals[CHANGED] =
    g_signal_new ("changed",
                  EDIT_TYPE_SHAPEBG_ROW,
                  G_SIGNAL_RUN_FIRST,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  IPUZ_TYPE_STYLE_SHAPE);
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BOX_LAYOUT);

  gtk_widget_class_set_css_name (widget_class, "row");
}

static void
notify_popover_visible_cb (EditShapebgRow *self)
{
  /* this prevents clicks on the popover widget also appearing visually on the row */
  if (gtk_widget_get_visible (GTK_WIDGET (self->shapebg_popover)))
    gtk_widget_add_css_class (GTK_WIDGET (self), "has-open-popup");
  else
    gtk_widget_remove_css_class (GTK_WIDGET (self), "has-open-popup");
}

static void
edit_shapebg_row_activate (AdwActionRow *action_row)
{
  EditShapebgRow *self;

  self = EDIT_SHAPEBG_ROW (action_row);

  gtk_popover_popup (GTK_POPOVER (self->shapebg_popover));
}
                                
/* Public methods */

static void
update_toggles (EditShapebgRow *self,
                IpuzStyleShape  shapebg)
{
  for (guint i = 0; shapebg_mapping[i].shapebg_str ; i ++)
    {
      GtkWidget *button;

      button = gtk_grid_get_child_at (GTK_GRID (self->popover_grid),
                                      shapebg_mapping[i].column, shapebg_mapping[i].row);
      g_assert (button);
      g_signal_handlers_block_by_func (button, shapebg_button_clicked, self);

      if (shapebg_mapping[i].shapebg == shapebg)
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), TRUE);
      else
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), FALSE);

      g_signal_handlers_unblock_by_func (button, shapebg_button_clicked, self);
    }
}

void
edit_shapebg_row_update (EditShapebgRow *self,
                         GridState      *grid_state)
{
  IpuzCell *cell;
  IpuzStyle *style;
  IpuzStyleShape shapebg = IPUZ_STYLE_SHAPE_NONE;

  g_assert (grid_state->xword != NULL);

  cell = ipuz_crossword_get_cell (grid_state->xword, &grid_state->cursor);
  g_assert (cell);

  style = ipuz_cell_get_style (cell);
  if (style)
    shapebg = ipuz_style_get_shapebg (style);
  update_toggles (self, shapebg);
  adw_preferences_row_set_title (ADW_PREFERENCES_ROW (self),
                                 ipuz_style_shape_get_display_name (shapebg));
  gtk_widget_set_sensitive (GTK_WIDGET (self), ! IPUZ_CELL_IS_NULL (cell));
}
