/* word-list-model.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <json-glib/json-glib.h>

#include "word-list.h"
#include "word-list-model.h"

struct _WordListModel
{
  GObject parent_object;
  WordList *word_list;
  GArray *obj_list;
};


static void     word_list_model_init            (WordListModel       *self);
static void     word_list_model_class_init      (WordListModelClass  *klass);
static void     word_list_model_list_model_init (GListModelInterface *iface);
static void     word_list_model_dispose         (GObject             *object);
static void     word_list_model_recalculate     (WordListModel       *self);
static GType    word_list_model_get_item_type   (GListModel          *list);
static guint    word_list_model_get_n_items     (GListModel          *list);
static gpointer word_list_model_get_item        (GListModel          *list,
                                                 guint                position);

static void     word_list_model_row_init       (WordListModelRow       *self);
static void     word_list_model_row_class_init (WordListModelRowClass  *klass);


G_DEFINE_TYPE_WITH_CODE (WordListModel, word_list_model, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL,
                                                word_list_model_list_model_init));

G_DEFINE_TYPE (WordListModelRow, word_list_model_row, G_TYPE_OBJECT);



static void
obj_list_clear (WordListModelRow **data)
{
  if (*data != NULL)
    {
      g_object_unref (G_OBJECT (*data));
      *data = NULL;
    }
}

static void
word_list_model_init (WordListModel *self)
{
  self->word_list = word_list_new ();
  g_signal_connect_swapped (self->word_list, "resource-changed",
                            G_CALLBACK (word_list_model_recalculate),
                            self);
  self->obj_list = g_array_new (FALSE, TRUE, sizeof (WordListModelRow *));
  g_array_set_clear_func (self->obj_list, (GDestroyNotify ) obj_list_clear);
}

static void
word_list_model_class_init (WordListModelClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = word_list_model_dispose;
}

static void
word_list_model_list_model_init (GListModelInterface *iface)
{
  iface->get_item_type = word_list_model_get_item_type;
  iface->get_n_items = word_list_model_get_n_items;
  iface->get_item = word_list_model_get_item;
}

static void
word_list_model_dispose (GObject *object)
{
  WordListModel *self;

  self = WORD_LIST_MODEL (object);

  g_clear_object (&self->word_list);
  g_clear_pointer (&self->obj_list, g_array_unref);

  G_OBJECT_CLASS (word_list_model_parent_class)->dispose (object);
}

static void
word_list_model_recalculate (WordListModel *self)
{
  guint old_len, new_len;

  g_return_if_fail (WORD_IS_LIST_MODEL (self));

  old_len = self->obj_list->len;
  new_len = word_list_get_n_items (self->word_list);

  g_array_set_size (self->obj_list, new_len);

  if (old_len > new_len)
    g_list_model_items_changed (G_LIST_MODEL (self),
                                new_len, old_len - new_len,
                                0);
  else if (new_len > old_len)
    g_list_model_items_changed (G_LIST_MODEL (self),
                                old_len, 0,
                                new_len - old_len);

  for (guint i = 0; i < new_len; i++)
    {
      WordListModelRow *row;
      const gchar *word;
      gint priority;
      const gchar *enumeration_src;

      g_assert (i < self->obj_list->len);
      row = g_array_index (self->obj_list, WordListModelRow *, i);
      if (row == NULL)
        break;

      word = word_list_get_word (self->word_list, i);
      priority = word_list_get_priority (self->word_list, i);
      enumeration_src = word_list_get_enumeration_src (self->word_list, i);

      word_list_model_row_changed (row, word, priority, enumeration_src);
    }
}


static GType
word_list_model_get_item_type (GListModel *model)
{
  return WORD_TYPE_LIST_MODEL_ROW;
}

static guint
word_list_model_get_n_items (GListModel *model)
{
  WordListModel *list_model;

  list_model = WORD_LIST_MODEL (model);

  return list_model->obj_list->len;
}

static gpointer
word_list_model_get_item (GListModel *model,
                          guint       position)
{
  WordListModel *list_model;
  WordListModelRow *row;
  const gchar *word;
  gint priority;
  const gchar *enumeration_src;

  list_model = WORD_LIST_MODEL (model);

  row  = g_array_index (list_model->obj_list, WordListModelRow *, position);
  word = word_list_get_word (list_model->word_list, position);
  priority = word_list_get_priority (list_model->word_list, position);
  enumeration_src = word_list_get_enumeration_src (list_model->word_list, position);

  if (row == NULL)
    {
      WordListModelRow **row_ptr = &(g_array_index (list_model->obj_list, WordListModelRow *, position));
      row = word_list_model_row_new (word, priority, enumeration_src);
      *row_ptr = row;
    }
  else
    {
      row->word = word;
      row->priority = priority;
      row->enumeration_src = enumeration_src;
    }

  return g_object_ref (row);
}


WordListModel *
word_list_model_new (void)
{
  return (WordListModel *) g_object_new (WORD_TYPE_LIST_MODEL, NULL);
}

void
word_list_model_set_filter (WordListModel *self,
                            const char    *filter,
                            WordListMode   mode)
{
  g_return_if_fail (WORD_IS_LIST_MODEL (self));

  /* Short-circuit if we set the same filter twice */
  if (! g_strcmp0 (filter, word_list_get_filter (self->word_list)))
    return;

  word_list_set_filter (self->word_list, filter, mode);
  word_list_model_recalculate (self);
}

void
word_list_model_set_threshold (WordListModel *self,
                               gint           threshold)
{

}


/* RowModel */

enum
{
  CHANGED,
  LAST_SIGNAL
};

static guint word_list_model_row_signals[LAST_SIGNAL] = { 0 };

static void
word_list_model_row_init (WordListModelRow *self)
{

}

static void
word_list_model_row_class_init (WordListModelRowClass *klass)
{
  word_list_model_row_signals [CHANGED] =
    g_signal_new ("changed",
                  WORD_TYPE_LIST_MODEL_ROW,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);
}

void
word_list_model_row_changed (WordListModelRow *self,
                             const gchar      *word,
                             gint              priority,
                             const gchar      *enumeration_src)
{
  g_return_if_fail (WORD_IS_LIST_MODEL_ROW (self));

  self->word = word;
  self->priority = priority;
  self->enumeration_src = enumeration_src;

  g_signal_emit (self, word_list_model_row_signals [CHANGED], 0);
}

const gchar *
word_list_model_row_get_word (WordListModelRow *self)
{
  return self->word;
}

gint
word_list_model_row_get_priority (WordListModelRow *self)
{
  return self->priority;
}

const gchar *
word_list_model_row_get_enumeration_src (WordListModelRow *self)
{
  return self->enumeration_src;
}

WordListModelRow *
word_list_model_row_new (const gchar *word,
                         gint         priority,
                         const gchar *enumeration_src)
{
  WordListModelRow *row;

  row = g_object_new (WORD_TYPE_LIST_MODEL_ROW, NULL);
  row->word = word;
  row->priority = priority;
  row->enumeration_src = enumeration_src;

  return row;
}
