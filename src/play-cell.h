/* play-cell.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>
#include "grid-layout.h"

G_BEGIN_DECLS

#define PLAY_TYPE_CELL (play_cell_get_type())

G_DECLARE_FINAL_TYPE (PlayCell, play_cell, PLAY, CELL, GtkWidget);


GtkWidget *play_cell_new                   (IpuzCellCoord        coord,
                                            LayoutConfig         config);
void       play_cell_select                (PlayCell            *cell);
void       play_cell_update                (PlayCell            *cell,
                                            LayoutCell          *layout);
void       play_cell_load_clue_block_state (PlayCell            *cell,
                                            IpuzCell            *puz_cell,
                                            LayoutClueBlockCell *layout);
void       play_cell_update_config         (PlayCell            *cell,
                                            LayoutConfig         config);

/* Helper functions */
guint      get_play_cell_size              (guint                base_size);


G_END_DECLS
