/* edit-clue-details.c
 *
 * Copyright 2021 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include <adwaita.h>
#include "clue-grid.h"
#include "crosswords-misc.h"
#include "edit-clue-details.h"
#include "edit-entry-row.h"
#include "edit-grid.h"
#include "play-grid.h"


enum
{
  CLUE_CHANGED,
  SELECTION_CHANGED,
  N_SIGNALS
};

static guint obj_signals[N_SIGNALS] = { 0 };


struct _EditClueDetails
{
  GtkWidget parent_instance;

  /* cached values from the current shown clue */
  gchar *clue_text;
  IpuzEnumeration *enumeration;
  guint clue_len;

  /* Template widgets */
  GtkWidget *clue_row;
  GtkWidget *enumeration_row;
  GtkWidget *answer_grid;
};


static void     edit_clue_details_init           (EditClueDetails      *self);
static void     edit_clue_details_class_init     (EditClueDetailsClass *klass);
static void     edit_clue_details_dispose        (GObject              *object);
static gboolean clue_row_committed_cb            (EditClueDetails      *self);
static gboolean enumeration_row_committed_cb     (EditClueDetails      *self);
static void     answer_grid_selection_changed_cb (EditClueDetails      *self,
                                                  const gchar          *selection_text);


G_DEFINE_TYPE (EditClueDetails, edit_clue_details, GTK_TYPE_WIDGET);


static void
edit_clue_details_init (EditClueDetails *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  /* This keeps us at the maximum height for a base_size of 20 */
  gtk_widget_set_size_request (self->answer_grid, -1, 66);

}

static void
edit_clue_details_class_init (EditClueDetailsClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = edit_clue_details_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-clue-details.ui");

  gtk_widget_class_bind_template_child (widget_class, EditClueDetails, answer_grid);
  gtk_widget_class_bind_template_child (widget_class, EditClueDetails, clue_row);
  gtk_widget_class_bind_template_child (widget_class, EditClueDetails, enumeration_row);
  gtk_widget_class_bind_template_callback (widget_class, clue_row_committed_cb);
  gtk_widget_class_bind_template_callback (widget_class, enumeration_row_committed_cb);
  gtk_widget_class_bind_template_callback (widget_class, answer_grid_selection_changed_cb);

  obj_signals[CLUE_CHANGED] =
    g_signal_new ("clue-changed",
                  EDIT_TYPE_CLUE_DETAILS,
                  G_SIGNAL_RUN_FIRST,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 2,
                  G_TYPE_STRING,
                  IPUZ_TYPE_ENUMERATION);

    obj_signals[SELECTION_CHANGED] =
    g_signal_new ("selection-changed",
                  EDIT_TYPE_CLUE_DETAILS,
                  G_SIGNAL_RUN_FIRST,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  G_TYPE_STRING);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
}

static void
edit_clue_details_dispose (GObject *object)
{
  EditClueDetails *self;
  GtkWidget *child;

  self = EDIT_CLUE_DETAILS (object);

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  g_clear_pointer (&self->clue_text, g_free);
  g_clear_pointer (&self->enumeration, ipuz_enumeration_unref);

  G_OBJECT_CLASS (edit_clue_details_parent_class)->dispose (object);
}

static gboolean
clue_row_committed_cb (EditClueDetails *self)
{
  const gchar *new_clue_text = NULL;

  /* get the texts */
  new_clue_text = gtk_editable_get_text (GTK_EDITABLE (self->clue_row));

  /* We only push if things have really changed */
  if (g_strcmp0 (self->clue_text, new_clue_text) != 0)
    {
      /* FIXME(text): We aren't really handling entities
       * well. IpuzClue::clue_text is defined to be valid */
      g_signal_emit (self, obj_signals[CLUE_CHANGED], 0, new_clue_text, self->enumeration);
      return TRUE;
    }

  return FALSE;
}

static gboolean
enumeration_row_committed_cb (EditClueDetails *self)
{
  g_autoptr (IpuzEnumeration) new_enumeration = NULL;
  const gchar *new_enumeration_text = NULL;

  new_enumeration_text = gtk_editable_get_text (GTK_EDITABLE (self->enumeration_row));
  if (new_enumeration_text != NULL && new_enumeration_text[0])
    {
      new_enumeration = ipuz_enumeration_new (new_enumeration_text, IPUZ_VERBOSITY_STANDARD);
    }
  /* Did the enumeration change. This should only trigger if something
   * weird happened*/
  if (ipuz_enumeration_equal (self->enumeration, new_enumeration))
    {
      return FALSE;
    }
  if (! ipuz_enumeration_get_has_delim (new_enumeration))
    {
      edit_entry_row_set_error (EDIT_ENTRY_ROW (self->enumeration_row),
                                _("_Enumeration — Invalid enumeration"));
      return FALSE;
    }

  g_signal_emit (self, obj_signals[CLUE_CHANGED], 0, self->clue_text, new_enumeration);
  return TRUE;
}

static void
answer_grid_selection_changed_cb (EditClueDetails *self,
                                  const gchar     *selection_text)
{
  g_signal_emit (self, obj_signals[SELECTION_CHANGED], 0, selection_text);
}


/* one weirdness is we want people to enter simple html and entities
 * as text — but the &apos; and &quot; entities are a bit too
 * far. PangoMarkup will happily consume text with stray quotes in it
 * without complaining, anyway.
 */
static gchar *
clean_markup_for_editing (const gchar *markup)
{
  GString *str;

  str = g_string_new (markup);

  g_string_replace (str, "&apos;", "'", 0);
  g_string_replace (str, "&quot;", "\"", 0);

  return g_string_free_and_steal (str);
}

void
edit_clue_details_update (EditClueDetails *self,
                          GridState       *clues_state,
                          LayoutConfig     layout_config)
{
  IpuzClue *clue = NULL;
  const gchar *enumeration_display = NULL;
  gboolean showenumerations = FALSE;
  GridState *answer_state = NULL;

  g_assert (EDIT_IS_CLUE_DETAILS (self));
  g_assert (clues_state->xword);

  clue = ipuz_crossword_get_clue_by_id (clues_state->xword, &clues_state->clue);

  /* This can happen in weird situations, like a puzzle with no space
     for clues.
   */
  if (clue == NULL)
    {
      edit_entry_row_set_text (EDIT_ENTRY_ROW (self->clue_row), "");
      edit_entry_row_set_text (EDIT_ENTRY_ROW (self->enumeration_row), "");
      gtk_widget_set_sensitive (GTK_WIDGET (self), FALSE);
      return;
    }
  else
    {
      gtk_widget_set_sensitive (GTK_WIDGET (self), TRUE);
    }

  /* clue text */
  g_clear_pointer (&self->clue_text, g_free);
  self->clue_text = clean_markup_for_editing (ipuz_clue_get_clue_text (clue));
  edit_entry_row_set_text (EDIT_ENTRY_ROW (self->clue_row), self->clue_text);

  /* enumeration */
  g_clear_pointer (&self->enumeration, ipuz_enumeration_unref);

  g_object_get (clues_state->xword,
                "showenumerations", &showenumerations,
                NULL);

  gtk_widget_set_visible (self->enumeration_row, showenumerations);
  if (showenumerations)
    {
      const GArray *cells;

      self->enumeration = ipuz_clue_get_enumeration (clue);
      cells = ipuz_clue_get_cells (clue);
      g_assert (cells);
      g_assert (self->enumeration);

      enumeration_display = ipuz_enumeration_get_display (self->enumeration);
      self->clue_len = ipuz_enumeration_delim_length (self->enumeration);

      edit_entry_row_set_text (EDIT_ENTRY_ROW (self->enumeration_row), enumeration_display);
      if (ipuz_enumeration_delim_length (self->enumeration) != (gint) cells->len)
        edit_entry_row_set_warning (EDIT_ENTRY_ROW (self->enumeration_row),
                                    _("_Enumeration — Warning: enumeration is a different length"));
    }


  /* answer grid */
  answer_state = grid_state_replace (NULL, grid_state_change_mode (clues_state, GRID_STATE_SELECT));
  clue_grid_update_state (CLUE_GRID (self->answer_grid),
                          answer_state,
                          clue,
                          layout_config);
  grid_state_free (answer_state);
}

/* Public functions */

/* If we have a clue half-written, commit it to the stack. This is
 * called during stage changes and when saving to disk.
 *
 * One weirdness is that if there are multiple changes, then they get
 * pushed to the stack serially, which may be unexpected for the user.
 */
void
edit_clue_details_commit_changes (EditClueDetails *self)
{
  g_return_if_fail (EDIT_IS_CLUE_DETAILS (self));

  edit_entry_row_commit (EDIT_ENTRY_ROW (self->clue_row));
  edit_entry_row_commit (EDIT_ENTRY_ROW (self->enumeration_row));
}

void
edit_clue_details_selection_invert (EditClueDetails *self)
{
  clue_grid_selection_invert (CLUE_GRID (self->answer_grid));
}

void
edit_clue_details_select_all (EditClueDetails *self)
{
  clue_grid_select_all (CLUE_GRID (self->answer_grid));
}
