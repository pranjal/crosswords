/* edit-bars.c
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <libipuz/libipuz.h>
#include <glib/gi18n-lib.h>
#include "edit-bars.h"
#include "edit-grid.h"
#include "edit-symmetry.h"
#include "grid-state.h"


enum
{
  SIDE_CHANGED,
  N_SIGNALS
};

static guint obj_signals[N_SIGNALS] = { 0 };


struct _EditBars
{
  GtkWidget parent_instance;

  GtkWidget *number_label;
  GtkWidget *top_toggle;
  GtkWidget *bottom_toggle;
  GtkWidget *left_toggle;
  GtkWidget *right_toggle;
};


G_DEFINE_FINAL_TYPE (EditBars, edit_bars, GTK_TYPE_WIDGET);


static void edit_bars_init         (EditBars              *self);
static void edit_bars_class_init   (EditBarsClass         *klass);
static void barred_toggled_cb      (EditBars              *self,
                                    GtkWidget             *toggle);


static void
edit_bars_class_init (EditBarsClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-bars.ui");

  gtk_widget_class_bind_template_child (widget_class, EditBars, number_label);
  gtk_widget_class_bind_template_child (widget_class, EditBars, top_toggle);
  gtk_widget_class_bind_template_child (widget_class, EditBars, bottom_toggle);
  gtk_widget_class_bind_template_child (widget_class, EditBars, left_toggle);
  gtk_widget_class_bind_template_child (widget_class, EditBars, right_toggle);

  gtk_widget_class_bind_template_callback (widget_class, barred_toggled_cb);

  /**
   * EditBars::side-changed:
   * @gobject: object which emitted the signal
   * @side: `IpuzStyleSides` with a single bit set, with the side that changed.
   *
   * This signal is emitted when the user changes one of the selected sides
   * by clicking on the respective toggle button.  Note that @side has only
   * a single bit set from the sides available in `IpuzStyleSides`.  To query the whole
   * state of the `EditBars`, use edit_bars_get_sides().
   */
  obj_signals[SIDE_CHANGED] =
    g_signal_new ("side-changed",
                  EDIT_TYPE_BARS,
                  G_SIGNAL_RUN_FIRST,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  IPUZ_TYPE_STYLE_SIDES);   /* this is really IpuzStyleSides */

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BOX_LAYOUT);
}

static void
edit_bars_init (EditBars *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  gtk_button_set_child (GTK_BUTTON (self->top_toggle), NULL);
  gtk_button_set_child (GTK_BUTTON (self->bottom_toggle), NULL);
  gtk_button_set_child (GTK_BUTTON (self->left_toggle), NULL);
  gtk_button_set_child (GTK_BUTTON (self->right_toggle), NULL);
}

static void
barred_toggled_cb (EditBars  *self,
                   GtkWidget *toggle)
{
  IpuzStyleSides side;

  if (toggle == self->left_toggle)
    side = IPUZ_STYLE_SIDES_LEFT;
  else if (toggle == self->right_toggle)
    side = IPUZ_STYLE_SIDES_RIGHT;
  else if (toggle == self->top_toggle)
    side = IPUZ_STYLE_SIDES_TOP;
  else if (toggle == self->bottom_toggle)
    side = IPUZ_STYLE_SIDES_BOTTOM;
  else
    {
      g_assert_not_reached ();
      return;
    }

  g_signal_emit (self, obj_signals[SIDE_CHANGED], 0, side);
}

static void
set_toggle (EditBars  *self,
            GtkWidget *toggle,
            gboolean   is_sensitive,
            gboolean   is_active)
{
  gtk_widget_set_sensitive (toggle, is_sensitive);

  g_signal_handlers_block_by_func (toggle, barred_toggled_cb, self);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (toggle), is_active);
  g_signal_handlers_unblock_by_func (toggle, barred_toggled_cb, self);
}

/* Public methods */

void
edit_bars_update (EditBars  *self,
                  GridState *grid_state)
{
  IpuzCell *cell;
  IpuzStyleSides bars;
  g_autofree gchar *cluenum = NULL;
  guint width, height;

  g_assert (grid_state->xword != NULL);

  if (!IPUZ_IS_BARRED (grid_state->xword))
    return;

  /* Set the outer bars */
  width = ipuz_crossword_get_width (grid_state->xword);
  height = ipuz_crossword_get_height (grid_state->xword);
  bars = ipuz_barred_get_cell_bars (IPUZ_BARRED (grid_state->xword),
                                    &grid_state->cursor);

  set_toggle (self, self->top_toggle,
              grid_state->cursor.row > 0,
              IPUZ_STYLE_SIDES_HAS_TOP (bars));
  set_toggle (self, self->left_toggle,
              grid_state->cursor.column > 0,
              IPUZ_STYLE_SIDES_HAS_LEFT (bars));
  set_toggle (self, self->bottom_toggle,
              grid_state->cursor.row + 1 < height,
              IPUZ_STYLE_SIDES_HAS_BOTTOM (bars));
  set_toggle (self, self->right_toggle,
              grid_state->cursor.column + 1 < width,
              IPUZ_STYLE_SIDES_HAS_RIGHT (bars));

  /* Set the label */
  cell = ipuz_crossword_get_cell (grid_state->xword,
                                  &grid_state->cursor);
  if (ipuz_cell_get_number (cell))
    cluenum = g_strdup_printf ("%d", ipuz_cell_get_number (cell));
  else if (ipuz_cell_get_label (cell))
    cluenum = g_strdup (ipuz_cell_get_label (cell));

  gtk_label_set_label (GTK_LABEL (self->number_label), cluenum);
}
