/* edit-autofill-details.c
 *
 * Copyright 2024 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include <adwaita.h>
#include "crosswords-misc.h"
#include "edit-autofill-details.h"
#include "word-array-model.h"
#include "word-list.h"
#include "word-list-model.h"


#define SOLVER_MAX_COUNT 500


enum
{
  GRID_SELECTED,
  START_SOLVE,
  STOP_SOLVE,
  RESET,
  N_SIGNALS
};

static guint obj_signals[N_SIGNALS] = { 0 };


struct _EditAutofillDetails
{
  GtkWidget parent_instance;

  WordSolverState solver_state;
  gboolean has_solutions;

  WordList *word_list;
  WordArrayModel *skip_model;

  GtkWidget *solver_spinner;
  GtkWidget *solver_label;
  GtkWidget *solver_reset_spinner_stack;
  GtkWidget *solver_reset_button;
  GtkWidget *solver_play_stop_button;
  GtkWidget *solver_scale;
  GtkAdjustment *solver_adjustment;
  GtkWidget *commit_row;
  GtkWidget *max_count_spin_row;
  GtkWidget *skip_word_group;
  GtkWidget *skip_word_list_box;
  GtkWidget *skip_word_entry_row;
  GtkWidget *skip_word_add_button;
};


static void edit_autofill_details_init       (EditAutofillDetails      *self);
static void edit_autofill_details_class_init (EditAutofillDetailsClass *klass);
static void edit_autofill_details_dispose    (GObject                  *object);
static void solver_scale_value_changed_cb    (EditAutofillDetails      *self);
static void reset_button_clicked_cb          (EditAutofillDetails      *self);
static void play_stop_button_clicked_cb      (EditAutofillDetails      *self);
static void skip_entry_add_word_cb           (EditAutofillDetails      *self);
static void skip_entry_row_insert_text_cb    (GtkEditable              *editable,
                                              gchar                    *text,
                                              gint                      length,
                                              gint                     *position,
                                              EditAutofillDetails      *self);
static void skip_entry_row_changed_cb        (EditAutofillDetails      *self);
static void resource_changed_cb              (EditAutofillDetails      *self);


G_DEFINE_TYPE (EditAutofillDetails, edit_autofill_details, GTK_TYPE_WIDGET);


static void
remove_item (GtkWidget   *button,
             const gchar *text)
{
  EditAutofillDetails *self;
  WordIndex word_index;

  self = (EditAutofillDetails *) gtk_widget_get_ancestor (button, EDIT_TYPE_AUTOFILL_DETAILS);

  g_assert (EDIT_IS_AUTOFILL_DETAILS (self));

  if (word_list_lookup_index (self->word_list, text, &word_index))
    word_array_model_remove (self->skip_model, word_index);
}

static GtkWidget *
skip_list_create_widget_func (WordListModelRow    *item,
                              EditAutofillDetails *self)
{
  GtkWidget *retval;
  GtkWidget *label;
  GtkWidget *button;
  g_autofree gchar *text = NULL;

  text = g_strdup_printf ("%s — (%d)",
                          word_list_model_row_get_word (item),
                          word_list_model_row_get_priority (item));
  retval = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 8);
  gtk_widget_add_css_class (retval, "sidebar-listitem");
  label = gtk_label_new (text);
  gtk_widget_set_hexpand (label, TRUE);
  gtk_label_set_xalign (GTK_LABEL (label), 0.0);
  gtk_box_prepend (GTK_BOX (retval), label);

  button = gtk_button_new ();
  gtk_button_set_icon_name (GTK_BUTTON (button), "list-remove-symbolic");
  gtk_widget_set_valign (button, GTK_ALIGN_CENTER);
  gtk_box_append (GTK_BOX (retval), button);
  g_signal_connect (button,
                    "clicked", G_CALLBACK (remove_item),
                    (gpointer) word_list_model_row_get_word (item));

  return retval;
}

static void
edit_autofill_details_init (EditAutofillDetails *self)
{
  GtkEditable *delegate;
  gtk_widget_init_template (GTK_WIDGET (self));

  self->solver_state = WORD_SOLVER_READY;
  self->has_solutions = FALSE;
  self->word_list = word_list_new ();
  g_signal_connect_swapped (self->word_list,
                            "resource-changed",
                            G_CALLBACK (resource_changed_cb),
                            self);
  self->skip_model = word_array_model_new (self->word_list);
  gtk_list_box_bind_model (GTK_LIST_BOX (self->skip_word_list_box),
                           G_LIST_MODEL (self->skip_model),
                           (GtkListBoxCreateWidgetFunc) skip_list_create_widget_func,
                           self, NULL);
  /* For reasons I don't understand, I can't set this adjustment
   * values from the .ui file. We set them here instead */
  adw_spin_row_set_value (ADW_SPIN_ROW (self->max_count_spin_row),
                          (gfloat) SOLVER_MAX_COUNT);

  gtk_widget_add_css_class (GTK_WIDGET (self), "autofill");
  delegate = gtk_editable_get_delegate (GTK_EDITABLE (self->skip_word_entry_row));
  g_signal_connect (delegate,
                    "insert-text",
                    G_CALLBACK (skip_entry_row_insert_text_cb),
                    self);
}

static void
edit_autofill_details_class_init (EditAutofillDetailsClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = edit_autofill_details_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-autofill-details.ui");

  gtk_widget_class_bind_template_child (widget_class, EditAutofillDetails, solver_spinner);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDetails, solver_label);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDetails, solver_reset_spinner_stack);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDetails, solver_scale);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDetails, solver_adjustment);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDetails, solver_reset_button);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDetails, solver_play_stop_button);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDetails, commit_row);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDetails, max_count_spin_row);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDetails, skip_word_group);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDetails, skip_word_list_box);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDetails, skip_word_entry_row);
  gtk_widget_class_bind_template_child (widget_class, EditAutofillDetails, skip_word_add_button);

  gtk_widget_class_bind_template_callback (widget_class, solver_scale_value_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, reset_button_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, play_stop_button_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, skip_entry_add_word_cb);
  gtk_widget_class_bind_template_callback (widget_class, skip_entry_row_changed_cb);

  obj_signals [GRID_SELECTED] =
    g_signal_new ("grid-selected",
                  EDIT_TYPE_AUTOFILL_DETAILS,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  G_TYPE_UINT);

  obj_signals [START_SOLVE] =
    g_signal_new ("start-solve",
                  EDIT_TYPE_AUTOFILL_DETAILS,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 2,
                  WORD_TYPE_ARRAY_MODEL,
                  G_TYPE_INT);

  obj_signals [STOP_SOLVE] =
    g_signal_new ("stop-solve",
                  EDIT_TYPE_AUTOFILL_DETAILS,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);

  obj_signals [RESET] =
    g_signal_new ("reset",
                  EDIT_TYPE_AUTOFILL_DETAILS,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
}

static void
edit_autofill_details_dispose (GObject *object)
{
  GtkWidget *child;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  G_OBJECT_CLASS (edit_autofill_details_parent_class)->dispose (object);
}

static void
solver_scale_value_changed_cb (EditAutofillDetails *self)
{
  gfloat value;

  g_assert (EDIT_IS_AUTOFILL_DETAILS (self));

  value = gtk_range_get_value (GTK_RANGE (self->solver_scale));

  g_signal_emit (self, obj_signals [GRID_SELECTED], 0, ((guint) value) - 1);
}

static void
reset_button_clicked_cb (EditAutofillDetails *self)
{
  g_assert (EDIT_IS_AUTOFILL_DETAILS (self));

  g_signal_emit (self, obj_signals [RESET], 0);
}

static void
play_stop_button_clicked_cb (EditAutofillDetails *self)
{
  g_assert (EDIT_IS_AUTOFILL_DETAILS (self));

  if (self->solver_state == WORD_SOLVER_RUNNING)
    g_signal_emit (self, obj_signals [STOP_SOLVE], 0);
  else
    {
      double value = adw_spin_row_get_value (ADW_SPIN_ROW (self->max_count_spin_row));
      g_signal_emit (self, obj_signals [START_SOLVE], 0, self->skip_model, (gint) value);
    }
}

static void
skip_entry_add_word_cb (EditAutofillDetails *self)
{
  const gchar *text;
  WordIndex word_index;

  text = gtk_editable_get_text (GTK_EDITABLE (self->skip_word_entry_row));

  if (word_list_lookup_index (self->word_list, text, &word_index))
    {
      word_array_model_add (self->skip_model, word_index);
      gtk_editable_set_text (GTK_EDITABLE (self->skip_word_entry_row), "");
    }
}

static void
skip_entry_row_insert_text_cb (GtkEditable         *editable,
                               gchar               *text,
                               gint                 length,
                               gint                *position,
                               EditAutofillDetails *self)
{
  g_autofree gchar *upper = g_utf8_strup (text, length);
  IpuzCharset *charset;

  charset = word_list_get_charset (self->word_list);
  g_signal_handlers_block_by_func (editable,
                                   (gpointer) skip_entry_row_insert_text_cb, self);
  if (ipuz_charset_check_text (charset, upper))
    gtk_editable_insert_text (editable,
                              upper, length, position);

  g_signal_handlers_unblock_by_func (editable,
                                     (gpointer) skip_entry_row_insert_text_cb, self);

  g_signal_stop_emission_by_name (editable, "insert_text");
}

static void
skip_entry_row_changed_cb (EditAutofillDetails *self)
{
  const gchar *text;
  WordIndex word_index;
  gboolean sensitive = FALSE;

  text = gtk_editable_get_text (GTK_EDITABLE (self->skip_word_entry_row));

  /* Only make the add button sensitive if its a valid word, and we
   * haven't added it already */
  if (word_list_lookup_index (self->word_list, text, &word_index))
    {
      if (! word_array_model_find (self->skip_model,
                                   word_index, NULL))
        sensitive = TRUE;
    }

  gtk_widget_set_sensitive (self->skip_word_add_button, sensitive);
}

static void
resource_changed_cb (EditAutofillDetails *self)
{
  /* clear the old model... */
  word_array_model_set_array (self->skip_model, NULL);

  /*... abd update the row button sensitivity */
  skip_entry_row_changed_cb (self);
}

void
edit_autofill_details_update (EditAutofillDetails *self,
                              GridState           *grid_state,
                              WordSolver          *solver)
{
  gboolean solver_button_sensitive = FALSE;
  const gchar *play_stop_icon_name;
  const gchar *stack_visible_name = "reset";
  g_autofree gchar *solver_text = NULL;
  g_autofree gchar *spinner_tooltip_text = NULL;
  gboolean solver_draw_value = FALSE;
  gboolean reset_button_sensitive = FALSE;
  gboolean skip_word_group_sensitive = FALSE;
  gboolean run_spinner = FALSE;
  gboolean has_solutions = FALSE;
  gboolean has_origin = FALSE;
  /* Note. We want the scale to display counting numbers and not
   * indices (eg, 1..n instead of 0..n-1). As a result, everything in
   * the adjustment is +1 from what might be expected */
  gfloat upper = 2.0;
  gint count;
  guint n_solutions;

  g_assert (EDIT_IS_AUTOFILL_DETAILS (self));
  g_assert (grid_state);

  self->solver_state = word_solver_get_state (solver);
  count = word_solver_get_count (solver);
  n_solutions = word_solver_get_n_solutions (solver);

  switch (self->solver_state)
    {
    case WORD_SOLVER_READY:
      if (grid_state->selected_cells &&
          cell_array_len (grid_state->selected_cells) > 0)
        solver_button_sensitive = TRUE;

      solver_text = g_strdup (_("Select a region to autosolve"));
      play_stop_icon_name = "media-playback-start";
      skip_word_group_sensitive = TRUE;
      break;
    case WORD_SOLVER_RUNNING:
      solver_button_sensitive = TRUE;
      play_stop_icon_name = "media-playback-stop";
      stack_visible_name = "spinner";
      run_spinner = TRUE;
      spinner_tooltip_text = g_strdup_printf (_("%d grids tried"),
                                              count);
      if (n_solutions == 0)
        {
          solver_text = g_strdup (_("Searching for grids"));
          has_solutions = FALSE;
        }
      else
        {
          has_solutions = TRUE;
          solver_text = g_strdup_printf (_("%d possible grids"),
                                         n_solutions);
          solver_draw_value = TRUE;
          upper = n_solutions + 1;
          has_origin = TRUE;
        }

      break;
    case WORD_SOLVER_COMPLETE:

      if (grid_state->selected_cells &&
          cell_array_len (grid_state->selected_cells) > 0)
        solver_button_sensitive = TRUE;

      reset_button_sensitive = TRUE;
      play_stop_icon_name = "media-playback-start";
      skip_word_group_sensitive = TRUE;
      if (n_solutions == 0)
        {
          has_solutions = FALSE;
          solver_text = g_strdup (_("No grids found"));
          upper = n_solutions;
        }
      else
        {
          has_solutions = TRUE;
          solver_text = g_strdup_printf (_("%d possible grids"),
                                         n_solutions);
          solver_draw_value = TRUE;
          upper = n_solutions + 1;
          has_origin = TRUE;
        }
      break;
    default:
      g_assert_not_reached ();
    }

  /* Update all the widgets */
  gtk_adjustment_set_lower (self->solver_adjustment, 1.0);
  gtk_adjustment_set_upper (self->solver_adjustment, upper);
  /* If we previously were in a not-selected state, we force emit a
   * GRID_SELECTED signal. */
  if (has_solutions && !self->has_solutions)
    {
      gtk_adjustment_set_value (self->solver_adjustment, 1.0);
      g_signal_emit (self, obj_signals [GRID_SELECTED], 0, 0);
    }
  self->has_solutions = has_solutions;

  gtk_scale_set_draw_value (GTK_SCALE (self->solver_scale), solver_draw_value);
  gtk_scale_set_has_origin (GTK_SCALE (self->solver_scale), has_origin);
  gtk_spinner_set_spinning (GTK_SPINNER (self->solver_spinner), run_spinner);
  gtk_widget_set_tooltip_text (self->solver_spinner, spinner_tooltip_text);
  gtk_label_set_text (GTK_LABEL (self->solver_label), solver_text);
  gtk_stack_set_visible_child_name (GTK_STACK (self->solver_reset_spinner_stack),
                                    stack_visible_name);
  gtk_button_set_icon_name (GTK_BUTTON (self->solver_play_stop_button), play_stop_icon_name);
  gtk_widget_set_sensitive (self->solver_reset_button,
                            reset_button_sensitive);
  gtk_widget_set_sensitive (self->solver_scale,
                            has_solutions);
  gtk_widget_set_sensitive (self->solver_play_stop_button,
                            solver_button_sensitive);
  gtk_widget_set_sensitive (self->commit_row,
                            has_solutions);
  gtk_widget_set_sensitive (self->skip_word_group,
                            skip_word_group_sensitive);
}
