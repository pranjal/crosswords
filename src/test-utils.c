#include "test-utils.h"

GridState *
load_state (const char *filename, GridStateMode mode)
{
  char *path = g_test_build_filename (G_TEST_DIST, filename, NULL);
  GError *error = NULL;
  IpuzPuzzle *puzzle = ipuz_puzzle_new_from_file (path, &error);
  GridState *state;

  g_assert_no_error (error);
  g_assert (IPUZ_IS_CROSSWORD (puzzle));

  state = grid_state_new (IPUZ_CROSSWORD (puzzle), NULL, mode);
  g_assert (state != NULL);

  g_object_unref (puzzle);
  g_free (path);

  return state;
}

GridState *
load_state_with_quirks (const char *filename, GridStateMode mode)
{
  char *path = g_test_build_filename (G_TEST_DIST, filename, NULL);
  GError *error = NULL;
  IpuzPuzzle *puzzle = ipuz_puzzle_new_from_file (path, &error);
  GridState *state;

  g_assert_no_error (error);
  g_assert (IPUZ_IS_CROSSWORD (puzzle));

  state = grid_state_new (IPUZ_CROSSWORD (puzzle), NULL, mode);
  g_assert (state != NULL);

  state->quirks = crosswords_quirks_test_new (puzzle);
  /* Set some default values for the quirks to give us predictable results */
  crosswords_quirks_test_set_guess_advance (state->quirks, QUIRKS_GUESS_ADVANCE_ADJACENT);
  crosswords_quirks_test_set_switch_on_move (state->quirks, FALSE);
  g_object_unref (puzzle);
  g_free (path);

  return state;
}
