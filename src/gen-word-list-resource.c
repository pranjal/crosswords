/* gen-word-list-resource.c
 *
 * Copyright 2021 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "crosswords-config.h"

#include <stdlib.h>
#include <stdio.h>
#include <locale.h>
#include <errno.h>

#include <glib.h>
#include <glib/gi18n.h>
#include <glib/gstdio.h>
#include <gio/gio.h>
#include "gen-word-list.h"


static char *identifier = NULL;
static char *display_name = NULL;
static int min_length = 2;
static int max_length = 21;
static int threshold = 50;
static char *directory = NULL;
static char **files = NULL;
static char *importer_str = NULL;
static gboolean verbose = FALSE;

static GOptionEntry entries[] = {
  { "identifier", 'i', 0, G_OPTION_ARG_STRING, &identifier, N_("Identifier for the word list"), N_("STRING") },
  { "directory", 'd', 0, G_OPTION_ARG_FILENAME, &directory, N_("Directory to write dict and index"), N_("FILE") },
  { "display-name", 'n', 0, G_OPTION_ARG_STRING, &display_name, N_("Human readable description of the word list"), N_("STRING") },
  { "min-length", 'm', 0, G_OPTION_ARG_INT, &min_length, N_("Minimum Word Length"), N_("LENGTH") },
  { "max-length", 'x', 0, G_OPTION_ARG_INT, &max_length, N_("Maximum Word Length"), N_("LENGTH") },
  { "threshold", 't', 0, G_OPTION_ARG_INT, &threshold, N_("Quality Threshold"), N_("THRESHOLD") },
  { "import-format", 'f', 0, G_OPTION_ARG_STRING, &importer_str, N_("Import format"), N_("IMPORTER") },
  { "verbose", 'v', 0, G_OPTION_ARG_NONE, &verbose, N_("Verbose"), NULL },
  { G_OPTION_REMAINING, 0, 0, G_OPTION_ARG_FILENAME_ARRAY, &files, NULL, N_("WORDLIST") },
  { NULL, 0, 0, 0, NULL, NULL, NULL },
};

int
main (int argc, char *argv[])
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GFile) file = NULL;
  g_autoptr (GInputStream) stream = NULL;
  g_autofree gchar *output_dict_filename = NULL;
  g_autofree gchar *output_index_filename = NULL;
  GOptionContext *context;
  const char *summary;
  const char *description;
  static GenWordList *word_list = NULL;
  WordListImporter importer;

  setlocale (LC_ALL, "");

  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);


  summary = _("Generate files for the word-list.");
  description = _("WORDLIST is an input list of words");

  context = g_option_context_new (NULL);
  g_option_context_set_summary (context, summary);
  g_option_context_set_description (context, description);
  g_option_context_add_main_entries (context, entries, GETTEXT_PACKAGE);
  g_option_context_parse (context, &argc, &argv, &error);
  g_option_context_free (context);

  if (error != NULL)
    {
      g_printerr (_("Error parsing commandline options: %s\n"), error->message);
      g_printerr ("\n");
      g_printerr (_("Try “%s --help” for more information."), g_get_prgname ());
      g_printerr ("\n");
      return 1;
    }

  if (identifier == NULL)
    {
      g_printerr (_("%s: missing identifier"), g_get_prgname ());
      g_printerr ("\n");
      g_printerr (_("Try “%s --help” for more information."), g_get_prgname ());
      g_printerr ("\n");
      return 1;
    }

  if (display_name == NULL)
    {
      g_printerr (_("%s: missing display name"), g_get_prgname ());
      g_printerr ("\n");
      g_printerr (_("Try “%s --help” for more information."), g_get_prgname ());
      g_printerr ("\n");
      return 1;
    }

  if (directory == NULL)
    {
      g_printerr (_("%s: missing directory"), g_get_prgname ());
      g_printerr ("\n");
      g_printerr (_("Try “%s --help” for more information."), g_get_prgname ());
      g_printerr ("\n");

      return 1;
    }

  output_dict_filename = g_strdup_printf ("%s/%s.dict", directory, identifier);
  output_index_filename = g_strdup_printf ("%s/%s.json", directory, identifier);

  if (files == NULL || files[1] != NULL)
    {
      /* Translators: the %s is the program name. This error message
       * means the user is calling gen-word-list-resource without any
       * arguments.
       */
      g_printerr (_("%s: missing files"), g_get_prgname ());
      g_printerr ("\n");
      g_printerr (_("Try “%s --help” for more information."), g_get_prgname ());
      g_printerr ("\n");
      return 1;
    }

  if (importer_str == NULL ||
      g_strcmp0 (importer_str, "BRODA-SCORED") == 0)
    importer = BRODA_SCORED;
  else if (g_strcmp0 (importer_str, "BRODA-FULL") == 0)
    importer = BRODA_FULL;
  else
    {
      g_printerr (_("importer must be one of BRODA-SCORED or BRODA-FULL\n"));
      return 1;
    }

  word_list = gen_word_list_new (min_length, max_length, threshold, importer);

  file = g_file_new_for_commandline_arg (files[0]);
  stream = G_INPUT_STREAM (g_file_read (file, NULL, &error));
  if (stream == NULL)
    {
      /* Translators: the first %s is the program name, the second one
       * is the URI of the file, the third is the error message.
       */
      g_printerr (_("%s: %s: error opening file: %s\n"),
                  g_get_prgname (), g_file_get_uri (file), error->message);
      return 1;
    }

  if (! gen_word_list_parse (word_list, stream))
    return 1;

  gen_word_list_build_enumerations (word_list);
  gen_word_list_remove_duplicates (word_list);
  gen_word_list_sort (word_list);
  gen_word_list_build_charset (word_list);
  gen_word_list_anagram_table (word_list);
  gen_word_list_calculate_offsets (word_list);
  create_anagram_fragments (word_list);
  gen_word_list_write (word_list, display_name, output_dict_filename);
  gen_word_list_write_index (word_list, display_name, output_index_filename);
  if (verbose)
    gen_word_list_print_stats (word_list);
  // gen_word_list_anagram_dump (word_list);

  return 0;
}
