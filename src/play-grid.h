/* play-grid.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <gtk/gtk.h>
#include "grid-layout.h"

G_BEGIN_DECLS


typedef enum
{
  PLAY_GRID_FIXED,
  PLAY_GRID_SHRINKABLE,
} PlayGridSizeMode;


#define PLAY_TYPE_GRID (play_grid_get_type())
G_DECLARE_DERIVABLE_TYPE (PlayGrid, play_grid, PLAY, GRID, GtkWidget);


typedef struct _PlayGridClass
{
  GtkWidgetClass parent_class;
} PlayGridClass;


GtkWidget *play_grid_new               (PlayGridSizeMode  size_mode);
void       play_grid_update_state      (PlayGrid         *self,
                                        GridState        *state,
                                        LayoutConfig      config);
void       play_grid_set_layout_config (PlayGrid         *self,
                                        LayoutConfig      config);
gboolean   play_grid_get_resizable     (PlayGrid         *self);
void       play_grid_focus_cell        (PlayGrid         *self,
                                        IpuzCellCoord     coord);


G_END_DECLS
