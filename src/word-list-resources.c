/* word-list-resources.c
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/**
 * WordListResources:
 *
 * Quick-and-dirty implementation to encapsulate multiple word
 * lists. It currently only supports a static set of resources at
 * loading time. If/when we decide to make this dynamic, we can turn
 * it into a GListModel to match PuzzleSets.
 *
 * We'll also want to add support for filtering by language in the
 * future.
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include "word-list.h"
#include "word-list-resources.h"
#include "word-list-index.h"


enum
{
  RESOURCE_CHANGED,
  N_SIGNALS
};
static guint obj_signals[N_SIGNALS] = { 0 };


static WordListResources *global_resources = NULL;


typedef struct
{
  gchar *id;
  gchar *display_name;
  gchar *lang;
  guint word_count;
  GResource *resource;
}  WordListResource;


struct _WordListResources
{
  GObject parent_object;

  GSettings *settings;
  GArray *resource_list;
  WordListResource *default_resource;
};


G_DEFINE_TYPE (WordListResources, word_list_resources, G_TYPE_OBJECT);


static void word_list_resources_init       (WordListResources      *self);
static void word_list_resources_class_init (WordListResourcesClass *klass);
static void word_list_resources_dispose    (GObject                *object);
static void update_default_resource        (WordListResources      *self);
static gint resources_list_cmp             (gconstpointer           a,
                                            gconstpointer           b);
static void load_resources_from_dir        (const gchar            *dir_name,
                                            GArray                 *list);


static void
word_list_resources_init (WordListResources *self)
{
  const gchar *const *datadirs;
  const gchar *WORD_LIST_PATH = NULL;

  self->settings = g_settings_new ("org.gnome.Crosswords.Editor");

  g_signal_connect_swapped (self->settings,
                            "changed::word-list",
                            G_CALLBACK (update_default_resource),
                            self);

  self->resource_list = g_array_new (FALSE, FALSE, sizeof (WordListResource));

  datadirs = g_get_system_data_dirs ();
  for (guint i = 0; datadirs[i] != NULL; i++)
    {
      g_autofree gchar *pathname = NULL;

      pathname = g_build_filename (datadirs[i], "crosswords/word-lists", NULL);
      load_resources_from_dir (pathname, self->resource_list);
    }

  datadirs = g_get_system_data_dirs ();
  for (guint i = 0; datadirs[i] != NULL; i++)
    {
      g_autofree gchar *pathname = NULL;

      pathname = g_build_filename (datadirs[i], "crosswords/extensions/word-lists", NULL);
      load_resources_from_dir (pathname, self->resource_list);
    }

  if (g_get_user_data_dir ())
    {
      g_autofree gchar *pathname = NULL;

      pathname = g_build_filename (g_get_user_data_dir (), "crosswords/word-lists", NULL);
      load_resources_from_dir (pathname, self->resource_list);
    }

  WORD_LIST_PATH = g_getenv ("WORD_LIST_PATH");
  if (WORD_LIST_PATH && WORD_LIST_PATH[0])
    {
      g_auto (GStrv) puzzle_set_paths = NULL;

      puzzle_set_paths = g_strsplit (WORD_LIST_PATH, ":", -1);
      for (guint i = 0; puzzle_set_paths[i]; i++)
        load_resources_from_dir (puzzle_set_paths[i], self->resource_list);
    }

  if (self->resource_list == NULL)
    g_error ("No word lists found. Check $WORD_LIST_PATH is set correctly. Aborting.");

  g_array_sort (self->resource_list, resources_list_cmp);
  update_default_resource (self);
}

static void
word_list_resources_class_init (WordListResourcesClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = word_list_resources_dispose;

  obj_signals [RESOURCE_CHANGED] =
    g_signal_new ("resource-changed",
                  WORD_TYPE_LIST_RESOURCES,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);
}

static void
word_list_resources_dispose (GObject       *object)
{
  WordListResources *self;

  self = WORD_LIST_RESOURCES (object);

  g_clear_object (&self->settings);
  g_clear_pointer (&self->resource_list, g_array_unref);
}


static gboolean
load_resources_from_resource (GResource        *resource,
                              const gchar      *name,
                              WordListResource *val)
{
  g_autoptr (GInputStream) stream = NULL;
  g_autofree gchar *path = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree WordListIndex *index = NULL;
  g_autoptr (JsonParser) parser = NULL;

  path = g_strdup_printf ("/org/gnome/Crosswords/word-list/%s.json", name);

  stream = g_resource_open_stream (resource, path,
                                   G_RESOURCE_LOOKUP_FLAGS_NONE, NULL);

  if (stream == NULL)
    {
      g_warning ("Word list %s: Missing index at %s", name, path);
      return FALSE;
    }

  parser = json_parser_new_immutable ();
  if (! json_parser_load_from_stream (parser, stream, NULL, &error))
    {
      g_warning ("Word list %s: Error parsing index", name);
      return FALSE;
    }

  index = word_list_index_new_from_json (json_parser_get_root (parser));

  val->id = g_strdup (name);
  val->display_name = g_strdup (index->display_name);
  val->word_count = index->word_count;
  val->resource = g_resource_ref (resource);

  return TRUE;
}

static void
load_resources_from_dir (const gchar *dir_name,
                         GArray      *list)
{
  const char *name;
  GDir *data_dir;

  data_dir = g_dir_open (dir_name, 0, NULL);
  /* Ignore directories that don't exist. This is fine, because we're
   * walking through $XDG_DATA_DIRS */
  if (data_dir == NULL)
    return;

  while ((name = g_dir_read_name (data_dir)) != NULL)
    {
      g_autoptr (GError) err = NULL;
      g_autofree gchar *file_name = NULL;
      g_autoptr (GResource) resource = NULL;
      WordListResource val;
      g_autoptr (GString) base_name = NULL;

      file_name = g_build_filename (dir_name, name, NULL);
      /* Recurse to subdirectories */
      /* FIXME(cleanup): Should we look for loops? */
      if (g_file_test (file_name, G_FILE_TEST_IS_DIR))
        {
          /* recurse down subdirs */
          load_resources_from_dir (file_name, list);
          continue;
        }

      if (! g_str_has_suffix (name, ".gresource"))
        continue;
      base_name = g_string_new (name);
      g_string_truncate (base_name, strlen (name) - strlen (".gresource"));

      resource = g_resource_load (file_name, &err);
      if (err)
        {
          g_warning ("Failed to load resource %s\nError %s", file_name, err->message);
          continue;
        }

      if (load_resources_from_resource (resource, base_name->str, &val))
        {
          g_array_append_val (list, val);
        }
    }
  g_dir_close (data_dir);
}

static gint
resources_list_cmp (gconstpointer a,
                    gconstpointer b)
{
  WordListResource *val_a = (WordListResource *) a;
  WordListResource *val_b = (WordListResource *) b;

  return g_strcmp0 (val_a->display_name, val_b->display_name);
}

static void
update_default_resource (WordListResources *self)
{
  g_autofree gchar *word_list = NULL;

  word_list = g_settings_get_string (self->settings, "word-list");

  /* Short-circuit when the new default resource already is set to the
   * current default resource */
  if (self->default_resource &&
      !g_strcmp0 (self->default_resource->id, word_list))
    return;

  /* Walk through our list linearly looking for a match. */
  for (guint i = 0; i < self->resource_list->len; i++)
    {
      self->default_resource = &(g_array_index (self->resource_list, WordListResource, i));
      if (g_strcmp0 (self->default_resource->id, word_list) == 0)
        {
          g_signal_emit (self, obj_signals[RESOURCE_CHANGED], 0);
          return;
        }
    }

  /* Hmmm. We didn't find any resource that matches the setting. We
   * will Set the default to the first one we have. Doing so will call this
   * function again, at which point we'll emit the signal*/
  self->default_resource = &(g_array_index (self->resource_list, WordListResource, 0));
  g_settings_set_string (self->settings, "word-list", self->default_resource->id);
}


/* Public Functions */


GObject *
word_list_resources_default (void)
{
  if (global_resources == NULL)
    global_resources = g_object_new (WORD_TYPE_LIST_RESOURCES, NULL);

  return (GObject *) global_resources;
}

guint
word_list_resources_get_count (WordListResources *self)
{
  g_return_val_if_fail (WORD_IS_LIST_RESOURCES (self), 0);

  return self->resource_list->len;
}

const gchar *
word_list_resources_get_id (WordListResources *self,
                            guint              index)
{
  WordListResource resource;

  g_return_val_if_fail (WORD_IS_LIST_RESOURCES (self), NULL);
  g_return_val_if_fail (index < self->resource_list->len, NULL);

  resource = g_array_index (self->resource_list, WordListResource, index);

  return resource.id;
}

const gchar *
word_list_resources_get_display_name (WordListResources *self,
                                      guint              index)
{
  WordListResource resource;

  g_return_val_if_fail (WORD_IS_LIST_RESOURCES (self), NULL);
  g_return_val_if_fail (index < self->resource_list->len, NULL);

  resource = g_array_index (self->resource_list, WordListResource, index);

  return resource.display_name;
}

const gchar *
word_list_resources_get_lang (WordListResources *self,
                              guint              index)
{
  WordListResource resource;

  g_return_val_if_fail (WORD_IS_LIST_RESOURCES (self), NULL);
  g_return_val_if_fail (index < self->resource_list->len, NULL);

  resource = g_array_index (self->resource_list, WordListResource, index);

  return resource.lang;
}

guint
word_list_resources_get_word_count (WordListResources *self,
                                    guint              index)
{
  WordListResource resource;

  g_return_val_if_fail (WORD_IS_LIST_RESOURCES (self), 0);
  g_return_val_if_fail (index < self->resource_list->len, 0);

  resource = g_array_index (self->resource_list, WordListResource, index);

  return resource.word_count;
}

GResource *
word_list_resources_get_resource (WordListResources *self,
                                  guint              index)
{
  WordListResource resource;

  g_return_val_if_fail (WORD_IS_LIST_RESOURCES (self), NULL);
  g_return_val_if_fail (index < self->resource_list->len, NULL);

  resource = g_array_index (self->resource_list, WordListResource, index);

  return resource.resource;
}

gboolean
word_list_resources_get_is_default (WordListResources *self,
                                    guint              index)
{
  WordListResource resource;
  g_autofree gchar *word_list = NULL;

  g_return_val_if_fail (WORD_IS_LIST_RESOURCES (self), FALSE);
  g_return_val_if_fail (index < self->resource_list->len, FALSE);

  resource = g_array_index (self->resource_list, WordListResource, index);
  word_list = g_settings_get_string (self->settings, "word-list");

  return !g_strcmp0 (word_list, resource.id);
}

void
word_list_resources_set_default (WordListResources *self,
                                 const gchar       *id)
{
  g_return_if_fail (WORD_IS_LIST_RESOURCES (self));
  g_return_if_fail (id != NULL);

  /* We set this blindly. If it's not a valid one, we'll correct it in
   * the callback */
  g_settings_set_string (self->settings, "word-list", id);
}

GResource *
word_list_resources_get_default_resource (WordListResources *self)
{
  g_return_val_if_fail (WORD_IS_LIST_RESOURCES (self), NULL);

  return self->default_resource->resource;
}

const gchar *
word_list_resources_get_default_id (WordListResources *self)
{
  g_return_val_if_fail (WORD_IS_LIST_RESOURCES (self), NULL);

  return self->default_resource->id;
}
