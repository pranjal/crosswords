/* play-xword.c
 *
 * Copyright 2021 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <libipuz/libipuz.h>
#include <glib/gi18n-lib.h>
#include <adwaita.h>
#include "crosswords-enums.h"
#include "crosswords-misc.h"
#include "crosswords-quirks.h"
#include "play-clue-list.h"
#include "play-clue-row.h"
#include "play-grid.h"
#include "play-xword.h"
#include "play-xword-column.h"
#include "play-window.h"
#include "puzzle-stack.h"
#include "word-list.h"


#define PRE_DATA_STRING  "pre-play-xword"
#define POST_DATA_STRING "post-play-xword"

#define XWORD_IN_WON_STATE(xword) ((xword->state)&&(!GRID_STATE_HAS_EDIT_CELLS (xword->state)))
#define DIRECTION_IS_VISIBLE(dir) (dir != IPUZ_CLUE_DIRECTION_NONE && dir != IPUZ_CLUE_DIRECTION_HIDDEN)
#define ACROSS_DOWN(primary_dir,secondary_dir) ((primary_dir == IPUZ_CLUE_DIRECTION_ACROSS && \
                                                 secondary_dir == IPUZ_CLUE_DIRECTION_DOWN) || \
                                                (primary_dir == IPUZ_CLUE_DIRECTION_DOWN && \
                                                 secondary_dir == IPUZ_CLUE_DIRECTION_ACROSS))


enum
{
  PROP_0,
  PROP_PUZZLE,
  PROP_MODE,
  PROP_COMPACT_MODE,
  N_PROPS,
};

enum
{
  GAME_WON,
  REVEAL_CANCELED,
  USER_IDLE,
  LAST_SIGNAL
};


static GParamSpec *obj_props[N_PROPS] =  {NULL, };
static guint play_xword_signals[LAST_SIGNAL] = { 0 };


struct _PlayXword
{
  GtkWidget parent_instance;

  PlayXwordMode  mode;
  gchar *title;
  GridState *state;
  CrosswordsQuirks *quirks;
  gboolean compact_mode;

  /* undo stack */
  PuzzleStack *puzzle_stack;
  gulong changed_id;

  /* Toasts for messaging */
  AdwToast *game_won_toast;
  AdwToast *warning_toast;
  AdwToast *hint_toast;

  LayoutConfig layout_config;
  GSettings *settings;
  ZoomLevel zoom_level;

  GridRevealMode reveal_mode;
  guint autosave_idle_handler;
  guint across_toggle_handler;
  guint down_toggle_handler;

  /* Hint */
  WordList *word_list;
  gchar *filter;
  GArray *filter_rand_array;
  guint filter_offset;

  /* Template widgets */
  GtkWidget *xword_column;
  GtkWidget *intro_label;
  GtkWidget *play_grid;
  GtkWidget *notes_label;
  GtkWidget *clue_label;
  GtkWidget *primary_clues;
  GtkWidget *secondary_clues;
  GtkWidget *action_bar;
  GtkWidget *across_toggle;
  GtkWidget *down_toggle;
  GtkSizeGroup *spacer_size_group;
};


static void play_xword_init                     (PlayXword             *self);
static void play_xword_class_init               (PlayXwordClass        *klass);
static void play_xword_set_property             (GObject               *object,
                                                 guint                  prop_id,
                                                 const GValue          *value,
                                                 GParamSpec            *pspec);
static void play_xword_get_property             (GObject               *object,
                                                 guint                  prop_id,
                                                 GValue                *value,
                                                 GParamSpec            *pspec);
static void play_xword_dispose                  (GObject               *object);
static void play_xword_activate_clipboard_copy  (PlayXword             *self,
                                                 const char            *name,
                                                 GVariant              *parameter);
static void play_xword_activate_clipboard_paste (PlayXword             *self,
                                                 const char            *name,
                                                 GVariant              *parameter);
static void play_xword_actions_undo             (PlayXword             *self,
                                                 const gchar           *action_name,
                                                 GVariant              *param);
static void play_xword_actions_redo             (PlayXword             *self,
                                                 const gchar           *action_name,
                                                 GVariant              *param);

static void play_xword_push_change              (PlayXword             *self,
                                                 PuzzleStackChangeType  change_type,
                                                 IpuzPuzzle            *puzzle,
                                                 GridState             *pre_state);
static void play_xword_post_change              (PlayXword             *self);
static void play_xword_set_filter               (PlayXword             *self,
                                                 const gchar           *filter);
static void play_xword_update_won               (PlayXword             *self);
static void play_xword_update_state             (PlayXword             *self);
static void play_xword_update_states            (PlayXword             *self);
static void direction_toggled_cb                (GtkWidget             *toggle,
                                                 PlayXword             *self);
static void zoom_changed_cb                     (PlayXword             *self);
static void grid_guess_cb                       (PlayGrid              *grid,
                                                 gchar                 *guess,
                                                 PlayXword             *self);
static void grid_guess_at_cell_cb               (PlayGrid              *grid,
                                                 gchar                 *guess,
                                                 guint                  row,
                                                 guint                  column,
                                                 PlayXword             *self);
static void update_autosave_idle_handler        (PlayXword             *self);
static void cell_selected_cb                    (PlayGrid              *grid,
                                                 guint                  row,
                                                 guint                  column,
                                                 PlayXword             *self);
static void grid_do_command_cb                  (PlayGrid              *grid,
                                                 GridCmdKind            kind,
                                                 PlayXword             *self);
static void clue_selected_cb                    (PlayXword             *self,
                                                 IpuzClueDirection      direction,
                                                 gint                   index,
                                                 PlayClueList          *clue_list);
static void clue_cell_selected_cb               (PlayXword             *self,
                                                 guint                  row,
                                                 guint                  column);
static void clue_cell_guess_cb                  (PlayXword             *self,
                                                 const gchar           *guess);
static void clue_cell_guess_at_cell_cb          (PlayXword             *self,
                                                 const gchar           *guess,
                                                 guint                  row,
                                                 guint                  column);
static void clue_cell_do_command_cb             (PlayXword             *self,
		                                 GridCmdKind            kind);
static void play_xword_puzzle_stack_changed     (PlayXword             *self,
                                                 PuzzleStackOperation   operation,
                                                 PuzzleStackChangeType  change_type,
                                                 PuzzleStack           *puzzle_stack);


G_DEFINE_TYPE (PlayXword, play_xword, GTK_TYPE_WIDGET);


static void
play_xword_init (PlayXword *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->mode = PLAY_XWORD_MODE_NONE;
  self->compact_mode = FALSE;

  /* Set up the toggles */
  gtk_toggle_button_set_group (GTK_TOGGLE_BUTTON (self->across_toggle),
                               GTK_TOGGLE_BUTTON (self->down_toggle));
  self->across_toggle_handler =
    g_signal_connect (self->across_toggle, "toggled",
                      G_CALLBACK (direction_toggled_cb), self);
  self->down_toggle_handler =
    g_signal_connect (self->down_toggle, "toggled",
                      G_CALLBACK (direction_toggled_cb), self);

  /* Toasts */
  self->game_won_toast = adw_toast_new ("");
  adw_toast_set_priority (self->game_won_toast, ADW_TOAST_PRIORITY_HIGH);
  adw_toast_set_button_label (self->game_won_toast, _("_Select another?"));
  adw_toast_set_action_name (self->game_won_toast, "win.go-prev");
  adw_toast_set_timeout (self->game_won_toast, 0);

  self->warning_toast = adw_toast_new (_("You have filled in the puzzle incorrectly"));
  adw_toast_set_button_label (self->warning_toast, _("_Reveal mistakes?"));
  adw_toast_set_action_name (self->warning_toast, "xword.reveal");

  self->hint_toast = adw_toast_new ("");

  self->settings = g_settings_new ("org.gnome.Crosswords");
  self->zoom_level = ZOOM_UNSET;

  self->word_list = word_list_new ();
  self->filter_rand_array = g_array_new (FALSE, FALSE, sizeof (gushort));

  g_signal_connect_object (self->settings,
                           "changed::zoom-level",
                           G_CALLBACK (zoom_changed_cb),
                           self,
                           G_CONNECT_SWAPPED);
  zoom_changed_cb (self);

  /* set up the size group with the headers with a blank label*/
  play_clue_list_set_header_size_group (PLAY_CLUE_LIST (self->primary_clues),
                                        self->spacer_size_group);
  play_clue_list_set_header_size_group (PLAY_CLUE_LIST (self->secondary_clues),
                                        self->spacer_size_group);
}

static void
play_xword_class_init (PlayXwordClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->set_property = play_xword_set_property;
  object_class->get_property = play_xword_get_property;
  object_class->dispose = play_xword_dispose;

  play_xword_signals [GAME_WON] =
    g_signal_new ("game-won",
                  G_OBJECT_CLASS_TYPE (object_class),
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);

  play_xword_signals [REVEAL_CANCELED] =
    g_signal_new ("reveal-canceled",
                  G_OBJECT_CLASS_TYPE (object_class),
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);
  play_xword_signals [USER_IDLE] =
    g_signal_new ("user-idle",
                  G_OBJECT_CLASS_TYPE (object_class),
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);

  obj_props[PROP_PUZZLE] = g_param_spec_object ("puzzle",
                                                "Puzzle",
                                                "Currently loaded Ipuz File",
                                                IPUZ_TYPE_PUZZLE,
                                                G_PARAM_READABLE);
  obj_props[PROP_MODE] = g_param_spec_enum ("mode",
                                            "Mode",
                                            "Game won effect mode",
                                            PLAY_TYPE_XWORD_MODE,
                                            PLAY_XWORD_MODE_NONE,
                                            G_PARAM_READABLE);
  obj_props[PROP_COMPACT_MODE] = g_param_spec_boolean ("compact-mode",
                                                       "Compact Mode",
                                                       "Compact Mode",
                                                       FALSE,
                                                       G_PARAM_READABLE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/play-xword.ui");
  gtk_widget_class_bind_template_child (widget_class, PlayXword, xword_column);
  gtk_widget_class_bind_template_child (widget_class, PlayXword, intro_label);
  gtk_widget_class_bind_template_child (widget_class, PlayXword, play_grid);
  gtk_widget_class_bind_template_child (widget_class, PlayXword, notes_label);
  gtk_widget_class_bind_template_child (widget_class, PlayXword, clue_label);
  gtk_widget_class_bind_template_child (widget_class, PlayXword, primary_clues);
  gtk_widget_class_bind_template_child (widget_class, PlayXword, secondary_clues);
  gtk_widget_class_bind_template_child (widget_class, PlayXword, action_bar);
  gtk_widget_class_bind_template_child (widget_class, PlayXword, across_toggle);
  gtk_widget_class_bind_template_child (widget_class, PlayXword, down_toggle);
  gtk_widget_class_bind_template_child (widget_class, PlayXword, spacer_size_group);

  gtk_widget_class_bind_template_callback (widget_class, play_xword_update_state);
  gtk_widget_class_bind_template_callback (widget_class, grid_guess_cb);
  gtk_widget_class_bind_template_callback (widget_class, grid_guess_at_cell_cb);
  gtk_widget_class_bind_template_callback (widget_class, grid_do_command_cb);
  gtk_widget_class_bind_template_callback (widget_class, cell_selected_cb);
  gtk_widget_class_bind_template_callback (widget_class, clue_selected_cb);
  gtk_widget_class_bind_template_callback (widget_class, clue_cell_selected_cb);
  gtk_widget_class_bind_template_callback (widget_class, clue_cell_guess_cb);
  gtk_widget_class_bind_template_callback (widget_class, clue_cell_guess_at_cell_cb);
  gtk_widget_class_bind_template_callback (widget_class, clue_cell_do_command_cb);

  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_c, GDK_CONTROL_MASK, "clipboard.copy", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_v, GDK_CONTROL_MASK, "clipboard.paste", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_z, GDK_CONTROL_MASK, "win.undo", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_y, GDK_CONTROL_MASK, "win.redo", NULL);

  gtk_widget_class_install_action (widget_class, "clipboard.copy", NULL,
                                   (GtkWidgetActionActivateFunc) play_xword_activate_clipboard_copy);
  gtk_widget_class_install_action (widget_class, "clipboard.paste", NULL,
                                   (GtkWidgetActionActivateFunc) play_xword_activate_clipboard_paste);
  gtk_widget_class_install_action (widget_class, "win.undo", NULL,
                                   (GtkWidgetActionActivateFunc) play_xword_actions_undo);
  gtk_widget_class_install_action (widget_class, "win.redo", NULL,
                                   (GtkWidgetActionActivateFunc) play_xword_actions_redo);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
}


static void
play_xword_set_property (GObject      *object,
                         guint         prop_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  switch (prop_id)
    {
    case PROP_PUZZLE:
      play_xword_load_puzzle (PLAY_XWORD (object),
                              IPUZ_PUZZLE (g_value_get_object (value)));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}


static void
play_xword_get_property (GObject    *object,
                         guint       prop_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  PlayXword *self;

  g_return_if_fail (object != NULL);

  self = PLAY_XWORD (object);

  switch (prop_id)
    {
    case PROP_PUZZLE:
      if (self->puzzle_stack)
        g_value_set_object (value, puzzle_stack_get_puzzle (self->puzzle_stack));
      else
        g_value_set_object (value, NULL);
      break;
    case PROP_MODE:
      g_value_set_enum (value, self->mode);
      break;
    case PROP_COMPACT_MODE:
      g_value_set_boolean (value, play_xword_column_get_clue_visible (PLAY_XWORD_COLUMN (self->xword_column)));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
play_xword_dispose (GObject *object)
{
  PlayXword *self;
  GtkWidget *child;

  g_return_if_fail (object != NULL);

  self = PLAY_XWORD (object);

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  g_clear_pointer (&self->title, g_free);
  g_clear_signal_handler (&self->changed_id,
                          self->puzzle_stack);
  g_clear_object (&self->puzzle_stack);
  g_clear_pointer (&self->state, grid_state_free);
  g_clear_object (&self->quirks);
  g_clear_object (&self->settings);
  g_clear_object (&self->game_won_toast);
  g_clear_object (&self->warning_toast);
  g_clear_object (&self->hint_toast);
  g_clear_object (&self->word_list);
  g_clear_pointer (&self->filter, g_free);
  if (self->filter_rand_array)
    {
      g_array_free (self->filter_rand_array, TRUE);
      self->filter_rand_array = NULL;
    }

  if (self->autosave_idle_handler)
    {
      g_source_remove (self->autosave_idle_handler);
      self->autosave_idle_handler = 0;
    }

  G_OBJECT_CLASS (play_xword_parent_class)->dispose (object);
}

static void
play_xword_activate_clipboard_copy (PlayXword  *self,
                                    const char *name,
                                    GVariant   *parameter)
{
  GdkClipboard *clipboard;
  g_autofree gchar *str = NULL;

  if (self->state == NULL)
    return;

  if (XWORD_IN_WON_STATE (self))
    return;

  /* If direction is NONE, we're probably on a block */
  if (self->state->clue.direction == IPUZ_CLUE_DIRECTION_NONE)
    return;

  str = ipuz_crossword_get_guess_string_by_id (self->state->xword,
                                               &self->state->clue);
  clipboard = gtk_widget_get_clipboard (GTK_WIDGET (self));
  gdk_clipboard_set_text (clipboard, str);
}

static void
paste_received (GObject      *clipboard,
                GAsyncResult *result,
                gpointer      data)
{
  PlayXword *self;
  g_autofree gchar *text = NULL;
  g_autofree gchar *upper = NULL;
  IpuzClueDirection direction;
  GridState *dehydrated_state;

  self = PLAY_XWORD (data);

  if (XWORD_IN_WON_STATE (self))
    return;

  text = gdk_clipboard_read_text_finish (GDK_CLIPBOARD (clipboard), result, NULL);
  if (text == NULL || self->state == NULL)
    {
      /* Strange text to get. */
      gtk_widget_error_bell (GTK_WIDGET (self));
      g_object_unref (self);
      return;
    }

  direction = self->state->clue.direction;
  upper = g_utf8_strup (text, -1);
  /* If we're on a BLOCK, arbitrarily paste across. */
  if (direction == IPUZ_CLUE_DIRECTION_NONE)
    direction = IPUZ_CLUE_DIRECTION_ACROSS;

  dehydrated_state = grid_state_dehydrate (self->state);
  self->state = grid_state_replace (self->state,
                                    grid_state_guess_word (self->state,
                                                           self->state->cursor,
                                                           direction, upper));

  play_xword_push_change (self, STACK_CHANGE_GUESS, IPUZ_PUZZLE (self->state->xword), dehydrated_state);
  g_object_unref (self);
}

static void
play_xword_activate_clipboard_paste (PlayXword  *self,
                                     const char *name,
                                     GVariant   *parameter)
{
  GdkClipboard *clipboard;

  if (self->state == NULL)
    return;

  if (XWORD_IN_WON_STATE (self))
    return;

  clipboard = gtk_widget_get_clipboard (GTK_WIDGET (self));
  gdk_clipboard_read_text_async (clipboard, NULL, paste_received, g_object_ref (self));

}

static void
play_xword_actions_undo (PlayXword   *self,
                         const gchar *action_name,
                         GVariant    *param)
{
  g_return_if_fail (PLAY_IS_XWORD (self));
  g_return_if_fail (PUZZLE_IS_STACK (self->puzzle_stack));

  if (XWORD_IN_WON_STATE (self))
    return;

  puzzle_stack_undo (self->puzzle_stack);
}

static void
play_xword_actions_redo (PlayXword   *self,
                         const gchar *action_name,
                         GVariant    *param)
{
  g_return_if_fail (PLAY_IS_XWORD (self));
  g_return_if_fail (PUZZLE_IS_STACK (self->puzzle_stack));

  if (XWORD_IN_WON_STATE (self))
    return;

  puzzle_stack_redo (self->puzzle_stack);
}

static void
play_xword_push_change (PlayXword             *self,
                        PuzzleStackChangeType  change_type,
                        IpuzPuzzle            *puzzle,
                        GridState             *pre_state)
{
  g_assert (PLAY_IS_XWORD (self));
  g_assert (GRID_STATE_DEHYDRATED (pre_state));

  puzzle_stack_push_change (self->puzzle_stack, change_type, puzzle);
  puzzle_stack_set_data (self->puzzle_stack,
                         PRE_DATA_STRING,
                         pre_state,
                         (GDestroyNotify) grid_state_free);
}

static void
play_xword_post_change (PlayXword *self)
{
  g_signal_emit (self, play_xword_signals[REVEAL_CANCELED], 0);
  update_autosave_idle_handler (self);
  play_xword_update_states (self);
}


static void
direction_toggled_cb (GtkWidget *toggle,
                      PlayXword *self)
{
  if (self->state == NULL)
    return;

  if (toggle == self->across_toggle &&
      gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle)) &&
      self->state->clue.direction == IPUZ_CLUE_DIRECTION_DOWN)
    {
      grid_do_command_cb (PLAY_GRID (self->play_grid), GRID_CMD_KIND_SWITCH, self);
      if (self->state->clue.direction != IPUZ_CLUE_DIRECTION_ACROSS)
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (self->down_toggle), TRUE);
    }
  else if (toggle == self->down_toggle &&
           gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle)) &&
           self->state->clue.direction == IPUZ_CLUE_DIRECTION_ACROSS)
    {
      grid_do_command_cb (PLAY_GRID (self->play_grid), GRID_CMD_KIND_SWITCH, self);
      if (self->state->clue.direction != IPUZ_CLUE_DIRECTION_DOWN)
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (self->across_toggle), TRUE);
    }
}

static void
zoom_changed_cb (PlayXword *self)
{
  ZoomLevel new_zoom_level;
  g_autofree gchar *zoom_level_str = NULL;
  IpuzPuzzleKind kind = IPUZ_PUZZLE_CROSSWORD;

  if (self->state)
    kind = ipuz_puzzle_get_puzzle_kind (IPUZ_PUZZLE (self->state->xword));

  zoom_level_str = g_settings_get_string (self->settings, "zoom-level");
  new_zoom_level = zoom_level_from_string (zoom_level_str);

  /* Same zoom_level as before */
  if (self->zoom_level == new_zoom_level)
    return;

  self->zoom_level = new_zoom_level;
  play_xword_column_set_skip_next_anim (PLAY_XWORD_COLUMN (self->xword_column), TRUE);
  self->layout_config = layout_config_at_zoom_level (kind, self->zoom_level);
  play_xword_update_states (self);
}


static gboolean
update_autosave_idle_cb (PlayXword *self)
{
  g_signal_emit (self, play_xword_signals[USER_IDLE], 0);

  self->autosave_idle_handler = 0;
  return G_SOURCE_REMOVE;
}

static void
update_autosave_idle_handler (PlayXword *self)
{
  if (self->autosave_idle_handler)
    g_source_remove (self->autosave_idle_handler);

  self->autosave_idle_handler =
    g_timeout_add_seconds (1, G_SOURCE_FUNC (update_autosave_idle_cb), self);
}


static void
grid_guess_cb (PlayGrid  *grid,
               gchar     *guess,
               PlayXword *self)
{
  GridState *dehydrated_state;

  if (IPUZ_IS_ACROSTIC (self->state->xword))
    crosswords_quirks_set_focus_location (self->state->quirks, QUIRKS_FOCUS_LOCATION_MAIN_GRID);

  dehydrated_state = grid_state_dehydrate (self->state);
  self->state = grid_state_replace (self->state, grid_state_guess (self->state, guess));

  play_xword_push_change (self, STACK_CHANGE_GUESS, IPUZ_PUZZLE (self->state->xword), dehydrated_state);
}

static void
grid_guess_at_cell_cb (PlayGrid  *grid,
                       gchar     *guess,
                       guint      row,
                       guint      column,
                       PlayXword *self)
{
  GridState *dehydrated_state;
  IpuzCellCoord coord = {
    .row = row,
    .column = column
  };

  if (IPUZ_IS_ACROSTIC (self->state->xword))
    crosswords_quirks_set_focus_location (self->state->quirks, QUIRKS_FOCUS_LOCATION_MAIN_GRID);

  dehydrated_state = grid_state_dehydrate (self->state);
  self->state = grid_state_replace (self->state, grid_state_guess_at_cell (self->state, guess, coord));

  play_xword_push_change (self, STACK_CHANGE_GUESS, IPUZ_PUZZLE (self->state->xword), dehydrated_state);
}

static void
grid_do_command_cb (PlayGrid    *grid,
                    GridCmdKind  kind,
                    PlayXword   *self)
{
  if (IPUZ_IS_ACROSTIC (self->state->xword))
    {
      if (kind == GRID_CMD_KIND_SWITCH)
        crosswords_quirks_set_focus_location (self->state->quirks, QUIRKS_FOCUS_LOCATION_CLUE_GRID);
      else
        crosswords_quirks_set_focus_location (self->state->quirks, QUIRKS_FOCUS_LOCATION_MAIN_GRID);
    }

  if (kind == GRID_CMD_KIND_BACKSPACE ||
      kind == GRID_CMD_KIND_DELETE)
    {
      GridState *dehydrated_state;

      g_autofree gchar *old_hash = NULL;
      g_autofree gchar *new_hash = NULL;

      dehydrated_state = grid_state_dehydrate (self->state);

      /* only push a change if the board actually changes. Otherwise,
         we assume they deleted a blank space */
      old_hash = ipuz_guesses_get_checksum (puzzle_stack_get_guesses (self->puzzle_stack), NULL);
      self->state = grid_state_replace (self->state, grid_state_do_command (self->state, kind));
      new_hash = ipuz_guesses_get_checksum (ipuz_crossword_get_guesses (IPUZ_CROSSWORD (self->state->xword)), NULL);

      if (g_strcmp0 (old_hash, new_hash) != 0)
        {
          play_xword_push_change (self,
                                  STACK_CHANGE_GUESS,
                                  IPUZ_PUZZLE (self->state->xword),
                                  dehydrated_state);
        }
      else
        {
          play_xword_post_change (self);
          grid_state_free (dehydrated_state);
        }
    }
  else
    {
      self->state = grid_state_replace (self->state, grid_state_do_command (self->state, kind));
      play_xword_post_change (self);
    }
}

static void
cell_selected_cb (PlayGrid *grid,
                  guint row,
                  guint column,
                  PlayXword *self)
{
  IpuzCellCoord coord = {
    .row = row,
    .column = column,
  };

  if (IPUZ_IS_ACROSTIC (self->state->xword))
    crosswords_quirks_set_focus_location (self->state->quirks, QUIRKS_FOCUS_LOCATION_MAIN_GRID);

  self->state = grid_state_replace (self->state, grid_state_cell_selected (self->state, coord));

  play_xword_post_change (self);
}

static void
clue_selected_cb (PlayXword         *self,
                  IpuzClueDirection  direction,
                  gint               index,
                  PlayClueList      *clue_list)
{
  IpuzPuzzle *puzzle;
  IpuzClue *clue;
  IpuzClueId clue_id = {
    .direction = direction,
    .index = index
  };

  if (IPUZ_IS_ACROSTIC (self->state->xword))
    crosswords_quirks_set_focus_location (self->state->quirks, QUIRKS_FOCUS_LOCATION_CLUE_GRID);

  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);
  clue = ipuz_crossword_get_clue_by_id (IPUZ_CROSSWORD (puzzle), &clue_id);
  self->state = grid_state_replace (self->state, grid_state_clue_selected (self->state, clue));

  play_xword_post_change (self);
}

static void
clue_cell_selected_cb (PlayXword             *self,
		       guint                  row,
		       guint                  column)
{
  IpuzCellCoord coord = {
    .row = row,
    .column = column,
  };

  crosswords_quirks_set_focus_location (self->state->quirks, QUIRKS_FOCUS_LOCATION_CLUE_GRID);

  self->state = grid_state_replace (self->state, grid_state_cell_selected (self->state, coord));

  play_xword_post_change (self);
}

static void
clue_cell_guess_cb (PlayXword    *self,
                    const gchar  *guess)
{
  crosswords_quirks_set_focus_location (self->state->quirks, QUIRKS_FOCUS_LOCATION_CLUE_GRID);

  self->state = grid_state_replace (self->state, grid_state_guess (self->state, guess));

  puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_GUESS, IPUZ_PUZZLE (self->state->xword));
}

static void
clue_cell_guess_at_cell_cb (PlayXword   *self,
		            const gchar *guess,
			    guint        row,
			    guint        column)
{
  IpuzCellCoord coord = {
    .row = row,
    .column = column
  };

  crosswords_quirks_set_focus_location (self->state->quirks, QUIRKS_FOCUS_LOCATION_CLUE_GRID);

  self->state = grid_state_replace (self->state, grid_state_guess_at_cell (self->state, guess, coord));

  puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_GUESS, IPUZ_PUZZLE (self->state->xword));
}

static void
clue_cell_do_command_cb (PlayXword   *self,
		         GridCmdKind  kind)
{
  if (IPUZ_IS_ACROSTIC (self->state->xword))
    {
      if (kind == GRID_CMD_KIND_SWITCH)
        crosswords_quirks_set_focus_location (self->state->quirks, QUIRKS_FOCUS_LOCATION_MAIN_GRID);
      else
        crosswords_quirks_set_focus_location (self->state->quirks, QUIRKS_FOCUS_LOCATION_CLUE_GRID);
     }

  if (kind == GRID_CMD_KIND_BACKSPACE ||
      kind == GRID_CMD_KIND_DELETE)
    {
      g_autofree gchar *old_hash = NULL;
      g_autofree gchar *new_hash = NULL;

      /* only push a change if the board actually changes. Otherwise,
         we assume they deleted a blank space */
      old_hash = ipuz_guesses_get_checksum (puzzle_stack_get_guesses (self->puzzle_stack), NULL);
      self->state = grid_state_replace (self->state, grid_state_do_command (self->state, kind));
      new_hash = ipuz_guesses_get_checksum (ipuz_crossword_get_guesses (IPUZ_CROSSWORD (self->state->xword)), NULL);

      if (g_strcmp0 (old_hash, new_hash) != 0)
        puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_GUESS, IPUZ_PUZZLE (self->state->xword));
      else
        play_xword_post_change (self);
    }
  else
    {
      self->state = grid_state_replace (self->state, grid_state_do_command (self->state, kind));
      play_xword_post_change (self);
    }
}

/* Shuffle the list of words so we don't go in pure alphabetical
 * order */
static void
shuffle_array (GArray *arr)
{
  if (arr->len == 0)
    return;

  for (guint i = 0; i < arr->len - 1; i++)
    {
      guint j = i + rand() / (RAND_MAX / (arr->len - i) + 1);
      gushort swap = g_array_index (arr, gushort, j);
      g_array_index (arr, gushort, j) = g_array_index (arr, gushort, i);
      g_array_index (arr, gushort, i) = swap;
    }
}

static void
play_xword_set_filter (PlayXword   *self,
                       const gchar *filter)
{
  gboolean is_filter = FALSE;
  guint n_items;

  if (g_strcmp0 (filter, self->filter) == 0)
    return;

  g_clear_pointer (&self->filter, g_free);

  for (const gchar *p = filter; p[0] != '\0'; p = g_utf8_next_char (p))
    {
      if (p[0] == '?')
        {
          is_filter = TRUE;
          break;
        }
    }

  if (! is_filter)
    return;

  self->filter = g_strdup (filter);
  word_list_set_filter (self->word_list, self->filter, WORD_LIST_MATCH);
  self->filter_offset = 0;

  n_items = word_list_get_n_items (self->word_list);
  g_array_set_size (self->filter_rand_array, n_items);

  for (guint i = 0; i < n_items; i++)
    g_array_index (self->filter_rand_array, gushort, i) = (gushort) i;

  shuffle_array (self->filter_rand_array);
}

/* This can be called multiple times when you win. It may be a problem
 * in the future.
 */
static void
play_xword_update_won (PlayXword *self)
{
  GtkRoot *root;
  GtkWidget *toast_overlay;
  IpuzPuzzle *puzzle;
  IpuzGuesses *guesses;

  g_return_if_fail (self->puzzle_stack != NULL);

  root = gtk_widget_get_root (GTK_WIDGET (self));
  toast_overlay = play_window_get_overlay (PLAY_WINDOW (root));
  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);
  if (ipuz_crossword_game_won (IPUZ_CROSSWORD (puzzle)))
    {
      /* Lock down the state so you can't edit it */
      self->state = grid_state_replace (self->state, grid_state_change_mode (self->state, GRID_STATE_BROWSE));

      adw_toast_overlay_add_toast (ADW_TOAST_OVERLAY (toast_overlay),
                                   g_object_ref (self->game_won_toast));
      adw_toast_dismiss (self->warning_toast);
      play_xword_update_states (self);
      return;
    }

  guesses = ipuz_crossword_get_guesses (IPUZ_CROSSWORD (puzzle));
  if (ipuz_guesses_get_percent (guesses) >= 1.0)
    {
      adw_toast_overlay_add_toast (ADW_TOAST_OVERLAY (toast_overlay),
                                   g_object_ref (self->warning_toast));
    }
}

static void
set_focused_cell (PlayXword *self)
{
  if (GRID_STATE_CURSOR_SET (self->state))
    {
      if (NONE_HAS_FOCUS (self->state) || ACROSTIC_MAIN_GRID_HAS_FOCUS (self->state))
        {
	  play_grid_focus_cell (PLAY_GRID (self->play_grid), self->state->cursor);
	}
      else if (!ACROSTIC_MAIN_GRID_HAS_FOCUS (self->state))
        {
	  if (play_xword_column_get_clue_visible (PLAY_XWORD_COLUMN (self->xword_column)))
	    clue_grid_focus_cell (CLUE_GRID (play_clue_row_get_selected_grid (PLAY_CLUE_ROW (self->clue_label))));
	  else
	    clue_grid_focus_cell (CLUE_GRID (play_clue_list_get_selected_grid (PLAY_CLUE_LIST (self->primary_clues))));
	}
    }
}

static void
update_caption (PlayXword *self)
{
  IpuzPuzzle *puzzle;
  g_autofree gchar *intro = NULL;
  g_autofree gchar *notes = NULL;
  g_autofree gchar *explanation = NULL;

  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);
  if (puzzle)
    g_object_get (puzzle,
                  "intro", &intro,
                  "notes", &notes,
                  "explanation", &explanation,
                  NULL);

  if (intro && *intro)
    {
      gtk_label_set_markup (GTK_LABEL (self->intro_label), intro);
      set_zoom_level_css_class (self->intro_label, "caption-", self->zoom_level);
    }
  else
    {
      gtk_label_set_markup (GTK_LABEL (self->intro_label), "");
    }

  if (notes && *notes)
    {
      gtk_widget_set_visible (self->notes_label, TRUE);
      gtk_label_set_markup (GTK_LABEL (self->notes_label), notes);
      set_zoom_level_css_class (self->notes_label, "caption-", self->zoom_level);
    }
  else
    {
      gtk_widget_set_visible (self->notes_label, FALSE);
    }

  if (explanation && *explanation)
    {
      g_autofree gchar *new_title = NULL;

      new_title = g_strdup_printf ("%s\n%s", _("Congratulations — you solved the puzzle!"), explanation);
      adw_toast_set_title (self->game_won_toast, new_title);
    }
  else
    {
      adw_toast_set_title (self->game_won_toast, _("Congratulations — you solved the puzzle!"));
    }
}

static void
update_clue_row (PlayXword *self)
{
  IpuzClue *clue = NULL;
  gboolean showenumerations;

  g_assert (self);
  g_assert (self->state);

  g_object_get (G_OBJECT (self->state->xword),
                "showenumerations", &showenumerations,
                NULL);

  if (self->state->clue.direction != IPUZ_CLUE_DIRECTION_NONE)
    clue = ipuz_crossword_get_clue_by_id (self->state->xword,
                                          &self->state->clue);

  if (clue)
    {
      gtk_widget_set_visible (self->clue_label, TRUE);
      play_clue_row_update (PLAY_CLUE_ROW (self->clue_label),
                            self->state, clue,
                            self->state->clue, showenumerations,
                            self->zoom_level);
    }
  else
    {
      gtk_widget_set_visible (self->clue_label, FALSE);
    }
}
static void
update_directions (PlayXword *self)
{
  guint n_clue_sets;
  IpuzClueDirection primary_dir = IPUZ_CLUE_DIRECTION_NONE;
  IpuzClueDirection secondary_dir = IPUZ_CLUE_DIRECTION_NONE;
  gboolean primary_visible;
  gboolean secondary_visible;

  n_clue_sets = ipuz_crossword_get_n_clue_sets (self->state->xword);
  /* FIXME(cluesets): We don't support tertiary clues yet. For now,
   * just manage the two */
  if (n_clue_sets > 2)
    {
      g_warning ("More than two sets of clues not supported");
      n_clue_sets = 2;
    }
  primary_visible = play_xword_column_get_primary_visible (PLAY_XWORD_COLUMN (self->xword_column));
  secondary_visible = play_xword_column_get_secondary_visible (PLAY_XWORD_COLUMN (self->xword_column));
  if (n_clue_sets > 0)
    primary_dir = ipuz_crossword_clue_set_get_dir (self->state->xword, 0);
  if (n_clue_sets > 1)
    secondary_dir = ipuz_crossword_clue_set_get_dir (self->state->xword, 1);

  /* First, update the visibility of the clue widgets */
  if (DIRECTION_IS_VISIBLE (primary_dir))
    gtk_widget_set_visible (self->primary_clues, TRUE);
  else
    {
      gtk_widget_set_visible (self->primary_clues, FALSE);
      primary_visible = FALSE;
    }

  if (DIRECTION_IS_VISIBLE (secondary_dir))
    gtk_widget_set_visible (self->secondary_clues, TRUE);
  else
    {
      gtk_widget_set_visible (self->secondary_clues, FALSE);
      secondary_visible = FALSE;
    }

  /* Special case when only one column of rows is visible and we're
   * across/down. Change the direction to the current clue
   * direction. */
  if (ACROSS_DOWN (primary_dir, secondary_dir) &&
      (primary_visible && !secondary_visible))
    {
      g_object_set (self->primary_clues, "direction", self->state->clue.direction, NULL);
    }
  else
    {
      if (DIRECTION_IS_VISIBLE (primary_dir))
        g_object_set (self->primary_clues, "direction", primary_dir, NULL);
      if (DIRECTION_IS_VISIBLE (secondary_dir))
        g_object_set (self->secondary_clues, "direction", secondary_dir, NULL);
    }

  /* Update the Across/Down toggles. We only do this when we're across/down xword */
  if (ACROSS_DOWN (primary_dir, secondary_dir))
    {
      gtk_widget_set_visible (self->action_bar, TRUE);
      g_signal_handler_block (self->across_toggle, self->across_toggle_handler);
      g_signal_handler_block (self->down_toggle, self->down_toggle_handler);

      if (self->state->clue.direction == IPUZ_CLUE_DIRECTION_ACROSS)
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (self->across_toggle), TRUE);
      else if  (self->state->clue.direction == IPUZ_CLUE_DIRECTION_DOWN)
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (self->down_toggle), TRUE);

      g_signal_handler_unblock (self->across_toggle, self->across_toggle_handler);
      g_signal_handler_unblock (self->down_toggle, self->down_toggle_handler);
    }
  else
    {
      gtk_widget_set_visible (self->action_bar, FALSE);
    }
}

/* Notifies the window when we're in compact mode so we can update the
 * main UI to adjust */
static void
update_compact_mode (PlayXword *self)
{
  gboolean primary_visible;
  gboolean secondary_visible;

  primary_visible = play_xword_column_get_primary_visible (PLAY_XWORD_COLUMN (self->xword_column));
  secondary_visible = play_xword_column_get_secondary_visible (PLAY_XWORD_COLUMN (self->xword_column));

  if (primary_visible || secondary_visible)
    self->compact_mode = FALSE;
  else
    self->compact_mode = TRUE;
  g_object_notify_by_pspec (G_OBJECT (self),
                            obj_props[PROP_COMPACT_MODE]);
}

/* WARNING: This function is triggered by both state changes (safe),
 * and changes in the visibility of elements in the
 * PlayXwordColumn. That happens in the allocation callback. Making
 * changes to widths of widgets could cause it to loop.
 */
static void
play_xword_update_state (PlayXword *self)
{
  if (self->state == NULL)
    return;

  update_directions (self);
  play_clue_list_update (PLAY_CLUE_LIST (self->primary_clues), self->state, self->zoom_level);
  play_clue_list_update (PLAY_CLUE_LIST (self->secondary_clues), self->state, self->zoom_level);
  update_clue_row (self);
  update_caption (self);
  update_compact_mode (self);
}

static void
play_xword_update_states (PlayXword *self)
{
  g_assert (PLAY_IS_XWORD (self));
  guint min_base_size;

  if (self->state == NULL)
    return;

  min_base_size = zoom_level_get_min_base_size (ipuz_puzzle_kind_from_gtype (G_OBJECT_TYPE (self->state->xword)),
                                                self->zoom_level);
  g_object_set (G_OBJECT (self->play_grid),
                "min-base-size", min_base_size,
                NULL);
  play_grid_update_state (PLAY_GRID (self->play_grid), self->state, self->layout_config);
  play_xword_update_state (self);
  set_focused_cell (self);
}

/* Called when a new frame is pushed on the stack */
static void
puzzle_stack_changed_new_frame (PlayXword   *self,
                                PuzzleStack *puzzle_stack)
{
  if (self->state)
    {
      GridState *dehydrated_state;
      dehydrated_state = grid_state_dehydrate (self->state);
      puzzle_stack_set_data (puzzle_stack,
                             POST_DATA_STRING,
                             dehydrated_state,
                             (GDestroyNotify) grid_state_free);
    }
}

static void
puzzle_stack_changed_undo (PlayXword   *self,
                           PuzzleStack *puzzle_stack)
{
  IpuzPuzzle *puzzle;
  GridState *dehydrated_state;

  puzzle = puzzle_stack_get_puzzle (puzzle_stack);
  dehydrated_state = puzzle_stack_peek_next_data (puzzle_stack, PRE_DATA_STRING);

  g_assert (dehydrated_state);
  self->state = grid_state_replace (self->state, grid_state_hydrate (dehydrated_state,
                                                                     IPUZ_CROSSWORD (puzzle),
                                                                     self->quirks));
}

static void
puzzle_stack_changed_redo (PlayXword   *self,
                           PuzzleStack *puzzle_stack)
{
  IpuzPuzzle *puzzle;
  GridState *dehydrated_state;

  puzzle = puzzle_stack_get_puzzle (puzzle_stack);
  dehydrated_state = puzzle_stack_get_data (puzzle_stack, POST_DATA_STRING);

  g_assert (dehydrated_state);
  self->state = grid_state_replace (self->state, grid_state_hydrate (dehydrated_state,
                                                                     IPUZ_CROSSWORD (puzzle),
                                                                     self->quirks));
}

static void
play_xword_puzzle_stack_changed (PlayXword             *self,
                                 PuzzleStackOperation   operation,
                                 PuzzleStackChangeType  change_type,
                                 PuzzleStack           *puzzle_stack)
{
  switch (operation)
    {
    case PUZZLE_STACK_OPERATION_NEW_FRAME:
      puzzle_stack_changed_new_frame (self, puzzle_stack);
      break;
    case PUZZLE_STACK_OPERATION_UNDO:
      puzzle_stack_changed_undo (self, puzzle_stack);
      break;
    case PUZZLE_STACK_OPERATION_REDO:
      puzzle_stack_changed_redo (self, puzzle_stack);
      break;
    default:
      g_assert_not_reached ();
    }

  /* We made a guess. Let's see if we won or filled in the grid. */
  play_xword_update_won (self);
  play_xword_post_change (self);
}


/* Public functions */

GtkWidget *
play_xword_new (void)
{
  return (GtkWidget *) g_object_new (PLAY_TYPE_XWORD, NULL);

}

void
play_xword_set_mode (PlayXword     *self,
                     PlayXwordMode  mode)
{
  g_return_if_fail (PLAY_IS_XWORD (self));

  self->mode = mode;
}


void
play_xword_load_puzzle (PlayXword  *self,
                        IpuzPuzzle *puzzle)
{
  g_autoptr (IpuzPuzzle) puzzle_copy = NULL;
  g_return_if_fail (PLAY_IS_XWORD (self));

  /* NOTE: We make our own copy of the puzzle to start. This is
   *  because this puzzle is owned by the puzzle-set, and that will
   *  keep the puzzle in sync with the guesses that are autosaved to
   *  disk, versus ours which will change depending on what's on the
   *  stack. */
  if (puzzle)
    puzzle_copy = ipuz_puzzle_deep_copy (IPUZ_PUZZLE (puzzle));

  g_clear_signal_handler (&self->changed_id,
                          self->puzzle_stack);
  g_clear_object (&self->puzzle_stack);
  g_clear_object (&self->quirks);
  g_clear_pointer (&self->state, grid_state_free);
  adw_toast_dismiss (self->game_won_toast);
  adw_toast_dismiss (self->warning_toast);

  /* Set the puzzle */
  self->puzzle_stack = puzzle_stack_new (PUZZLE_STACK_PLAY, puzzle_copy);
  self->changed_id = g_signal_connect_swapped (self->puzzle_stack,
                                                "changed",
                                                G_CALLBACK (play_xword_puzzle_stack_changed),
                                                self);
  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_PUZZLE]);

  g_clear_pointer (&self->title, g_free);
  if (puzzle_copy)
    {
      IpuzPuzzleKind kind;

      self->quirks = crosswords_quirks_new (puzzle_copy);
      g_object_get (puzzle_copy,
                    "title", &self->title,
                    "puzzle-kind", &kind,
                    NULL);
      self->state = grid_state_new (IPUZ_CROSSWORD (puzzle_copy), self->quirks, GRID_STATE_SOLVE);
      self->layout_config = layout_config_at_zoom_level (kind, self->zoom_level);
      /* Force a change signal to get everything set up */
      play_xword_puzzle_stack_changed (self, PUZZLE_STACK_OPERATION_NEW_FRAME, STACK_CHANGE_GUESS, self->puzzle_stack);
      play_xword_column_set_skip_next_anim (PLAY_XWORD_COLUMN (self->xword_column), TRUE);
    }

  play_xword_update_won (self);
}

IpuzPuzzle *
play_xword_get_puzzle (PlayXword *self)
{
  g_return_val_if_fail (PLAY_IS_XWORD (self), NULL);

  if (self->puzzle_stack)
    return puzzle_stack_get_puzzle (self->puzzle_stack);
  return NULL;
}

IpuzGuesses *
play_xword_get_guesses (PlayXword *self)
{
  g_return_val_if_fail (PLAY_IS_XWORD (self), NULL);

  if (self->puzzle_stack)
    return puzzle_stack_get_guesses (self->puzzle_stack);
  return NULL;
}

const gchar *
play_xword_get_title (PlayXword *self)
{
  g_return_val_if_fail (PLAY_IS_XWORD (self), NULL);

  return self->title;
}

void
play_xword_show_hint (PlayXword *self)
{
  g_autofree gchar *guess_string = NULL;
  g_autofree gchar *hint = NULL;
  GtkRoot *root;
  GtkWidget *toast_overlay;

  g_return_if_fail (PLAY_IS_XWORD (self));

  if (! GRID_STATE_CURSOR_SET (self->state))
    {
      word_list_set_filter (self->word_list, NULL, WORD_LIST_MATCH);
      return;
    }

  guess_string = ipuz_crossword_get_guess_string_by_id (self->state->xword,
                                                        &self->state->clue);
  play_xword_set_filter (self, guess_string);

  if (self->filter)
    {
      guint n_hints;

      n_hints = word_list_get_n_items (self->word_list);

      if (n_hints == 0)
        hint = g_strdup (_("No words found"));
      else
        {
          guint j = g_array_index (self->filter_rand_array, gushort, self->filter_offset);
          if (n_hints == 1)
            hint = g_strdup_printf (_("Suggestion: %s"),
                                    word_list_get_word (self->word_list, j));
          else
            hint = g_strdup_printf (_("Suggestion (%u/%u): %s"),
                                    self->filter_offset + 1,
                                    n_hints,
                                    word_list_get_word (self->word_list, j));
          self->filter_offset++;

          /* Wrap around to 0 */
          if (self->filter_offset == n_hints)
            self->filter_offset = 0;
        }
    }
  else
    {
      hint = g_strdup (_("No words found"));
    }

  root = gtk_widget_get_root (GTK_WIDGET (self));
  toast_overlay = play_window_get_overlay (PLAY_WINDOW (root));
  adw_toast_set_title (ADW_TOAST (self->hint_toast), hint);
  adw_toast_overlay_add_toast (ADW_TOAST_OVERLAY (toast_overlay),
                               g_object_ref (self->hint_toast));
}

void
play_xword_set_reveal_mode (PlayXword      *self,
                            GridRevealMode  reveal_mode)
{
  g_return_if_fail (PLAY_IS_XWORD (self));

  if (self->reveal_mode == reveal_mode)
    return;

  self->reveal_mode = reveal_mode;
  self->state = grid_state_replace (self->state, grid_state_set_reveal_mode (self->state, self->reveal_mode));
  play_xword_update_states (self);
}

static void
play_xword_clear_dialog_response_cb (PlayXword   *self,
                                     const gchar *response,
                                     GtkWidget   *dialog)
{
  if (g_strcmp0 (response, "clear") == 0)
    {
      g_autoptr (IpuzPuzzle) puzzle = NULL;
      g_autoptr (IpuzGuesses) guesses = NULL;

      /* This is a little wacky; we implement clearing the dialog by
       * just clearing the guesses on the current puzzle, and
       * reloading it. The expectation that load_puzzle() will set
       * everything up correctly and get is in a valid state. Since we
       * destroy the existing puzzle set and it's the only ref to the
       * puzzle, we have to keep a ref around for load_puzzle to
       * work. */
      puzzle = play_xword_get_puzzle (self);
      g_object_ref (puzzle);
      guesses = ipuz_guesses_new_from_board (ipuz_crossword_get_board (IPUZ_CROSSWORD (puzzle)), FALSE);
      ipuz_crossword_set_guesses (IPUZ_CROSSWORD (puzzle), guesses);
      play_xword_load_puzzle (self, puzzle);
    }
}

void
play_xword_clear_puzzle (PlayXword *self)
{
  IpuzGuesses *guesses;
  gfloat percentage = 0.0;

  g_return_if_fail (PLAY_IS_XWORD (self));

  if (self->puzzle_stack == NULL)
    return;

  guesses = puzzle_stack_get_guesses (self->puzzle_stack);
  percentage = ipuz_guesses_get_percent (guesses);

  if (percentage > 0.0)
    {
      GtkRoot *root;
      GtkWidget *dialog;

      root = gtk_widget_get_root (GTK_WIDGET (self));
      dialog = adw_message_dialog_new (GTK_WINDOW (root),
                                       _("Clear this puzzle?"),
                                       _("If you clear this puzzle, all your progress will be lost."));

      adw_message_dialog_add_responses (ADW_MESSAGE_DIALOG (dialog),
                                        "cancel",  _("_Cancel"),
                                        "clear", _("C_lear"),
                                        NULL);
      adw_message_dialog_set_response_appearance (ADW_MESSAGE_DIALOG (dialog),
                                                  "clear",
                                                  ADW_RESPONSE_DESTRUCTIVE);

      g_signal_connect_swapped (dialog, "response", G_CALLBACK (play_xword_clear_dialog_response_cb), self);
      adw_message_dialog_set_default_response (ADW_MESSAGE_DIALOG (dialog), "cancel");
      adw_message_dialog_set_close_response (ADW_MESSAGE_DIALOG (dialog), "cancel");
      gtk_window_present (GTK_WINDOW (dialog));
    }
}

void
play_xword_clear_toasts (PlayXword *self)
{
  g_return_if_fail (PLAY_IS_XWORD (self));

  adw_toast_dismiss (self->game_won_toast);
  adw_toast_dismiss (self->warning_toast);
  adw_toast_dismiss (self->hint_toast);
}
