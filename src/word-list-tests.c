#include <glib.h>
#include <locale.h>
#include "gen-word-list.h"
#include "word-list.h"
#include "test-words-resources.h"


static GBytes *
get_test_words_bytes (void)
{
  GResource *resource = wordlist_get_resource ();
  GBytes *bytes;

  bytes = g_resource_lookup_data (resource,
                                  "/org/gnome/Crosswords/word-list/test-words.dict",
                                  0, NULL);
  g_assert (bytes);

  return bytes;
}

static void
dump_bytes (GBytes *bytes)
{
  GError *error = NULL;
  GSubprocess *sub = g_subprocess_new ((G_SUBPROCESS_FLAGS_STDIN_PIPE
                                        | G_SUBPROCESS_FLAGS_STDOUT_PIPE
                                        | G_SUBPROCESS_FLAGS_STDERR_MERGE),
                                       &error,
                                       "od",
                                       /* hex offsets */
                                       "-A",
                                       "x",
                                       /* hex bytes, 1 byte per integer, add printout */
                                       "-t",
                                       "x1z",
                                       /* no duplicate suppression */
                                       "-v",
                                       NULL);
  GBytes *stdout_buf = NULL;
  gpointer stdout_data;
  gsize stdout_size;

  if (!sub)
    {
      g_error ("Could not spawn od(1): %s", error->message);
      return; /* unreachable */
    }

  if (!g_subprocess_communicate (sub,
                                 bytes, /* stdin */
                                 NULL, /* cancellable */
                                 &stdout_buf,
                                 NULL, /* stderr_buf */
                                 &error))
    {
      g_error ("Error while running od(1): %s", error->message);
      return; /* unreachable */
    }

  stdout_data = g_bytes_unref_to_data (stdout_buf, &stdout_size);

  fwrite (stdout_data, 1, stdout_size, stdout);

  g_free (stdout_data);
  g_object_unref (sub);
}

static void
loads_default (void)
{
  g_autoptr (GBytes) bytes = get_test_words_bytes ();
  WordList *wl = word_list_new_from_bytes (bytes);
  word_list_set_filter (wl, "A??", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (wl), ==, 16);
  g_object_unref (wl);
}

typedef struct {
  const char *word;
  glong priority;
} WordToAdd;

static WordList *
create_test_word_list (const WordToAdd *words,
                       gsize            num_words,
                       gint             min_length,
                       gint             max_length,
                       gint             threshold)
{
  GenWordList *gen = gen_word_list_new (min_length, max_length, 0, BRODA_SCORED);

  for (gsize i = 0; i < num_words; i++)
    {
      gen_word_list_add_test_word (gen, words[i].word, words[i].priority);
    }

  gen_word_list_sort (gen);
  gen_word_list_build_charset (gen);
  gen_word_list_anagram_table (gen);
  gen_word_list_calculate_offsets (gen);
  create_anagram_fragments (gen);

  GOutputStream *stream = g_memory_output_stream_new_resizable ();
  gen_word_list_write_to_stream (gen, "Test Wordlist", stream);
  g_output_stream_close (stream, NULL, NULL);
  gen_word_list_free (gen);

  GBytes *bytes = g_memory_output_stream_steal_as_bytes (G_MEMORY_OUTPUT_STREAM (stream));
  g_object_unref (stream);

  dump_bytes (bytes);

  WordList *word_list = word_list_new_from_bytes (bytes);
  g_bytes_unref (bytes);

  return word_list;
}

static void
ascii_roundtrip (void)
{
  const WordToAdd words[] = {
    { .word = "GHIJK", .priority = 3 },
    { .word = "DEF", .priority = 1 },
    { .word = "ABC", .priority = 2 },
  };
  WordList *word_list = create_test_word_list (words, G_N_ELEMENTS (words), 3, 5, 0);

  word_list_set_filter (word_list, "???", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 2);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "ABC");
  g_assert_cmpstr (word_list_get_word (word_list, 1), ==, "DEF");
  g_assert_cmpint (word_list_get_priority (word_list, 0), ==, 2);
  g_assert_cmpint (word_list_get_priority (word_list, 1), ==, 1);
  WordIndex abc;
  WordIndex def;
  g_assert_true (word_list_get_word_index (word_list, 0, &abc));
  g_assert_true (word_list_get_word_index (word_list, 1, &def));

  word_list_set_filter (word_list, "????", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 0);

  word_list_set_filter (word_list, "?????", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 1);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "GHIJK");
  g_assert_cmpint (word_list_get_priority (word_list, 0), ==, 3);
  WordIndex ghijk;
  g_assert_true (word_list_get_word_index (word_list, 0, &ghijk));

  g_assert_cmpstr (word_list_get_indexed_word (word_list, abc), ==, "ABC");
  g_assert_cmpstr (word_list_get_indexed_word (word_list, def), ==, "DEF");
  g_assert_cmpstr (word_list_get_indexed_word (word_list, ghijk), ==, "GHIJK");

  g_assert_cmpint (word_list_get_indexed_priority (word_list, abc), ==, 2);
  g_assert_cmpint (word_list_get_indexed_priority (word_list, def), ==, 1);
  g_assert_cmpint (word_list_get_indexed_priority (word_list, ghijk), ==, 3);

  g_object_unref (word_list);
}

static void
utf8_roundtrip (void)
{
  const WordToAdd words[] = {
    { .word = "AÑO", .priority = 1 },
    { .word = "ÉSE", .priority = 1 },

    { .word = "HOLA", .priority = 2 },
    { .word = "PÂTÉ", .priority = 2 },
    { .word = "ZULU", .priority = 2 },
  };
  WordList *word_list = create_test_word_list (words, G_N_ELEMENTS (words), 3, 5, 0);

  word_list_set_filter (word_list, "???", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 2);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "AÑO");
  g_assert_cmpstr (word_list_get_word (word_list, 1), ==, "ÉSE");
  g_assert_cmpint (word_list_get_priority (word_list, 0), ==, 1);
  g_assert_cmpint (word_list_get_priority (word_list, 1), ==, 1);
  WordIndex ano;
  WordIndex ese;
  g_assert_true (word_list_get_word_index (word_list, 0, &ano));
  g_assert_true (word_list_get_word_index (word_list, 1, &ese));

  g_assert_cmpstr (word_list_get_indexed_word (word_list, ano), ==, "AÑO");
  g_assert_cmpstr (word_list_get_indexed_word (word_list, ese), ==, "ÉSE");

  g_assert_cmpint (word_list_get_indexed_priority (word_list, ano), ==, 1);
  g_assert_cmpint (word_list_get_indexed_priority (word_list, ese), ==, 1);

  word_list_set_filter (word_list, "A?O", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 1);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "AÑO");

  word_list_set_filter (word_list, "??E", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 1);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "ÉSE");

  word_list_set_filter (word_list, "????", WORD_LIST_MATCH);
  g_assert_cmpint (word_list_get_n_items (word_list), ==, 3);
  g_assert_cmpstr (word_list_get_word (word_list, 0), ==, "HOLA");
  g_assert_cmpstr (word_list_get_word (word_list, 1), ==, "PÂTÉ");
  g_assert_cmpstr (word_list_get_word (word_list, 2), ==, "ZULU");

  g_object_unref (word_list);
}

static void
anagram_test (void)
{
  g_autoptr (GBytes) bytes = get_test_words_bytes ();
  WordList *wl = word_list_new_from_bytes (bytes);

  word_list_set_filter (wl, "CAT", WORD_LIST_ANAGRAM);
  g_assert (word_list_get_n_items (wl) == 4);

  g_assert (!g_strcmp0 (word_list_get_word (wl, 0), "ACT"));
  g_assert (!g_strcmp0 (word_list_get_word (wl, 1), "CAT"));
  g_assert (!g_strcmp0 (word_list_get_word (wl, 2), "CTA"));
  g_assert (!g_strcmp0 (word_list_get_word (wl, 3), "TAC"));

  word_list_set_filter (wl, "NOTANANAGRAM", WORD_LIST_ANAGRAM);
  g_assert (word_list_get_n_items (wl) == 0);

  g_object_unref (wl);
}

//#define DEBUG_INTERSECTION
static void
intersection_test (WordList *wl,
                   const gchar *filter1,
                   guint        pos1,
                   const gchar *filter2,
                   guint        pos2,
                   const gchar *intersection_str)
{
  g_autoptr (IpuzCharset) intersecting_chars = NULL;
  g_autofree gchar *charset_str = NULL;
  g_autoptr (WordArray) word_array1 = NULL;
  g_autoptr (WordArray) word_array2 = NULL;

#ifdef DEBUG_INTERSECTION
  g_autoptr (GTimer) timer;

  timer = g_timer_new ();
  g_timer_start (timer);
#endif

  word_list_find_intersection (wl,
                               filter1, pos1,
                               filter2, pos2,
                               &intersecting_chars,
                               &word_array1,
                               &word_array2);

  if (intersecting_chars)
    charset_str = ipuz_charset_serialize (intersecting_chars);

#ifdef DEBUG_INTERSECTION
  g_timer_stop (timer);
  g_print ("Intersecting: %s (%u) with %s (%u)\n\tIntersection: %s\n\tTime Elapsed: %f\n",
           filter1, pos1,
           filter2, pos2,
           charset_str,
           g_timer_elapsed (timer, NULL));

  for (guint i = 0; i < word_array1->len; i++)
    {
      WordIndex word_index =
        word_array_index (word_array1, i);
      g_print ("%s\n", word_list_get_indexed_word (wl, word_index));
    }

  for (guint i = 0; i < word_array2->len; i++)
    {
      WordIndex word_index =
        word_array_index (word_array2, i);
      g_print ("%s\n", word_list_get_indexed_word (wl, word_index));
    }
#endif
  g_assert_cmpstr (intersection_str, ==, charset_str);

}

static void
find_intersection (void)
{
  g_autoptr (GBytes) bytes = get_test_words_bytes ();
  g_autoptr (WordList) wl = word_list_new_from_bytes (bytes);

  intersection_test (wl,
                     "???", 1,
                     NULL, 0,
                     "ABCDEFGHIJKLMNOPQRSTUVWXY");
  intersection_test (wl,
                     "D???", 1,
                     "E???Y", 2,
                     "EI");
  intersection_test (wl,
                     "E???Y", 2,
                     "D???", 1,
                     "EI");

  intersection_test (wl,
                     "???????", 2,
                     "??????????????", 13,
                     "ABCDEFGHIKLMNOPRSTUVWXYZ");
  intersection_test (wl,
                     "??????????????", 13,
                     "???????", 2,
                     "ABCDEFGHIKLMNOPRSTUVWXYZ");

  intersection_test (wl,
                     "A????E", 3,
                     "??????????", 4,
                     "AELRT");
}

int
main (int argc, char **argv)
{
  setlocale(LC_ALL, "en_US.utf8");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/word_list/loads_default", loads_default);

  g_test_add_func ("/word_list/ascii_roundtrip", ascii_roundtrip);
  g_test_add_func ("/word_list/utf8_roundtrip", utf8_roundtrip);

  g_test_add_func ("/word_list/set_anagram", anagram_test);
  g_test_add_func ("/word_list/find_intersection", find_intersection);

  return g_test_run ();
}
