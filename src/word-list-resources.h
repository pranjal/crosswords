/* word-list-resources.h
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once


G_BEGIN_DECLS


#define WORD_TYPE_LIST_RESOURCES (word_list_resources_get_type())
G_DECLARE_FINAL_TYPE (WordListResources, word_list_resources, WORD, LIST_RESOURCES, GObject);
#define WORD_LIST_RESOURCES_DEFAULT ((WordListResources*)word_list_resources_default())


GObject     *word_list_resources_default              (void);
guint        word_list_resources_get_count            (WordListResources *self);
const gchar *word_list_resources_get_id               (WordListResources *self,
                                                       guint              index);
const gchar *word_list_resources_get_display_name     (WordListResources *self,
                                                       guint              index);
const gchar *word_list_resources_get_lang             (WordListResources *self,
                                                       guint              index);
guint        word_list_resources_get_word_count       (WordListResources *self,
                                                       guint              index);
GResource   *word_list_resources_get_resource         (WordListResources *self,
                                                       guint              index);
gboolean     word_list_resources_get_is_default       (WordListResources *self,
                                                       guint              index);
void         word_list_resources_set_default          (WordListResources *self,
                                                       const gchar       *id);
GResource   *word_list_resources_get_default_resource (WordListResources *self);
const gchar *word_list_resources_get_default_id       (WordListResources *self);


G_END_DECLS
