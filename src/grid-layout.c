/* layout.h - Grid layout for crosswords
 *
 * Copyright 2021 Federico Mena Quintero <federico@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include "grid-layout.h"
#include "play-cell.h"
#include "play-style.h"


static guint default_base_sizes[] =
{
  DEFAULT_CELL_BASE_SIZE - 5,
  DEFAULT_CELL_BASE_SIZE - 2,
  DEFAULT_CELL_BASE_SIZE,
  DEFAULT_CELL_BASE_SIZE + 4,
  DEFAULT_CELL_BASE_SIZE + 8,
};

static guint default_min_sizes[] =
{
  DEFAULT_CELL_BASE_SIZE - 7,
  DEFAULT_CELL_BASE_SIZE - 6,
  DEFAULT_CELL_BASE_SIZE - 5,
  DEFAULT_CELL_BASE_SIZE - 3,
  DEFAULT_CELL_BASE_SIZE - 1,
};

static guint arrowword_base_sizes[] =
{
  DEFAULT_CELL_BASE_SIZE,
  DEFAULT_CELL_BASE_SIZE + 6,
  DEFAULT_CELL_BASE_SIZE + 12,
  DEFAULT_CELL_BASE_SIZE + 18,
  DEFAULT_CELL_BASE_SIZE + 24,
};

static guint arrowword_min_sizes[] =
{
  DEFAULT_CELL_BASE_SIZE - 2,
  DEFAULT_CELL_BASE_SIZE,
  DEFAULT_CELL_BASE_SIZE + 4,
  DEFAULT_CELL_BASE_SIZE + 8,
  DEFAULT_CELL_BASE_SIZE + 12,
};


#define LAYOUT_KIND_CELL(l)         (l->kind==LAYOUT_ITEM_KIND_CELL)
#define LAYOUT_KIND_BORDER(l)       ((l->kind==LAYOUT_ITEM_KIND_BORDER_HORIZONTAL)||(l->kind==LAYOUT_ITEM_KIND_BORDER_VERTICAL))
#define LAYOUT_KIND_VERTICAL(l)     (l->kind==LAYOUT_ITEM_KIND_BORDER_VERTICAL)
#define LAYOUT_KIND_HORIZONTAL(l)   (l->kind==LAYOUT_ITEM_KIND_BORDER_HORIZONTAL)
#define LAYOUT_KIND_INTERSECTION(l) (l->kind==LAYOUT_ITEM_KIND_INTERSECTION)

/* Internal storage of a Layout item. */
typedef struct
{
  LayoutItemKind kind;

  /* The overlay for this item. Only used for debugging purposes (eg,
   * the print function. This assumes that there's only one overlay
   * per cell which may not be true. Do not free. */
  LayoutOverlay *overlay;

  union {
    LayoutIntersection intersection;
    LayoutBorderHorizontal border_horizontal;
    LayoutBorderVertical border_vertical;
    LayoutCell cell;
    LayoutClueBlockCell clue_block_cell;
  } u;
} LayoutItem;

static IpuzCellType
cell_type_or_null (IpuzCrossword *xword, IpuzCellCoord coord, gint row_offset, gint column_offset)
{
  guint columns = ipuz_crossword_get_width (xword);
  guint rows = ipuz_crossword_get_height (xword);

  /* underflow? */
  if ((row_offset < 0 && (guint) -row_offset > coord.row)
      || (column_offset < 0 && (guint) -column_offset > coord.column))
    {
      return IPUZ_CELL_NULL;
    }

  /* overflow? */
  guint sought_row = coord.row + row_offset;
  guint sought_column = coord.column + column_offset;

  if (sought_row >= rows || sought_column >= columns)
    {
      return IPUZ_CELL_NULL;
    }

  /* okay, we have a cell */
  IpuzCellCoord sought_coord = {
    .row = sought_row,
    .column = sought_column,
  };

  IpuzCell *cell = ipuz_board_get_cell (ipuz_crossword_get_board (xword), &sought_coord);
  g_assert (cell != NULL);
  return ipuz_cell_get_cell_type (cell);
}

gboolean
layout_cell_equal (const LayoutCell *a,
                   const LayoutCell *b)
{
  return (a->cell_type == b->cell_type
          && a->css_class == b->css_class
          && gdk_rgba_equal (&a->bg_color, &b->bg_color)
          && a->bg_color_set == b->bg_color_set
          && gdk_rgba_equal (&a->text_color, &b->text_color)
          && a->text_color_set == b->text_color_set
          && g_strcmp0 (a->main_text, b->main_text) == 0);
}

gboolean
grid_coord_equal (const GridCoord *a,
                  const GridCoord *b)
{
  return (a->column == b->column
          && a->row == b->row);
}

gboolean
layout_overlay_equal (const LayoutOverlay *a,
                      const LayoutOverlay *b)
{
  if (a->kind != b->kind)
    {
      return FALSE;
    }

  switch (a->kind) {
  case LAYOUT_OVERLAY_KIND_BARRED:
    return grid_coord_equal (&a->u.barred.coord, &b->u.barred.coord);

  case LAYOUT_OVERLAY_KIND_ENUMERATION_SPACE:
  case LAYOUT_OVERLAY_KIND_ENUMERATION_DASH:
  case LAYOUT_OVERLAY_KIND_ENUMERATION_APOSTROPHE:
  case LAYOUT_OVERLAY_KIND_ENUMERATION_PERIOD:
    return grid_coord_equal (&a->u.enumeration.coord, &b->u.enumeration.coord);

  default:
    g_assert_not_reached ();
    return FALSE;
  }
}

/* In the diagram below, all grid coordinates XYZ will map to the
 * cell coordinate C - (0, 0) in this example.
 *
 *    0 2 4 6
 * 0  XY+-+-+
 *    ZC| | |
 * 2  +-+-+-+
 *    | | | |
 * 4  +-+-+-+
 *    | | | |
 * 6  +-+-+-+
 */
static IpuzCellCoord
cell_coord_from_grid (GridCoord grid_coord)
{
  IpuzCellCoord cell_coord = {
    .row = grid_coord.row / 2,
    .column = grid_coord.column / 2,
  };

  return cell_coord;
}

static GridCoord
grid_coord_from_cell (IpuzCellCoord cell_coord)
{
  GridCoord grid_coord = {
    .row = cell_coord.row * 2 + 1,
    .column = cell_coord.column * 2 + 1,
  };

  return grid_coord;
}

/* Keep these next three functions in sync */
static LayoutItem
compute_intersection (GridState *state,
                      GridCoord  grid_coord)
{
  IpuzCellCoord cell_coord = cell_coord_from_grid (grid_coord);

  /* Compute whether the intersection is filled based on the four cells around it */

  IpuzCellType top_left     = cell_type_or_null (state->xword, cell_coord, -1, -1);
  IpuzCellType top_right    = cell_type_or_null (state->xword, cell_coord, -1,  0);
  IpuzCellType bottom_left  = cell_type_or_null (state->xword, cell_coord,  0, -1);
  IpuzCellType bottom_right = cell_type_or_null (state->xword, cell_coord,  0,  0);

  gboolean filled = (top_left != IPUZ_CELL_NULL
                     || top_right != IPUZ_CELL_NULL
                     || bottom_left != IPUZ_CELL_NULL
                     || bottom_right != IPUZ_CELL_NULL);

  LayoutItem item = {
    .kind = LAYOUT_ITEM_KIND_INTERSECTION,
    .u.intersection = {
      .filled = filled,
      .css_class = LAYOUT_BORDER_STYLE_DARK,
    }
  };

  if (top_left == IPUZ_CELL_NORMAL &&
      top_right == IPUZ_CELL_NORMAL &&
      bottom_left == IPUZ_CELL_NORMAL &&
      bottom_right == IPUZ_CELL_NORMAL)
    {
      if (ACROSTIC_MAIN_GRID_HAS_FOCUS (state))
        item.u.intersection.css_class = LAYOUT_BORDER_STYLE_SELECTED;
      else
        item.u.intersection.css_class = LAYOUT_BORDER_STYLE_NORMAL;
    }

  if (ACROSTIC_MAIN_GRID_HAS_FOCUS (state) &&
      (top_left == IPUZ_CELL_NULL ||
       top_right == IPUZ_CELL_NULL ||
       bottom_left == IPUZ_CELL_NULL ||
       bottom_right == IPUZ_CELL_NULL))
    item.u.intersection.css_class = LAYOUT_BORDER_STYLE_GRID_SELECTED;

  if (!filled)
    {
      if (GRID_STATE_HAS_EDIT_CELLS (state) &&
          !GRID_STATE_HAS_SHOW_GUESS (state))
        item.u.intersection.css_class = LAYOUT_BORDER_STYLE_VISIBLE_NULL;
      else
        item.u.intersection.css_class = LAYOUT_BORDER_STYLE_UNSET;
    }

  return item;
}

static LayoutItem
compute_border_horizontal (GridState *state,
                           GridCoord  grid_coord)
{
  IpuzCellCoord cell_coord = cell_coord_from_grid (grid_coord);

  /* Compute whether the border is filled based on the cells above and below it */

  IpuzCellType top    = cell_type_or_null (state->xword, cell_coord, -1, 0);
  IpuzCellType bottom = cell_type_or_null (state->xword, cell_coord,  0, 0);

  gboolean filled = (top != IPUZ_CELL_NULL
                     || bottom != IPUZ_CELL_NULL);

  LayoutItem item = {
    .kind = LAYOUT_ITEM_KIND_BORDER_HORIZONTAL,
    .u.border_horizontal = {
      .css_class = LAYOUT_BORDER_STYLE_DARK,
      .filled = filled,
    }
  };

  if (top == IPUZ_CELL_NORMAL && bottom == IPUZ_CELL_NORMAL)
    item.u.border_horizontal.css_class = LAYOUT_BORDER_STYLE_NORMAL;

  if (ACROSTIC_MAIN_GRID_HAS_FOCUS (state) && (top == IPUZ_CELL_NULL || bottom == IPUZ_CELL_NULL))
    item.u.border_horizontal.css_class = LAYOUT_BORDER_STYLE_GRID_SELECTED;

  if (!filled)
    {
      if (GRID_STATE_HAS_EDIT_CELLS (state) &&
          !GRID_STATE_HAS_SHOW_GUESS (state))
        item.u.intersection.css_class = LAYOUT_BORDER_STYLE_VISIBLE_NULL;
      else
        item.u.intersection.css_class = LAYOUT_BORDER_STYLE_UNSET;
    }

  return item;
}

static LayoutItem
compute_border_vertical (GridState *state,
                         GridCoord  grid_coord)
{
  IpuzCellCoord cell_coord = cell_coord_from_grid (grid_coord);

  /* Compute whether the border is filled based on the cells to its left and right */

  IpuzCellType left  = cell_type_or_null (state->xword, cell_coord, 0, -1);
  IpuzCellType right = cell_type_or_null (state->xword, cell_coord, 0,  0);

  gboolean filled = (left != IPUZ_CELL_NULL
                     || right != IPUZ_CELL_NULL);

  LayoutItem item = {
    .kind = LAYOUT_ITEM_KIND_BORDER_VERTICAL,
    .u.border_vertical = {
      .css_class = LAYOUT_BORDER_STYLE_DARK,
      .filled = filled,
    }
  };

  if (left == IPUZ_CELL_NORMAL && right == IPUZ_CELL_NORMAL)
    item.u.border_horizontal.css_class = LAYOUT_BORDER_STYLE_NORMAL;

  if (ACROSTIC_MAIN_GRID_HAS_FOCUS (state) && (left == IPUZ_CELL_NULL || right == IPUZ_CELL_NULL))
    item.u.border_horizontal.css_class = LAYOUT_BORDER_STYLE_GRID_SELECTED;

  if (!filled)
    {
      if (GRID_STATE_HAS_EDIT_CELLS (state) &&
          !GRID_STATE_HAS_SHOW_GUESS (state))
        item.u.intersection.css_class = LAYOUT_BORDER_STYLE_VISIBLE_NULL;
      else
        item.u.intersection.css_class = LAYOUT_BORDER_STYLE_UNSET;
    }

  return item;
}

/* Result of processing one cell.  These values must be added to the GridLayout by the caller. */
typedef struct
{
  LayoutItem item;
  GArray *overlays;
} CellResult;

static void
add_barred_overlay (GArray *overlays, GridCoord coord)
{
  LayoutOverlay overlay = {
    .kind = LAYOUT_OVERLAY_KIND_BARRED,
    .u.barred.coord = coord,
  };

  g_array_append_val (overlays, overlay);
}

static GridCoord
grid_coord_offset (GridCoord coord, gint row_offset, gint col_offset)
{
  GridCoord c = {
    .row = coord.row + row_offset,
    .column = coord.column + col_offset,
  };
  return c;
}

static GridCoord
above (GridCoord coord)
{
  return grid_coord_offset (coord, -1, 0);
}

static GridCoord
below (GridCoord coord)
{
  return grid_coord_offset (coord, 1, 0);
}

static GridCoord
left (GridCoord coord)
{
  return grid_coord_offset (coord, 0, -1);
}

static GridCoord
right (GridCoord coord)
{
  return grid_coord_offset (coord, 0, 1);
}

static void
add_barred_overlays_for_cell (GArray *overlays, IpuzStyle *style, GridCoord grid_coord)
{
  IpuzStyleSides sides = ipuz_style_get_barred (style);

  if (sides & IPUZ_STYLE_SIDES_TOP)
    add_barred_overlay (overlays, above (grid_coord));

  if (sides & IPUZ_STYLE_SIDES_BOTTOM)
    add_barred_overlay (overlays, below (grid_coord));

  if (sides & IPUZ_STYLE_SIDES_LEFT)
    add_barred_overlay (overlays, left (grid_coord));

  if (sides & IPUZ_STYLE_SIDES_RIGHT)
    add_barred_overlay (overlays, right (grid_coord));
}


typedef struct
{
  gchar *cluenum_text;
  IpuzStyleMark cluenum_location;
} MarkTuple;

static void
compute_cell_mark_func (IpuzStyle     *style,
                        IpuzStyleMark  mark,
                        const gchar   *label,
                        gpointer       user_data)
{
  MarkTuple *tuple = user_data;

  g_clear_pointer (&tuple->cluenum_text, g_free);
  tuple->cluenum_text = g_strdup (label);
  tuple->cluenum_location = mark;
}

static CellResult
compute_cell (GridState  *state,
              GridLayout *layout,
              GridCoord   grid_coord)
{
  IpuzCellCoord cell_coord = cell_coord_from_grid (grid_coord);
  IpuzGuesses *guesses;

  IpuzCell *cell = ipuz_board_get_cell (ipuz_crossword_get_board (state->xword), &cell_coord);
  g_assert (cell != NULL);
  IpuzCellType cell_type = ipuz_cell_get_cell_type (cell);

  IpuzStyle *style = ipuz_cell_get_style (cell);
  IpuzStyleDivided divided = IPUZ_STYLE_DIVIDED_NONE;
  IpuzStyleShape shapebg = IPUZ_STYLE_SHAPE_NONE;
  gboolean bg_color_set = FALSE;
  gboolean text_color_set = FALSE;
  GdkRGBA bg_color;
  GdkRGBA text_color;
  LayoutItemCellForegroundStyle foreground_css_class;
  LayoutItemCellStyle css_class;
  IpuzClue *clue;
  GArray *overlays = g_array_new (FALSE, TRUE, sizeof (LayoutOverlay));
  const char *initial_val = ipuz_cell_get_initial_val (cell);
  const char *main_text = NULL;
  g_autofree gchar *cluenum_text = NULL;
  IpuzStyleMark cluenum_location = IPUZ_STYLE_MARK_TL;
  gboolean editable = GRID_STATE_HAS_EDIT_CELLS (state);


  if (ACROSTIC_MAIN_GRID_HAS_FOCUS (state))
    {
      clue = GET_QUOTE_CLUE (state->xword);
    }
  else if (state->clue.direction != IPUZ_CLUE_DIRECTION_NONE && !IPUZ_IS_ACROSTIC (state->xword))
    {
      clue = ipuz_crossword_get_clue_by_id (state->xword, &state->clue);
    }
  else
    {
      clue = NULL;
    }

  foreground_css_class = LAYOUT_ITEM_FOREGROUND_STYLE_NORMAL;
  css_class = LAYOUT_ITEM_STYLE_NULL;

  if (GRID_STATE_HAS_EDIT_CELLS (state) &&
      !GRID_STATE_HAS_SHOW_GUESS (state))
    css_class = LAYOUT_ITEM_STYLE_VISIBLE_NULL;

  if (IPUZ_CELL_IS_NORMAL (cell))
    {
      css_class = LAYOUT_ITEM_STYLE_NORMAL;
    }
  else if (IPUZ_CELL_IS_BLOCK (cell))
    {
      css_class = LAYOUT_ITEM_STYLE_BLOCK;
      if (GRID_STATE_HAS_SHOW_GUESS (state))
        editable = FALSE;
    }
  else
    {
      if (GRID_STATE_HAS_SHOW_GUESS (state))
        editable = FALSE;
    }

  if (GRID_STATE_CURSOR_SET (state) &&
      GRID_STATE_HAS_USE_CURSOR (state) &&
      !GRID_STATE_HAS_SELECTABLE (state))
    {
      if (cell_coord.row == state->cursor.row && cell_coord.column == state->cursor.column)
        {
          if (IPUZ_CELL_IS_BLOCK (cell))
            css_class = LAYOUT_ITEM_STYLE_FOCUSED_BLOCK;
          else
            css_class = LAYOUT_ITEM_STYLE_FOCUSED;
        }
      else if (clue && ipuz_clue_contains_cell (clue, cell_coord))
        {
          css_class = LAYOUT_ITEM_STYLE_SELECTED;
        }
    }

  if (style)
    {
      if (ipuz_style_get_highlight (style))
        {
          if (css_class == LAYOUT_ITEM_STYLE_SELECTED)
            css_class = LAYOUT_ITEM_STYLE_HIGHLIGHTED_SELECTED;
          else if (css_class == LAYOUT_ITEM_STYLE_NORMAL)
            css_class = LAYOUT_ITEM_STYLE_HIGHLIGHTED;
        }
    }

  /* Main text */
  guesses = ipuz_crossword_get_guesses (state->xword);
  const gchar *guess = NULL;
  const gchar *solution = ipuz_cell_get_solution (cell);

  if (guesses)
    guess = ipuz_guesses_get_guess (guesses, &cell_coord);

  if (GRID_STATE_HAS_SHOW_GUESS (state))
    {
      if (guess)
        {
          if (GRID_STATE_HAS_EDIT_CELLS (state) &&
              state->reveal_mode != GRID_REVEAL_NONE &&
              g_strcmp0 (guess, solution))
            css_class = LAYOUT_ITEM_STYLE_ERROR;

          main_text = guess;
        }
    }
  else
    {
      main_text = solution;
      if (solution == NULL && guess != NULL)
        {
          main_text = guess;
          foreground_css_class = LAYOUT_ITEM_FOREGROUND_STYLE_PENCIL;
        }
    }

  if (GRID_STATE_HAS_SELECTABLE (state))
    {
      guint unused;
      if (cell_array_find (state->selected_cells, cell_coord, &unused))
        css_class = LAYOUT_ITEM_STYLE_SELECTED;
    }

  /* cluenum */
  if (style)
    {
      MarkTuple mark_tuple = {0, };

      ipuz_style_foreach_mark (style, compute_cell_mark_func, &mark_tuple);
      if (mark_tuple.cluenum_text != NULL)
        {
          cluenum_text = mark_tuple.cluenum_text;
          cluenum_location = mark_tuple.cluenum_location;
        }
    }
  if (cluenum_text == NULL && ipuz_cell_get_number (cell))
    {
      cluenum_text = g_strdup_printf ("%d",ipuz_cell_get_number (cell));
    }
  else if (cluenum_text == NULL && ipuz_cell_get_label (cell))
    {
      cluenum_text = g_strdup (ipuz_cell_get_label (cell));
    }

  /* colors */
  if (style)
    {
      const gchar *bg_color_str = ipuz_style_get_bg_color (style);
      if (bg_color_str)
        {
          bg_color_set = gdk_rgba_parse (&bg_color, bg_color_str);
        }
      else
        {
          bg_color_set = FALSE;
        }

      const gchar *text_color_str = ipuz_style_get_text_color (style);
      if (text_color_str)
        {
          text_color_set = gdk_rgba_parse (&text_color, text_color_str);
        }
      else
        {
          text_color_set = FALSE;
        }

      divided = ipuz_style_get_divided (style);
      shapebg = ipuz_style_get_shapebg (style);
      add_barred_overlays_for_cell (overlays, style, grid_coord);
    }
  else
    {
      bg_color_set = FALSE;
      text_color_set = FALSE;
    }

  /* FIXME(refactor): This could really be cleaned up. */
  /* Set some bg_colors even if we don't load them. This is for the
   * thumbnail svg.
   */
  if (cell_type == IPUZ_CELL_BLOCK)
    {
      if (!bg_color_set)
        {
          bg_color = *(get_color (BLOCK));
        }
      if (!text_color_set)
	{
          text_color = *(get_color (NORMAL));
	}
    }
  else
    {
      if (!bg_color_set)
        {
          bg_color = *(get_color (WHITE));
        }
      if (!text_color_set)
        {
          text_color = *(get_color (BLACK));
        }
    }

  if (initial_val)
    {
      darken_color (&bg_color);
      css_class = LAYOUT_ITEM_STYLE_INITIAL_VAL;
      foreground_css_class = LAYOUT_ITEM_FOREGROUND_STYLE_INITIAL_VAL;
      main_text = initial_val;
    }

  if (main_text == NULL)
    main_text = "";

  LayoutItem item = {
    .kind = LAYOUT_ITEM_KIND_CELL,
    .u.cell = {
      .cell_type = cell_type,
      .css_class = css_class,
      .foreground_css_class = foreground_css_class,
      .bg_color = bg_color,
      .bg_color_set = bg_color_set,
      .text_color = text_color,
      .text_color_set = text_color_set,
      .main_text = g_string_chunk_insert (layout->strings, main_text),
      .cluenum_text = cluenum_text ? g_string_chunk_insert (layout->strings, cluenum_text) : NULL,
      .cluenum_location = cluenum_location,
      .divided = divided,
      .shapebg = shapebg,
      .editable = editable,
    }
  };

  CellResult result = {
    .item = item,
    .overlays = overlays,
  };

  return result;
}

/* Infallible version: always returns a valid LayoutItem *, asserts that the coord is in range */
static LayoutItem *
get_layout_item (GridLayout *layout,
                 GridCoord    coord)
{
  g_assert (coord.row < layout->grid_rows);
  g_assert (coord.column < layout->grid_columns);

  return &g_array_index (layout->grid, LayoutItem, coord.row * layout->grid_columns + coord.column);
}

/* Fallible version: returns NULL if the coord is out of range */
static LayoutItem *
get_layout_item_or_null (GridLayout  *layout,
                         GridCoord    coord)
{
  if (coord.row >= layout->grid_rows ||
      coord.column >= layout->grid_columns)
    {
      return NULL;
    }

  return &g_array_index (layout->grid, LayoutItem, coord.row * layout->grid_columns + coord.column);
}

static gboolean
compute_intersection_css (GridLayout *layout,
                          LayoutItem *item,
                          GridCoord   coord)
{
  LayoutItem *ne_item = NULL; /* left */
  LayoutItem *se_item = NULL; /* right */
  LayoutItem *sw_item = NULL; /* top */
  LayoutItem *nw_item = NULL; /* bottom */
  LayoutItemCellStyle ne_style = LAYOUT_ITEM_STYLE_NULL;
  LayoutItemCellStyle se_style = LAYOUT_ITEM_STYLE_NULL;
  LayoutItemCellStyle sw_style = LAYOUT_ITEM_STYLE_NULL;
  LayoutItemCellStyle nw_style = LAYOUT_ITEM_STYLE_NULL;

  if (! LAYOUT_KIND_INTERSECTION (item))
    return FALSE;

  if (coord.column > 0 && coord.row > 0)
    nw_item = get_layout_item_or_null (layout, above (left (coord)));
  if (coord.column > 0)
    sw_item = get_layout_item_or_null (layout, below (left (coord)));
  if (coord.row > 0)
    ne_item = get_layout_item_or_null (layout, above (right (coord)));
  se_item = get_layout_item_or_null (layout, below (right (coord)));

  if (ne_item)
    ne_style = ne_item->u.cell.css_class;
  if (nw_item)
    nw_style = nw_item->u.cell.css_class;
  if (se_item)
    se_style = se_item->u.cell.css_class;
  if (sw_item)
    sw_style = sw_item->u.cell.css_class;

  if (ne_style == LAYOUT_ITEM_STYLE_FOCUSED ||
      nw_style == LAYOUT_ITEM_STYLE_FOCUSED ||
      se_style == LAYOUT_ITEM_STYLE_FOCUSED ||
      sw_style == LAYOUT_ITEM_STYLE_FOCUSED)
    {
      item->u.intersection.css_class = LAYOUT_BORDER_STYLE_FOCUSED;
      return TRUE;
    }


  if (ne_style == LAYOUT_ITEM_STYLE_INITIAL_VAL ||
      nw_style == LAYOUT_ITEM_STYLE_INITIAL_VAL ||
      se_style == LAYOUT_ITEM_STYLE_INITIAL_VAL ||
      sw_style == LAYOUT_ITEM_STYLE_INITIAL_VAL)
    {
      if ((ne_style != LAYOUT_ITEM_STYLE_NULL) &&
          (ne_style != LAYOUT_ITEM_STYLE_BLOCK) &&
          (nw_style != LAYOUT_ITEM_STYLE_NULL) &&
          (nw_style != LAYOUT_ITEM_STYLE_BLOCK) &&
          (se_style != LAYOUT_ITEM_STYLE_NULL) &&
          (se_style != LAYOUT_ITEM_STYLE_BLOCK) &&
          (sw_style != LAYOUT_ITEM_STYLE_NULL) &&
          (sw_style != LAYOUT_ITEM_STYLE_BLOCK))
        {
          item->u.intersection.css_class = LAYOUT_BORDER_STYLE_INITIAL_VAL;
          return TRUE;
        }
    }

  if (LAYOUT_ITEM_CSS_GUESSABLE (ne_style) &&
      LAYOUT_ITEM_CSS_GUESSABLE (nw_style) &&
      LAYOUT_ITEM_CSS_GUESSABLE (se_style) &&
      LAYOUT_ITEM_CSS_GUESSABLE (sw_style))
    {
      if (ne_item->u.cell.bg_color_set ||
          nw_item->u.cell.bg_color_set ||
          se_item->u.cell.bg_color_set ||
          sw_item->u.cell.bg_color_set)
        {
          GdkRGBA color = {0, };
          gint count = 0;

          /* Terrible blending. Basically only works when the colors
           * are the same */
          if (ne_item->u.cell.bg_color_set)
            {
              color.red += ne_item->u.cell.bg_color.red;
              color.green += ne_item->u.cell.bg_color.green;
              color.blue += ne_item->u.cell.bg_color.blue;
              color.alpha += ne_item->u.cell.bg_color.alpha;
              count++;
            }
          if (nw_item->u.cell.bg_color_set)
            {
              color.red += nw_item->u.cell.bg_color.red;
              color.green += nw_item->u.cell.bg_color.green;
              color.blue += nw_item->u.cell.bg_color.blue;
              color.alpha += nw_item->u.cell.bg_color.alpha;
              count++;
            }
          if (se_item->u.cell.bg_color_set)
            {
              color.red += se_item->u.cell.bg_color.red;
              color.green += se_item->u.cell.bg_color.green;
              color.blue += se_item->u.cell.bg_color.blue;
              color.alpha += se_item->u.cell.bg_color.alpha;
              count++;
            }
          if (sw_item->u.cell.bg_color_set)
            {
              color.red += sw_item->u.cell.bg_color.red;
              color.green += sw_item->u.cell.bg_color.green;
              color.blue += sw_item->u.cell.bg_color.blue;
              color.alpha += sw_item->u.cell.bg_color.alpha;
              count++;
            }
          color.red /= count;
          color.green /= count;
          color.blue /= count;
          color.alpha /= count;
          item->u.intersection.bg_color_set = TRUE;
          item->u.intersection.bg_color = color;
          darken_color (&item->u.intersection.bg_color);
        }
    }

  return FALSE;
}

static gboolean
check_matching_style (LayoutItemCellStyle style1,
                 LayoutItemCellStyle style2,
                 LayoutItemCellStyle check1)
{
  return (style1 == check1 && style2 == check1);
}

static gboolean
check_two_styles (LayoutItemCellStyle style1,
                  LayoutItemCellStyle style2,
                  LayoutItemCellStyle check1,
                  LayoutItemCellStyle check2)
{
  return ((style1 == check1 && style2 == check2) || (style1 == check2 && style2 == check1));
}

static GdkRGBA
blend_color (GdkRGBA a, GdkRGBA b)
{
  GdkRGBA retval;

  retval.red = (a.red + b.red) /2.0;
  retval.green = (a.green + b.green) /2.0;
  retval.blue = (a.blue + b.blue) /2.0;
  retval.alpha = (a.alpha + b.alpha) /2.0;

  return retval;
}

static gboolean
compute_border_css (GridLayout *layout,
                    LayoutItem *item,
                    GridCoord   coord)
{
  LayoutItem *item1 = NULL;
  LayoutItem *item2 = NULL;
  LayoutItemCellStyle style1 = LAYOUT_ITEM_STYLE_UNSET;
  LayoutItemCellStyle style2 = LAYOUT_ITEM_STYLE_UNSET;
  LayoutItemBorderStyle new_css_class;
  GdkRGBA bg_color;
  gboolean bg_color_set = FALSE;

  if (! LAYOUT_KIND_BORDER (item))
    return FALSE;

  if (LAYOUT_KIND_VERTICAL (item))
    {
      if (coord.column > 0)
        item1 = get_layout_item (layout, left (coord));
      item2 = get_layout_item_or_null (layout, right (coord));
      new_css_class = item->u.border_vertical.css_class;
    }
  else
    {
      if (coord.row > 0)
        item1 = get_layout_item (layout, above (coord));
      item2 = get_layout_item_or_null (layout, below (coord));
      new_css_class = item->u.border_horizontal.css_class;
    }

  if (item1)
    style1 = item1->u.cell.css_class;
  if (item2)
    style2 = item2->u.cell.css_class;


  /* Determine the style of the border, through a series of
   * heuristics. Note, some of these may match multiple times. We go
   * in order of increasing priority */
  if (check_matching_style (style1, style2, LAYOUT_ITEM_STYLE_SELECTED))
    new_css_class = LAYOUT_BORDER_STYLE_SELECTED;

  if (check_two_styles (style1, style2, LAYOUT_ITEM_STYLE_INITIAL_VAL, LAYOUT_ITEM_STYLE_NORMAL))
    new_css_class = LAYOUT_BORDER_STYLE_INITIAL_VAL;

  if (check_two_styles (style1, style2, LAYOUT_ITEM_STYLE_INITIAL_VAL, LAYOUT_ITEM_STYLE_SELECTED))
    new_css_class = LAYOUT_BORDER_STYLE_INITIAL_VAL;

  if (check_two_styles (style1, style2, LAYOUT_ITEM_STYLE_INITIAL_VAL, LAYOUT_ITEM_STYLE_HIGHLIGHTED))
    new_css_class = LAYOUT_BORDER_STYLE_INITIAL_VAL;

  if (check_two_styles (style1, style2, LAYOUT_ITEM_STYLE_INITIAL_VAL, LAYOUT_ITEM_STYLE_HIGHLIGHTED_SELECTED))
    new_css_class = LAYOUT_BORDER_STYLE_INITIAL_VAL;

  if (check_matching_style (style1, style2, LAYOUT_ITEM_STYLE_INITIAL_VAL))
    new_css_class = LAYOUT_BORDER_STYLE_INITIAL_VAL;

  if (check_matching_style (style1, style2, LAYOUT_ITEM_STYLE_HIGHLIGHTED))
    new_css_class = LAYOUT_BORDER_STYLE_HIGHLIGHTED;

  if (check_two_styles (style1, style2, LAYOUT_ITEM_STYLE_HIGHLIGHTED, LAYOUT_ITEM_STYLE_HIGHLIGHTED_SELECTED))
    new_css_class = LAYOUT_BORDER_STYLE_HIGHLIGHTED;

  if (check_two_styles (style1, style2, LAYOUT_ITEM_STYLE_SELECTED, LAYOUT_ITEM_STYLE_HIGHLIGHTED_SELECTED))
    new_css_class = LAYOUT_BORDER_STYLE_SELECTED;

  /* This catches borders next to colored squares, only when they're not currently focused etc */
  if ((style1 == LAYOUT_ITEM_STYLE_NORMAL && item1->u.cell.bg_color_set && LAYOUT_ITEM_CSS_GUESSABLE (style2)) ||
      (style2 == LAYOUT_ITEM_STYLE_NORMAL && item2->u.cell.bg_color_set && LAYOUT_ITEM_CSS_GUESSABLE (style1)))
    {
      if (item1->u.cell.bg_color_set && item2->u.cell.bg_color_set)
        bg_color = blend_color (item1->u.cell.bg_color, item2->u.cell.bg_color);
      else if (item1->u.cell.bg_color_set)
        bg_color = item1->u.cell.bg_color;
      else
        bg_color = item2->u.cell.bg_color;
      bg_color_set = TRUE;
    }

  /* Focused always takes top priority */
  if (style1 == LAYOUT_ITEM_STYLE_FOCUSED ||
      style2 == LAYOUT_ITEM_STYLE_FOCUSED)
    {
      bg_color_set = FALSE;
      new_css_class = LAYOUT_BORDER_STYLE_FOCUSED;
    }


  /* Handle whatever we set the style to */
  if (LAYOUT_KIND_VERTICAL (item))
    {
      item->u.border_vertical.css_class = new_css_class;
      if (bg_color_set)
        {
          item->u.border_vertical.bg_color_set = TRUE;
          item->u.border_vertical.bg_color = bg_color;
          darken_color (&item->u.border_vertical.bg_color);
        }
    }
  else
    {
      item->u.border_horizontal.css_class = new_css_class;
      if (bg_color_set)
        {
          item->u.border_horizontal.bg_color_set = TRUE;
          item->u.border_horizontal.bg_color = bg_color;
          darken_color (&item->u.border_horizontal.bg_color);
        }
    }

  return TRUE;
}

static void
compute_border_colors (GridLayout *layout)
{
  guint grid_row, grid_column;

  g_assert (layout);

  for (grid_row = 0; grid_row < layout->grid_rows; grid_row++)
    {
      for (grid_column = 0; grid_column < layout->grid_columns; grid_column++)
        {
          GridCoord coord = { .row = grid_row, .column = grid_column };
          LayoutItem *item = NULL;

          item = get_layout_item (layout, coord);
          if (item->kind == LAYOUT_ITEM_KIND_CELL)
            continue;

          /* Initial borders */
          if (compute_border_css (layout, item, coord))
            continue;
          if (compute_intersection_css (layout, item, coord))
            continue;
        }
    }
}

typedef struct
{
  const GArray *cells;
  IpuzClue *clue;
  GridLayout *layout;
} DelimTuple;

static void
enumeration_delim_foreach (IpuzEnumeration *enumeration,
                           IpuzDeliminator  delim,
                           guint            grid_offset,
                           gboolean         final_word,
                           gpointer         user_data)
{
  DelimTuple *tuple = user_data;
  IpuzCellCoord cell_coord;
  guint cell_offset;
  LayoutOverlay overlay;

  if (final_word)
    return;

  cell_offset = (grid_offset - 1)/2;
  if (cell_offset >= tuple->cells->len)
    return;

  cell_coord = g_array_index (tuple->cells, IpuzCellCoord, cell_offset);

  if (tuple->clue->direction == IPUZ_CLUE_DIRECTION_ACROSS)
    {
      overlay.u.enumeration.coord.row = cell_coord.row * 2 + 1;
      overlay.u.enumeration.coord.column = cell_coord.column * 2 + 2;
    }
  else
    {
      overlay.u.enumeration.coord.row = cell_coord.row * 2 + 2;
      overlay.u.enumeration.coord.column = cell_coord.column * 2 + 1;
    }
  if (delim == IPUZ_DELIMINATOR_WORD_BREAK)
    overlay.kind = LAYOUT_OVERLAY_KIND_ENUMERATION_SPACE;
  else if (delim == IPUZ_DELIMINATOR_DASH)
    overlay.kind = LAYOUT_OVERLAY_KIND_ENUMERATION_DASH;
  else if (delim == IPUZ_DELIMINATOR_APOSTROPHE)
    overlay.kind = LAYOUT_OVERLAY_KIND_ENUMERATION_APOSTROPHE;
  else if (delim == IPUZ_DELIMINATOR_PERIOD)
    overlay.kind = LAYOUT_OVERLAY_KIND_ENUMERATION_PERIOD;

  g_array_append_val (tuple->layout->overlays, overlay);
}

/* FIXME(enumerations): We don't handle enumerations which continue
 * across multiple clues
 */
static void
compute_enumeration (GridLayout *layout,
                     IpuzClue   *clue)
{
  g_autoptr (IpuzEnumeration) enumeration = NULL;
  DelimTuple tuple = {
    .cells = ipuz_clue_get_cells (clue),
    .clue = clue,
    .layout = layout,
  };

  /* handle weird puzzles */
  if (tuple.cells == NULL)
    return;
  enumeration = ipuz_clue_get_enumeration (clue);

  if (enumeration)
    ipuz_enumeration_foreach_delim (enumeration,
                                    enumeration_delim_foreach,
                                    &tuple);
}


static void
compute_enumerations (GridState  *state,
                      GridLayout *layout)
{
  guint i;
  gboolean showenumerations;
  IpuzClueId clue_id;

  g_assert (state);
  g_assert (state->xword);
  g_assert (layout);

  g_object_get (state->xword,
                "showenumerations", &showenumerations,
                NULL);

  if (!showenumerations)
    return;

  for (i = 0; i < ipuz_crossword_get_n_clues (state->xword, IPUZ_CLUE_DIRECTION_ACROSS); i++)
    {
      IpuzClue *clue;

      clue_id.direction = IPUZ_CLUE_DIRECTION_ACROSS;
      clue_id.index = i;

      clue = ipuz_crossword_get_clue_by_id (state->xword, &clue_id);
      compute_enumeration (layout, clue);
    }

  for (i = 0; i < ipuz_crossword_get_n_clues (state->xword, IPUZ_CLUE_DIRECTION_DOWN); i++)
    {
      IpuzClue *clue;

      clue_id.direction = IPUZ_CLUE_DIRECTION_DOWN;
      clue_id.index = i;

      clue = ipuz_crossword_get_clue_by_id (state->xword, &clue_id);
      compute_enumeration (layout, clue);
    }
}

typedef struct
{
  IpuzClue *selected_clue;
  GridLayout *layout;
} ArrowwordForeachTuple;


static void
arrowword_foreach_func (IpuzArrowword          *self,
                        IpuzCellCoord          *block_coord,
                        IpuzClue               *clue,
                        IpuzArrowwordPlacement  placement,
                        IpuzArrowwordArrow      arrow,
                        ArrowwordForeachTuple  *tuple)
{
  GridCoord coord = grid_coord_from_cell (*block_coord);
  LayoutItem *item = get_layout_item (tuple->layout, coord);

  /* We should only ever expect CELLs to be promoted to arrowword clue
   * blocks */
  g_assert (item->kind == LAYOUT_ITEM_KIND_CELL ||
            item->kind == LAYOUT_ITEM_KIND_CLUE_BLOCK_CELL);

  /* Clear the old values, which might be garbage */
  if (item->kind == LAYOUT_ITEM_KIND_CELL)
    {
      item->kind = LAYOUT_ITEM_KIND_CLUE_BLOCK_CELL;
      item->u.clue_block_cell.divided = IPUZ_STYLE_DIVIDED_NONE;
      item->u.clue_block_cell.main_text = NULL;
      item->u.clue_block_cell.secondary_text = NULL;
      item->u.clue_block_cell.top_arrow = IPUZ_ARROWWORD_ARROW_NONE;
      item->u.clue_block_cell.bottom_arrow = IPUZ_ARROWWORD_ARROW_NONE;
      item->u.clue_block_cell.split_highlight_state = LAYOUT_SPLIT_HIGHLIGHT_NONE;
    }

  item->u.clue_block_cell.css_class = LAYOUT_ITEM_STYLE_CLUE_BLOCK;
  if (placement == IPUZ_ARROWWORD_PLACEMENT_TOP ||
      placement == IPUZ_ARROWWORD_PLACEMENT_FILL)
    {
      item->u.clue_block_cell.main_text =
        g_string_chunk_insert (tuple->layout->strings,
                               ipuz_clue_get_clue_text (clue));
      item->u.clue_block_cell.top_arrow = arrow;
    }
  else
    {
      item->u.clue_block_cell.divided = IPUZ_STYLE_DIVIDED_HORIZ;
      item->u.clue_block_cell.secondary_text =
        g_string_chunk_insert (tuple->layout->strings,
                               ipuz_clue_get_clue_text (clue));
      item->u.clue_block_cell.bottom_arrow = arrow;
    }

  if (ipuz_clue_equal (clue, tuple->selected_clue))
    {
      if (placement == IPUZ_ARROWWORD_PLACEMENT_FILL)
        {
          item->u.clue_block_cell.split_highlight_state =
            LAYOUT_SPLIT_HIGHLIGHT_FULL;
        }
      else if (placement == IPUZ_ARROWWORD_PLACEMENT_TOP)
        {
          item->u.clue_block_cell.split_highlight_state =
            LAYOUT_SPLIT_HIGHLIGHT_PRIMARY;
        }
      else if (placement == IPUZ_ARROWWORD_PLACEMENT_BOTTOM)
        {
          item->u.clue_block_cell.split_highlight_state =
            LAYOUT_SPLIT_HIGHLIGHT_SECONDARY;
        }
    }
}

/*
 * Public functions
 */

LayoutConfig
layout_config_default (IpuzPuzzleKind kind)
{
  return layout_config_at_zoom_level (kind, ZOOM_NORMAL);
}

LayoutConfig
layout_config_at_base_size (guint base_size)
{
  LayoutConfig config;

  /* Set some limits on the base size */
  base_size = MIN (base_size, MAX_CELL_BASE_SIZE);
  config.base_size = base_size;

  if (base_size < DEFAULT_CELL_BASE_SIZE)
    config.border_size = 1;
  else if (base_size < DEFAULT_CELL_BASE_SIZE + 7)
    config.border_size = 2;
  else if (base_size < DEFAULT_CELL_BASE_SIZE + 16)
    config.border_size = 3;
  else
    config.border_size = 4;

  return config;
}

LayoutConfig
layout_config_at_zoom_level (IpuzPuzzleKind kind,
                             ZoomLevel      zoom_level)
{
  guint *base_sizes;

  if (kind == IPUZ_PUZZLE_ARROWWORD)
    base_sizes = arrowword_base_sizes;
  else
    base_sizes = default_base_sizes;

  return layout_config_at_base_size (base_sizes[zoom_level]);
}


void
layout_config_size (LayoutConfig  config,
                    guint         xword_width,
                    guint         xword_height,
                    gint         *width,
                    gint         *height)
{
  g_assert (width);
  g_assert (height);

  *width = xword_width * (get_play_cell_size (config.base_size) + config.border_size) + config.border_size;
  *height = xword_height * (get_play_cell_size (config.base_size) + config.border_size) + config.border_size;
}

void
layout_config_size_at_base_size (guint  base_size,
                                 guint  xword_width,
                                 guint  xword_height,
                                 gint  *width,
                                 gint  *height)
{
  LayoutConfig config = layout_config_at_base_size (base_size);

  layout_config_size (config, xword_width, xword_height, width, height);
}

LayoutConfig
layout_config_within_bounds (guint     min_base_size,
                             guint     max_base_size,
                             gint      bounds_width,
                             gint      bounds_height,
                             guint     xword_width,
                             guint     xword_height,
                             gboolean *valid)
{
  LayoutConfig config = {
    .base_size = 0,
    .border_size = 0,
  };

  g_assert (max_base_size >= min_base_size);

  if (valid) *valid = FALSE;
  for (guint i = min_base_size; i <= max_base_size; i++)
    {
      gint width, height;

      layout_config_size_at_base_size (i, xword_width, xword_height, &width, &height);

      if (bounds_width != -1 && width > bounds_width)
        break;
      if (bounds_height != -1 && height > bounds_height)
        break;
      config = layout_config_at_base_size (i);
      if (valid) *valid = TRUE;
    }
  return config;
}

gboolean
layout_config_equal (LayoutConfig *a, LayoutConfig *b)
{
  return (a->border_size == b->border_size
          && a->base_size == b->base_size);
}


/* Even grid_rows or grid_columns correspond to grid lines:
 *
 *    0 2 4 6
 * 0  +-+-+-+
 *    |C|C|C|
 * 2  +-+-+-+
 *    |C|C|C|
 * 4  +-+-+-+
 *    |C|C|C|
 * 6  +-+-+-+
 *
 * Horizontal and vertical borders correspond to even rows or even
 * columns, respectively.
 *
 * This function is used to get an initial grid up, before handling
 * any special cases.
 */
static LayoutItemKind
grid_layout_guess_kind (GridLayout *layout,
                        GridCoord   coord)
{
  if ((coord.row % 2 == 0) && (coord.column % 2 == 0))
    return LAYOUT_ITEM_KIND_INTERSECTION;
  else if (coord.row % 2 == 0)
    return LAYOUT_ITEM_KIND_BORDER_HORIZONTAL;
  else if (coord.column % 2 == 0)
    return LAYOUT_ITEM_KIND_BORDER_VERTICAL;
  else
    return LAYOUT_ITEM_KIND_CELL;
}

static LayoutGeometry
compute_geometry (GridLayout *layout)
{
  guint horizontal_lines = layout->grid_rows - layout->board_rows;
  guint vertical_lines = layout->grid_columns - layout->board_columns;
  guint board_rows = layout->board_rows;
  guint board_columns = layout->board_columns;
  guint cell_size = 3 * layout->config.base_size;

  LayoutGeometry geom = {
    .document_width = vertical_lines * layout->config.border_size + board_columns * cell_size,
    .document_height = horizontal_lines * layout->config.border_size + board_rows * cell_size,
    .border_size = layout->config.border_size,
    .cell_size = cell_size,
  };

  return geom;
}

GridLayout *
grid_layout_new (GridState    *state,
                 LayoutConfig  config)
{
  g_assert (state);

  GridLayout *layout = g_new0 (GridLayout, 1);
  guint board_columns = ipuz_crossword_get_width (state->xword);
  guint board_rows = ipuz_crossword_get_height (state->xword);

  /* See the comment for GridLayout about how it's laid out */
  guint grid_columns = 2 * board_columns + 1;
  guint grid_rows = 2 * board_rows + 1;

  layout->board_rows = board_rows;
  layout->board_columns = board_columns;
  layout->grid_rows = grid_rows;
  layout->grid_columns = grid_columns;
  layout->grid = g_array_sized_new (FALSE, TRUE, sizeof (LayoutItem), grid_rows * grid_columns);
  layout->config = config;
  layout->overlays = g_array_new (FALSE, TRUE, sizeof (LayoutOverlay));
  layout->strings = g_string_chunk_new (1024);
  layout->geometry = compute_geometry (layout);

  guint grid_row, grid_column;

  /* Calculate the main grid */
  for (grid_row = 0; grid_row < grid_rows; grid_row++)
    {
      for (grid_column = 0; grid_column < grid_columns; grid_column++)
        {
          GridCoord grid_coord = { .row = grid_row, .column = grid_column };
          LayoutItem item;

          switch (grid_layout_guess_kind (layout, grid_coord))
            {
            case LAYOUT_ITEM_KIND_INTERSECTION:
              item = compute_intersection (state, grid_coord);
              g_array_append_val (layout->grid, item);
              break;

            case LAYOUT_ITEM_KIND_BORDER_HORIZONTAL:
              item = compute_border_horizontal (state, grid_coord);
              g_array_append_val (layout->grid, item);
              break;

            case LAYOUT_ITEM_KIND_BORDER_VERTICAL:
              item = compute_border_vertical (state, grid_coord);
              g_array_append_val (layout->grid, item);
              break;

            case LAYOUT_ITEM_KIND_CELL: {
              CellResult result = compute_cell (state, layout, grid_coord);

              /* pull out the main_text and intern it */
              const char *interned = g_string_chunk_insert (layout->strings, result.item.u.cell.main_text);
              result.item.u.cell.main_text = interned;

              g_array_append_val (layout->grid, result.item);
              g_array_append_vals (layout->overlays, result.overlays->data, result.overlays->len);
              g_array_free (result.overlays, TRUE);
              break;
            }

            default:
              g_assert_not_reached ();
              return NULL;
            }
        }
    }

  /* Clean up the border colors. We need all the cell colors loaded for this to work. */
  compute_border_colors (layout);

  /* Add the enumerations */
  compute_enumerations (state, layout);


  /* If it's an arrowword, update the blocks */
  if (IPUZ_IS_ARROWWORD (state->xword))
    {
      ArrowwordForeachTuple tuple;
      if (! IPUZ_CLUE_ID_IS_UNSET (&state->clue))
        tuple.selected_clue = ipuz_crossword_get_clue_by_id (state->xword, &state->clue);
      else
        tuple.selected_clue = NULL;
      tuple.layout = layout;

      ipuz_arrowword_foreach_blocks (IPUZ_ARROWWORD (state->xword),
                                     (IpuzArrowwordForeachBlocksFunc) arrowword_foreach_func,
                                     &tuple);
    }
  return layout;
}

void
grid_layout_free (GridLayout *layout)
{
  g_array_free (layout->grid, TRUE);
  g_array_free (layout->overlays, TRUE);
  g_string_chunk_free (layout->strings);
  g_free (layout);
}

LayoutItemKind
grid_layout_get_kind (GridLayout *layout,
                      GridCoord   coord)
{
  LayoutItem *item = get_layout_item (layout, coord);

  return item->kind;
}

LayoutIntersection
grid_layout_get_intersection (GridLayout *layout,
                              GridCoord   coord)
{
  g_assert (layout != NULL);

  LayoutItem *item = get_layout_item (layout, coord);
  g_assert (item->kind == LAYOUT_ITEM_KIND_INTERSECTION);

  return item->u.intersection;
}

LayoutBorderHorizontal
grid_layout_get_border_horizontal (GridLayout *layout,
                                   GridCoord   coord)
{
  g_assert (layout != NULL);

  LayoutItem *item = get_layout_item (layout, coord);
  g_assert (item->kind == LAYOUT_ITEM_KIND_BORDER_HORIZONTAL);

  return item->u.border_horizontal;
}

LayoutBorderVertical
grid_layout_get_border_vertical (GridLayout *layout,
                                 GridCoord   coord)
{
  g_assert (layout != NULL);

  LayoutItem *item = get_layout_item (layout, coord);
  g_assert (item->kind == LAYOUT_ITEM_KIND_BORDER_VERTICAL);

  return item->u.border_vertical;
}

LayoutItemBorderStyle
grid_layout_get_border_style (GridLayout *layout,
                              GridCoord   coord)
{
  g_assert (layout != NULL);

  LayoutItem *item = get_layout_item (layout, coord);
  if (item->kind == LAYOUT_ITEM_KIND_BORDER_HORIZONTAL)
    return item->u.border_horizontal.css_class;
  else if (item->kind == LAYOUT_ITEM_KIND_BORDER_VERTICAL)
    return item->u.border_vertical.css_class;
  else if (item->kind == LAYOUT_ITEM_KIND_INTERSECTION)
    return item->u.intersection.css_class;
  else
    g_assert_not_reached ();
}

LayoutCell
grid_layout_get_cell (GridLayout *layout,
                      GridCoord   coord)
{
  g_assert (layout != NULL);

  LayoutItem *item = get_layout_item (layout, coord);
  g_assert (item->kind == LAYOUT_ITEM_KIND_CELL);

  return item->u.cell;
}

LayoutClueBlockCell
grid_layout_get_clue_block_cell(GridLayout *layout,
                                GridCoord   coord)
{
  g_assert (layout != NULL);

  LayoutItem *item = get_layout_item (layout, coord);
  g_assert (item->kind == LAYOUT_ITEM_KIND_CLUE_BLOCK_CELL);

  return item->u.clue_block_cell;
}

static void
grid_layout_print_overlay (LayoutItem *item)
{

}


static void
grid_layout_print_item (LayoutItem *item)
{
  g_assert (item);

  if (item->overlay)
    {
      grid_layout_print_overlay (item);
      return;
    }
  switch (item->kind)
    {
    case LAYOUT_ITEM_KIND_INTERSECTION:
      if (item->u.intersection.filled)
        {
          putchar ('+');
        }
      else
        {
          putchar ('.');
        }
      break;

    case LAYOUT_ITEM_KIND_BORDER_HORIZONTAL:
      if (item->u.border_horizontal.filled)
        {
          putchar ('-');
        }
      else
        {
          putchar ('.');
        }
      break;

    case LAYOUT_ITEM_KIND_BORDER_VERTICAL:
      if (item->u.border_vertical.filled)
        {
          putchar ('|');
        }
      else
        {
          putchar ('.');
        }
      break;

    case LAYOUT_ITEM_KIND_CELL:
      if (item->u.cell.cell_type == IPUZ_CELL_NULL)
        {
          putchar (' ');
        }
      else
        {
          if (item->u.cell.cell_type == IPUZ_CELL_BLOCK)
            putchar ('#');
          else
            {
              if (item->u.cell.css_class == LAYOUT_ITEM_STYLE_SELECTED)
                g_print ("\033[1m");
              else if (item->u.cell.css_class == LAYOUT_ITEM_STYLE_FOCUSED)
                g_print ("\033[1;4m");
              putchar ('C');
              if (item->u.cell.css_class == LAYOUT_ITEM_STYLE_FOCUSED ||
                  item->u.cell.css_class == LAYOUT_ITEM_STYLE_SELECTED)
                g_print ("\033[0m");
            }
        }
      break;

    default:
      g_assert_not_reached ();
    }
}

void
grid_layout_print (GridLayout *layout)
{
  guint grid_rows = layout->grid_rows;
  guint grid_columns = layout->grid_columns;
  guint row, column;

  for (row = 0; row < grid_rows; row++)
    {
      for (column = 0; column < grid_columns; column++)
        {
          GridCoord coord = { .row = row, .column = column };
          LayoutItem *item = get_layout_item (layout, coord);

          grid_layout_print_item (item);
        }
      puts ("");
    }
}

/* FIXME(cleanup): These strings are different than the ones defined
 * by glib-mkenums. Perhaps we want to sync this with
 * g_enum_to_string(), etc in the future. */
const gchar *
zoom_level_to_string (ZoomLevel zoom_level)
{
  if (zoom_level == ZOOM_XSMALL)
    return "extra-small";
  if (zoom_level == ZOOM_SMALL)
    return "small";
  else if (zoom_level == ZOOM_NORMAL)
    return "normal";
  else if (zoom_level == ZOOM_LARGE)
    return "large";
  else if (zoom_level == ZOOM_XLARGE)
    return "extra-large";

  g_warning ("Unknown zoom level");
  return "zoom-normal";
}

ZoomLevel
zoom_level_from_string (const char *zoom)
{
  if (g_strcmp0 (zoom, "extra-small") == 0)
    return ZOOM_XSMALL;
  if (g_strcmp0 (zoom, "small") == 0)
    return ZOOM_SMALL;
  else if (g_strcmp0 (zoom, "normal") == 0)
    return ZOOM_NORMAL;
  else if (g_strcmp0 (zoom, "large") == 0)
    return ZOOM_LARGE;
  else if (g_strcmp0 (zoom, "extra-large") == 0)
    return ZOOM_XLARGE;

  g_warning ("Unknown zoom level string: %s", zoom);
  return ZOOM_NORMAL;
}

ZoomLevel
zoom_level_zoom_in (ZoomLevel zoom_level)
{
  if (zoom_level != ZOOM_XLARGE)
    return zoom_level+1;

  return zoom_level;
}

ZoomLevel
zoom_level_zoom_out (ZoomLevel zoom_level)
{
  if (zoom_level != ZOOM_XSMALL)
    return zoom_level-1;

  return zoom_level;
}

guint
zoom_level_get_min_base_size (IpuzPuzzleKind kind,
                              ZoomLevel      zoom_level)
{
  g_assert (zoom_level >= ZOOM_XSMALL &&
            zoom_level < ZOOM_UNSET);

  if (kind == IPUZ_PUZZLE_ARROWWORD)
    return arrowword_min_sizes[(guint) zoom_level];

  return default_min_sizes[(guint) zoom_level];
}
