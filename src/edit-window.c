/* edit-window.c
 *
 * Copyright 2021 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include <adwaita.h>
#include "crosswords-misc.h"
#include "edit-app.h"
#include "edit-clue-details.h"
#include "edit-clue-info.h"
#include "edit-metadata.h"
#include "edit-save-changes-dialog.h"
#include "edit-window.h"
#include "edit-window-private.h"


enum {
  PROP_0,
  PROP_PUZZLE_STACK,
  PROP_PUZZLE,
  N_PROPS
};


static GParamSpec *obj_props[N_PROPS] = { NULL, };


static void              edit_window_init               (EditWindow             *self);
static void              edit_window_class_init         (EditWindowClass        *klass);
static void              edit_window_set_property       (GObject                *object,
                                                         guint                   prop_id,
                                                         const GValue           *value,
                                                         GParamSpec             *pspec);
static void              edit_window_dispose            (GObject                *object);
static void              edit_window_finalize           (GObject                *object);
static gboolean          edit_window_close_request      (GtkWindow              *window);
static void              stage_toggled_cb               (EditWindow             *self,
                                                         GtkWidget              *toggle);
static void              grid_stack_visible_child_notify_cb (EditWindow *self);
static void              edit_window_update             (EditWindow             *self);
static void              edit_window_get_title_and_path (EditWindow             *self,
                                                         gchar                 **title,
                                                         gchar                 **path);
static void              puzzle_changed_cb              (EditWindow             *self,
                                                         PuzzleStackOperation    operation,
                                                         PuzzleStackChangeType   change_type,
                                                         PuzzleStack            *puzzle_stack);
static void              edit_window_set_puzzle         (EditWindow             *self,
                                                         IpuzPuzzle             *puzzle);
static const char       *stage_to_stage_name            (PuzzleStackStage        stage);


G_DEFINE_TYPE (EditWindow, edit_window, ADW_TYPE_APPLICATION_WINDOW);


static void
edit_window_init (EditWindow *self)
{
  GMenu *menu;
  GtkApplication *app;

  gtk_widget_init_template (GTK_WIDGET (self));
  gtk_widget_add_css_class (GTK_WIDGET (self), "edit");

  /* Menus */
  app = GTK_APPLICATION (EDIT_APP_DEFAULT);
  menu = gtk_application_get_menu_by_id (GTK_APPLICATION (app), "edit-menu");
  gtk_menu_button_set_menu_model (GTK_MENU_BUTTON (self->edit_menu_button), G_MENU_MODEL (menu));
}

static void
edit_window_class_init (EditWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GtkWindowClass *window_class = GTK_WINDOW_CLASS (klass);

  object_class->set_property = edit_window_set_property;
  object_class->dispose = edit_window_dispose;
  object_class->finalize = edit_window_finalize;
  window_class->close_request = edit_window_close_request;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-window.ui");


  gtk_widget_class_bind_template_child (widget_class, EditWindow, puzzle_stack);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, split_view);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, modified_label);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, subtitle_label);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, edit_menu_button);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, grid_toggle);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, clues_toggle);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, style_toggle);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, metadata_toggle);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, main_stack);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, main_paned);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, sidebar_stack);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, info_stack);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, grid_switcher_bar);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, grid_stack);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, main_grid);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, edit_metadata);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, grid_preview);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, symmetry_widget);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, cell_widget);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, word_list_widget);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, autofill_details);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, acrostic_details);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, edit_grid_widget);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, letter_histogram_group);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, letter_histogram_widget);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, cluelen_histogram_group);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, cluelen_histogram_widget);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, clue_details_widget);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, clue_list_widget);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, clue_info_anagram);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, clue_info_odd);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, clue_info_even);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, style_preview);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, style_shapebg_row);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, style_foreground_color_row);
  gtk_widget_class_bind_template_child (widget_class, EditWindow, style_background_color_row);

  gtk_widget_class_bind_template_callback (widget_class, stage_toggled_cb);
  gtk_widget_class_bind_template_callback (widget_class, grid_stack_visible_child_notify_cb);

  _edit_window_class_controls_init (widget_class);
  _edit_window_class_actions_init (widget_class);

  /* FIXME(loading): "puzzle" should really be construct-only, and
   * "puzzle-stack" shoudln't exist. We should also add a "uri"
   * property, and move the loading code to edit-app.*/
  obj_props[PROP_PUZZLE] = g_param_spec_object ("puzzle", NULL, NULL,
                                                IPUZ_TYPE_PUZZLE,
                                                G_PARAM_WRITABLE);
  obj_props[PROP_PUZZLE_STACK] = g_param_spec_object ("puzzle-stack",
                                                      NULL, NULL,
                                                      PUZZLE_TYPE_STACK,
                                                      G_PARAM_WRITABLE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);
}

static void
edit_window_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  EditWindow *self;
  IpuzPuzzle *puzzle;

  self = EDIT_WINDOW (object);

  switch (prop_id)
    {
    case PROP_PUZZLE:
      puzzle = IPUZ_PUZZLE (g_value_get_object (value));
      edit_window_set_puzzle (self, puzzle);
      break;
    case PROP_PUZZLE_STACK:
      g_clear_object (&self->puzzle_stack);
      self->puzzle_stack = g_value_dup_object (value);
      if (self->puzzle_stack)
        {
          /* Warning: The order here matters: We need this callback to
           * run before the local one */
          g_signal_connect_swapped (self->puzzle_stack,
                                    "changed",
                                    G_CALLBACK (_edit_window_puzzle_changed_cb),
                                    self);
          g_signal_connect_swapped (self->puzzle_stack,
                                    "changed",
                                    G_CALLBACK (puzzle_changed_cb),
                                    self);
          _edit_window_puzzle_changed_cb (self, TRUE, STACK_CHANGE_PUZZLE, self->puzzle_stack);
          puzzle_changed_cb (self, TRUE, STACK_CHANGE_PUZZLE, self->puzzle_stack);
        }
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
edit_window_dispose (GObject *object)
{
  EditWindow *self;

  self = EDIT_WINDOW (object);

  g_clear_object (&self->puzzle_stack);
  g_clear_pointer (&self->uri, g_free);
  g_clear_handle_id (&self->edit_state->solver_timeout, g_source_remove);
  g_clear_handle_id (&self->edit_state->solver_finished_handler, g_source_remove);

  G_OBJECT_CLASS (edit_window_parent_class)->dispose (object);
}


static void
edit_window_finalize (GObject *object)
{
  EditWindow *self;

  self = EDIT_WINDOW (object);

  g_clear_pointer (&self->uri, g_free);
  g_clear_pointer (&self->edit_state, edit_state_free);

  G_OBJECT_CLASS (edit_window_parent_class)->finalize (object);
}

static gboolean
edit_window_close_request (GtkWindow *window)
{
  GList *save_info_list = NULL;
  g_autofree gchar *title = NULL;
  g_autofree gchar *path = NULL;
  EditWindow *self;

  self = EDIT_WINDOW (window);
  g_assert (EDIT_IS_WINDOW (self));

  if (edit_window_get_unsaved_changes (self, &title, &path))
    {
      /* FIXME(uri): We are converting our URI to a path and back to a
       * URI. We should keep a URI throughout the dialog and convert
       * it to a path just to present it. */
      save_info_list = edit_save_info_list_add (save_info_list,
                                                GTK_WIDGET (self),
                                                title, path);
    }

  if (save_info_list == NULL)
    return FALSE;

  edit_save_changes_dialog_run (GTK_WINDOW (self),
                                save_info_list,
                                NULL);

  return TRUE;
}

struct stage_name_table {
  const char *stage_name;
  PuzzleStackStage stage;
};

static const struct stage_name_table stage_names[] = {
  { "grid",     EDIT_STAGE_GRID },
  { "clues",    EDIT_STAGE_CLUES },
  { "style",    EDIT_STAGE_STYLE },
  { "metadata", EDIT_STAGE_METADATA },
};

static const char *
stage_to_stage_name (PuzzleStackStage stage)
{
  guint i;

  for (i = 0; i < G_N_ELEMENTS (stage_names); i++)
    {
      if (stage == stage_names[i].stage)
        return stage_names[i].stage_name;
    }

  g_assert_not_reached ();
}

void
_edit_window_set_stage (EditWindow       *self,
                        PuzzleStackStage  stage)
{
  g_assert (EDIT_IS_WINDOW (self));

  if (self->edit_state->stage != stage)
    {
      self->edit_state->stage = stage;
      edit_window_update (self);
      _edit_window_control_update_stage (self);
    }
}

static void
stage_toggled_cb (EditWindow *self,
                  GtkWidget  *toggle)
{
  PuzzleStackStage stage;

  if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle)))
    return;

  if (toggle == self->grid_toggle)
    stage = EDIT_STAGE_GRID;
  else if (toggle == self->clues_toggle)
    stage = EDIT_STAGE_CLUES;
  else if (toggle == self->style_toggle)
    stage = EDIT_STAGE_STYLE;
  else if (toggle == self->metadata_toggle)
    stage = EDIT_STAGE_METADATA;
  else
    g_assert_not_reached ();

  _edit_window_set_stage (self, stage);
}

static void
grid_stack_visible_child_notify_cb (EditWindow *self)
{
  const gchar *child_name;
  EditGridStage stage;

  child_name = adw_view_stack_get_visible_child_name (ADW_VIEW_STACK (self->grid_stack));
  stage = name_to_edit_grid_stage (child_name);
  if (self->edit_state->grid_stage != stage)
    {
      self->edit_state->grid_stage = stage;
      edit_window_update (self);
      _edit_window_control_update_stage (self);
    }
}

static void
edit_window_update_title (EditWindow *self)
{
  g_autofree gchar *title = NULL;
  g_autofree gchar *path = NULL;

  edit_window_get_title_and_path (self, &title, &path);

  if (title && title[0])
    {
      g_autofree gchar *label = NULL;

      if (path)
        {
          label = g_strdup_printf ("%s — %s", title, path);
        }

      gtk_label_set_label (GTK_LABEL (self->subtitle_label),
                           label?label:title);
    }
  else if (path)
    {
      gtk_label_set_label (GTK_LABEL (self->subtitle_label), path);
    }
  else
    {
      gtk_label_set_label (GTK_LABEL (self->subtitle_label), "");
    }

  gtk_widget_set_visible (self->modified_label,
                          puzzle_stack_get_unsaved_changes (self->puzzle_stack));
}

static void
edit_window_update_dock (EditWindow *self)
{
  gboolean reveal_panes = FALSE;
  GtkWidget *toggle;

  if (self->edit_state->stage == EDIT_STAGE_METADATA)
    {
      reveal_panes = FALSE;
      gtk_stack_set_visible_child_name (GTK_STACK (self->main_stack),
                                        "metadata");
    }
  else
    {
      reveal_panes = TRUE;
      gtk_stack_set_visible_child_name (GTK_STACK (self->sidebar_stack),
                                        stage_to_stage_name (self->edit_state->stage));
      adw_view_stack_set_visible_child_name (ADW_VIEW_STACK (self->grid_stack),
                                        edit_grid_stage_to_name (self->edit_state->grid_stage));
      gtk_stack_set_visible_child_name (GTK_STACK (self->info_stack),
                                        stage_to_stage_name (self->edit_state->stage));
      gtk_stack_set_visible_child_name (GTK_STACK (self->main_stack),
                                        "grid");
    }

  
  gtk_widget_set_visible (self->info_stack, reveal_panes);
  adw_overlay_split_view_set_show_sidebar (ADW_OVERLAY_SPLIT_VIEW (self->split_view), reveal_panes);

  gtk_widget_action_set_enabled (GTK_WIDGET (self), "win.autofill",
                                 self->edit_state->stage == EDIT_STAGE_GRID);
  gtk_widget_set_visible (self->grid_switcher_bar,
                          self->edit_state->stage == EDIT_STAGE_GRID);

  switch (self->edit_state->stage)
    {
    case EDIT_STAGE_GRID:
      toggle = self->grid_toggle;
      break;
    case EDIT_STAGE_CLUES:
      toggle = self->clues_toggle;
      break;
    case EDIT_STAGE_STYLE:
      toggle = self->style_toggle;
      break;
    case EDIT_STAGE_METADATA:
      toggle = self->metadata_toggle;
      break;
    default:
      g_assert_not_reached ();
    }

  g_signal_handlers_block_by_func (toggle, stage_toggled_cb, self);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (toggle), TRUE);
  g_signal_handlers_unblock_by_func (toggle, stage_toggled_cb, self);
}

static void
edit_window_update (EditWindow *self)
{
  edit_window_update_title (self);
  edit_window_update_dock (self);
}

static void
puzzle_changed_cb (EditWindow            *self,
                   PuzzleStackOperation   operation,
                   PuzzleStackChangeType  change_type,
                   PuzzleStack           *puzzle_stack)
{
  IpuzPuzzle *puzzle;

  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);
  if (puzzle == NULL)
    return;

  gtk_widget_action_set_enabled (GTK_WIDGET (self),
                                 "win.undo",
                                 puzzle_stack_can_undo (puzzle_stack));
  gtk_widget_action_set_enabled (GTK_WIDGET (self),
                                 "win.redo",
                                 puzzle_stack_can_redo (puzzle_stack));
  gtk_widget_action_set_enabled (GTK_WIDGET (self),
                                 "win.preview",
                                 (puzzle_stack_get_puzzle (puzzle_stack) != NULL));

  edit_window_update (self);
}

static void
edit_window_get_title_and_path (EditWindow  *self,
                                gchar      **title,
                                gchar      **path)
{
  IpuzPuzzle *puzzle;

  g_return_if_fail (EDIT_IS_WINDOW (self));
  g_return_if_fail (title != NULL);
  g_return_if_fail (path != NULL);

  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);

  if (puzzle == NULL)
    return;

  g_object_get (puzzle,
                "title", title,
                NULL);

  if (self->uri)
    *path = g_filename_from_uri (self->uri, NULL, NULL);
}

static void
edit_window_set_puzzle (EditWindow *self,
                        IpuzPuzzle *puzzle)
{
  g_return_if_fail (EDIT_WINDOW (self));
  g_return_if_fail (IPUZ_IS_PUZZLE (puzzle));

  puzzle_stack_push_change (self->puzzle_stack, STACK_CHANGE_PUZZLE, puzzle);
}

static gchar *
get_temp_file_for_title (const gchar *title)
{
  const gchar *dir = NULL;
  gchar *filename;
  gchar *path;
  guint i = 1;

  dir = g_get_user_special_dir (G_USER_DIRECTORY_DOCUMENTS);
  filename = g_strdup_printf ("%s.ipuz", title);
  path = g_build_filename (dir, filename, NULL);
  g_free (filename);

  while (g_file_test (path, G_FILE_TEST_EXISTS))
    {
      g_free (path);

      filename = g_strdup_printf ("%s(%u).ipuz", title, i++);
      path = g_build_filename (dir, filename, NULL);
      g_free (filename);
    }

  return path;
}

/* Public methods */

/**
 * edit_window_get_unsaved_changes:
 * @self: A `EditWindow`
 * @title: location to store the title of the puzzle
 * @path: location to store the path of the puzzle
 *
 * Returns %TRUE if there are unsaved changes in @self. @title and
 * @path will be set to the title in that case. They will always be
 * set, either to the existing values or predictive values.
 *
 * Returns: %TRUE, if there are unsaved changes
 **/
gboolean
edit_window_get_unsaved_changes (EditWindow  *self,
                                 gchar      **title,
                                 gchar      **path)
{
  g_return_val_if_fail (EDIT_IS_WINDOW (self), FALSE);
  g_return_val_if_fail (title != NULL, FALSE);
  g_return_val_if_fail (path != NULL, FALSE);

  if (puzzle_stack_get_unsaved_changes (self->puzzle_stack))
    {
      edit_window_get_title_and_path (self, title, path);

      if (*title == NULL)
        *title = g_strdup (_("Untitled Puzzle"));
      if (*path == NULL)
        *path = get_temp_file_for_title (*title);
      return TRUE;
    }

  return FALSE;
}

static void
edit_window_commit_changes (EditWindow *self)
{
  g_assert (EDIT_IS_WINDOW (self));

  if (self->edit_state->stage == EDIT_STAGE_CLUES)
    edit_clue_details_commit_changes (EDIT_CLUE_DETAILS (self->clue_details_widget));
  else if (self->edit_state->stage == EDIT_STAGE_METADATA)
    edit_metadata_commit_changes (EDIT_METADATA (self->edit_metadata));
}

/**
 * edit_window_save:
 * @edit_window:
 * @new_uri:
 *
 * If @new_uri is NULL, save at the current location. If non-null,
 * save at the new location.
 **/
void
edit_window_save (EditWindow  *self,
                  const gchar *new_uri)
{
  IpuzPuzzle *puzzle;
  g_autoptr (GError) error = NULL;
  g_autoptr (GFile) file = NULL;
  g_autofree char *buffer = NULL;
  gsize len = 0;

  g_return_if_fail (EDIT_IS_WINDOW (self));

  if (new_uri)
    {
      g_clear_pointer (&self->uri, g_free);
      self->uri = g_strdup (new_uri);
    }
  g_assert (self->uri != NULL);

  edit_window_commit_changes (self);

  puzzle = puzzle_stack_get_puzzle (self->puzzle_stack);
  if (puzzle == NULL)
    return;

  buffer = ipuz_puzzle_save_to_data (puzzle,
                                     &len);

  /* FIXME(error): We should handle errors better */
  file = g_file_new_for_uri (self->uri);
  if (! g_file_replace_contents (file, buffer, len,
                                 NULL,
                                 TRUE,
                                 G_FILE_CREATE_REPLACE_DESTINATION,
                                 NULL, NULL,
                                 &error))
    {
      g_print ("error saving file: %s\n", error->message);
    }
  else
    {
      puzzle_stack_set_saved (self->puzzle_stack);
      edit_window_update (self);
    }
}


void
edit_window_load_uri (EditWindow  *edit_window,
                      const gchar *uri)
{
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  g_autoptr (GFile) file = NULL;
  g_autoptr (GFileInputStream) stream = NULL;

  g_return_if_fail (EDIT_IS_WINDOW (edit_window));

  file = g_file_new_for_uri (uri);
  stream = g_file_read (file, NULL, NULL);

  if (stream)
    {
      puzzle = ipuz_puzzle_new_from_stream (G_INPUT_STREAM (stream), NULL, NULL);
      g_input_stream_close (G_INPUT_STREAM (stream), NULL, NULL);
    }

  if (puzzle)
    {
      g_clear_pointer (&edit_window->uri, g_free);
      edit_window->uri = g_strdup (uri);

      edit_window_set_puzzle (edit_window, puzzle);
    }
  else
    {
      /* FIXME(dialog): Catch and warn for errors here */
    }
}
