/* word-solver.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <libipuz/libipuz.h>
#include "cell-array.h"
#include "word-list-misc.h"

G_BEGIN_DECLS


#define WORD_TYPE_SOLVER (word_solver_get_type())
G_DECLARE_FINAL_TYPE (WordSolver, word_solver, WORD, SOLVER, GObject);

typedef enum
{
  WORD_SOLVER_READY,    /* @ Ready to start @ */
  WORD_SOLVER_RUNNING,  /* @ Currently running @ */
  WORD_SOLVER_COMPLETE, /* @ Finished running @ */
} WordSolverState;


WordSolver      *word_solver_new             (void);
WordSolverState  word_solver_get_state       (WordSolver    *solver);
void             word_solver_run             (WordSolver    *solver,
                                              IpuzCrossword *xword,
                                              CellArray     *cell_array,
                                              WordArray     *skip_list,
                                              gint           max_iterations,
                                              gint           min_priority);
void             word_solver_reset           (WordSolver    *solver);
void             word_solver_cancel          (WordSolver    *solver);
gint             word_solver_get_count       (WordSolver    *solver);
guint            word_solver_get_n_solutions (WordSolver    *solver);
IpuzGuesses     *word_solver_get_solution    (WordSolver    *solver,
                                              guint          index);


G_END_DECLS
