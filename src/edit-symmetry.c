/* edit-symmetry.c
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include <adwaita.h>
#include "edit-symmetry.h"
#include "play-cell.h"


enum
{
  SYMMETRY_CHANGED,
  N_SIGNALS
};

static guint obj_signals[N_SIGNALS] = { 0 };


struct _EditSymmetry
{
  GtkWidget parent_instance;

  PuzzleStack *puzzle_stack;
  IpuzSymmetry symmetry;

  GtkWidget *symmetry_icon;
  GtkWidget *symmetry_combo_row;
};


G_DEFINE_FINAL_TYPE (EditSymmetry, edit_symmetry, GTK_TYPE_WIDGET);


static void edit_symmetry_init       (EditSymmetry          *self);
static void edit_symmetry_class_init (EditSymmetryClass     *klass);
static void edit_symmetry_dispose    (GObject               *object);
static void update_icon              (EditSymmetry          *self);
static void combo_row_changed_cb     (EditSymmetry          *self);


static void
edit_symmetry_init (EditSymmetry *self)
{
  self->symmetry = IPUZ_SYMMETRY_ROTATIONAL_QUARTER;
  gtk_widget_init_template (GTK_WIDGET (self));
  update_icon (self);
}

static void
edit_symmetry_class_init (EditSymmetryClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = edit_symmetry_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-symmetry.ui");
  gtk_widget_class_bind_template_child (widget_class, EditSymmetry, symmetry_icon);
  gtk_widget_class_bind_template_child (widget_class, EditSymmetry, symmetry_combo_row);
  gtk_widget_class_bind_template_callback (widget_class, combo_row_changed_cb);

  obj_signals [SYMMETRY_CHANGED] =
    g_signal_new ("symmetry-changed",
                  EDIT_TYPE_SYMMETRY,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  IPUZ_TYPE_SYMMETRY);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);

  gtk_widget_class_set_css_name (widget_class, "edit-symmetry");
}

static void
edit_symmetry_dispose (GObject *object)
{
  EditSymmetry *self;
  GtkWidget *child;

  self = EDIT_SYMMETRY (object);

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  g_clear_object (&self->puzzle_stack);

  G_OBJECT_CLASS (edit_symmetry_parent_class)->dispose (object);
}

static void
update_icon (EditSymmetry *self)
{
  const gchar *icon_name = NULL;
  switch (self->symmetry)
    {
    case IPUZ_SYMMETRY_NONE:
      icon_name = "symmetry-none";
      break;
    case IPUZ_SYMMETRY_ROTATIONAL_QUARTER:
      icon_name = "symmetry-quarter";
      break;
    case IPUZ_SYMMETRY_ROTATIONAL_HALF:
      icon_name = "symmetry-half";
      break;
    case IPUZ_SYMMETRY_HORIZONTAL:
      icon_name = "symmetry-horizontal";
      break;
    case IPUZ_SYMMETRY_VERTICAL:
      icon_name = "symmetry-vertical";
      break;
    case IPUZ_SYMMETRY_MIRRORED:
      icon_name = "symmetry-mirrored";
      break;
    default:
      icon_name = "broken";
      break;
    }
  gtk_image_set_from_icon_name (GTK_IMAGE (self->symmetry_icon), icon_name);
}

static void
combo_row_changed_cb (EditSymmetry *self)
{
  self->symmetry = (IpuzSymmetry) adw_combo_row_get_selected (ADW_COMBO_ROW (self->symmetry_combo_row));
  update_icon (self);
  
  g_signal_emit (self, obj_signals [SYMMETRY_CHANGED], 0, self->symmetry);
}

/* Public methods */

IpuzSymmetry
edit_symmetry_get_symmetry (EditSymmetry *self)
{
  g_return_val_if_fail (EDIT_IS_SYMMETRY (self), IPUZ_SYMMETRY_NONE);

  return self->symmetry;
}

void
edit_symmetry_set_symmetry (EditSymmetry *self,
                            IpuzSymmetry  symmetry,
                            LayoutConfig  config)
{
  g_return_if_fail (EDIT_IS_SYMMETRY (self));

  self->symmetry = symmetry;
  adw_combo_row_set_selected (ADW_COMBO_ROW (self->symmetry_combo_row), symmetry);

  /* Update the image to match the logo size */
  gtk_image_set_pixel_size (GTK_IMAGE (self->symmetry_icon),
                            config.base_size*2);
}
