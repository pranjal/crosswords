
/* puzzle-downloader-dialog.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include "puzzle-downloader-dialog.h"

struct _PuzzleDownloaderDialog
{
  GtkDialog dialog;

  DownloaderType type;

  /* template widgets */
  GtkWidget *primary_label;
  GtkWidget *secondary_label;
  GtkWidget *link_button;
  GtkWidget *number_spin;
  GtkAdjustment *number_adjustment;
  GtkWidget *text_entry;
  GtkWidget *date_calendar;
  GtkWidget *download_button;
};


static void puzzle_downloader_dialog_init       (PuzzleDownloaderDialog      *self);
static void puzzle_downloader_dialog_class_init (PuzzleDownloaderDialogClass *klass);
static void puzzle_downloader_dialog_dispose    (GObject                     *object);


G_DEFINE_TYPE (PuzzleDownloaderDialog, puzzle_downloader_dialog, GTK_TYPE_DIALOG);


static void
puzzle_downloader_dialog_init (PuzzleDownloaderDialog *self)
{
  GtkSettings *settings;
  gboolean use_caret;

  gtk_widget_init_template (GTK_WIDGET (self));

  /* Mirror GtkMessageDialog here */
  settings = gtk_widget_get_settings (GTK_WIDGET (self));
  g_object_get (settings, "gtk-keynav-use-caret", &use_caret, NULL);
  gtk_label_set_selectable (GTK_LABEL (self->primary_label), use_caret);
  gtk_label_set_selectable (GTK_LABEL (self->secondary_label), use_caret);
}

static void
puzzle_downloader_dialog_class_init (PuzzleDownloaderDialogClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = puzzle_downloader_dialog_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/puzzle-downloader-dialog.ui");

  gtk_widget_class_bind_template_child (widget_class, PuzzleDownloaderDialog, primary_label);
  gtk_widget_class_bind_template_child (widget_class, PuzzleDownloaderDialog, secondary_label);
  gtk_widget_class_bind_template_child (widget_class, PuzzleDownloaderDialog, link_button);
  gtk_widget_class_bind_template_child (widget_class, PuzzleDownloaderDialog, number_spin);
  gtk_widget_class_bind_template_child (widget_class, PuzzleDownloaderDialog, number_adjustment);
  gtk_widget_class_bind_template_child (widget_class, PuzzleDownloaderDialog, text_entry);
  gtk_widget_class_bind_template_child (widget_class, PuzzleDownloaderDialog, date_calendar);
  gtk_widget_class_bind_template_child (widget_class, PuzzleDownloaderDialog, download_button);
}

static void
puzzle_downloader_dialog_dispose (GObject *object)
{
  /* Pass */

  G_OBJECT_CLASS (puzzle_downloader_dialog_parent_class)->dispose (object);
}

/* Helper methods */

static void
url_entry_changed (GtkEntry               *entry,
                   GParamSpec             *pspec,
                   PuzzleDownloaderDialog *dialog)
{
  const char *text;
  g_autoptr (GError) error = NULL;
  g_autoptr (GUri) uri = NULL;

  text = gtk_editable_get_text (GTK_EDITABLE (entry));
  uri = g_uri_parse (text, G_URI_FLAGS_PARSE_RELAXED,
                     &error);

  if (strlen (text) > 0 && error)
    {
      gtk_widget_set_tooltip_text (GTK_WIDGET (entry), error->message);
      gtk_widget_add_css_class (GTK_WIDGET (entry), "error");
      gtk_widget_set_sensitive (dialog->download_button, FALSE);
    }
  else
    {
      gtk_widget_set_tooltip_text (GTK_WIDGET (entry), "");
      gtk_widget_remove_css_class (GTK_WIDGET (entry), "error");
      gtk_widget_set_sensitive (dialog->download_button, TRUE);
    }

  /* We are just using g_uri_parse() as a validator; we don't care about the actual GUri.
   * Attempt to use the variable so static-scan doesn't think its value is unused.
   */
  (void) uri;
}

static void
setup_url_dialog (PuzzleDownloaderDialog *dialog)
{
  g_signal_connect (dialog->text_entry, "notify::text",
                    G_CALLBACK (url_entry_changed),
                    dialog);
  gtk_entry_set_placeholder_text (GTK_ENTRY (dialog->text_entry), _("Enter a URL"));
  gtk_widget_set_visible (dialog->text_entry, TRUE);
  gtk_widget_grab_focus (dialog->text_entry);
}


/* Public methods */

GtkDialog *
puzzle_downloader_dialog_new (DownloaderType  type,
                              GtkWindow      *parent_window,
                              const gchar    *primary_text,
                              const gchar    *secondary_text,
                              const gchar    *uri,
                              const gchar    *link_text)
{
  PuzzleDownloaderDialog *dialog;

  dialog = g_object_new (PUZZLE_TYPE_DOWNLOADER_DIALOG,
                         "use-header-bar", FALSE,
                         "modal", TRUE,
                         "transient-for", parent_window,
                         "destroy-with-parent", TRUE,
                         NULL);

  gtk_label_set_label (GTK_LABEL (dialog->primary_label),
                       primary_text);

  if (secondary_text)
    {
      gtk_label_set_label (GTK_LABEL (dialog->secondary_label),
                           secondary_text);
      gtk_widget_add_css_class (dialog->primary_label, "title-2");
    }
  else
    {
      gtk_widget_set_visible (dialog->secondary_label, FALSE);
    }

  if (uri)
    {
      gtk_link_button_set_uri (GTK_LINK_BUTTON (dialog->link_button), uri);
      gtk_button_set_label (GTK_BUTTON (dialog->link_button), link_text ? link_text : uri);
    }
  else
    {
      gtk_widget_set_visible (dialog->link_button, FALSE);
    }

  /* Set up the UI */
  switch (type)
    {
    case DOWNLOADER_TYPE_DATE:
      gtk_widget_set_visible (dialog->date_calendar, TRUE);
      gtk_widget_set_sensitive (dialog->download_button, TRUE);
      break;
    case DOWNLOADER_TYPE_NUMBER:
      gtk_widget_set_visible (dialog->number_spin, TRUE);
      gtk_widget_grab_focus (dialog->number_spin);
      gtk_widget_set_sensitive (dialog->download_button, TRUE);
      break;
    case DOWNLOADER_TYPE_URL:
      setup_url_dialog (dialog);
      break;
    case DOWNLOADER_TYPE_ENTRY:
      break;
    default:
      g_assert_not_reached ();
    }
  return (GtkDialog *) dialog;
}

void
puzzle_downloader_dialog_set_number_limits (PuzzleDownloaderDialog *dialog,
                                            gint                    lower,
                                            gint                    upper,
                                            gint                    value)
{
  g_return_if_fail (PUZZLE_IS_DOWNLOADER_DIALOG (dialog));

  gtk_adjustment_set_lower (dialog->number_adjustment, (double) lower);
  gtk_adjustment_set_upper (dialog->number_adjustment, (double) upper);
  gtk_adjustment_set_value (dialog->number_adjustment, (double) value);
}

gint
puzzle_downloader_dialog_get_number (PuzzleDownloaderDialog *dialog)
{
  g_return_val_if_fail (PUZZLE_IS_DOWNLOADER_DIALOG (dialog), 0);

  return (gint) gtk_adjustment_get_value (dialog->number_adjustment);
}

GDateTime *
puzzle_downloader_dialog_get_date (PuzzleDownloaderDialog *dialog)
{
  g_return_val_if_fail (PUZZLE_IS_DOWNLOADER_DIALOG (dialog), NULL);

  return gtk_calendar_get_date (GTK_CALENDAR (dialog->date_calendar));
}

gchar *
puzzle_downloader_dialog_get_url (PuzzleDownloaderDialog *dialog)
{
  g_return_val_if_fail (PUZZLE_IS_DOWNLOADER_DIALOG (dialog), NULL);

  return g_strdup (gtk_editable_get_text (GTK_EDITABLE (dialog->text_entry)));
}

gchar *
puzzle_downloader_dialog_get_entry (PuzzleDownloaderDialog *dialog)
{
  g_return_val_if_fail (PUZZLE_IS_DOWNLOADER_DIALOG (dialog), NULL);

  return g_strdup (gtk_editable_get_text (GTK_EDITABLE (dialog->text_entry)));
}
