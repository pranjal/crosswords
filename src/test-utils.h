#pragma once

#include "grid-state.h"

G_GNUC_WARN_UNUSED_RESULT GridState *load_state (const char *filename, GridStateMode mode);

G_GNUC_WARN_UNUSED_RESULT GridState *load_state_with_quirks (const char *filename, GridStateMode mode);
