/* gen-word-list.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdio.h>
#include <locale.h>
#include "gen-word-list-importer.h"

static gboolean
valid_char (gunichar c)
{
  return g_unichar_type (c) == G_UNICODE_UPPERCASE_LETTER;
}

static gboolean
valid_word (const char *word)
{
  const char *ptr;
  gsize len = 0;

  for (ptr = word; ptr[0]; ptr = g_utf8_next_char (ptr))
    {
      gunichar c;

      c = g_utf8_get_char (ptr);
      if (!valid_char (c))
        return FALSE;

      len++;
    };

  if (len == 0)
    return FALSE;
  else
    return TRUE;
}

ParseLineStatus
validate_word (const gchar   *word,
               gboolean       free_word,
               gssize         byte_len,
               gint           word_len,
               glong          priority,
               gint           min_length,
               gint           max_length,
               guchar         threshold,
               ValidatedWord *out_validated)
{
  if (!valid_word (word))
    {
      return PARSE_LINE_STATUS_BAD_SYNTAX;
    }

  if (priority < threshold || priority > 255)
    {
      return PARSE_LINE_STATUS_PRIORITY_OUT_OF_RANGE;
    }

  if (word_len < min_length || word_len > max_length)
    {
      return PARSE_LINE_STATUS_WORD_LENGTH_OUT_OF_RANGE;
    }
  else
    {
      out_validated->word = (char *) word;
      out_validated->free_word = free_word;
      out_validated->byte_len = byte_len;
      out_validated->word_len = word_len;
      out_validated->priority = priority;
      out_validated->enumeration_src = NULL;

      return PARSE_LINE_STATUS_OK;
    }
}

void
validated_word_free (ValidatedWord v)
{
  if (v.free_word)
    {
      g_free (v.word);
    }
}

/* Consumes the line; then @out_validated must be freed by the caller with
 * validated_word_free().
 */
ParseLineStatus
parse_line_broda_scored (ValidatedWord *out_validated,
                         const gchar   *line,
                         gint           min_length,
                         gint           max_length,
                         guchar         threshold)
{
  ParseLineStatus status;
  gchar *composed;
  gssize byte_len;
  gint word_len;
  glong number;
  gchar *start_of_number;
  gchar *after_number;
  gchar *semicolon;

  composed = g_utf8_normalize (line, -1, G_NORMALIZE_NFC);
  semicolon = g_strstr_len (composed, -1, ";");
  if (semicolon == NULL)
    {
      status = PARSE_LINE_STATUS_BAD_SYNTAX;
      goto err;
    }

  semicolon[0] = '\0';
  byte_len = semicolon - composed;
  word_len = g_utf8_strlen (composed, byte_len);

  start_of_number = semicolon + 1;
  number = strtol (start_of_number, &after_number, 10);
  if (after_number == start_of_number
      || *after_number != '\0')
    {
      status = PARSE_LINE_STATUS_BAD_SYNTAX;
      goto err;
    }

  status = validate_word (composed, TRUE, byte_len, word_len,
                          number, min_length, max_length,
                          threshold, out_validated);

  if (status == PARSE_LINE_STATUS_OK)
    return PARSE_LINE_STATUS_OK;

 err:
  g_free (composed);
  return status;
}


static gchar *
scored_parse_word (const gchar *unparsed_word,
                   IpuzCharset *alphabet)
{
  GString *parsed_word;
  const gchar *ptr;

  parsed_word = g_string_new (NULL);
  for (ptr = unparsed_word;
       ptr[0];
       ptr = g_utf8_next_char (ptr))
    {
      gunichar c;
      c = g_utf8_get_char (ptr);
      c = g_unichar_toupper (c);
      if (g_unichar_isdigit (c))
        {
          g_string_free (parsed_word, TRUE);
          return NULL;
        }
      /* The non-full version of this changes '&' to and directly */
      else if (c == '&')
        {
          g_string_append (parsed_word, "AND");

        }
      else if (ipuz_charset_get_char_count (alphabet, c))
        g_string_append_unichar (parsed_word, c);
    }

  return g_string_free_and_steal (parsed_word);
}

static gchar *
scored_parse_enumeration (const gchar *unparsed_word,
                          IpuzCharset *alphabet)
{
  GString *enumeration;
  const gchar *ptr;
  guint char_count;
  gboolean found_char;
  gboolean in_word;
  gboolean valid_enumeration;
  IpuzDeliminator delim;

  enumeration = g_string_new (NULL);

  char_count = 0;
  found_char = FALSE; /* TRUE when we've found any word */
  in_word = FALSE;
  delim = IPUZ_DELIMINATOR_WORD_BREAK;
  valid_enumeration = FALSE;

  for (ptr = unparsed_word;
       ptr[0];
       ptr = g_utf8_next_char (ptr))
    {
      gunichar c;
      c = g_utf8_get_char (ptr);
      c = g_unichar_toupper (c);
      if (c =='"')
        continue;

      if (ipuz_charset_get_char_count (alphabet, c))
        {
          if (!in_word && found_char)
            {
              switch (delim)
                {
                case IPUZ_DELIMINATOR_WORD_BREAK:
                  g_string_append_printf (enumeration, " ");
                  break;
                case IPUZ_DELIMINATOR_PERIOD:
                  g_string_append_printf (enumeration, ".");
                  break;
                case IPUZ_DELIMINATOR_DASH:
                  g_string_append_printf (enumeration, "-");
                  break;
                case IPUZ_DELIMINATOR_APOSTROPHE:
                  g_string_append_printf (enumeration, "'");
                  break;
                default:
                  break;
                }
              valid_enumeration = TRUE;
            }
          in_word = TRUE;
          char_count ++;
          found_char = TRUE;
        }
      else
        {
          if (in_word)
            {
              g_string_append_printf (enumeration, "%u", char_count);
              char_count = 0;
              delim = IPUZ_DELIMINATOR_WORD_BREAK;
            }
          if (c == '.')
            delim = IPUZ_DELIMINATOR_PERIOD;
          else if (c == '-')
            delim = IPUZ_DELIMINATOR_DASH;
          else if (c == '\'')
            delim = IPUZ_DELIMINATOR_APOSTROPHE;
          in_word = FALSE;
        }
    }

  if (in_word)
    g_string_append_printf (enumeration, "%u", char_count);

  if (valid_enumeration)
    return g_string_free_and_steal (enumeration);
  return NULL;
}

ParseLineStatus
parse_line_broda_full (ValidatedWord *out_validated,
                       IpuzCharset   *alphabet,
                       const gchar   *line,
                       gint           min_length,
                       gint           max_length,
                       guchar         threshold)
{
  ParseLineStatus status;
  gchar *composed;
  gchar *parsed_word;
  gssize byte_len;
  gint word_len;
  glong number;
  gchar *start_of_number;
  gchar *after_number;
  gchar *separator;

  composed = g_utf8_normalize (line, -1, G_NORMALIZE_NFC);
  separator = g_strstr_len (composed, -1, "::");
  if (separator == NULL)
    {
      /* If there's no explicit priority, we default to 50 */
      number = 50;
    }
  else
    {
      separator[0] = '\0';

      start_of_number = separator + 2;
      number = strtol (start_of_number, &after_number, 10);
      if (after_number == start_of_number
          || *after_number != '\0')
        {
          status = PARSE_LINE_STATUS_BAD_SYNTAX;
          goto err;
        }
    }

  parsed_word = scored_parse_word (composed, alphabet);
  if (parsed_word == NULL)
    {
      status = PARSE_LINE_STATUS_BAD_SYNTAX;
      goto err;
    }


  byte_len = strlen (parsed_word);
  word_len = g_utf8_strlen (parsed_word, byte_len);
  status = validate_word (parsed_word, TRUE, byte_len, word_len,
                          number, min_length, max_length,
                          threshold, out_validated);

  out_validated->enumeration_src = scored_parse_enumeration (composed, alphabet);

  if (status == PARSE_LINE_STATUS_OK)
    return PARSE_LINE_STATUS_OK;

 err:
  g_free (composed);
  return status;
}


/*
 * TESTING
 */
#ifdef TESTING

static void
rejects_invalid_lines (void)
{
  ValidatedWord v;

  /* invalid syntax */
  g_assert_cmpint (parse_line_broda_scored (&v, g_strdup (""), 1, 100, 40), ==, PARSE_LINE_STATUS_BAD_SYNTAX);
  g_assert_cmpint (parse_line_broda_scored (&v, g_strdup ("FOO"), 1, 100, 40), ==, PARSE_LINE_STATUS_BAD_SYNTAX);
  g_assert_cmpint (parse_line_broda_scored (&v, g_strdup (";42"), 1, 100, 40), ==, PARSE_LINE_STATUS_BAD_SYNTAX);
  g_assert_cmpint (parse_line_broda_scored (&v, g_strdup ("a;42"), 1, 100, 40), ==, PARSE_LINE_STATUS_BAD_SYNTAX);
  g_assert_cmpint (parse_line_broda_scored (&v, g_strdup ("A;"), 1, 100, 0), ==, PARSE_LINE_STATUS_BAD_SYNTAX);
  g_assert_cmpint (parse_line_broda_scored (&v, g_strdup ("A;B"), 1, 100, 0), ==, PARSE_LINE_STATUS_BAD_SYNTAX);
  g_assert_cmpint (parse_line_broda_scored (&v, g_strdup ("A;-"), 1, 100, 0), ==, PARSE_LINE_STATUS_BAD_SYNTAX);

  /* word length out of range_broda_scored */
  g_assert_cmpint (parse_line_broda_scored (&v, g_strdup ("ABC;10"), 1, 2, 1), ==, PARSE_LINE_STATUS_WORD_LENGTH_OUT_OF_RANGE);
  g_assert_cmpint (parse_line_broda_scored (&v, g_strdup ("A;10"), 3, 3, 1), ==, PARSE_LINE_STATUS_WORD_LENGTH_OUT_OF_RANGE);
  g_assert_cmpint (parse_line_broda_scored (&v, g_strdup ("ABCD;10"), 3, 3, 1), ==, PARSE_LINE_STATUS_WORD_LENGTH_OUT_OF_RANGE);

  /* priority out of range */
  g_assert_cmpint (parse_line_broda_scored (&v, g_strdup ("ABCD;10"), 4, 4, 20), ==, PARSE_LINE_STATUS_PRIORITY_OUT_OF_RANGE);
  g_assert_cmpint (parse_line_broda_scored (&v, g_strdup ("ABCD;-10"), 4, 4, 20), ==, PARSE_LINE_STATUS_PRIORITY_OUT_OF_RANGE);
  g_assert_cmpint (parse_line_broda_scored (&v, g_strdup ("ABCD;10000000000000000000000000000000000000000000"), 4, 4, 20),
                   ==,
                   PARSE_LINE_STATUS_PRIORITY_OUT_OF_RANGE);
}

static void
get_entry_works_for_ascii_words (void)
{
  ValidatedWord v;

  /* Plain old word */
  g_assert_cmpint (parse_line_broda_scored (&v, g_strdup ("FOOBAR;42"), 1, 100, 40), ==, PARSE_LINE_STATUS_OK);
  g_assert_cmpstr (v.word, ==, "FOOBAR");
  g_assert_cmpint (v.byte_len, ==, 6);
  g_assert_cmpint (v.word_len, ==, 6);
  g_assert_cmpint (v.priority, ==, 42);
  validated_word_free (v);

  /* Exact size */
  g_assert_cmpint (parse_line_broda_scored (&v, g_strdup ("A;42"), 1, 1, 40), ==, PARSE_LINE_STATUS_OK);
  g_assert_cmpstr (v.word, ==, "A");
  g_assert_cmpint (v.byte_len, ==, 1);
  g_assert_cmpint (v.word_len, ==, 1);
  g_assert_cmpint (v.priority, ==, 42);
  validated_word_free (v);

  /* Just at threshold */
  g_assert_cmpint (parse_line_broda_scored (&v, g_strdup ("FOO;50"), 2, 10, 50), ==, PARSE_LINE_STATUS_OK);
  g_assert_cmpstr (v.word, ==, "FOO");
  g_assert_cmpint (v.byte_len, ==, 3);
  g_assert_cmpint (v.word_len, ==, 3);
  g_assert_cmpint (v.priority, ==, 50);
  validated_word_free (v);

  /* Max priority */
  g_assert_cmpint (parse_line_broda_scored (&v, g_strdup ("FOO;255"), 2, 10, 40), ==, PARSE_LINE_STATUS_OK);
  g_assert_cmpstr (v.word, ==, "FOO");
  g_assert_cmpint (v.byte_len, ==, 3);
  g_assert_cmpint (v.word_len, ==, 3);
  g_assert_cmpint (v.priority, ==, 255);
  validated_word_free (v);
}

static void
get_entry_works_for_non_ascii_words (void)
{
  ValidatedWord v;

  /* Normal, already composed */
  g_assert_cmpint (parse_line_broda_scored (&v, g_strdup ("AÑO;50"), 2, 10, 40), ==, PARSE_LINE_STATUS_OK);
  g_assert_cmpstr (v.word, ==, "AÑO");
  g_assert_cmpint (v.byte_len, ==, 4);
  g_assert_cmpint (v.word_len, ==, 3);
  g_assert_cmpint (v.priority, ==, 50);
  validated_word_free (v);

  /* Decomposed to composed */
  g_assert_cmpint (parse_line_broda_scored (&v, g_strdup ("AÑO;50"), 2, 10, 40), ==, PARSE_LINE_STATUS_OK);
  g_assert_cmpstr (v.word, ==, "AÑO");
  g_assert_cmpint (v.byte_len, ==, 4);
  g_assert_cmpint (v.word_len, ==, 3);
  g_assert_cmpint (v.priority, ==, 50);
  validated_word_free (v);

  /* Decomposed to composed */
  g_assert_cmpint (parse_line_broda_scored (&v, g_strdup ("BLÅHAJ;50"), 2, 10, 40), ==, PARSE_LINE_STATUS_OK);
  g_assert_cmpstr (v.word, ==, "BLÅHAJ");
  g_assert_cmpint (v.byte_len, ==, 7);
  g_assert_cmpint (v.word_len, ==, 6);
  g_assert_cmpint (v.priority, ==, 50);
  validated_word_free (v);
}

static void
test_enumeration (IpuzCharset *alphabet,
                  const gchar *enumeration,
                  const gchar *src)
{
  g_autofree gchar *test_src = NULL;

  test_src = scored_parse_enumeration (enumeration, alphabet);
  g_assert (g_strcmp0 (test_src, src) == 0);
}

static void
parse_enumerations_test (void)
{
  g_autoptr (IpuzCharset) alphabet = ipuz_charset_deserialize ("ABCDEFGHIJKLMNOPQRSTUVWXYZ");

  test_enumeration (alphabet, "Test this", "4 4");
  test_enumeration (alphabet, "ONEWORD", NULL);
  test_enumeration (alphabet, "A.M. P.M.", "1.1.1.1");
  test_enumeration (alphabet, "fly-away home", "3-4 4");
  test_enumeration (alphabet, " .!\'", NULL);
}

int
main (int argc, char **argv)
{
  setlocale(LC_ALL, "en_US.utf8");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/gen_word_list_importer/rejects_invalid_lines", rejects_invalid_lines);
  g_test_add_func ("/gen_word_list_importer/get_entry_works_for_ascii_words", get_entry_works_for_ascii_words);
  g_test_add_func ("/gen_word_list_importer/get_entry_works_for_non_ascii_words", get_entry_works_for_non_ascii_words);
  g_test_add_func ("/gen_word_list_importer/parse_enumerations", parse_enumerations_test);

  return g_test_run ();
}

#endif
