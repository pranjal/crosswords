/* edit-window-actions.c
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* This file is a part of the EditWindow widget and is not a
 * standalone object. Instead, it is meant to encapsulate all code
 * related to actions in one separate file for convenience.
 */


#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include "edit-clue-details.h"
#include "edit-window.h"
#include "edit-save-changes-dialog.h"
#include "edit-window-private.h"
#include "edit-preferences-dialog.h"
#include "edit-preview-window.h"


static void edit_window_actions_show_primary_menu (EditWindow  *self,
                                                   const gchar *action_name,
                                                   GVariant    *param);
static void edit_window_actions_undo              (EditWindow  *self,
                                                   const gchar *action_name,
                                                   GVariant    *param);
static void edit_window_actions_redo              (EditWindow  *self,
                                                   const gchar *action_name,
                                                   GVariant    *param);
static void edit_window_actions_prev_stage        (EditWindow  *self,
                                                   const gchar *action_name,
                                                   GVariant    *param);
static void edit_window_actions_next_stage        (EditWindow  *self,
                                                   const gchar *action_name,
                                                   GVariant    *param);
static void edit_window_actions_goto_stage        (EditWindow  *self,
                                                   const gchar *action_name,
                                                   GVariant    *param);
static void edit_window_actions_goto_grid_stage   (EditWindow  *self,
                                                   const gchar *action_name,
                                                   GVariant    *param);
static void edit_window_actions_open              (EditWindow  *self,
                                                   const gchar *action_name,
                                                   GVariant    *param);
static void edit_window_actions_save              (EditWindow  *self,
                                                   const gchar *action_name,
                                                   GVariant    *param);
static void edit_window_actions_save_as           (EditWindow  *self,
                                                   const gchar *action_name,
                                                   GVariant    *param);
static void edit_window_actions_preview           (EditWindow  *self,
                                                   const gchar *action_name,
                                                   GVariant    *param);
static void edit_window_actions_show_preferences  (EditWindow  *self,
                                                   const gchar *action_name,
                                                   GVariant    *param);
static void edit_window_actions_show_help_overlay (EditWindow  *self,
                                                   const gchar *action_name,
                                                   GVariant    *param);
static void edit_window_actions_close             (EditWindow  *self,
                                                   const gchar *action_name,
                                                   GVariant    *param);
static void edit_window_actions_invert            (EditWindow  *self,
                                                   const gchar *action_name,
                                                   GVariant    *param);


/* Actions */

static void
edit_window_actions_show_primary_menu (EditWindow  *self,
                                       const gchar *action_name,
                                       GVariant    *param)
{
  g_assert (EDIT_IS_WINDOW (self));

  /* FIXME(mystery): Why no keyboard focus? */
  gtk_menu_button_popup (GTK_MENU_BUTTON (self->edit_menu_button));
}

static
void edit_window_actions_undo (EditWindow  *self,
                               const gchar *action_name,
                               GVariant    *param)
{
  g_assert (EDIT_IS_WINDOW (self));

  if (puzzle_stack_can_undo (self->puzzle_stack))
    puzzle_stack_undo (self->puzzle_stack);
  else
    g_warning ("win.undo called on a puzzle that can't be undone\n");
}

static void
edit_window_actions_redo (EditWindow  *self,
                          const gchar *action_name,
                          GVariant    *param)
{
  g_assert (EDIT_IS_WINDOW (self));

  if (puzzle_stack_can_redo (self->puzzle_stack))
    puzzle_stack_redo (self->puzzle_stack);
  else
    g_warning ("win.redo called on a puzzle that can't be redone\n");
}

static void
edit_window_actions_prev_stage (EditWindow  *self,
                                const gchar *action_name,
                                GVariant    *param)
{
  g_assert (EDIT_IS_WINDOW (self));

  //  if (self->edit_state->stage == EDIT_STAGE_METADATA) self->edit_state->stage = EDIT_STAGE_STYLE; /* FIXME(style): remove for now */

  if (self->edit_state->stage > EDIT_STAGE_GRID)
    _edit_window_set_stage (self, self->edit_state->stage - 1);
}

static
void edit_window_actions_next_stage (EditWindow  *self,
                                     const gchar *action_name,
                                     GVariant    *param)
{
  g_assert (EDIT_IS_WINDOW (self));

  //  if (self->edit_state->stage == EDIT_STAGE_CLUES) self->edit_state->stage = EDIT_STAGE_STYLE; /* FIXME(style): remove for now */

  if (self->edit_state->stage < EDIT_STAGE_METADATA)
    _edit_window_set_stage (self, self->edit_state->stage + 1);
}

static void
edit_window_actions_goto_stage (EditWindow  *self,
                                const gchar *action_name,
                                GVariant    *param)
{
  PuzzleStackStage stage;

  g_assert (g_variant_is_of_type (param, G_VARIANT_TYPE_INT32));

  stage = g_variant_get_int32 (param);
  //  if (stage != EDIT_STAGE_STYLE) /* FIXME(style): remove for now */
  _edit_window_set_stage (self, stage);
}

static void
edit_window_actions_goto_grid_stage (EditWindow  *self,
                                     const gchar *action_name,
                                     GVariant    *param)
{
  const gchar *grid_stage;

  g_assert (g_variant_is_of_type (param, G_VARIANT_TYPE_STRING));

  grid_stage = g_variant_get_string (param, NULL);

  _edit_window_set_stage (self, EDIT_STAGE_GRID);
  adw_view_stack_set_visible_child_name (ADW_VIEW_STACK (self->grid_stack),
                                         grid_stage);
}

static void
edit_window_open_response_cb (GtkFileDialog *dialog,
                              GAsyncResult  *result,
                              EditWindow    *self)
{
  g_autoptr(GFile) file = NULL;

  g_assert (EDIT_IS_WINDOW (self));

  file = gtk_file_dialog_open_finish (dialog, result, NULL);

  if (file)
    {
      g_autofree gchar *uri = g_file_get_uri (file);

      edit_window_load_uri (self, uri);
    }
}

static void
edit_window_actions_open (EditWindow  *self,
                          const gchar *action_name,
                          GVariant    *param)
{
  g_autoptr (GtkFileDialog) file_dialog = NULL;
  g_autoptr (GListStore) filters = NULL;
  GtkFileFilter *filter = NULL;

  g_assert (EDIT_IS_WINDOW (self));

  file_dialog = gtk_file_dialog_new ();
  gtk_file_dialog_set_title (file_dialog, _("Select a crossword"));

  filters = g_list_store_new (GTK_TYPE_FILE_FILTER);

  filter = gtk_file_filter_new ();
  gtk_file_filter_set_name (filter, _("Ipuz Puzzles (.ipuz)"));
  gtk_file_filter_add_pattern (filter, "*.ipuz");
  g_list_store_append (filters, filter);
  g_object_unref (filter);

  gtk_file_dialog_set_filters (file_dialog, G_LIST_MODEL (filters));

  gtk_file_dialog_open (file_dialog,
                        GTK_WINDOW (self), NULL,
                        (GAsyncReadyCallback) edit_window_open_response_cb,
                        self);
}

static void
edit_window_actions_save (EditWindow  *self,
                          const gchar *action_name,
                          GVariant    *param)
{
  if (self->uri == NULL)
    edit_window_actions_save_as (self, action_name, param);
  else
    edit_window_save (self, NULL);
}

static void
editor_window_save_as_response (GtkFileDialog *dialog,
                                GAsyncResult  *result,
                                EditWindow    *self)
{
  g_autoptr(GFile) dest = NULL;

  g_assert (EDIT_IS_WINDOW (self));

  dest = gtk_file_dialog_save_finish (dialog, result, NULL);
  if (dest != NULL)
    {
      g_autofree char *uri = g_file_get_uri (dest);
      g_assert (uri != NULL);
      edit_window_save (self, uri);
    }
}

static void
edit_window_actions_save_as (EditWindow  *self,
                             const gchar *action_name,
                             GVariant    *param)
{
  g_autoptr (GtkFileDialog) file_dialog = NULL;
  g_autoptr (GFile) file = NULL;

  g_assert (EDIT_IS_WINDOW (self));

  file_dialog = gtk_file_dialog_new ();
  file = g_file_new_for_path (g_get_user_special_dir (G_USER_DIRECTORY_DOCUMENTS));
  gtk_file_dialog_set_title (file_dialog, _("Save puzzle as"));
  gtk_file_dialog_set_initial_folder (file_dialog, file);

  gtk_file_dialog_save (file_dialog, GTK_WINDOW (self), NULL,
                        (GAsyncReadyCallback) editor_window_save_as_response,
                        self);
}


static void
edit_window_actions_preview (EditWindow  *self,
                             const gchar *action_name,
                             GVariant    *param)
{
  g_assert (EDIT_IS_WINDOW (self));


  if (self->edit_state->preview_window == NULL)
    self->edit_state->preview_window = edit_preview_window_new ();
  edit_preview_window_update (EDIT_PREVIEW_WINDOW (self->edit_state->preview_window),
                              self->edit_state->preview_state);
  gtk_window_present (GTK_WINDOW (self->edit_state->preview_window));
}

static void
edit_window_actions_show_preferences (EditWindow  *self,
                                      const gchar *action_name,
                                      GVariant    *param)
{
  adw_dialog_present (ADW_DIALOG (edit_preferences_dialog_new ()), GTK_WIDGET (self));
}

/* FIXME(mystery): Why is this needed? GtkApplication should do this
 * automatically */
static void
edit_window_actions_show_help_overlay (EditWindow  *self,
                                       const gchar *action_name,
                                       GVariant    *param)
{
  g_autoptr(GtkBuilder) builder = NULL;
  GObject *help_overlay;

  g_assert (EDIT_IS_WINDOW (self));

#ifdef DEVELOPMENT_BUILD
  builder = gtk_builder_new_from_resource ("/org/gnome/Crosswords/Editor/Devel/gtk/help-overlay.ui");
#else
  builder = gtk_builder_new_from_resource ("/org/gnome/Crosswords/Editor/gtk/help-overlay.ui");
#endif

  help_overlay = gtk_builder_get_object (builder, "help_overlay");

  if (GTK_IS_SHORTCUTS_WINDOW (help_overlay))
    {
      gtk_window_set_transient_for (GTK_WINDOW (help_overlay), GTK_WINDOW (self));
      gtk_window_present (GTK_WINDOW (help_overlay));
    }
}

static void
edit_window_actions_close (EditWindow  *self,
                           const gchar *action_name,
                           GVariant    *param)
{
  GList *save_info_list = NULL;
  g_autofree gchar *title = NULL;
  g_autofree gchar *path = NULL;

  g_assert (EDIT_IS_WINDOW (self));

  if (edit_window_get_unsaved_changes (self, &title, &path))
    {
      /* FIXME(uri): We are converting our URI to a path and back to a
       * URI. We should keep a URI throughout the dialog and convert
       * it to a path just to present it. */
      save_info_list = edit_save_info_list_add (save_info_list,
                                                GTK_WIDGET (self),
                                                title, path);
    }

  if (save_info_list == NULL)
    {
      gtk_window_destroy (GTK_WINDOW (self));
    }
  else
    {
      edit_save_changes_dialog_run (GTK_WINDOW (self),
                                    save_info_list,
                                    NULL);
    }
}

static void
edit_window_actions_invert (EditWindow  *self,
                            const gchar *action_name,
                            GVariant    *param)
{
  g_assert (EDIT_IS_WINDOW (self));

  if (self->edit_state->stage != EDIT_STAGE_CLUES)
    return;

  edit_clue_details_selection_invert (EDIT_CLUE_DETAILS (self->clue_details_widget));
}

static void
edit_window_actions_select_all (EditWindow  *self,
                                const gchar *action_name,
                                GVariant    *param)
{
  g_assert (EDIT_IS_WINDOW (self));

  if (self->edit_state->stage != EDIT_STAGE_CLUES)
    return;

  edit_clue_details_select_all (EDIT_CLUE_DETAILS (self->clue_details_widget));
}

void
_edit_window_class_actions_init (GtkWidgetClass *widget_class)
{
  g_assert (EDIT_IS_WINDOW_CLASS (widget_class));

  gtk_widget_class_install_action (widget_class, "win.show-primary-menu", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_show_primary_menu);
  gtk_widget_class_install_action (widget_class, "win.undo", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_undo);
  gtk_widget_class_install_action (widget_class, "win.redo", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_redo);
  gtk_widget_class_install_action (widget_class, "win.prev-stage", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_prev_stage);
  gtk_widget_class_install_action (widget_class, "win.next-stage", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_next_stage);
  gtk_widget_class_install_action (widget_class, "win.goto-stage", "i",
                                   (GtkWidgetActionActivateFunc) edit_window_actions_goto_stage);
  gtk_widget_class_install_action (widget_class, "win.goto-grid-stage", "s",
                                   (GtkWidgetActionActivateFunc) edit_window_actions_goto_grid_stage);
  gtk_widget_class_install_action (widget_class, "win.open", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_open);
  gtk_widget_class_install_action (widget_class, "win.save", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_save);
  gtk_widget_class_install_action (widget_class, "win.save-as", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_save_as);
  gtk_widget_class_install_action (widget_class, "win.grid.commit-pencil", NULL,
                                   (GtkWidgetActionActivateFunc) _win_grid_actions_commit_pencil);
  gtk_widget_class_install_action (widget_class, "win.preview", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_preview);
  gtk_widget_class_install_action (widget_class, "win.show-preferences", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_show_preferences);
  gtk_widget_class_install_action (widget_class, "win.show-help-overlay", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_show_help_overlay);
  gtk_widget_class_install_action (widget_class, "win.close", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_close);

  gtk_widget_class_install_action (widget_class, "clue-details.invert", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_invert);
  gtk_widget_class_install_action (widget_class, "clue-details.select-all", NULL,
                                   (GtkWidgetActionActivateFunc) edit_window_actions_select_all);

  /* Non-visible actions */
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_F10, 0, "win.show-primary-menu", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_z, GDK_CONTROL_MASK, "win.undo", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_z, GDK_CONTROL_MASK | GDK_SHIFT_MASK, "win.redo", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_y, GDK_CONTROL_MASK, "win.redo", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_Page_Up, GDK_CONTROL_MASK, "win.prev-stage", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_Page_Down, GDK_CONTROL_MASK, "win.next-stage", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_1, GDK_ALT_MASK, "win.goto-stage", "i", EDIT_STAGE_GRID);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_2, GDK_ALT_MASK, "win.goto-stage", "i", EDIT_STAGE_CLUES);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_3, GDK_ALT_MASK, "win.goto-stage", "i", EDIT_STAGE_STYLE);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_4, GDK_ALT_MASK, "win.goto-stage", "i", EDIT_STAGE_METADATA);

  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_q, GDK_ALT_MASK, "win.goto-grid-stage", "s", "cell");
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_w, GDK_ALT_MASK, "win.goto-grid-stage", "s", "word-list");
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_e, GDK_ALT_MASK, "win.goto-grid-stage", "s", "autofill");

  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_equal, GDK_CONTROL_MASK, "win.grid.commit-pencil", NULL);

  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_o, GDK_CONTROL_MASK, "win.open", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_s, GDK_CONTROL_MASK, "win.save", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_s, GDK_CONTROL_MASK | GDK_SHIFT_MASK, "win.save-as", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_p, GDK_CONTROL_MASK | GDK_SHIFT_MASK, "win.preview", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_comma, GDK_CONTROL_MASK, "win.show-preferences", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_question, GDK_CONTROL_MASK, "win.show-help-overlay", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_w, GDK_CONTROL_MASK, "win.close", NULL);

  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_i, GDK_CONTROL_MASK, "clue-details.invert", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_a, GDK_CONTROL_MASK, "clue-details.select-all", NULL);
}
