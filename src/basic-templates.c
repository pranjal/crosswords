/* basic-templates.c
 *
 * Copyright 2022 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include "basic-templates.h"
#include "crosswords-misc.h"


GHashTable *barred_models = NULL;
GHashTable *crossword_models = NULL;
GHashTable *cryptic_models = NULL;


struct _BasicTemplateRow
{
  GObject parent_object;
  GdkPixbuf *pixbuf;
  IpuzPuzzle *template;
};


static void              basic_template_row_init       (BasicTemplateRow      *self);
static void              basic_template_row_class_init (BasicTemplateRowClass *klass);
static void              basic_template_row_dispose    (GObject               *object);
static BasicTemplateRow *basic_template_row_new        (IpuzPuzzle            *template,
                                                        GdkPixbuf             *pixbuf);


G_DEFINE_TYPE (BasicTemplateRow, basic_template_row, G_TYPE_OBJECT);


static void
append_template (GListStore *store,
                 IpuzPuzzle *template)
{
  g_autoptr (BasicTemplateRow) row = NULL;
  g_autoptr (GdkPixbuf) pixbuf = NULL;

  g_return_if_fail (G_IS_LIST_STORE (store));
  g_return_if_fail (IPUZ_IS_PUZZLE (template));

  pixbuf = xwd_thumbnail_puzzle (template);
  row = basic_template_row_new (template, pixbuf);

  g_list_store_append (store, row);
}

static gboolean
append_resource (GListStore  *store,
                 const gchar *resource_path)
{
  g_autoptr (IpuzPuzzle) template = NULL;

  g_return_val_if_fail (G_IS_LIST_STORE (store), FALSE);

  template = xwd_load_puzzle_from_resource (NULL, resource_path, NULL);
  if (template == NULL)
    return FALSE;

  append_template (store, template);

  return TRUE;
}

static void
append_resources (GListStore  *store,
                  const gchar *size,
                  const gchar *type)
{
  guint i = 1;

  while (TRUE)
    {
      g_autofree gchar *resource_path = NULL;

      resource_path =
        g_strdup_printf ("/org/gnome/Crosswords/crosswords/%s-%s-%u.ipuz",
                         size, type, i);

      if (! append_resource (store, resource_path))
        break;
      i++;
    }
}

static GListStore *
create_model (IpuzPuzzleKind kind,
              guint          width,
              guint          height)
{
  GListStore *model;
  g_autoptr (IpuzCrossword) template = NULL;
  GType type;

  switch (kind)
    {
    case IPUZ_PUZZLE_ACROSTIC:
      type = IPUZ_TYPE_ACROSTIC;
      break;
    case IPUZ_PUZZLE_ARROWWORD:
      type = IPUZ_TYPE_ARROWWORD;
      break;
    case IPUZ_PUZZLE_BARRED:
      type = IPUZ_TYPE_BARRED;
      break;
    case IPUZ_PUZZLE_CROSSWORD:
      type = IPUZ_TYPE_CROSSWORD;
      break;
    case IPUZ_PUZZLE_CRYPTIC:
      type = IPUZ_TYPE_CRYPTIC;
      break;
    case IPUZ_PUZZLE_FILIPPINE:
      type = IPUZ_TYPE_FILIPPINE;
      break;
    default:
      g_assert_not_reached ();
    }

  model = g_list_store_new (BASIC_TYPE_TEMPLATE_ROW);
  template = g_object_new (type,
                           "width", (gint) width,
                           "height", (gint) height,
                           NULL);
  if (IPUZ_IS_CROSSWORD (template))
    ipuz_crossword_fix_all (IPUZ_CROSSWORD (template), NULL);
  append_template (model, IPUZ_PUZZLE (template));

  return model;
}

static void
basic_template_load_type (GHashTable     *hash,
                          IpuzPuzzleKind  kind)
{
  GListStore *store;
  const gchar *kind_str = NULL;

  switch (kind)
    {
    case IPUZ_PUZZLE_ACROSTIC:
      kind_str = "acrostic";
      break;
    case IPUZ_PUZZLE_ARROWWORD:
      kind_str = "arrowword";
      break;
    case IPUZ_PUZZLE_BARRED:
      kind_str = "barred";
      break;
    case IPUZ_PUZZLE_CROSSWORD:
      kind_str = "standard";
      break;
    case IPUZ_PUZZLE_CRYPTIC:
      kind_str = "cryptic";
      break;
    case IPUZ_PUZZLE_FILIPPINE:
      kind_str = "filippine";
      break;
    default:
      g_assert_not_reached ();
    }

  /* Small  */
  store = create_model (kind, SMALL_SIZE, SMALL_SIZE);
  append_resources (store, "small", kind_str);
  g_hash_table_insert (hash, GINT_TO_POINTER (BASIC_SIZE_SMALL), store);

  /* Medium */
  store = create_model (kind, MEDIUM_SIZE, MEDIUM_SIZE);
  append_resources (store, "medium", kind_str);
  g_hash_table_insert (hash, GINT_TO_POINTER (BASIC_SIZE_MEDIUM), store);

  /* Large */
  store = create_model (kind, LARGE_SIZE, LARGE_SIZE);
  append_resources (store, "large", kind_str);
  g_hash_table_insert (hash, GINT_TO_POINTER (BASIC_SIZE_LARGE), store);

  /* X-Large */
  store = create_model (kind, X_LARGE_SIZE, X_LARGE_SIZE);
  append_resources (store, "x-large", kind_str);
  g_hash_table_insert (hash, GINT_TO_POINTER (BASIC_SIZE_X_LARGE), store);
}

static void
basic_template_load (void)
{
  static gsize guard = 0;

  if (!g_once_init_enter (&guard))
    return;

  barred_models = g_hash_table_new (g_direct_hash, g_direct_equal);
  crossword_models = g_hash_table_new (g_direct_hash, g_direct_equal);
  cryptic_models = g_hash_table_new (g_direct_hash, g_direct_equal);

  basic_template_load_type (barred_models, IPUZ_PUZZLE_BARRED);
  basic_template_load_type (crossword_models, IPUZ_PUZZLE_CROSSWORD);
  basic_template_load_type (cryptic_models, IPUZ_PUZZLE_CRYPTIC);

  g_once_init_leave (&guard, 1);
}

GListModel *
basic_template_get_model (IpuzPuzzleKind kind,
                          BasicSize      size)
{
  basic_template_load ();

  g_assert (size >= BASIC_SIZE_SMALL &&
            size < BASIC_SIZE_CUSTOM);
  switch (kind)
    {
    case IPUZ_PUZZLE_CROSSWORD:
      return (GListModel *) g_hash_table_lookup (crossword_models, GINT_TO_POINTER (size));
    case IPUZ_PUZZLE_CRYPTIC:
      return (GListModel *) g_hash_table_lookup (cryptic_models, GINT_TO_POINTER (size));
    case IPUZ_PUZZLE_BARRED:
      return (GListModel *) g_hash_table_lookup (barred_models, GINT_TO_POINTER (size));
    default:
      break;
    }
  return NULL;
}



static void
basic_template_row_init (BasicTemplateRow *self)
{

}

static void
basic_template_row_class_init (BasicTemplateRowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = basic_template_row_dispose;
}

static void
basic_template_row_dispose (GObject *object)
{
  BasicTemplateRow *self = BASIC_TEMPLATE_ROW (object);

  g_clear_object (&self->pixbuf);
  g_clear_object (&self->template);
}


static BasicTemplateRow *
basic_template_row_new (IpuzPuzzle *template,
                        GdkPixbuf  *pixbuf)
{
  BasicTemplateRow *row;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (template), NULL);
  g_return_val_if_fail (GDK_IS_PIXBUF (pixbuf), NULL);

  row = g_object_new (BASIC_TYPE_TEMPLATE_ROW, NULL);

  row->template = g_object_ref (template);
  row->pixbuf = g_object_ref (pixbuf);

  return row;
}

/* Public methods */

GListModel *
basic_template_create_custom_model (IpuzPuzzleKind kind,
                                    guint          width,
                                    guint          height)
{
  return (GListModel *) create_model (kind, width, height);
}

GdkPixbuf *
basic_template_row_get_pixbuf (BasicTemplateRow *template_row)
{
  g_return_val_if_fail (BASIC_IS_TEMPLATE_ROW (template_row), NULL);

  return template_row->pixbuf;
}

IpuzPuzzle *
basic_template_row_get_template (BasicTemplateRow *template_row)
{
  g_return_val_if_fail (BASIC_IS_TEMPLATE_ROW (template_row), NULL);

  return template_row->template;
}
