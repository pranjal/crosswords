/* edit-preferences-dialog.c
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include <glib/gi18n.h>

#include "edit-app.h"
#include "edit-preferences-dialog.h"
#include "word-list-resources.h"

struct _EditPreferencesDialog
{
  AdwPreferencesDialog parent_instance;

  GSettings *settings;

  /* Template widgets */
  GtkWidget *author_row;
  GtkWidget *copyright_row;
  GtkWidget *url_row;
  GtkWidget *word_list_group;
};


static void       edit_preferences_dialog_init       (EditPreferencesDialog      *self);
static void       edit_preferences_dialog_class_init (EditPreferencesDialogClass *klass);
static void       edit_preferences_dialog_dispose    (GObject                    *object);
static GtkWidget *create_word_list_row               (guint                       index,
                                                      GtkWidget                  *radio);


G_DEFINE_TYPE (EditPreferencesDialog, edit_preferences_dialog, ADW_TYPE_PREFERENCES_DIALOG);


static void
edit_preferences_dialog_init (EditPreferencesDialog *self)
{
  GtkWidget *group = NULL;
  gtk_widget_init_template (GTK_WIDGET (self));

#if DEVELOPMENT_BUILD
  gtk_widget_add_css_class (GTK_WIDGET (self), "devel");
#endif

  self->settings = g_settings_new ("org.gnome.Crosswords.Editor");
  g_settings_bind (self->settings, "default-author",
                   self->author_row, "text",
                   G_SETTINGS_BIND_DEFAULT);
  g_settings_bind (self->settings, "default-copyright",
                   self->copyright_row, "text",
                   G_SETTINGS_BIND_DEFAULT);
  g_settings_bind (self->settings, "default-url",
                   self->url_row, "text",
                   G_SETTINGS_BIND_DEFAULT);


  for (guint i = 0; i < word_list_resources_get_count(WORD_LIST_RESOURCES_DEFAULT); i++)
    {
      GtkWidget *radio;

      radio = gtk_check_button_new ();
      if (group == NULL)
        group = radio;
      else
        gtk_check_button_set_group (GTK_CHECK_BUTTON (radio), GTK_CHECK_BUTTON (group));

      adw_preferences_group_add (ADW_PREFERENCES_GROUP (self->word_list_group),
                                 create_word_list_row (i, radio));
    }
}

static void
edit_preferences_dialog_class_init (EditPreferencesDialogClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = edit_preferences_dialog_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-preferences-dialog.ui");

  gtk_widget_class_bind_template_child (widget_class, EditPreferencesDialog, author_row);
  gtk_widget_class_bind_template_child (widget_class, EditPreferencesDialog, copyright_row);
  gtk_widget_class_bind_template_child (widget_class, EditPreferencesDialog, url_row);
  gtk_widget_class_bind_template_child (widget_class, EditPreferencesDialog, word_list_group);
}

static void
edit_preferences_dialog_dispose (GObject *object)
{
  EditPreferencesDialog *self;

  self = EDIT_PREFERENCES_DIALOG (object);

  g_clear_object (&self->settings);

  G_OBJECT_CLASS (edit_preferences_dialog_parent_class)->dispose (object);
}

static void
radio_toggled_cb (GtkWidget *radio,
                  gpointer   user_data)
{
  if (gtk_check_button_get_active (GTK_CHECK_BUTTON (radio)))
    {
      guint index = GPOINTER_TO_UINT (user_data);

      word_list_resources_set_default (WORD_LIST_RESOURCES_DEFAULT,
                                       word_list_resources_get_id (WORD_LIST_RESOURCES_DEFAULT, index));
    }
}

static GtkWidget *
create_word_list_row (guint      index,
                      GtkWidget *radio)
{
  GtkWidget *row;
  g_autofree gchar *subtitle = NULL;

  subtitle = g_strdup_printf (_("%'u words"), word_list_resources_get_word_count (WORD_LIST_RESOURCES_DEFAULT, index));
  gtk_widget_set_valign (radio, GTK_ALIGN_CENTER);

  row = g_object_new (ADW_TYPE_ACTION_ROW,
                      "activatable-widget", radio,
                      "title", word_list_resources_get_display_name (WORD_LIST_RESOURCES_DEFAULT,
                                                                     index),
                      "subtitle", subtitle,
                      NULL);
  adw_action_row_add_prefix (ADW_ACTION_ROW (row), radio);
  gtk_check_button_set_active (GTK_CHECK_BUTTON (radio),
                               word_list_resources_get_is_default (WORD_LIST_RESOURCES_DEFAULT,
                                                                   index));
  g_signal_connect (radio, "toggled",
                    G_CALLBACK (radio_toggled_cb),
                    GUINT_TO_POINTER (index));

  return row;
}


/* Public functions */

GtkWidget *
edit_preferences_dialog_new (void)
{
  GtkWidget *dialog;

  dialog = g_object_new (EDIT_TYPE_PREFERENCES_DIALOG, NULL);

  return dialog;
}
