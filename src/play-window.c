/* play-window.c
 *
 * Copyright 2021 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include <adwaita.h>
#include "crosswords-app.h"
#include "crosswords-quirks.h"
#include "play-grid.h"
#include "play-preferences-dialog.h"
#include "play-window.h"
#include "puzzle-set.h"
#include "puzzle-set-action-row.h"
#include "puzzle-set-list.h"

#define LIST_ROW_USER_DATA "puzzle_set"
#define MAX_URL_LEN 50

struct _PlayWindow
{
  AdwApplicationWindow parent_instance;

  PuzzleSet *uri_puzzle_set;
  PuzzleSet *active_puzzle_set;
  PuzzlePhase current_phase;
  GSettings *settings;
  GAction *zoom_action;

  /* Template widgets */
  GtkWidget *toast_overlay;
  GtkWidget *header_bar;
  GtkWidget *title_label;
  GtkWidget *prev_button;
  GtkWidget *reveal;
  GtkWidget *help_menu_button;
  GtkWidget *info_menu_button;
  GtkWidget *stack;
  GtkWidget *box;
  GtkWidget *main_listbox;
  GtkWidget *hero_grid;
  GtkWidget *info_title_label;
  GtkWidget *info_difficulty_label;
  GtkWidget *info_author_label;
  GtkWidget *info_editor_label;
  GtkWidget *info_publisher_label;
  GtkWidget *info_publication_label;
  GtkWidget *info_origin_label;
  GtkWidget *info_date_label;
  GtkWidget *info_copyright_label;
  GtkWidget *info_link_separator;
  GtkWidget *info_link_button;
  GtkWidget *info_devel_separator;
  GtkWidget *info_devel_file_button;
};


static void       play_window_init                         (PlayWindow         *self);
static void       play_window_class_init                   (PlayWindowClass    *klass);
static void       play_window_dispose                      (GObject            *object);
static void       play_window_constructed                  (GObject            *object);
static gboolean   play_window_close_request                (GtkWindow          *window);
static void       play_window_actions_go_home_cb           (GtkWidget          *widget,
                                                            const gchar        *action_name,
                                                            GVariant           *param);
static void       play_window_actions_go_prev_cb           (GtkWidget          *widget,
                                                            const gchar        *action_name,
                                                            GVariant           *param);
static void       play_window_actions_zoom_in_out_cb       (GtkWidget          *widget,
                                                            const gchar        *action_name,
                                                            GVariant           *param);
static void       play_window_actions_reveal_cb            (GtkWidget          *widget,
                                                            const gchar        *action_name,
                                                            GVariant           *param);
static void       play_window_actions_reset_cb             (GtkWidget          *widget,
                                                            const gchar        *action_name,
                                                            GVariant           *param);
static void       play_window_actions_hint_cb              (GtkWidget          *widget,
                                                            const gchar        *action_name,
                                                            GVariant           *param);
static void       play_window_actions_show_help_overlay_cb (GtkWidget          *widget,
                                                            const gchar        *action_name,
                                                            GVariant           *param);
static void       play_window_actions_show_preferences_cb  (GtkWidget          *widget,
                                                            const gchar        *action_name,
                                                            GVariant           *param);
static void       add_puzzle_sets_activated_cb             (PlayWindow         *play_window);
static GtkWidget *create_listrow_func                      (PuzzleSet          *puzzle_set,
                                                            PlayWindow         *play_window);
static void       play_window_update_title                 (PlayWindow         *play_window);
static void       play_window_update                       (PlayWindow         *play_window);
static void       row_activated_cb                         (PuzzleSetActionRow *row,
                                                            PlayWindow         *play_window);
static void       change_phase_cb                          (PuzzleSet          *puzzle_set,
                                                            PuzzlePhase         phase,
                                                            PlayWindow         *play_window);
static void       puzzles_done_cb                          (PuzzleSet          *puzzle_set,
                                                            PlayWindow         *play_window);
static void       reveal_canceled_cb                       (PuzzleSet          *puzzle_set,
                                                            PlayWindow         *play_window);




G_DEFINE_TYPE (PlayWindow, play_window, ADW_TYPE_APPLICATION_WINDOW);


static void
play_window_init (PlayWindow *self)
{
  g_autoptr (GInputStream) stream = NULL;
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  g_autoptr (GSimpleActionGroup) group = NULL;
  g_autoptr (GAction) zoom_level_action = NULL;
  g_autoptr (GAction) zoom_in_out_action = NULL;
  GListModel *puzzle_set_list;
  GtkFilter *puzzle_set_filter;
  g_autoptr (GtkFilterListModel) filter_model = NULL;
  GridState *state;
  guint n_items;

  gtk_widget_init_template (GTK_WIDGET (self));
  gtk_widget_add_css_class (GTK_WIDGET (self), "play");

  /* Load the hero section */
  stream = g_resources_open_stream ("/org/gnome/Crosswords/crosswords/hero.ipuz", 0, NULL);
  puzzle = ipuz_puzzle_new_from_stream (stream, NULL, NULL);
  g_input_stream_close (G_INPUT_STREAM (stream), NULL, NULL);
  g_assert (puzzle != NULL);

  state = grid_state_new (IPUZ_CROSSWORD (puzzle), NULL, GRID_STATE_VIEW);
  play_grid_update_state (PLAY_GRID (self->hero_grid), state, layout_config_default (IPUZ_PUZZLE_CROSSWORD));
  grid_state_free (state);

  /* hook up to settings */
  self->settings = g_settings_new ("org.gnome.Crosswords");

  /* Initial window size */
  g_settings_bind (self->settings, "width",
                   self, "default-width",
                   G_SETTINGS_BIND_DEFAULT);
  g_settings_bind (self->settings, "height",
                   self, "default-height",
                    G_SETTINGS_BIND_DEFAULT);
  g_settings_bind (self->settings, "is-maximized",
                   self, "maximized",
                   G_SETTINGS_BIND_DEFAULT);
  g_settings_bind (self->settings, "is-fullscreen",
                   self, "fullscreened",
                   G_SETTINGS_BIND_DEFAULT);

  /* Zoom */
  group = g_simple_action_group_new ();
  zoom_level_action = g_settings_create_action (self->settings, "zoom-level");
  g_action_map_add_action (G_ACTION_MAP (group), zoom_level_action);
  zoom_in_out_action = (GAction *) g_simple_action_new ("zoom-in-out", G_VARIANT_TYPE ("s"));
  g_action_map_add_action (G_ACTION_MAP (group), zoom_in_out_action);

  /* This action is used just to set the sensitivity of the zoom menu */
  self->zoom_action = (GAction *)g_simple_action_new ("zoom", NULL);
  g_action_map_add_action (G_ACTION_MAP (group), self->zoom_action);
  gtk_widget_insert_action_group (GTK_WIDGET (self), "win", G_ACTION_GROUP (group));

  /* Load the list of games */
  self->current_phase = PUZZLE_PHASE_MAIN;
  puzzle_set_list = puzzle_set_list_get ();
  puzzle_set_filter = puzzle_set_filter_new (PUZZLE_SET_FILTER_USER_SHOWN | PUZZLE_SET_FILTER_SPECIAL,
                                             self->settings);
  filter_model = gtk_filter_list_model_new (g_object_ref (puzzle_set_list),
                                            puzzle_set_filter);

  /* Set up the signals on all the PuzzleSets, regardless of whether
   * we show them. If it ever turns out to be a problem, we will fix
   * it. */
  n_items = g_list_model_get_n_items (puzzle_set_list);
  for (guint i = 0; i < n_items; i++)
    {
      GObject *puzzle_set;

      puzzle_set = g_list_model_get_object (puzzle_set_list, i);
      g_signal_connect (puzzle_set, "change-phase", G_CALLBACK (change_phase_cb), self);
      g_signal_connect (puzzle_set, "puzzles-done", G_CALLBACK (puzzles_done_cb), self);
      g_signal_connect (puzzle_set, "reveal-canceled", G_CALLBACK (reveal_canceled_cb), self);

      if (!g_strcmp0 ("uri", puzzle_set_get_id (PUZZLE_SET (puzzle_set))))
        self->uri_puzzle_set = PUZZLE_SET (puzzle_set);
    }

  gtk_list_box_bind_model (GTK_LIST_BOX (self->main_listbox),
                           G_LIST_MODEL (filter_model),
                           (GtkListBoxCreateWidgetFunc) create_listrow_func,
                           self, NULL);

  play_window_update (self);

  /* This assertion tests that when the PlayWindow is just created, it
   * has a refcount of 1, like a plain GtkApplicationWindow.
   *
   * We had a bug where one of the objects initialized above inadvertently created
   * a strong reference to the PlayWindow, without PlayWindow being aware of that (e.g. so that
   * it could unref those objects at window-closing time).  This caused
   * the window to never be disposed/finalized.
   *
   * If you ever really want the window to start with a refcount higher than 1,
   * make sure that refcount gets reduced when the window is being closed.
   */
  g_assert_cmpint (G_OBJECT (self)->ref_count, ==, 1);

}

static void
play_window_class_init (PlayWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GtkWindowClass *window_class = GTK_WINDOW_CLASS (klass);

  object_class->dispose = play_window_dispose;
  object_class->constructed = play_window_constructed;
  window_class->close_request = play_window_close_request;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/play-window.ui");
  gtk_widget_class_bind_template_child (widget_class, PlayWindow, toast_overlay);
  gtk_widget_class_bind_template_child (widget_class, PlayWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, PlayWindow, title_label);
  gtk_widget_class_bind_template_child (widget_class, PlayWindow, help_menu_button);
  gtk_widget_class_bind_template_child (widget_class, PlayWindow, info_menu_button);
  gtk_widget_class_bind_template_child (widget_class, PlayWindow, prev_button);
  gtk_widget_class_bind_template_child (widget_class, PlayWindow, stack);
  gtk_widget_class_bind_template_child (widget_class, PlayWindow, box);
  gtk_widget_class_bind_template_child (widget_class, PlayWindow, main_listbox);
  gtk_widget_class_bind_template_child (widget_class, PlayWindow, hero_grid);
  gtk_widget_class_bind_template_child (widget_class, PlayWindow, info_title_label);
  gtk_widget_class_bind_template_child (widget_class, PlayWindow, info_difficulty_label);
  gtk_widget_class_bind_template_child (widget_class, PlayWindow, info_author_label);
  gtk_widget_class_bind_template_child (widget_class, PlayWindow, info_editor_label);
  gtk_widget_class_bind_template_child (widget_class, PlayWindow, info_publisher_label);
  gtk_widget_class_bind_template_child (widget_class, PlayWindow, info_publication_label);
  gtk_widget_class_bind_template_child (widget_class, PlayWindow, info_origin_label);
  gtk_widget_class_bind_template_child (widget_class, PlayWindow, info_date_label);
  gtk_widget_class_bind_template_child (widget_class, PlayWindow, info_copyright_label);
  gtk_widget_class_bind_template_child (widget_class, PlayWindow, info_link_separator);
  gtk_widget_class_bind_template_child (widget_class, PlayWindow, info_link_button);
  gtk_widget_class_bind_template_child (widget_class, PlayWindow, info_devel_separator);
  gtk_widget_class_bind_template_child (widget_class, PlayWindow, info_devel_file_button);

  gtk_widget_class_install_action (widget_class, "win.go-home", NULL,
                                   play_window_actions_go_home_cb);
  gtk_widget_class_install_action (widget_class, "win.go-prev", NULL,
                                   play_window_actions_go_prev_cb);
  gtk_widget_class_install_action (widget_class, "win.zoom-in-out", "s",
                                   play_window_actions_zoom_in_out_cb);
  gtk_widget_class_install_action (widget_class, "xword.reveal", NULL,
                                   play_window_actions_reveal_cb);
  gtk_widget_class_install_action (widget_class, "xword.clear", NULL,
                                   play_window_actions_reset_cb);
  gtk_widget_class_install_action (widget_class, "xword.hint", NULL,
                                   play_window_actions_hint_cb);
  gtk_widget_class_install_action (widget_class, "win.show-help-overlay", NULL,
                                   play_window_actions_show_help_overlay_cb);
  gtk_widget_class_install_action (widget_class, "win.show-preferences", NULL,
                                   play_window_actions_show_preferences_cb);

  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_Left, GDK_ALT_MASK, "win.go-prev", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_Home, 0, "win.go-home", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_Back, 0, "win.go-prev", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_s, GDK_CONTROL_MASK, "xword.reveal", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_r, GDK_CONTROL_MASK, "xword.clear", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_h, GDK_CONTROL_MASK, "xword.hint", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_question, GDK_CONTROL_MASK, "win.show-help-overlay", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_comma, GDK_CONTROL_MASK, "win.show-preferences", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_0, GDK_CONTROL_MASK, "win.zoom-level", "s", "normal");
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_plus, GDK_CONTROL_MASK, "win.zoom-in-out", "s", "in");
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_ZoomIn, GDK_CONTROL_MASK, "win.zoom-in-out", "s", "in");
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_minus, GDK_CONTROL_MASK, "win.zoom-in-out", "s", "out");
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_ZoomOut, GDK_CONTROL_MASK, "win.zoom-in-out", "s", "out");
}

static void
play_window_dispose (GObject *object)
{
  PlayWindow *self;

  self = PLAY_WINDOW (object);

  if (self->active_puzzle_set)
    puzzle_set_puzzles_done (self->active_puzzle_set);

  g_clear_object (&self->settings);
  self->active_puzzle_set = NULL;

  G_OBJECT_CLASS (play_window_parent_class)->dispose (object);
}

static void
play_window_constructed (GObject *object)
{
  PlayWindow *self;

  self = PLAY_WINDOW (object);

  /* Initial window size */
  /* Note, this has to be done in the constructed vfunc instead of
   * _init because maximized and fullscreen are construct properties
   * and will override whatever is set there.
   */
  g_settings_bind (self->settings, "width",
                   self, "default-width",
                   G_SETTINGS_BIND_DEFAULT);
  g_settings_bind (self->settings, "height",
                   self, "default-height",
                    G_SETTINGS_BIND_DEFAULT);
  g_settings_bind (self->settings, "is-maximized",
                   self, "maximized",
                   G_SETTINGS_BIND_DEFAULT);
  g_settings_bind (self->settings, "is-fullscreen",
                   self, "fullscreened",
                   G_SETTINGS_BIND_DEFAULT);

  G_OBJECT_CLASS (play_window_parent_class)->constructed (object);
}


static void
add_puzzle_sets_activated_cb (PlayWindow *play_window)
{
  adw_dialog_present (ADW_DIALOG (play_preferences_dialog_new (TRUE)),
                      GTK_WIDGET (play_window));
}

static GtkWidget *
create_listrow_func (PuzzleSet *puzzle_set, PlayWindow *play_window)
{
  GtkWidget *list_row;

  /* Special case the last button */
  if (g_strcmp0 (puzzle_set_get_id (puzzle_set), "add-puzzle-sets") == 0)
    {
      list_row = (GtkWidget *) g_object_new (ADW_TYPE_ACTION_ROW,
                                             "title", puzzle_set_get_short_name (puzzle_set),
                                             "icon-name", "list-add-symbolic",
                                             "activatable", TRUE,
                                             NULL);
      g_signal_connect_swapped (G_OBJECT (list_row), "activated",
                                G_CALLBACK (add_puzzle_sets_activated_cb), play_window);

      return list_row;
    }
  list_row = puzzle_set_action_row_new (puzzle_set);
  g_object_set (G_OBJECT (list_row),
                "title", puzzle_set_get_short_name (puzzle_set),
                "subtitle", puzzle_set_get_long_name (puzzle_set),
                "activatable", TRUE,
                NULL);
  g_signal_connect (G_OBJECT (list_row), "activated",
                    G_CALLBACK (row_activated_cb), play_window);
  return list_row;
}

static gboolean
play_window_close_request (GtkWindow *window)
{
  PlayWindow *self;

  self = PLAY_WINDOW (window);

  if (self->active_puzzle_set)
    puzzle_set_puzzles_done (self->active_puzzle_set);

  return GTK_WINDOW_CLASS (play_window_parent_class)->close_request (window);
}

static void
play_window_actions_go_home_cb (GtkWidget   *widget,
                                const gchar *action_name,
                                GVariant    *param)
{
  PlayWindow *play_window = PLAY_WINDOW (widget);

  if (play_window->active_puzzle_set)
    puzzle_set_puzzles_done (play_window->active_puzzle_set);
}

static void
play_window_actions_go_prev_cb (GtkWidget   *widget,
                                const gchar *action_name,
                                GVariant    *param)
{
  PlayWindow *play_window = PLAY_WINDOW (widget);

  if (play_window->current_phase == PUZZLE_PHASE_GAME)
      puzzle_set_change_phase (play_window->active_puzzle_set, PUZZLE_PHASE_PICKER);
  else if (play_window->current_phase == PUZZLE_PHASE_PICKER)
    puzzle_set_puzzles_done (play_window->active_puzzle_set);
}

static void
play_window_begin_active_set (PlayWindow *play_window)
{
  GtkWidget *widget;

  g_assert (play_window->active_puzzle_set != NULL);

  puzzle_set_puzzles_start (play_window->active_puzzle_set);
  widget = puzzle_set_get_widget (play_window->active_puzzle_set,
                                  PUZZLE_PHASE_PICKER);
  if (widget)
    gtk_stack_add_named (GTK_STACK (play_window->stack), widget, "picker");

  widget = puzzle_set_get_widget (play_window->active_puzzle_set,
                                  PUZZLE_PHASE_GAME);
  if (widget)
    {
      gtk_stack_add_named (GTK_STACK (play_window->stack), widget, "game");
      g_signal_connect_swapped (widget, "notify::compact-mode", G_CALLBACK (play_window_update_title), play_window);
    }

  puzzle_set_change_phase (play_window->active_puzzle_set, PUZZLE_PHASE_PICKER);
}

static void
play_window_actions_zoom_in_out_cb (GtkWidget   *widget,
                                    const gchar *action_name,
                                    GVariant    *param)
{
  PlayWindow *play_window = PLAY_WINDOW (widget);
  g_autofree gchar *zoom_level = NULL;
  const gchar *direction;
  ZoomLevel level = ZOOM_NORMAL;

  g_assert (g_variant_is_of_type (param, G_VARIANT_TYPE_STRING));

  zoom_level = g_settings_get_string (play_window->settings, "zoom-level");

  direction = g_variant_get_string (param, NULL);
  level = zoom_level_from_string (zoom_level);

  if (g_strcmp0 (direction, "in") == 0)
    level = zoom_level_zoom_in (level);
  else if (g_strcmp0 (direction, "out") == 0)
    level = zoom_level_zoom_out (level);
  else
    g_assert_not_reached ();

  g_settings_set_string (play_window->settings, "zoom-level",
                         zoom_level_to_string (level));
}

static void
play_window_actions_reveal_cb (GtkWidget   *widget,
                               const gchar *action_name,
                               GVariant    *param)
{
  PlayWindow *play_window = PLAY_WINDOW (widget);

  if (play_window->current_phase != PUZZLE_PHASE_GAME)
    return;

  if (play_window->active_puzzle_set)
    puzzle_set_reveal_toggled (play_window->active_puzzle_set, TRUE);
}


static void
play_window_actions_reset_cb (GtkWidget   *widget,
                              const gchar *action_name,
                              GVariant    *param)
{
  PlayWindow *play_window = PLAY_WINDOW (widget);

  if (play_window->current_phase != PUZZLE_PHASE_GAME)
    return;

  if (play_window->active_puzzle_set)
    puzzle_set_reset_puzzle (play_window->active_puzzle_set);
}

static void
play_window_actions_hint_cb (GtkWidget   *widget,
                             const gchar *action_name,
                             GVariant    *param)
{
  PlayWindow *play_window = PLAY_WINDOW (widget);

  if (play_window->current_phase != PUZZLE_PHASE_GAME)
    return;

  puzzle_set_show_hint (play_window->active_puzzle_set);
}


static void
play_window_actions_show_help_overlay_cb (GtkWidget   *widget,
                                          const gchar *action_name,
                                          GVariant    *param)
{
  PlayWindow *play_window = PLAY_WINDOW (widget);
  g_autoptr(GtkBuilder) builder = NULL;
  g_autoptr(CrosswordsQuirks) quirks = NULL;
  IpuzPuzzle *puzzle = NULL;
  GObject *help_overlay;
  GtkWidget *ij_digraph_shortcut;
  GtkWidget *normal_switch;
  GtkWidget *acrostic_switch;
  gboolean is_acrostic = FALSE;

#ifdef DEVELOPMENT_BUILD
  builder = gtk_builder_new_from_resource ("/org/gnome/Crosswords/Devel/gtk/help-overlay.ui");
#else
  builder = gtk_builder_new_from_resource ("/org/gnome/Crosswords/gtk/help-overlay.ui");
#endif

  help_overlay = gtk_builder_get_object (builder, "help_overlay");

  /* Adjust the help overlay to match the current puzzle type/style */
  if (play_window->current_phase == PUZZLE_PHASE_GAME)
    {
      puzzle = puzzle_set_get_puzzle (play_window->active_puzzle_set, PUZZLE_PHASE_GAME);
      g_assert (puzzle);
      is_acrostic = IPUZ_IS_ACROSTIC (puzzle);
    }

  /* We create the quirks even if puzzle is null. that means we show
   * the ij digraph hint even if no puzzle is set, IFF your LANG is
   * dutch */
  quirks = crosswords_quirks_new (puzzle);
  ij_digraph_shortcut = (GtkWidget *) gtk_builder_get_object (builder, "ij_digraph");
  normal_switch = (GtkWidget *) gtk_builder_get_object (builder, "normal_switch");
  acrostic_switch = (GtkWidget *) gtk_builder_get_object (builder, "acrostic_switch");


  gtk_widget_set_visible (ij_digraph_shortcut,
                          crosswords_quirks_get_ij_digraph (quirks));
  gtk_widget_set_visible (normal_switch, !is_acrostic);;
  gtk_widget_set_visible (acrostic_switch, is_acrostic);

  if (GTK_IS_SHORTCUTS_WINDOW (help_overlay))
    {
      gtk_window_set_transient_for (GTK_WINDOW (help_overlay), GTK_WINDOW (play_window));
      gtk_window_present (GTK_WINDOW (help_overlay));
    }

}

static void
play_window_actions_show_preferences_cb (GtkWidget   *widget,
                                         const gchar *action_name,
                                         GVariant    *param)
{
  adw_dialog_present (ADW_DIALOG (play_preferences_dialog_new (FALSE)),
                      widget);
}

static void
set_link_button_uri (GtkLinkButton *link_button,
                     const gchar   *uri)
{
  GtkWidget *label;

  gtk_link_button_set_uri (link_button, uri);
  gtk_button_set_label (GTK_BUTTON (link_button), uri);
  label = gtk_button_get_child (GTK_BUTTON (link_button));
  if (g_utf8_strlen (uri, -1) > MAX_URL_LEN)
    {
      gtk_label_set_max_width_chars (GTK_LABEL (label), MAX_URL_LEN);
      gtk_label_set_ellipsize (GTK_LABEL (label), PANGO_ELLIPSIZE_END);
      gtk_widget_set_tooltip_text (label, uri);
    }
  else
    {
      gtk_label_set_max_width_chars (GTK_LABEL (label), -1);
      gtk_widget_set_tooltip_text (label, NULL);
    }
}

static gboolean
set_info_label (GtkWidget *label,
                gchar     *prefix,
                gchar     *text,
                gboolean   use_markup)
{
  gboolean info_set = FALSE;
  if (text && text[0])
    {
      g_autofree gchar *combined = NULL;

      if (use_markup)
        {
          combined = g_strdup_printf ("<b>%s</b>%s", prefix, text);
        }
      else
        {
          g_autofree gchar *escaped = NULL;
          escaped = g_markup_escape_text (text, -1);
          combined = g_strdup_printf ("<b>%s</b>%s", prefix, escaped);
        }
      gtk_label_set_markup (GTK_LABEL (label), combined);
      info_set = TRUE;
    }

  gtk_widget_set_visible (label, info_set);
  return info_set;
}

static void
play_window_update_info_popover (PlayWindow *play_window)
{
  g_autofree gchar *title = NULL;
  g_autofree gchar *difficulty = NULL;
  g_autofree gchar *author = NULL;
  g_autofree gchar *editor = NULL;
  g_autofree gchar *publisher = NULL;
  g_autofree gchar *publication = NULL;
  g_autofree gchar *origin = NULL;
  g_autofree gchar *date = NULL;
  g_autofree gchar *copyright = NULL;
  g_autofree gchar *url = NULL;
  IpuzPuzzle *puzzle;
  gboolean info_set = FALSE;

  g_assert (play_window->current_phase == PUZZLE_PHASE_GAME);

  puzzle = puzzle_set_get_puzzle (play_window->active_puzzle_set, PUZZLE_PHASE_GAME);

  if (puzzle == NULL)
    {
      gtk_widget_set_sensitive (play_window->info_menu_button, FALSE);
      return;
    }


  g_object_get (puzzle,
                "title", &title,
                "difficulty", &difficulty,
                "author", &author,
                "editor", &editor,
                "publisher", &publisher,
                "publication", &publication,
                "origin", &origin,
                "date", &date,
                "copyright", &copyright,
                "url", &url,
                NULL);


  /* Title */
  info_set = set_info_label (play_window->info_title_label, "", title, TRUE) || info_set;
  info_set = set_info_label (play_window->info_difficulty_label, _("Difficulty: "), difficulty, TRUE) || info_set;
  info_set = set_info_label (play_window->info_author_label, _("Author: "), author, TRUE) || info_set;
  info_set = set_info_label (play_window->info_editor_label, _("Editor: "), editor, TRUE) || info_set;
  info_set = set_info_label (play_window->info_publisher_label, _("Publisher: "), publisher, TRUE) || info_set;
  info_set = set_info_label (play_window->info_publication_label, _("Publication: "), publication, TRUE) || info_set;
  info_set = set_info_label (play_window->info_date_label, _("Date: "), date, FALSE) || info_set;
  info_set = set_info_label (play_window->info_copyright_label, _("Copyright: "), copyright, TRUE) || info_set;

  /* URL */

  if (url && g_uri_is_valid (url, G_URI_FLAGS_PARSE_RELAXED, NULL))
    {
      set_link_button_uri (GTK_LINK_BUTTON (play_window->info_link_button), url);
      gtk_widget_set_visible (play_window->info_link_separator, TRUE);
      gtk_widget_set_visible (play_window->info_link_button, TRUE);
      info_set = TRUE;
    }
  else
    {
      gtk_widget_set_visible (play_window->info_link_separator, FALSE);
      gtk_widget_set_visible (play_window->info_link_button, FALSE);
    }

#ifdef DEVELOPMENT_BUILD
  info_set = set_info_label (play_window->info_origin_label, _("Origin: "), origin, FALSE) || info_set;
  const gchar *file_uri = puzzle_set_get_uri (play_window->active_puzzle_set, PUZZLE_PHASE_GAME);
  if (file_uri)
    {
      set_link_button_uri (GTK_LINK_BUTTON (play_window->info_devel_file_button), file_uri);
      gtk_widget_set_visible (play_window->info_devel_separator, TRUE);
      gtk_widget_set_visible (play_window->info_devel_file_button, TRUE);
      info_set = TRUE;
    }
  else
    {
      gtk_widget_set_visible (play_window->info_devel_separator, FALSE);
      gtk_widget_set_visible (play_window->info_devel_file_button, FALSE);
    }
#endif

  gtk_widget_set_sensitive (play_window->info_menu_button, info_set);
}


/* Updates the state of the UI based on the current phase */
static void
play_window_update_title (PlayWindow *play_window)
{
  const gchar *title = NULL;
  gboolean compact_mode = FALSE;

  if (play_window->active_puzzle_set)
    {
      GtkWidget *widget;

      if (play_window->current_phase == PUZZLE_PHASE_GAME)
        {
          widget = puzzle_set_get_widget (play_window->active_puzzle_set, PUZZLE_PHASE_GAME);
          g_object_get (G_OBJECT (widget),
                        "compact-mode", &compact_mode,
                        NULL);
        }
      title = puzzle_set_get_title (play_window->active_puzzle_set, play_window->current_phase);
    }

  if (title == NULL)
    title = _("Crosswords");

  gtk_label_set_markup (GTK_LABEL (play_window->title_label), title);
  /* Since the title can have markup, we grab the text from the
   * label. This is quick way to remove markup from the string
   */
  gtk_window_set_title (GTK_WINDOW (play_window),
                        gtk_label_get_text (GTK_LABEL (play_window->title_label)));
}

static void
play_window_update (PlayWindow *play_window)
{
  /* Always unset the reveal state */
  if (play_window->current_phase == PUZZLE_PHASE_MAIN)
    {
      g_simple_action_set_enabled (G_SIMPLE_ACTION (play_window->zoom_action), FALSE);
      gtk_widget_set_visible (play_window->prev_button, FALSE);
      gtk_widget_set_visible (play_window->help_menu_button, FALSE);
      gtk_widget_set_visible (play_window->info_menu_button, FALSE);
    }
  else if (play_window->current_phase == PUZZLE_PHASE_PICKER)
    {
      g_simple_action_set_enabled (G_SIMPLE_ACTION (play_window->zoom_action), FALSE);
      gtk_widget_set_visible (play_window->prev_button, TRUE);
      gtk_widget_set_visible (play_window->help_menu_button, FALSE);
      gtk_widget_set_visible (play_window->info_menu_button, FALSE);
    }
  else if (play_window->current_phase == PUZZLE_PHASE_GAME)
    {
      g_simple_action_set_enabled (G_SIMPLE_ACTION (play_window->zoom_action), TRUE);
      gtk_widget_set_visible (play_window->prev_button, TRUE);
      gtk_widget_set_visible (play_window->help_menu_button, TRUE);
      gtk_widget_set_visible (play_window->info_menu_button, TRUE);
      play_window_update_info_popover (play_window);
    }

  play_window_update_title (play_window);
}

static void
row_activated_cb (PuzzleSetActionRow *row,
                  PlayWindow         *play_window)
{
  PuzzleSet *new_set;

  new_set = puzzle_set_action_row_get_puzzle_set (row);
  if (play_window->active_puzzle_set == new_set)
    g_print ("IDK\n");
  play_window->active_puzzle_set = new_set;

  play_window_begin_active_set (play_window);
}

static void
change_phase_cb (PuzzleSet   *puzzle_set,
                 PuzzlePhase  phase,
                 PlayWindow  *play_window)
{
  const gchar *name;
  switch (phase)
    {
    case PUZZLE_PHASE_PICKER:
      name = "picker";
      break;
    case PUZZLE_PHASE_GAME:
      name = "game";
      break;
    case PUZZLE_PHASE_MAIN:
    default:
      name = "main";
      break;
    }

  gtk_stack_set_visible_child_name (GTK_STACK (play_window->stack), name);
  play_window->current_phase = phase;

  play_window_update (play_window);
}

static void
puzzles_done_clear_stack (PlayWindow *play_window)
{
  GtkWidget *child;

  child =
    gtk_stack_get_child_by_name (GTK_STACK (play_window->stack), "picker");
  if (child)
    gtk_stack_remove (GTK_STACK (play_window->stack), child);

  child =
    gtk_stack_get_child_by_name (GTK_STACK (play_window->stack), "game");
  if (child)
    gtk_stack_remove (GTK_STACK (play_window->stack), child);
}

static void
puzzles_done_cb (PuzzleSet  *puzzle_set,
                 PlayWindow *play_window)
{
  puzzles_done_clear_stack (play_window);
  play_window->active_puzzle_set = NULL;
  play_window->current_phase = PUZZLE_PHASE_MAIN;
  gtk_stack_set_visible_child_name (GTK_STACK (play_window->stack), "main");
  play_window_update (play_window);
}

static void
reveal_canceled_cb (PuzzleSet  *puzzle_set,
                    PlayWindow *play_window)
{
  puzzle_set_reveal_toggled (play_window->active_puzzle_set, FALSE);
}

/* Public methods */

void
play_window_load_uri (PlayWindow  *play_window,
                      const gchar *uri)
{
  g_return_if_fail (PLAY_IS_WINDOW (play_window));

  /* set the puzzle */
  if (play_window->active_puzzle_set != NULL)
    puzzle_set_puzzles_done (play_window->active_puzzle_set);
  play_window->active_puzzle_set = play_window->uri_puzzle_set;
  play_window_begin_active_set (play_window);

  puzzle_set_import_uri (PUZZLE_SET (play_window->uri_puzzle_set), uri);
}

void
play_window_save_state (PlayWindow *play_window)
{
  g_return_if_fail (PLAY_IS_WINDOW (play_window));

  if (play_window->active_puzzle_set)
    puzzle_set_puzzles_done (play_window->active_puzzle_set);
}

GtkWidget *
play_window_get_overlay (PlayWindow *play_window)
{
  g_return_val_if_fail (PLAY_IS_WINDOW (play_window), NULL);

  return play_window->toast_overlay;
}
