/* clue-grid.c
 *
 * Copyright 2023 Tanmay Patil <tanmaynpatil105@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include "crosswords-enums.h"
#include "crosswords-misc.h"
#include "play-grid.h"
#include "play-cell.h"
#include <libipuz/libipuz.h>
#include "clue-grid.h"

enum
{
  PROP_0,
  PROP_CLUE_ID,
  N_PROPS
};

enum
{
  CLUE_CELL_SELECTED,
  CLUE_CELL_GUESS,
  CLUE_CELL_GUESS_AT_CELL,
  CLUE_CELL_DO_COMMAND,
  SELECTION_CHANGED,
  N_SIGNALS
};

static GParamSpec *obj_props[N_PROPS] = {NULL, };
static guint obj_signals[N_SIGNALS] = { 0 };


struct _ClueGrid
{
  PlayGrid parent_instance;

  IpuzClueId clue_id;
  guint selected_column;

  /* Reference to the clue's cell array */
  GridState *answer_state;
  LayoutConfig layout_config;
  GArray *cells_map;

  gchar *selection_text;
};


static void clue_grid_init             (ClueGrid          *self);
static void clue_grid_class_init       (ClueGridClass     *klass);
static void clue_grid_set_property     (GObject           *object,
                                        guint              prop_id,
                                        const GValue      *value,
                                        GParamSpec        *psec);
static void clue_grid_get_property     (GObject           *object,
                                        guint              prop_id,
                                        GValue            *value,
                                        GParamSpec        *psec);
static void clue_grid_dispose          (GObject           *object);
static void clue_cell_selected_cb      (ClueGrid          *self,
                                        guint              row,
                                        guint              column,
                                        gpointer           user_data);
static void clue_cell_guess_cb         (ClueGrid          *self,
                                        const gchar       *guess,
                                        gpointer           user_data);
static void clue_cell_guess_at_cell_cb (ClueGrid          *self,
                                        const gchar       *guess,
                                        guint              row,
                                        guint              column,
                                        gpointer           user_data);
static void clue_cell_do_command_cb    (ClueGrid          *self,
                                        GridCmdKind        kind,
                                        gpointer           user_data);
static void select_drag_start_cb       (ClueGrid          *self,
                                        IpuzCellCoord     *anchor_coord,
                                        IpuzCellCoord     *new_coord,
                                        GridSelectionMode  mode);
static void select_drag_update_cb      (ClueGrid          *self,
                                        IpuzCellCoord     *anchor_coord,
                                        IpuzCellCoord     *new_coord,
                                        GridSelectionMode  mode);
static void select_drag_end_cb         (ClueGrid          *self);


G_DEFINE_TYPE (ClueGrid, clue_grid, PLAY_TYPE_GRID);


static void
clue_grid_init (ClueGrid *self)
{
  g_signal_connect (self, "cell-selected", G_CALLBACK (clue_cell_selected_cb), NULL);
  g_signal_connect (self, "guess", G_CALLBACK (clue_cell_guess_cb), NULL);
  g_signal_connect (self, "guess-at-cell", G_CALLBACK (clue_cell_guess_at_cell_cb), NULL);
  g_signal_connect (self, "do-command", G_CALLBACK (clue_cell_do_command_cb), NULL);
  g_signal_connect (self, "select-drag-start", G_CALLBACK (select_drag_start_cb), NULL);
  g_signal_connect (self, "select-drag-update", G_CALLBACK (select_drag_update_cb), NULL);
  g_signal_connect (self, "select-drag-end", G_CALLBACK (select_drag_end_cb), NULL);
}

static void
clue_grid_class_init (ClueGridClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->set_property = clue_grid_set_property;
  object_class->get_property = clue_grid_get_property;
  object_class->dispose = clue_grid_dispose;

  obj_signals [CLUE_CELL_SELECTED] =
    g_signal_new ("clue-cell-selected",
		  CLUE_TYPE_GRID,
		  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
		  0,
		  NULL, NULL,
		  NULL,
		  G_TYPE_NONE, 2,
		  G_TYPE_UINT, G_TYPE_UINT);

  obj_signals [CLUE_CELL_GUESS] =
    g_signal_new ("clue-cell-guess",
                  CLUE_TYPE_GRID,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  G_TYPE_STRING);

  obj_signals [CLUE_CELL_GUESS_AT_CELL] =
    g_signal_new ("clue-cell-guess-at-cell",
                  CLUE_TYPE_GRID,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 3,
                  G_TYPE_STRING,
                  G_TYPE_UINT, G_TYPE_UINT);

  obj_signals [CLUE_CELL_DO_COMMAND] =
    g_signal_new ("clue-cell-do-command",
                  CLUE_TYPE_GRID,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  GRID_TYPE_CMD_KIND);

  obj_signals [SELECTION_CHANGED] =
    g_signal_new ("selection-changed",
                  CLUE_TYPE_GRID,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  G_TYPE_STRING);

  obj_props [PROP_CLUE_ID] = g_param_spec_boxed ("clue-id",
		                                 "Clue Id",
					         "Clue Id of Selected Clue",
					         IPUZ_TYPE_CLUE_ID,
					         G_PARAM_READWRITE);

  g_object_class_install_properties (object_class, N_PROPS, obj_props);

  gtk_widget_class_set_css_name (widget_class, "clue-grid");
}

static void
clue_grid_set_property (GObject      *object,
		        guint         prop_id,
			const GValue *value,
			GParamSpec   *psec)
{
  ClueGrid* self;

  g_return_if_fail (CLUE_IS_GRID (object));

  self = CLUE_GRID (object);

  switch (prop_id)
    {
      case PROP_CLUE_ID:
        self->clue_id = *(IpuzClueId *)g_value_get_boxed (value);
	break;
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, psec);
	break;
    }
}

static void
clue_grid_get_property (GObject    *object,
		        guint       prop_id,
			GValue     *value,
			GParamSpec *psec)
{
  ClueGrid* self;

  g_return_if_fail (CLUE_IS_GRID (object));

  self = CLUE_GRID (object);

  switch (prop_id)
    {
      case PROP_CLUE_ID:
        g_value_set_boxed (value, &self->clue_id);
	break;
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, psec);
	break;
    }
}

static void
clue_grid_dispose (GObject *object)
{
  ClueGrid *self;

  self = CLUE_GRID (object);

  g_clear_pointer (&self->answer_state, grid_state_free);
  g_clear_pointer (&self->cells_map, g_array_unref);

  G_OBJECT_CLASS (clue_grid_parent_class)->dispose (object);
};

static void
clue_cell_selected_cb (ClueGrid *self,
		       guint     row,
		       guint     column,
		       gpointer  user_data)
{
  IpuzCellCoord coord;

  coord = g_array_index (self->cells_map, IpuzCellCoord, column);

  g_signal_emit (self,
		 obj_signals[CLUE_CELL_SELECTED],
		 0,
		 coord.row, coord.column);
};

static void
clue_cell_guess_cb (ClueGrid    *self,
		    const gchar *guess,
		    gpointer     user_data)
{
  g_signal_emit (self,
		 obj_signals[CLUE_CELL_GUESS],
		 0,
		 guess);
};

static void
clue_cell_guess_at_cell_cb (ClueGrid    *self,
		            const gchar *guess,
			    guint        row,
			    guint        column,
			    gpointer     user_data)
{
  g_signal_emit (self,
		 obj_signals[CLUE_CELL_GUESS_AT_CELL],
		 0,
		 guess,
		 row, column);
};

static void
clue_cell_do_command_cb (ClueGrid    *self,
		         GridCmdKind  kind,
			 gpointer     user_data)
{
  g_signal_emit (self,
		 obj_signals[CLUE_CELL_DO_COMMAND],
		 0,
		 kind);
};

static void
update_selection_string (ClueGrid *self)
{
  g_autoptr (GString) new_str = NULL;
  g_assert (self->answer_state != NULL);

  new_str = g_string_new (NULL);

  if (self->answer_state->selected_cells != NULL)
    {
      for (guint i = 0; i < self->answer_state->selected_cells->len; i++)
        {
          IpuzCellCoord coord;
          IpuzCell *cell;
          const gchar *solution;

          coord = g_array_index (self->answer_state->selected_cells, IpuzCellCoord, i);
          cell = ipuz_crossword_get_cell (self->answer_state->xword, &coord);
          solution = ipuz_cell_get_solution (cell);

          if (solution)
            new_str = g_string_append (new_str, solution);
          else
            new_str = g_string_append (new_str, "?");
        }
    }

  if (g_strcmp0 (new_str->str, self->selection_text))
    {
      g_free (self->selection_text);
      self->selection_text = g_string_free_and_steal (new_str);
      new_str = NULL;
      g_signal_emit (self, obj_signals[SELECTION_CHANGED], 0, self->selection_text);
    }
}

static void
select_drag_start_cb (ClueGrid          *self,
                      IpuzCellCoord     *anchor_coord,
                      IpuzCellCoord     *new_coord,
                      GridSelectionMode  mode)
{
  self->answer_state =
    grid_state_replace (self->answer_state,
                        grid_state_select_drag_start (self->answer_state,
                                                      *anchor_coord,
                                                      *new_coord,
                                                      mode));
  play_grid_update_state (PLAY_GRID (self), self->answer_state, self->layout_config);
  update_selection_string (self);
}

static void
select_drag_update_cb (ClueGrid          *self,
                       IpuzCellCoord     *anchor_coord,
                       IpuzCellCoord     *new_coord,
                       GridSelectionMode  mode)
{
  self->answer_state =
    grid_state_replace (self->answer_state,
                        grid_state_select_drag_update (self->answer_state,
                                                       *anchor_coord,
                                                       *new_coord,
                                                       mode));
  play_grid_update_state (PLAY_GRID (self), self->answer_state, self->layout_config);
  update_selection_string (self);
}

static void
select_drag_end_cb (ClueGrid *self)
{
  self->answer_state =
    grid_state_replace (self->answer_state,
                        grid_state_select_drag_end (self->answer_state));
  play_grid_update_state (PLAY_GRID (self), self->answer_state, self->layout_config);
  update_selection_string (self);
}

/* Public Functions */

GtkWidget *
clue_grid_new ()
{
  GObject *object;
  IpuzClueId clue_id = {
    .direction = IPUZ_CLUE_DIRECTION_NONE,
    .index = 0,
  };

  object = g_object_new (CLUE_TYPE_GRID, NULL);
  g_object_set (object, "clue-id", &clue_id, NULL);

  return GTK_WIDGET (object);
}

void
clue_grid_set_clue_id (ClueGrid *self,
		       IpuzClueId  clue_id)
{
  g_return_if_fail (CLUE_IS_GRID (self));

  self->clue_id = clue_id;
}

static void
clue_grid_set_selected_column (ClueGrid *self,
		               guint     index)
{
  g_return_if_fail (CLUE_IS_GRID (self));

  self->selected_column = index;
}

static GridState *
clue_grid_create_answer_state (ClueGrid  *self,
		               GridState *state,
		               IpuzClue  *clue)
{
  IpuzBoard *board;
  IpuzGuesses *saved_guesses;
  IpuzGuesses *new_guesses = NULL;
  GridState *answer_state;
  gboolean has_cursor = FALSE;
  IpuzCellCoord cursor = {
    .row = 0,
    .column = 0,
  };
  IpuzClueId answer_clue_id;
  g_autoptr (IpuzCrossword) xword = NULL;
  guint width = 0;

  if (clue->cells)
    width = clue->cells->len;

  xword = ipuz_crossword_new ();
  ipuz_crossword_set_size (xword, width, 1);
  ipuz_crossword_fix_numbering (xword);
  ipuz_crossword_fix_clues (xword);

  board = ipuz_crossword_get_board (xword);

  saved_guesses = ipuz_crossword_get_guesses (state->xword);
  if (saved_guesses)
    {
      new_guesses = ipuz_guesses_new_from_board (board, TRUE);
      ipuz_crossword_set_guesses (xword, new_guesses);
    }

  for (guint i = 0; i < width; i++)
    {
      IpuzStyle *style;
      gint number;
      const gchar *saved_guess, *label, *solution;
      IpuzCellCoord saved_coord;
      IpuzCell *saved_cell;
      IpuzCell *dest_cell;

      IpuzCellCoord coord = {
        .row = 0,
	.column = i
      };

      saved_coord = g_array_index (clue->cells, IpuzCellCoord, i);
      saved_cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (state->xword), &saved_coord);

      if (ipuz_cell_coord_equal (& (state->cursor), &saved_coord))
        {
          has_cursor = TRUE;
          cursor = coord;
          clue_grid_set_selected_column (self, i);
	}

      number = ipuz_cell_get_number (saved_cell);
      label = ipuz_cell_get_label (saved_cell);
      solution = ipuz_cell_get_solution (saved_cell);
      style = ipuz_cell_get_style (saved_cell);

      dest_cell = ipuz_crossword_get_cell (xword, &coord);
      ipuz_cell_set_number (dest_cell, number);
      ipuz_cell_set_label (dest_cell, label);
      ipuz_cell_set_solution (dest_cell, solution);

      ipuz_cell_set_style (dest_cell, style, NULL);

      if (saved_guesses)
        {
          saved_guess = ipuz_guesses_get_guess (saved_guesses, &saved_coord);

          if (saved_guess != NULL)
            ipuz_guesses_set_guess (new_guesses, &coord, saved_guess);
        }
    }

  if (has_cursor == TRUE)
    answer_state = grid_state_new_from_behavior (xword, NULL, state->behavior);
  else
    answer_state = grid_state_new_from_behavior (xword, NULL, GRID_BEHAVIOR_SHOW_GUESS);

  if (!ACROSTIC_MAIN_GRID_HAS_FOCUS (state) && ipuz_clue_id_equal (& (state->clue), & (self->clue_id)))
    answer_clue_id.direction = IPUZ_CLUE_DIRECTION_ACROSS;
  else
    answer_clue_id.direction = IPUZ_CLUE_DIRECTION_DOWN;
  answer_clue_id.index = 0;

  answer_state->clue = answer_clue_id;
  answer_state->cursor = cursor;

  if (GRID_STATE_HAS_SELECTABLE (answer_state))
    answer_state = grid_state_replace (answer_state, grid_state_select_all (answer_state));

  return answer_state;
}

void
clue_grid_update_state (ClueGrid     *self,
		        GridState    *state,
			IpuzClue     *clue,
			LayoutConfig  layout_config)
{
  g_return_if_fail (CLUE_IS_GRID (self));

  g_clear_pointer (&self->answer_state, grid_state_free);
  g_clear_pointer (&self->cells_map, g_array_unref);

  self->answer_state = clue_grid_create_answer_state (self, state, clue);
  self->layout_config = layout_config;

  play_grid_update_state (PLAY_GRID (self), self->answer_state, self->layout_config);

  self->cells_map = g_array_ref ((GArray*) ipuz_clue_get_cells (clue));
  update_selection_string (self);
}

void
clue_grid_focus_cell (ClueGrid *self)
{
  g_return_if_fail (CLUE_IS_GRID (self));

  IpuzCellCoord coord = {
    .row = 0,
    .column = self->selected_column,
  };

  play_grid_focus_cell (PLAY_GRID (self), coord);
}

void
clue_grid_selection_invert (ClueGrid *self)
{
  g_return_if_fail (CLUE_IS_GRID (self));

  self->answer_state =
    grid_state_replace (self->answer_state,
                        grid_state_select_invert (self->answer_state));
  play_grid_update_state (PLAY_GRID (self), self->answer_state, self->layout_config);
  update_selection_string (self);
}

void
clue_grid_select_all (ClueGrid *self)
{
  g_return_if_fail (CLUE_IS_GRID (self));

  self->answer_state =
    grid_state_replace (self->answer_state,
                        grid_state_select_all (self->answer_state));
  play_grid_update_state (PLAY_GRID (self), self->answer_state, self->layout_config);
  update_selection_string (self);
}
