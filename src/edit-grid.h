/* edit-grid.h
 *
 * Copyright 2021 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>
#include "grid-state.h"
#include "puzzle-stack.h"

G_BEGIN_DECLS


#define EDIT_TYPE_GRID (edit_grid_get_type())
G_DECLARE_FINAL_TYPE (EditGrid, edit_grid, EDIT, GRID, GtkWidget);


void edit_grid_update        (EditGrid         *self,
                              GridState        *state);
void edit_grid_save_state    (EditGrid         *self,
                              PuzzleStack      *stack,
                              GridState        *state);
void edit_grid_restore_state (EditGrid         *self,
                              PuzzleStack      *stack,
                              GridState        *state);
void edit_grid_set_stage     (EditGrid         *self,
                              PuzzleStackStage  stage);


G_END_DECLS
