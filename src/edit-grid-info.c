/* edit-grid-info.c
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <libipuz/libipuz.h>
#include <glib/gi18n-lib.h>
#include "edit-grid-info.h"


struct _EditGridInfo
{
  GtkWidget parent_instance;

  GtkWidget *dimensions_label;
  GtkWidget *composition_label;
  GtkWidget *completion_label;
  GtkWidget *pangram_label;
};


G_DEFINE_FINAL_TYPE (EditGridInfo, edit_grid_info, GTK_TYPE_WIDGET);


static void edit_grid_info_init       (EditGridInfo      *self);
static void edit_grid_info_class_init (EditGridInfoClass *klass);
static void edit_grid_info_dispose    (GObject           *object);


static void
edit_grid_info_init (EditGridInfo *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

static void
edit_grid_info_class_init (EditGridInfoClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = edit_grid_info_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-grid-info.ui");
  gtk_widget_class_bind_template_child (widget_class, EditGridInfo, dimensions_label);
  gtk_widget_class_bind_template_child (widget_class, EditGridInfo, composition_label);
  gtk_widget_class_bind_template_child (widget_class, EditGridInfo, completion_label);
  gtk_widget_class_bind_template_child (widget_class, EditGridInfo, pangram_label);

  gtk_widget_class_set_css_name (widget_class, "gridinfo");
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
}

static void
edit_grid_info_dispose (GObject *object)
{
  GtkWidget *child;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  G_OBJECT_CLASS (edit_grid_info_parent_class)->dispose (object);
}

/* Public methods */

void
edit_grid_info_update (EditGridInfo   *self,
                       GridState      *grid_state,
                       IpuzPuzzleInfo *info)
{
  IpuzCellStats stats;
  GString *text_str;
  gint width, height;
  guint pangram_count;

  text_str = g_string_new (NULL);

  stats = ipuz_puzzle_info_get_cell_stats (info);
  g_object_get (G_OBJECT (grid_state->xword),
                "width", &width,
                "height", &height,
                NULL);
  pangram_count = ipuz_puzzle_info_get_pangram_count (info);

  g_return_if_fail (width != 0 && height != 0);

  /* Completion */
  gtk_widget_remove_css_class (self->completion_label, "accent");
  gtk_widget_remove_css_class (self->completion_label, "error");

  if (stats.normal_count)
    {
      guint n_letters;

      n_letters = ipuz_charset_get_size (ipuz_puzzle_info_get_solution_chars (info));

      n_letters = MIN (n_letters, stats.normal_count);
      g_string_append_printf (text_str, "%.0f%%", 100 * n_letters / (gfloat) stats.normal_count);

      if (n_letters == stats.normal_count)
        gtk_widget_add_css_class (self->completion_label, "accent");
    }
  else
    {
      g_string_append (text_str, "N/A");
      gtk_widget_add_css_class (self->completion_label, "error");
    }
  gtk_label_set_markup (GTK_LABEL (self->completion_label), text_str->str);
  g_string_set_size (text_str, 0);

  /* Dimensions */
  g_string_append_printf (text_str,
                          _("%d × %d (%d total cells)"), width, height, width * height);
  gtk_label_set_label (GTK_LABEL (self->dimensions_label), text_str->str);
  g_string_set_size (text_str, 0);

  /* Composition */
  if (stats.normal_count)
    g_string_append_printf (text_str, _("%u normal cells"), stats.normal_count);

  if (stats.block_count)
    {
      if (stats.normal_count)
        g_string_append (text_str, ", ");
      g_string_append_printf (text_str, _("%u block cells"), stats.block_count);
    }

  if (stats.null_count)
    {
      if (stats.normal_count || stats.block_count)
        g_string_append (text_str, ", ");
      g_string_append_printf (text_str, _("%u null cells"), stats.null_count);
    }
  gtk_label_set_markup (GTK_LABEL (self->composition_label), text_str->str);
  g_string_set_size (text_str, 0);

  /* Pangrammatic */
  gtk_widget_set_visible (self->pangram_label, pangram_count > 0);
  if (pangram_count == 1)
    g_string_append (text_str, _("<b>Puzzle has a pangram</b>"));
  else if (pangram_count == 2)
    g_string_append (text_str, _("<b>Puzzle has two pangrams</b>"));
  else if (pangram_count == 3)
    g_string_append (text_str, _("<b>Puzzle has three pangrams!</b>"));
  else if (pangram_count > 3)
    g_string_append (text_str, _("<b>Puzzle has four or more pangrams! <i>Amazing</i></b>"));
  gtk_label_set_markup (GTK_LABEL (self->pangram_label), text_str->str);
  g_string_free (text_str, TRUE);
}
