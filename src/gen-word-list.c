/* gen-word-list.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdio.h>
#include <locale.h>
#include "word-list-index.h"
#include "word-list-misc.h"
#include "gen-word-list.h"
#include "gen-word-list-importer.h"

#include <glib/gi18n.h>
#include <glib.h>
#include <gio/gio.h>
#include <gio/gunixoutputstream.h>


/* A single word in the word list.
 *
 * This starts out with all fields filled by parse_line() and gen_word_list_add_word(),
 * except for the index field, which gets initialized when the WordSection buckets are
 * computed after sorting all the words.
 */
typedef struct
{
  /* Index of the word within its bucket.  Only valid after gen_word_list_calculate_offsets() is called. */
  guint index;

  /* The word plus its nul terminator. */
  gchar *word;

  /* The enumeration source, if it exists */
  gchar *enumeration_src;

  /* Number of code points in the word (3 for FOO and FÖÖ). */
  gint word_len;

  /* Collation key for sorting. */
  gchar *collate_key;

  /* Number of bytes in the word, without the nul terminator (3 for FOO, 5 for FÖÖ). */
  gssize byte_len;

  /* Priority of the word; comes from the corpus. */
  guchar priority;
} WordEntry;

typedef struct
{
  gchar *enumeration_src;
  gssize offset;
} EnumerationEntry;

typedef struct
{
  /* Elements are gushort, which are indices into each word-length bucket. */
  GArray *array;
  guint offset;
} FilterFragmentList;

typedef struct
{
  guint hash;
  WordEntry *entry;
} HashInfo;

/* This is a g_array_set_clear_func */
static void
clear_word_entry (gpointer data)
{
  WordEntry *entry = data;

  g_clear_pointer (&entry->word, g_free);
  g_clear_pointer (&entry->collate_key, g_free);
  entry->index = 0;
  entry->word_len = 0;
  entry->byte_len = 0;
  entry->priority = 0;
}

GenWordList  *
gen_word_list_new (gint             min_length,
                   gint             max_length,
                   gint             threshold,
                   WordListImporter importer)
{
  GenWordList *word_list;

  word_list = g_new0 (GenWordList, 1);
  word_list->charset = NULL;

  word_list->words = g_array_new (FALSE, TRUE, sizeof (WordEntry));
  g_array_set_clear_func (word_list->words, clear_word_entry);

  word_list->word_index = g_array_new (FALSE, TRUE, sizeof (WordListSection));
  word_list->enumerations = g_array_new (FALSE, TRUE, sizeof (EnumerationEntry));

  word_list->threshold = threshold;
  word_list->min_length = min_length;
  word_list->max_length = max_length;
  word_list->importer = importer;

  return word_list;
}

void
gen_word_list_free (GenWordList *word_list)
{
  g_clear_pointer (&word_list->words, g_array_unref);
  g_clear_pointer (&word_list->letters, g_array_unref);
  g_clear_pointer (&word_list->word_index, g_array_unref);

  if (word_list->charset)
    {
      ipuz_charset_unref (word_list->charset);
      word_list->charset = NULL;
    }

  g_free (word_list);
}

static ParseLineStatus
parse_line (WordListImporter  importer,
            IpuzCharset      *alphabet,
            ValidatedWord    *out_validated,
            gchar            *line,
            gint              min_length,
            gint              max_length,
            guchar            threshold)
{
  line = g_strstrip (line);
  switch (importer)
    {
    case BRODA_SCORED:
      return parse_line_broda_scored (out_validated, line, min_length, max_length, threshold);
    case BRODA_FULL:
      return parse_line_broda_full (out_validated, alphabet, line, min_length, max_length, threshold);
    default:
      g_assert_not_reached ();
    }
}

static void
gen_word_list_add_word (GenWordList   *word_list,
                        ValidatedWord *validated)
{
  WordEntry entry = {
    .index = G_MAXUINT, /* "uninitialized" */
    .word = g_strdup (validated->word),
    .enumeration_src = g_strdup (validated->enumeration_src),
    .word_len = validated->word_len,
    .byte_len = validated->byte_len,
    .collate_key = g_utf8_collate_key (validated->word, validated->byte_len),
    .priority = validated->priority,
  };

  g_array_append_val (word_list->words, entry);
}

/**
 * gen_word_list_add_test_word():
 * @word_list: The word list generator.
 * @word: Word to add; must be valid or this function will abort.
 * @priority: Priority for the added word, must be above the @word_list's threshold.
 *
 * Adds a test word to the word list.  It is a programming error to
 * try to add an invalid word, per validate_word() - this function will
 * abort if an invalid word gets passed.
 */
void
gen_word_list_add_test_word (GenWordList *word_list,
                             const char  *word,
                             glong        priority)
{
  gssize byte_len = strlen (word);
  gint word_len = g_utf8_strlen (word, byte_len);
  ValidatedWord validated;
  ParseLineStatus status;

  status = validate_word (word, FALSE, byte_len, word_len, priority,
                          word_list->min_length, word_list->max_length, word_list->threshold,
                          &validated);
  switch (status)
    {
    case PARSE_LINE_STATUS_OK:
      break;

    case PARSE_LINE_STATUS_BAD_SYNTAX:
      g_error ("bad syntax: word %s", word);
      return; /* unreachable */

    case PARSE_LINE_STATUS_WORD_LENGTH_OUT_OF_RANGE:
      g_error ("word length out of range: %s", word);
      return; /* unreachable */

    case PARSE_LINE_STATUS_PRIORITY_OUT_OF_RANGE:
      g_error ("priority out of range: %ld", priority);
      return; /* unreachable */

    default:
      g_assert_not_reached ();
      return; /* unreachable */
    }

  gen_word_list_add_word (word_list, &validated);
  validated_word_free (validated);
}

gboolean
gen_word_list_parse (GenWordList  *word_list,
                     GInputStream *stream)
{
  g_autoptr (GDataInputStream) data_stream = NULL;
  gchar *line;

  /* FIXME(alphabet): Get this from the command line, or parser */
  g_autoptr (IpuzCharset) alphabet = ipuz_charset_deserialize ("ABCDEFGHIJKLMNOPQRSTUVWXYZ");

  data_stream = g_data_input_stream_new (stream);

  line = g_data_input_stream_read_line_utf8 (data_stream, NULL, NULL, NULL);

  while (line)
    {
      ValidatedWord validated;

      if (parse_line (word_list->importer,
                      alphabet,
                      &validated,
                      line,
                      word_list->min_length,
                      word_list->max_length,
                      word_list->threshold) == PARSE_LINE_STATUS_OK)
        {
          gen_word_list_add_word (word_list, &validated);
          g_free ((char *) validated.word);
        }

      g_free (line);
      line = g_data_input_stream_read_line_utf8 (data_stream, NULL, NULL, NULL);
    }
  return TRUE;
}

void
gen_word_list_build_enumerations (GenWordList *word_list)
{
  for (guint i = 0; i < word_list->words->len; i++)
    {
      WordEntry entry = g_array_index (word_list->words, WordEntry, i);

      if (entry.enumeration_src)
        {
          EnumerationEntry enum_entry;
          enum_entry.enumeration_src = g_strdup (entry.enumeration_src);
          g_array_append_val (word_list->enumerations, enum_entry);
        }
    }
}

static gint
word_array_dupe_sort (gconstpointer a,
                      gconstpointer b)
{
  gint collate_compare;
  WordEntry *entry_a = (WordEntry *)a;
  WordEntry *entry_b = (WordEntry *)b;

  if (entry_a->word_len != entry_b->word_len)
    return (entry_a->word_len - entry_b->word_len);

  collate_compare = strcmp (entry_a->collate_key, entry_b->collate_key);
  if (collate_compare == 0)
    return entry_b->priority - entry_a->priority;

  return collate_compare;
}

static gint
enumerations_sort (gconstpointer a,
                   gconstpointer b)
{
  EnumerationEntry *entry_a = (EnumerationEntry *)a;
  EnumerationEntry *entry_b = (EnumerationEntry *)b;

  return strcmp (entry_a->enumeration_src, entry_b->enumeration_src);
}


/* occasionally, we have two words with the same letters once
 * reduced. for example MIA and M.I.A!. We keep the one with the
 * higher priority. */
void
gen_word_list_remove_duplicates (GenWordList *word_list)
{
  guint i = 0;
  const gchar *last = NULL;

  g_array_sort (word_list->words, (GCompareFunc) word_array_dupe_sort);
  g_array_sort (word_list->enumerations, (GCompareFunc) enumerations_sort);

  while (i < word_list->words->len)
    {
      WordEntry entry = g_array_index (word_list->words, WordEntry, i);

      if (! g_strcmp0 (entry.word, last))
        {
          g_array_remove_index (word_list->words, i);
        }
      else
        {
          last = entry.word;
          i++;
        }
    }

  i = 0;
  last = NULL;
  while (i < word_list->enumerations->len)
    {
      EnumerationEntry entry = g_array_index (word_list->enumerations, EnumerationEntry, i);

      if (! g_strcmp0 (entry.enumeration_src, last))
        {
          g_array_remove_index (word_list->enumerations, i);
        }
      else
        {
          last = entry.enumeration_src;
          i++;
        }
    }
}

static gint
word_array_sort (gconstpointer a,
                 gconstpointer b)
{
  WordEntry *entry_a = (WordEntry *)a;
  WordEntry *entry_b = (WordEntry *)b;

  if (entry_a->word_len != entry_b->word_len)
    return (entry_a->word_len - entry_b->word_len);
  if (entry_a->priority != entry_b->priority)
    return (entry_b->priority - entry_a->priority);
  return strcmp (entry_a->collate_key, entry_b->collate_key);
}

void
gen_word_list_sort (GenWordList *word_list)
{
  g_array_sort (word_list->words, (GCompareFunc) word_array_sort);
  /* Enumerations are pre-sorted from the dupe removal */
#if 0
  for (uint i = 0; i < word_list->words->len; i++)
    {
      WordEntry entry = g_array_index (word_list->words, WordEntry, i);

      g_print ("%s:%d\n", entry.word, entry.priority);
    }
#endif
}

void
gen_word_list_build_charset (GenWordList *word_list)
{
  IpuzCharsetBuilder *builder = ipuz_charset_builder_new();
  guint i;

  for (i = 0; i < word_list->words->len; i++)
    {
      WordEntry entry = g_array_index (word_list->words, WordEntry, i);
      ipuz_charset_builder_add_text (builder, entry.word);
    }

  word_list->charset = ipuz_charset_builder_build (builder);
}

static void
gen_word_list_add_word_to_letters (GenWordList *word_list,
                                   const gchar *word,
                                   WordIndex    word_index)
{
  gint pos = 0;
  const gchar *ptr;
  FilterFragment fragment;
  fragment.length = g_utf8_strlen (word, -1);

  for (ptr = word; ptr[0] != '\0'; ptr = g_utf8_next_char (ptr))
    {
      FilterFragmentList *frag_list;
      gsize index;

      fragment.position = pos;
      fragment.char_index = ipuz_charset_get_char_index (word_list->charset, g_utf8_get_char (ptr));

      index = word_index_fragment_index (word_list->min_length, ipuz_charset_get_n_chars (word_list->charset), fragment);
      g_assert (index < word_list->letters->len);
      frag_list = &(g_array_index (word_list->letters, FilterFragmentList, index));

      /* if this triggers, we have more letters in a given filter
       * fragment than we can store (more than 65536). In that
       * instance, we need to cull our input or adjust the format to support wider ints */
      g_assert (word_index.index < G_MAXUINT16);
      g_array_append_val (frag_list->array, word_index.index);
      /* g_print ("adding %d for word %s at index %d\n", word_index.index, word, index); */

      pos++;
    }

}

/* Comparison function for array sorting */
static gint
compare_hashes (gconstpointer a,
                gconstpointer b)
{
  const HashInfo* point_a = (const HashInfo*)a;
  const HashInfo* point_b = (const HashInfo*)b;

  if (point_a->hash < point_b->hash)
    return -1;
  else if (point_a->hash > point_b->hash)
    return 1;

  /* if the hash is the same, sort alphabetically */
  return g_strcmp0 (point_a->entry->word, point_b->entry->word);
}

void
gen_word_list_anagram_table (GenWordList *word_list)
{
  guint i;
  HashInfo info;
  WordEntry *entry;
  word_list->anagram_list = g_array_new (FALSE, TRUE, sizeof (HashInfo));

  for (i = 0; i < word_list->words->len; i++)
    {
      entry = &(g_array_index (word_list->words, WordEntry, i));
      info.hash = word_list_hash_func (entry->word);
      info.entry = entry;
      g_array_append_val (word_list->anagram_list, info);
    }

  /* Sort the array by hash */
  g_array_sort (word_list->anagram_list, compare_hashes);
}

void
gen_word_list_anagram_dump (GenWordList * word_list)
{
  guint i;
  g_print ("\ncheck length: %u \n", word_list->anagram_list->len);
  g_print ("\nTotal Fragments %u \n", word_list->anagram_fragments->len);

  /* Checking the filterfragments here */
  for (i=0; i< word_list->anagram_fragments->len; i++)
    {
      guint j;
      FilterFragmentList data = g_array_index (word_list->anagram_fragments, FilterFragmentList, i);
      g_print ("Listlen:%u ",data.array->len);
      for (j=0; j<data.array->len; j++)
        {
          gushort k = g_array_index (data.array, gushort, j);
          g_print ("ItIndex:%u ", j);
          g_print ("WordIndexStored:%u ", k);
        }
      g_print ("Hash:%u ", g_array_index (word_list->corresponding_hash, guint, i));
      g_print ("FragmentNo:%u %u \n ",i, word_list->anagram_fragments->len);
    }
  g_print (" \nInside check ");
}

void
create_anagram_fragments (GenWordList *word_list)
{
  guint i;
  word_list->anagram_fragments = g_array_new (FALSE, FALSE, sizeof (FilterFragmentList));
  word_list->corresponding_hash = g_array_new (FALSE, FALSE, sizeof (guint));

  for (i=0; i < word_list->anagram_list->len; i++)
    {
      FilterFragmentList frag_list;
      HashInfo* data;

      /* Create a new fragment list */
      frag_list.array = g_array_new (FALSE, TRUE, sizeof (gushort));
      frag_list.offset = 0;

      /* Get details word data */
      data = &g_array_index (word_list->anagram_list, HashInfo, i);
      g_array_append_val (frag_list.array, data->entry->index);
      /* g_print("For i:%u hash:%u word:%s \n", i, data->hash, data->entry->word); */

      if(i < word_list->anagram_list->len - 1)
        {
          HashInfo *next_data;

          next_data = &g_array_index (word_list->anagram_list, HashInfo, i + 1);

          /* populate the fragment inside the while loop */
          while (next_data->hash == data->hash && i < word_list->anagram_list->len)
            {
              /* g_print ("While index:%u hash:%u word:%s \n", next_data->entry->index, next_data->hash, next_data->entry->word); */
              g_array_append_val (frag_list.array, next_data->entry->index);
              i++;
              next_data = &g_array_index (word_list->anagram_list, HashInfo, i + 1);
            }
        }

      /* Append the generated fragment */
      /* g_print ("appending fragment %d \n", frag_list.array->len); */
      g_array_append_val (word_list->corresponding_hash, data->hash);
      g_array_append_val (word_list->anagram_fragments, frag_list);
    }
}

static void
calculate_enumeration_offsets (GenWordList *word_list)
{
  guint enumeration_offset = 1;
  for (guint i = 0; i < word_list->enumerations->len; i++)
    {
      EnumerationEntry *entry = &(g_array_index (word_list->enumerations, EnumerationEntry, i));

      entry->offset = enumeration_offset;
      enumeration_offset += (strlen (entry->enumeration_src) + NIL_SIZE);
    }

  g_assert (enumeration_offset < G_MAXUINT16);
}

/* This is a g_array_set_clear_func */
static void
clear_filter_fragment_list (gpointer data)
{
  FilterFragmentList *list = data;

  g_clear_pointer (&list->array, g_array_unref);
}

static guint
word_entry_stride (WordEntry *entry)
{
  /* 1 byte for priority, 2 bytes for enumerations, plus UTF-8 bytes,
   * plus nul terminator. */
  return entry->byte_len + NIL_SIZE + WORD_OFFSET;
}

void
gen_word_list_calculate_offsets (GenWordList *word_list)
{
  guint i;
  guint letters_size;
  guint section_stride = 0;
  guint section_offset = 0;

  letters_size = word_index_calculate_letters_size (word_list->min_length,
                                                    word_list->max_length,
                                                    ipuz_charset_get_n_chars (word_list->charset));
  word_list->letters = g_array_new (FALSE, TRUE, sizeof (FilterFragmentList));
  g_array_set_clear_func (word_list->letters, clear_filter_fragment_list);
  g_array_set_size (word_list->letters, letters_size);

  for (i = 0; i < letters_size; i++)
    {
      FilterFragmentList *frag_list;
      frag_list = &(g_array_index (word_list->letters, FilterFragmentList, i));
      frag_list->array = g_array_new (FALSE, TRUE, sizeof (gushort));
      frag_list->offset = 0;
    }

  /* Find the start and end words of the current section */

  /* Compute the maximum stride for all the words in the section */

  /* Use the maximum stride as the offset between words */

  guint section_start_idx = 0;
  while (section_start_idx < word_list->words->len)
    {
      WordEntry *first_entry = &(g_array_index (word_list->words, WordEntry, section_start_idx));
      section_stride = word_entry_stride (first_entry);
      guint next_section_idx;

      /* Walk forwards until we reach the next section (i.e. the first word
       * with a different length).
       *
       * Along the way, find the maximum stride of all the words in the present section.
       */

      for (next_section_idx = section_start_idx + 1; next_section_idx < word_list->words->len; next_section_idx++)
        {
          WordEntry *next_entry = &(g_array_index (word_list->words, WordEntry, next_section_idx));
          if (next_entry->word_len != first_entry->word_len)
            break;

          guint next_stride = word_entry_stride (next_entry);
          section_stride = MAX (section_stride, next_stride);
        }

      /* Generate section info */

      WordListSection section = {
        .word_len = first_entry->word_len,
        .stride = section_stride,
        .count = next_section_idx - section_start_idx,
        .offset = section_offset,
      };
      g_array_append_val (word_list->word_index, section);

      section_offset += section.count * section_stride;

      /* Now assign the indexes to each word in the present section */

      gint index = 0;
      guint k;

      for (k = section_start_idx; k < next_section_idx; k++)
        {
          WordEntry *entry = &(g_array_index (word_list->words, WordEntry, k));
          entry->index = index;

          WordIndex word_index = {
            .length = entry->word_len,
            .index = index,
          };

          index++;
          gen_word_list_add_word_to_letters (word_list, entry->word, word_index);
        }

      section_start_idx = next_section_idx;
    }

  calculate_enumeration_offsets(word_list);
}

static gint
enumeration_entry_cmp (gconstpointer a,
                       gconstpointer b)
{
  const EnumerationEntry *entry_a = a;
  const EnumerationEntry *entry_b = b;

  return g_strcmp0 (entry_a->enumeration_src,
                    entry_b->enumeration_src);
}

gushort
get_enumeration_offset (GArray      *enumerations,
                        const gchar *enumeration_src)
{
  guint out_index = 0;
  EnumerationEntry target_entry;

  if (enumeration_src == NULL)
    return 0;

  target_entry.enumeration_src = (gchar *) enumeration_src;
  if (g_array_binary_search (enumerations,
                             &target_entry,
                             enumeration_entry_cmp,
                             &out_index))
    {
      EnumerationEntry entry;

      entry = g_array_index (enumerations,
                             EnumerationEntry,
                             out_index);

      return entry.offset;
    }

  return 0;
}

static void
word_list_write_words (GenWordList   *word_list,
                       GOutputStream *stream)
{
  g_autoptr (GError) error = NULL;
  guint i;
  guint word_num = 0;
  guint section_num;
  gssize total_offset = 0;

  for (section_num = 0; section_num < word_list->word_index->len; section_num++)
    {
      WordListSection *section = &(g_array_index (word_list->word_index, WordListSection, section_num));
      guint i;

      for (i = 0; i < section->count; i++)
        {
          WordEntry *entry;
          gssize entry_bytes = 0;
          gushort enumeration_offset = 0;

          entry = &(g_array_index (word_list->words, WordEntry, word_num));
          enumeration_offset = get_enumeration_offset (word_list->enumerations, entry->enumeration_src);

          entry_bytes += g_output_stream_write (stream,
                                                &(entry->priority), PRIORITY_SIZE,
                                                NULL, &error);
          g_assert (error == NULL);

          entry_bytes += g_output_stream_write (stream,
                                                &(enumeration_offset), ENUMERATION_SIZE,
                                                NULL, &error);
          g_assert (error == NULL);

          entry_bytes += g_output_stream_write (stream,
                                                entry->word, entry->byte_len,
                                                NULL, &error);
          g_assert (error == NULL);

          int padding;
          for (padding = 0; padding < section->stride - entry_bytes; padding++)
            {
              g_output_stream_write (stream, "\0", 1, NULL, &error);
            }

          total_offset += section->stride;

          word_num++;
        }
    }

  /* Write the letter list to file */
  word_list->letter_list_offset = total_offset;

  for (i = 0; i < word_list->letters->len; i++)
    {
      FilterFragmentList *frag_list;

      frag_list = &(g_array_index (word_list->letters, FilterFragmentList, i));
      frag_list->offset = total_offset;
      /* FIXME(serialize): We are counting on this serializing correctly. */
      if (frag_list->array->len > 0)
        total_offset += g_output_stream_write (stream,
                                               frag_list->array->data,
                                               (frag_list->array->len) * 2,
                                               NULL, &error);
      g_assert (error == NULL);
    }

  /* write the letter_index for the letter list */
  word_list->letter_index_offset = total_offset;

  for (i = 0; i < word_list->letters->len; i++)
    {
      FilterFragmentList *frag_list;
      guchar buf[6];
      gushort len;

      frag_list = &(g_array_index (word_list->letters, FilterFragmentList , i));
      g_assert (frag_list->array->len < G_MAXUINT16);
      len = (gushort) frag_list->array->len;

      /* FIXME(serialize): This marshalling is also arch-dependent */
      /* FIXME(serialize): our offset includes the header, so is
         absolute and not relative. We fix it on the other end, but
         it's different from the documentation. We should make this
         consistent */
      memcpy (buf, (char*) &(frag_list->offset), sizeof (guint));
      memcpy (buf + 4, (char *) &(len), sizeof (gushort));

      total_offset += g_output_stream_write (stream,
                                             buf,
                                             6,
                                             NULL, &error);
    }

  /* write enumerations to the file */
  word_list->enumerations_offset = total_offset;
  char zero = 0;
  total_offset += g_output_stream_write (stream,
                                         &zero,
                                         NIL_SIZE,
                                         NULL, &error);
  for (i = 0; i < word_list->enumerations->len; i++)
    {
      EnumerationEntry entry = g_array_index (word_list->enumerations, EnumerationEntry, i);

      total_offset += g_output_stream_write (stream,
                                             entry.enumeration_src,
                                             strlen (entry.enumeration_src) + NIL_SIZE,
                                             NULL, &error);
    }

  /* write anagram fragments to the file */
  word_list->anagram_word_list_offset = total_offset;
  for (i = 0; i < word_list->anagram_fragments->len; i++)
    {
      FilterFragmentList *frag_list;

      frag_list = &(g_array_index (word_list->anagram_fragments, FilterFragmentList, i));
      frag_list->offset = total_offset;

      if (frag_list->array->len > 0)
        total_offset += g_output_stream_write (stream,
                                               frag_list->array->data,
                                               (frag_list->array->len) * 2,
                                               NULL, &error);
      g_assert (error == NULL);
    }

  /* write anagram list offsets into the file */
  word_list->anagram_hash_index_offset = total_offset;
  for (i = 0; i < word_list->anagram_fragments->len; i++)
    {
      FilterFragmentList *frag_list;
      guchar buf[9];
      guchar len;
      guint hash;

      frag_list = &(g_array_index (word_list->anagram_fragments, FilterFragmentList , i));
      hash = g_array_index (word_list->corresponding_hash, guint, i);

      g_assert (frag_list->array->len < 255);
      len = (guchar) frag_list->array->len;

      memcpy (buf, (char*) &(frag_list->offset), sizeof (guint));
      memcpy (buf + 4, (char*) &(hash), sizeof (guint));
      memcpy (buf + 8, (char *) &(len), sizeof (guchar));

      total_offset += g_output_stream_write (stream,
                                             buf,
                                             9,
                                             NULL, &error);
    }

  /* NULL terminate the section */
  char nil = '\0';
  g_output_stream_write (stream, &nil, 1, NULL, &error);
}

void
gen_word_list_write_index_to_stream (GenWordList   *word_list,
                                     const char    *display_name,
                                     GOutputStream *stream)
{
  GString *string;
  guint i;
  g_autofree char *charset_str = NULL;
  guint count = 0;

  charset_str = ipuz_charset_serialize (word_list->charset);

  string = g_string_new (NULL);
  g_string_append (string, "{\n");
  g_string_append_printf (string, "  \"display-name\": \"%s\",\n", display_name);
  g_string_append_printf (string, "  \"charset\": \"%s\",\n", charset_str);
  g_string_append (string, "  \"filterchar\": \"?\",\n");
  g_string_append_printf (string, "  \"min-length\": %d,\n", word_list->min_length);
  g_string_append_printf (string, "  \"max-length\": %d,\n", word_list->max_length);
  g_string_append_printf (string, "  \"threshold\": %d,\n", word_list->threshold);
  g_string_append_printf (string, "  \"letter-list-offset\": %d,\n", (gint)word_list->letter_list_offset);
  g_string_append_printf (string, "  \"letter-index-offset\": %d,\n", (gint)word_list->letter_index_offset);
  g_string_append_printf (string, "  \"enumerations-offset\": %d,\n", (gint)word_list->enumerations_offset);
  g_string_append_printf (string, "  \"anagram-word-list-offset\": %d,\n", (gint)word_list->anagram_word_list_offset);
  g_string_append_printf (string, "  \"anagram-hash-index-offset\": %d,\n", (gint)word_list->anagram_hash_index_offset);
  g_string_append_printf (string, "  \"anagram-hash-index-length\": %d,\n", (gint)word_list->anagram_fragments->len);

  g_string_append (string, "  \"words\": [\n");

  for (i = 0; i < word_list->word_index->len; i++)
    {
      WordListSection *header_section;

      header_section = &(g_array_index (word_list->word_index, WordListSection, i));
      g_string_append_printf (string, "\t\t[%d, %d, %u, %d]",
                              header_section->word_len,
                              header_section->stride,
                              header_section->offset,
                              header_section->count);
      if (i + 1 < word_list->word_index->len)
        g_string_append (string, ",\n");
      else
        g_string_append (string, "\n");
      count += header_section->count;
    }
  g_string_append (string, "\t],\n");
  g_string_append_printf (string, "  \"word-count\": %u\n", count);
  g_string_append (string, "\n}\n");
  g_output_stream_write (stream,
                         string->str, string->len,
                         NULL, NULL);

  g_string_free (string, TRUE);
}

void
gen_word_list_write_to_stream (GenWordList   *word_list,
                               const char    *display_name,
                               GOutputStream *stream)
{
  word_list_write_words (word_list, stream);
  gen_word_list_write_index_to_stream (word_list, display_name, stream);
}

void
gen_word_list_write (GenWordList *word_list,
                     const char  *display_name,
                     const char  *output_filename)

{
  g_autoptr (GFile) file = NULL;
  g_autoptr (GOutputStream) stream = NULL;
  g_autoptr (GError) error = NULL;

  if (output_filename != NULL)
    {
      file = g_file_new_for_commandline_arg (output_filename);
      stream = (GOutputStream *) g_file_replace (file, NULL,
                                                 FALSE, G_FILE_CREATE_REPLACE_DESTINATION,
                                                 NULL, &error);
      if (error)
        {
          g_printerr (_("Error saving file %s: %s\n"), output_filename, error->message);
          return;
        }
    }
  else
    {
      stream = g_unix_output_stream_new (1, FALSE);
    }

  g_assert (stream);

  gen_word_list_write_to_stream (word_list, display_name, stream);

  g_output_stream_close (stream, NULL, NULL);
}

void
gen_word_list_write_index (GenWordList *word_list,
                           const char  *display_name,
                           const char  *output_filename)

{
  g_autoptr (GFile) file = NULL;
  g_autoptr (GOutputStream) stream = NULL;
  g_autoptr (GError) error = NULL;

  if (output_filename != NULL)
    {
      file = g_file_new_for_commandline_arg (output_filename);
      stream = (GOutputStream *) g_file_replace (file, NULL,
                                                 FALSE, G_FILE_CREATE_REPLACE_DESTINATION,
                                                 NULL, &error);
      if (error)
        {
          g_printerr (_("Error saving file %s: %s\n"), output_filename, error->message);
          return;
        }
    }
  else
    {
      stream = g_unix_output_stream_new (1, FALSE);
    }

  g_assert (stream);

  gen_word_list_write_index_to_stream (word_list, display_name, stream);

  g_output_stream_close (stream, NULL, NULL);
}

void
gen_word_list_print_stats (GenWordList *word_list)
{
  g_print ("GenWordList\n");
  if (word_list->importer == BRODA_SCORED)
    g_print ("\tImporter: BRODA_SCORED\n");
  else if (word_list->importer == BRODA_FULL)
    g_print ("\tImporter: BRODA_FULL\n");
  g_print ("\t Total word count: %u\n", word_list->words->len);
}

#ifdef TESTING

static void
sorts_words (void)
{
  const char *input = "ÁRBOL;1\nARTES;1\nPERRO;1\n";
  g_autoptr (GInputStream) stream = g_memory_input_stream_new_from_data (input, strlen (input), NULL);

  GenWordList *word_list = gen_word_list_new (1, 10, 1, BRODA_SCORED);
  g_assert (gen_word_list_parse (word_list, stream));
  gen_word_list_sort (word_list);

  g_assert_cmpint (word_list->words->len, ==, 3);

  WordEntry entry;

  entry = g_array_index (word_list->words, WordEntry, 0);
  g_assert_cmpstr (entry.word, ==, "ÁRBOL");

  entry = g_array_index (word_list->words, WordEntry, 1);
  g_assert_cmpstr (entry.word, ==, "ARTES");

  entry = g_array_index (word_list->words, WordEntry, 2);
  g_assert_cmpstr (entry.word, ==, "PERRO");

  gen_word_list_free (word_list);
}

static void
word_list_builds_charset (void)
{
  const char *input = "ÁRBOL;1\nBLÅHAJ;1\nBORLA;1\nTRALALA;1\n";
  g_autoptr (GInputStream) stream = g_memory_input_stream_new_from_data (input, strlen (input), NULL);

  GenWordList *word_list = gen_word_list_new (1, 10, 1, BRODA_SCORED);
  g_assert (gen_word_list_parse (word_list, stream));
  gen_word_list_sort (word_list);
  gen_word_list_build_charset (word_list);

  g_assert_cmpint (ipuz_charset_get_n_chars (word_list->charset), ==, 10);
  g_autofree char *serialized = ipuz_charset_serialize (word_list->charset);

  /* Characters are sorted by Unicode code point, not logically.  I guess that doesn't matter? */
  g_assert_cmpstr (serialized, ==, "ABHJLORTÁÅ");

  gen_word_list_free (word_list);
}

int
main (int argc, char **argv)
{
  setlocale(LC_ALL, "en_US.utf8");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/gen_word_list/sorts_words", sorts_words);
  g_test_add_func ("/gen_word_list/builds_charset", word_list_builds_charset);

  return g_test_run ();
}
#endif

