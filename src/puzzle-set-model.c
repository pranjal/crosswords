/* puzzle-set-model.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include <libipuz/libipuz.h>
#include <glib/gi18n-lib.h>

#include "crosswords-misc.h"
#include "puzzle-set-model.h"


#define DOMAIN_DOWNLOADS     "downloads"
#define DOMAIN_SAVED_PUZZLES "saved-puzzles"


enum {
  MODEL_WON_CHANGED,
  MODEL_N_SIGNALS
};

enum
{
  MODEL_PROP_0,
  MODEL_PROP_CONFIG,
  MODEL_N_PROPS
};

static guint model_obj_signals[MODEL_N_SIGNALS] = { 0 };
static GParamSpec *model_obj_props[MODEL_N_PROPS] = {NULL, };


struct _PuzzleSetModel
{
  GObject parent_object;
  GListModel *puzzles;
  PuzzleSetConfig *config;
  GHashTable *skip_puzzles;
  /* Keep a cumulative count of this as a convenience */
  guint n_won;

  gchar *loading_error_message;

  /* Only used in DEVELOPMENT_BUILD */
  gboolean debug_n_won;
};


enum {
  ROW_CHANGED,
  ROW_N_SIGNALS
};

static guint row_obj_signals[ROW_N_SIGNALS] = { 0 };


struct _PuzzleSetModelRow
{
  GObject parent_object;
  PuzzleSetModel *model;
  gchar *file_path;   /* only when readonly is FALSE */
  IpuzPuzzle *puzzle;
  GDateTime *puzzle_mtime;
  gchar *puzzle_name;
  gchar *last_saved_checksum;
  GError *loading_error;
  /* Cache these next two values to avoid recalculating them every
   * time */
  gfloat percent;
  gboolean won;

  gboolean readonly;  /* TRUE, when puzzle is loaded from the resource file */
  gboolean mark; /* used for clearing old puzzles */
};


struct _PuzzleSetModelFilter
{
  GtkFilter parent_object;
  PuzzleSetModelFilterType type;
  GSettings *settings;
  gulong hide_solved_puzzles_handler;
};


/* PuzzleSetModel */
static void               puzzle_set_model_init                  (PuzzleSetModel            *self);
static void               puzzle_set_model_class_init            (PuzzleSetModelClass       *klass);
static void               puzzle_set_model_list_model_init       (GListModelInterface       *iface);
static void               puzzle_set_model_set_property          (GObject                   *object,
                                                                  guint                      prop_id,
                                                                  const GValue              *value,
                                                                  GParamSpec                *pspec);
static void               puzzle_set_model_get_property          (GObject                   *object,
                                                                  guint                      prop_id,
                                                                  GValue                    *value,
                                                                  GParamSpec                *pspec);
static void               puzzle_set_model_dispose               (GObject                   *object);
static GType              puzzle_set_model_get_item_type         (GListModel                *list);
static guint              puzzle_set_model_get_n_items           (GListModel                *list);
static gpointer           puzzle_set_model_get_item              (GListModel                *list,
                                                                  guint                      position);
static guint              calculate_won_count                    (PuzzleSetModel            *self);
static void               update_won_count                       (PuzzleSetModel            *self,
                                                                  gint                       change);


/* PuzzleSetModelRow */
static void               puzzle_set_model_row_init              (PuzzleSetModelRow         *self);
static void               puzzle_set_model_row_class_init        (PuzzleSetModelRowClass    *klass);
static void               puzzle_set_model_row_dispose           (GObject                   *object);
static PuzzleSetModelRow *puzzle_set_model_row_new               (PuzzleSetModel            *model);
static void               puzzle_set_model_row_set_puzzle        (PuzzleSetModelRow         *self,
                                                                  IpuzPuzzle                *puzzle);
static void               puzzle_set_model_row_set_file_path     (PuzzleSetModelRow         *self,
                                                                  const gchar               *file_path);
static void               puzzle_set_model_row_set_puzzle_mtime  (PuzzleSetModelRow         *self,
                                                                  GDateTime                 *puzzle_mtime);
static void               puzzle_set_model_row_set_puzzle_name   (PuzzleSetModelRow         *self,
                                                                  const gchar               *puzzle_name);
static void               puzzle_set_model_row_set_checksum      (PuzzleSetModelRow         *self,
                                                                  const gchar               *last_saved_checksum);
static void               puzzle_set_model_row_set_readonly      (PuzzleSetModelRow         *self,
                                                                  gboolean                   readonly);
static void               puzzle_set_model_row_set_mark          (PuzzleSetModelRow         *self,
                                                                  gboolean                   mark);


/* PuzzleSetModelFilter */
static void               puzzle_set_model_filter_init           (PuzzleSetModelFilter      *self);
static void               puzzle_set_model_filter_class_init     (PuzzleSetModelFilterClass *klass);
static void               puzzle_set_model_filter_dispose        (GObject                   *object);
static gboolean           puzzle_set_model_filter_match          (GtkFilter                 *filter,
                                                                  gpointer                   item);
static GtkFilterMatch     puzzle_set_model_filter_get_strictness (GtkFilter                 *filter);



G_DEFINE_TYPE_WITH_CODE (PuzzleSetModel, puzzle_set_model, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL,
                                                puzzle_set_model_list_model_init));

G_DEFINE_TYPE (PuzzleSetModelRow, puzzle_set_model_row, G_TYPE_OBJECT);


static void
puzzle_set_model_init (PuzzleSetModel *self)
{
  self->puzzles = (GListModel *) g_list_store_new (PUZZLE_TYPE_SET_MODEL_ROW);
  g_signal_connect_swapped (self->puzzles,
                            "items-changed",
                            G_CALLBACK (g_list_model_items_changed),
                            self);

  self->skip_puzzles = g_hash_table_new_full (g_str_hash, g_str_equal,
                                              g_free, (GDestroyNotify) g_date_time_unref);
}

static void
puzzle_set_model_class_init (PuzzleSetModelClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = puzzle_set_model_get_property;
  object_class->set_property = puzzle_set_model_set_property;
  object_class->dispose = puzzle_set_model_dispose;

  model_obj_props[MODEL_PROP_CONFIG] =
    g_param_spec_object ("config",
                         "Config",
                         "Config that the picker set is loaded from",
                         PUZZLE_TYPE_SET_CONFIG,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT);
  g_object_class_install_properties (object_class, MODEL_N_PROPS, model_obj_props);

  model_obj_signals[MODEL_WON_CHANGED] =
    g_signal_new ("won-changed",
                  PUZZLE_TYPE_SET_MODEL,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);
}

static void
puzzle_set_model_list_model_init (GListModelInterface *iface)
{
  iface->get_item_type = puzzle_set_model_get_item_type;
  iface->get_n_items = puzzle_set_model_get_n_items;
  iface->get_item = puzzle_set_model_get_item;
}

static void
puzzle_set_model_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  PuzzleSetModel *self;

  self = PUZZLE_SET_MODEL (object);

  switch (prop_id)
    {
    case MODEL_PROP_CONFIG:
      g_clear_object (&self->config);
      self->config = (PuzzleSetConfig *) g_value_dup_object (value);
      puzzle_set_model_reload (PUZZLE_SET_MODEL (object));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
puzzle_set_model_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  PuzzleSetModel *self;

  self = PUZZLE_SET_MODEL (object);

  switch (prop_id)
    {
    case MODEL_PROP_CONFIG:
      g_value_set_object (value, self->config);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
puzzle_set_model_dispose (GObject *object)
{
  PuzzleSetModel *self;

  self = PUZZLE_SET_MODEL (object);

  g_clear_object (&self->puzzles);
  g_clear_object (&self->config);
  g_clear_pointer (&self->skip_puzzles, g_hash_table_unref);

  G_OBJECT_CLASS (puzzle_set_model_parent_class)->dispose (object);
}


static gchar *
build_file_path (const gchar *domain,
                 const gchar *id,
                 const gchar *puzzle_name)
{
  return g_build_filename (g_get_user_data_dir (),
                           "gnome-crosswords",
                           domain, id, puzzle_name,
                           NULL);
}

static gboolean
assure_directory (const gchar *file_path)
{
  if (g_mkdir_with_parents (file_path, 0755))
    {
      /* FIXME(error): Find a way to warn up to the top */
      g_warning ("Unable to create directory %s. Please check your permissions.\n", file_path);
      return FALSE;
    }
  return TRUE;
}

static void
row_load_guesses (const gchar       *id,
                  PuzzleSetModelRow *row)
{
  IpuzGuesses *guesses;
  g_autofree gchar *pathname = NULL;
  g_autofree gchar *saved_file_name = NULL;

  saved_file_name = g_strdup_printf ("%s.saved", row->puzzle_name);
  pathname = g_build_filename (g_get_user_data_dir (),
                               "gnome-crosswords",
                               DOMAIN_SAVED_PUZZLES,
                               id, saved_file_name,
                               NULL);

  /* FIXME(error): should we guard against changes in format and pass
   * in an error? */
  guesses = ipuz_guesses_new_from_file (pathname, NULL);
  if (guesses == NULL)
    guesses = ipuz_guesses_new_from_board (ipuz_crossword_get_board (IPUZ_CROSSWORD (row->puzzle)), FALSE);

  row->last_saved_checksum = ipuz_guesses_get_checksum (guesses, NULL);
  puzzle_set_model_row_set_guesses (row, guesses);
}

static gboolean
find_row_equal (PuzzleSetModelRow *row_a,
                PuzzleSetModelRow *row_b)
{
  g_assert (row_a);
  g_assert (row_b);

  return g_strcmp0 (row_a->puzzle_name, row_b->puzzle_name) == 0;
}

/* This checks if file_name is already stored, either in the
 * skip_puzzles or in the main model. If it exists there and has the
 * same mtime, then it will return TRUE, and we don't need to do
 * anything with it.
 *
 * Returns false if it's not present or if it exists with an older
 * mtime. In that second case, we return the existing row so it can be
 * updated.
 */
static gboolean
puzzle_name_is_cached (PuzzleSetModel     *self,
                       const gchar        *puzzle_name,
                       const GDateTime    *puzzle_mtime,
                       PuzzleSetModelRow **row)
{
  g_autoptr (PuzzleSetModelRow) temp_row = NULL;
  guint position = 0;

  /* we don't let a NULL row to be passed in */
  g_assert (row);
  *row = NULL;

  temp_row = puzzle_set_model_row_new (self);
  puzzle_set_model_row_set_puzzle_name (temp_row, puzzle_name);

  /* Check to see if it's in the model */
  if (g_list_store_find_with_equal_func (G_LIST_STORE (self->puzzles), temp_row,
                                         (GEqualFunc) find_row_equal, &position))
    {
      *row = g_list_model_get_item (self->puzzles, position);

      /* Puzzle exists with the same time, or we don't care about the mtime */
      if (puzzle_mtime == NULL ||
          g_date_time_compare (puzzle_mtime, (*row)->puzzle_mtime) == 0)
        return TRUE;

      /* It's a different time. The direction doesn't matter too much,
         I suppose. */
      return FALSE;
    }

  /* Check the skip_puzzles */
  if (g_hash_table_contains (self->skip_puzzles, puzzle_name))
    {
      /* We don't have an mtime in the case of resources, so we can
       * simply skip this puzzle.
       */
      if (puzzle_mtime)
        {
          const GDateTime *skip_time = g_hash_table_lookup (self->skip_puzzles, puzzle_name);
          if (g_date_time_compare (puzzle_mtime, skip_time) == 0)
            return TRUE;
          /* If we exist in the skip_puzzles already, but with a
           * different time, we should try to load this puzzle
           * again. */
          g_hash_table_remove (self->skip_puzzles, puzzle_name);
          return FALSE;
        }
      else
        return TRUE;
    }
  return FALSE;
}

static void
mark_puzzles (PuzzleSetModel *self,
              gboolean        readonly)
{
  readonly = !! readonly;

  for (guint i = 0; i < g_list_model_get_n_items (self->puzzles); i++)
    {
      PuzzleSetModelRow *row = g_list_model_get_item (self->puzzles, i);

      g_assert (row->mark == FALSE);
      if (readonly == row->readonly)
        puzzle_set_model_row_set_mark (row, TRUE);
    }
}

static void
remove_marked_puzzles (PuzzleSetModel *self)
{
  guint i = 0;

  while (i < g_list_model_get_n_items (self->puzzles))
    {
      PuzzleSetModelRow *row = g_list_model_get_item (self->puzzles, i);

      if (row->mark)
        g_list_store_remove (G_LIST_STORE (self->puzzles), i);
      else
        i++;
    }
}

static void
load_puzzles_from_directory (PuzzleSetModel *self)
{
  gchar *directory;
  g_autoptr (GDir) dir = NULL;
  const gchar *file_name;
  const gchar *id;

  id = puzzle_set_config_get_id (self->config);
  directory = g_build_filename (g_get_user_data_dir (),
                                "gnome-crosswords",
                                DOMAIN_DOWNLOADS,
                                id, NULL);

  /* Make sure the directory exists */
  if (! assure_directory (directory))
    return;

  mark_puzzles (self, FALSE);

  dir = g_dir_open (directory, 0, NULL);
  while ((file_name = g_dir_read_name (dir)) != NULL)
    {
      g_autoptr (PuzzleSetModelRow) row = NULL;
      g_autofree gchar *file_path = NULL;
      g_autoptr (GFile) file = NULL;
      g_autoptr (GError) error = NULL;
      g_autoptr (GFileInfo) file_info = NULL;
      g_autoptr (GDateTime) puzzle_mtime = NULL;
      g_autoptr (IpuzPuzzle) puzzle = NULL;

      file_path = build_file_path (DOMAIN_DOWNLOADS, id, file_name);
      file = g_file_new_for_path (file_path);
      file_info = g_file_query_info (file,
                                     G_FILE_ATTRIBUTE_TIME_MODIFIED,
                                     G_FILE_QUERY_INFO_NONE,
                                     NULL, &error);
      puzzle_mtime = g_file_info_get_modification_date_time (file_info);
      if (puzzle_mtime == NULL && error)
        {
          /* FIXME(filesystem) I'm not sure what to do here? If we
           * can't get the mtime from the file system, it'll be hard
           * to cache. Error out for now. */
          g_error ("Unable to get the mtime of puzzle. %s", error->message);
        }

      /* We want to see if this puzzle is stored already, or in
       * skip_list. See above for more details */
      if (puzzle_name_is_cached (self, file_name, puzzle_mtime, &row))
        {
          puzzle_set_model_row_set_mark (row, FALSE);
          continue;
        }

      /* */
      if (row == NULL)
        row = puzzle_set_model_row_new (self);

      /* Now we can load the puzzle, which is more expensive. We don't
       * do anything with the error, as we'll clean it up later */
      puzzle = ipuz_puzzle_new_from_file (file_path, &row->loading_error);

      puzzle_set_model_row_set_mark (row, FALSE);
      puzzle_set_model_row_set_puzzle_name (row, file_name);
      puzzle_set_model_row_set_file_path (row, file_path);
      puzzle_set_model_row_set_puzzle (row, puzzle);
      puzzle_set_model_row_set_puzzle_mtime (row, puzzle_mtime);
      row_load_guesses (id, row);

      /* It's fine to add this to the puzzle regardless of the state
       * of the errors or puzzle. if they didn't load correctly, we'll
       * clear it out later.
       */
      g_list_store_append (G_LIST_STORE (self->puzzles), g_object_ref (row));
    }
  remove_marked_puzzles (self);
}

static void
load_puzzles_from_resources (PuzzleSetModel *self)
{
  guint i = 0;
  const gchar *id;

  id = puzzle_set_config_get_id (self->config);

  mark_puzzles (self, TRUE);
  while (TRUE)
    {
      g_autoptr (PuzzleSetModelRow) row = NULL;
      const gchar *puzzle_name;
      IpuzPuzzle *puzzle;

      puzzle_name = puzzle_set_config_get_puzzle_name (self->config, i);
      if (puzzle_name == NULL)
        break; /* we reached the end of the puzzles */

      if (puzzle_name_is_cached (self, puzzle_name, NULL, &row))
        {
          puzzle_set_model_row_set_mark (row, FALSE);
          i++;
          continue;
        }

      if (row == NULL)
        row = puzzle_set_model_row_new (self);

      puzzle = puzzle_set_config_load_puzzle (self->config, i, &row->loading_error);
      puzzle_set_model_row_set_mark (row, FALSE);
      puzzle_set_model_row_set_puzzle_name (row, puzzle_name);
      puzzle_set_model_row_set_readonly (row, TRUE);
      puzzle_set_model_row_set_puzzle (row, puzzle);
      row_load_guesses (id, row);

      /* As above, we always add the puzzle even if it fails to
       * load. We clean them out and warn the user at the end. */
      g_list_store_append (G_LIST_STORE (self->puzzles), g_object_ref (row));
      i++;
    }
  remove_marked_puzzles (self);
}

static void
check_puzzles_for_errors (PuzzleSetModel *self)
{
  guint n_broken_files = 0;
  guint n_broken_resources = 0;
  guint i = 0;

  /* Count how many problem files we have. */
  for (i = 0; i < g_list_model_get_n_items (self->puzzles); i++)
    {
      PuzzleSetModelRow *row;

      row = g_list_model_get_item (self->puzzles, i);
      if (row->puzzle == NULL || row->loading_error)
        {
          if (row->readonly)
            n_broken_resources ++;
          else
            n_broken_files ++;
        }
    }

  /* All fine. No need to clean anything up  */
  if (n_broken_resources == 0 && n_broken_files == 0)
    return;

  /* Generate the error and delete the files */
  if (n_broken_files > 0)
    {
      i = 0;
      while (i < g_list_model_get_n_items (self->puzzles))
        {
          PuzzleSetModelRow *row;

          row = g_list_model_get_item (self->puzzles, i);

          /* We had problems loading the puzzle. We need to remove
             this row */
          if (row->puzzle == NULL || row->loading_error)
            {
              g_hash_table_insert (self->skip_puzzles, g_strdup (row->puzzle_name), row->puzzle_mtime);
              if ((n_broken_files == 1 && n_broken_resources == 0) ||
                  (n_broken_files == 0 && n_broken_resources == 1))
                {
                  if (row->loading_error)
                    self->loading_error_message = g_strdup_printf (_("Puzzle %s couldn't be loaded.\n<i>Error: %s</i>"),
                                                                   row->puzzle_name, row->loading_error->message);
                  else
                    self->loading_error_message = g_strdup_printf (_("Puzzle %s couldn't be loaded."), row->puzzle_name);
                }
              g_list_store_remove (G_LIST_STORE (self->puzzles), i);
            }
          else
            i++;
        }
    }

  if (n_broken_resources > 1 && n_broken_files == 0)
    self->loading_error_message = g_strdup_printf (_("Puzzle set %s contains invalid puzzle files."),
                                                   puzzle_set_config_get_id (self->config));
  else if (n_broken_resources == 0 && n_broken_files > 1)
    self->loading_error_message = g_strdup_printf (_("Multiple files couldn't be loaded. Cleaning them up."));

  if (self->loading_error_message == NULL) /* Both broken_files and broken_resources. Rare */
    self->loading_error_message =
      g_strdup_printf (_("Puzzle set %s contains invalid puzzle files and some files couldn't be loaded. Cleaning them up."),
                       puzzle_set_config_get_id (self->config));
}


/* PuzzleSetModelItem */

static GType
puzzle_set_model_get_item_type (GListModel *model)
{
  return PUZZLE_TYPE_SET_MODEL_ROW;
}

static guint
puzzle_set_model_get_n_items (GListModel *model)
{
  PuzzleSetModel *self;

  self = PUZZLE_SET_MODEL (model);

  return g_list_model_get_n_items (self->puzzles);
}

static gpointer
puzzle_set_model_get_item (GListModel *model,
                          guint       position)
{
  PuzzleSetModel *self;

  self = PUZZLE_SET_MODEL (model);

  return g_list_model_get_item (self->puzzles, position);
}


static guint
calculate_won_count (PuzzleSetModel *self)
{
  guint won_count = 0;
  for (guint i = 0; i < g_list_model_get_n_items (G_LIST_MODEL (self)); i++)
    {
      PuzzleSetModelRow *row;

      row = g_list_model_get_item (G_LIST_MODEL (self), i);
      if (puzzle_set_model_row_won (row))
        won_count ++;
    }
  return won_count;
}

static void
update_won_count (PuzzleSetModel *self,
                  gint            change)
{
  g_return_if_fail (PUZZLE_IS_SET_MODEL (self));

  self->n_won += change;

#ifdef DEVELOPMENT_BUILD
  if (self->debug_n_won)
    g_assert (self->n_won == calculate_won_count (self));
#endif
  
  g_signal_emit (self, model_obj_signals [MODEL_WON_CHANGED], 0);
}

/* Public methods */

PuzzleSetModel *
puzzle_set_model_new (PuzzleSetConfig *config)
{
  PuzzleSetModel *model;
  model = (PuzzleSetModel *) g_object_new (PUZZLE_TYPE_SET_MODEL,
                                           "config", config,
                                           NULL);

  return model;
}

gboolean
puzzle_set_model_find (PuzzleSetModel    *self,
                       PuzzleSetModelRow *row,
                       guint             *index)
{
  g_return_val_if_fail (PUZZLE_IS_SET_MODEL (self), FALSE);
  g_return_val_if_fail (PUZZLE_IS_SET_MODEL_ROW (row), FALSE);

  return g_list_store_find (G_LIST_STORE (self->puzzles), row, index);
}

gboolean
puzzle_set_model_find_by_puzzle_name (PuzzleSetModel *self,
                                      const gchar    *puzzle_name,
                                      guint          *index)
{
  g_autoptr (PuzzleSetModelRow) temp_row = NULL;

  g_return_val_if_fail (PUZZLE_IS_SET_MODEL (self), FALSE);

  temp_row = puzzle_set_model_row_new (self);
  puzzle_set_model_row_set_puzzle_name (temp_row, puzzle_name);

  return g_list_store_find_with_equal_func (G_LIST_STORE (self->puzzles), temp_row,
                                            (GEqualFunc) find_row_equal, index);
}

/* Convenience function as we call this a lot */
IpuzPuzzle *
puzzle_set_model_puzzle_by_puzzle_name (PuzzleSetModel *self,
                                        const gchar    *puzzle_name)
{
  guint index;

  if (puzzle_set_model_find_by_puzzle_name (self, puzzle_name, &index))
    {
      PuzzleSetModelRow *row;

      row = g_list_model_get_item (self->puzzles, index);
      return row->puzzle;
    }
  return NULL;
}


void
puzzle_set_model_reload (PuzzleSetModel *self)
{
  guint n_won;

  g_return_if_fail (PUZZLE_IS_SET_MODEL (self));

  self->debug_n_won = FALSE;
  load_puzzles_from_directory (self);
  load_puzzles_from_resources (self);
  check_puzzles_for_errors (self);
  self->debug_n_won = TRUE;

  n_won = calculate_won_count (self);

  if (self->n_won != n_won)
    {
      self->n_won = n_won;
      g_signal_emit (self, model_obj_signals[MODEL_WON_CHANGED], 0);
    }
  //puzzle_set_model_print (self);
}

/* This is used for the temporary files in /tmp. Before we copy the
 * file into the final, we want to give it a new name based on the
 * title of the puzzle. This will help dedup it, as well as catch
 * malformed files early.
 *
 * As we put the uri in /tmp ourselves, we know this should be fast
 * and local */
static gchar *
build_puzzle_name_from_puzzle (IpuzPuzzle  *puzzle,
                               GError     **error)
{
  g_autofree gchar *uniqueid = NULL;
  g_autofree gchar *title = NULL;
  g_autofree gchar *date = NULL;
  g_autofree gchar *escaped = NULL;


  if (puzzle == NULL)
    return NULL;

  /* Find a string to name the file */
  g_object_get (puzzle,
                "uniqueid", &uniqueid,
                "title", &title,
                "date", &date,
                NULL);

  if (uniqueid)
    escaped = g_uri_escape_string (uniqueid, NULL, TRUE);
  else if (title)
    escaped = g_uri_escape_string (title, NULL, TRUE);
  else if (date)
    escaped = g_uri_escape_string (date, NULL, TRUE);
  else /* No obvious identifier. Just have to call it puzzle, and a timestamp */
    escaped = g_strdup_printf ("puzzle-%" G_GINT64_FORMAT, g_get_monotonic_time ());

  return g_strdup_printf ("%s.ipuz", escaped);
}

/* This is a temporary struct to pass two pieces of data into a callback
 */
typedef struct
{
  PuzzleSetModel *model;
  gchar *puzzle_path;
  gchar *puzzle_name;
  IpuzPuzzle *puzzle;
  gboolean delete_when_done;
  gchar *src_path;
  ModelImportDoneFunc import_done_func;
  gpointer user_data;
  GDestroyNotify destroy_func;
} PuzzleInstallTuple;

/* This function will try to delete the temporary file. It has a check
 * to stop something weird happening and us deleting a file outside of
 * tmp/, but we count on the flatpak setup as our primary security
 * mechanism. */
static void
delete_tmp_file (const gchar *file_path)
{
  g_autoptr(GFile) file = NULL;
  g_autoptr(GFile) tmp_dir = NULL;

  file = g_file_new_for_path (file_path);
  tmp_dir = g_file_new_for_path (g_get_tmp_dir ());

  if (g_file_has_prefix (file, tmp_dir))
    {
      /* Best effort. No need to check for an error */
      g_file_delete (file, NULL, NULL);
    }
  else
    {
      g_warning ("trying to delete %s, which is not a temp file\n", file_path);
    }
}

static void
copy_ready_cb (GFile              *file,
               GAsyncResult       *res,
               PuzzleInstallTuple *tuple)
{
  PuzzleSetModelRow *row = NULL;
  g_autoptr (GFileInfo) file_info = NULL;
  g_autoptr (GDateTime) puzzle_mtime = NULL;
  const gchar *id;
  guint index;
  gboolean new_row = FALSE;

  /* Finish the copy and load the new file */
  g_file_copy_finish (file, res, NULL);

  if (puzzle_set_model_find_by_puzzle_name (tuple->model,
                                            tuple->puzzle_name,
                                            &index))
    {
      row = g_list_model_get_item (G_LIST_MODEL (tuple->model),
                                   index);
    }
  else
    {
      row = puzzle_set_model_row_new (tuple->model);
      new_row = TRUE;
    }

  file_info = g_file_query_info (file,
                                 G_FILE_ATTRIBUTE_TIME_MODIFIED,
                                 G_FILE_QUERY_INFO_NONE,
                                 NULL, NULL);
  puzzle_mtime = g_file_info_get_modification_date_time (file_info);
  id = puzzle_set_config_get_id (tuple->model->config);

  puzzle_set_model_row_set_mark (row, FALSE);
  puzzle_set_model_row_set_puzzle_name (row, tuple->puzzle_name);
  puzzle_set_model_row_set_file_path (row, tuple->puzzle_path);
  puzzle_set_model_row_set_puzzle (row, tuple->puzzle);
  puzzle_set_model_row_set_puzzle_mtime (row, puzzle_mtime);
  row_load_guesses (id, row);
  if (new_row)
    g_list_store_append (G_LIST_STORE (tuple->model->puzzles), g_object_ref (row));

  if (tuple->import_done_func)
    (tuple->import_done_func) (row, tuple->user_data);

  if (tuple->delete_when_done)
    delete_tmp_file (tuple->src_path);

  /* FIXME(cleanup):  */
  g_free (tuple->puzzle_path);
  g_free (tuple->puzzle_name);
  g_free (tuple->src_path);
  if (tuple->destroy_func)
    tuple->destroy_func (tuple->user_data);
  g_free (tuple);
}

void
puzzle_set_model_import_puzzle_path (PuzzleSetModel       *self,
                                     const gchar          *puzzle_path,
                                     gboolean              delete_when_done,
                                     ModelImportDoneFunc   import_done_func,
                                     gpointer              user_data,
                                     GDestroyNotify        destroy_func,
                                     GError              **error)
{
  GError *tmp_error = NULL;
  IpuzPuzzle *puzzle;
  PuzzleInstallTuple *tuple;
  g_autoptr (GFile) source = NULL;
  g_autoptr (GFile) destination = NULL;
  gchar *puzzle_name = NULL;

  g_return_if_fail (PUZZLE_IS_SET_MODEL (self));
  g_return_if_fail (puzzle_path != NULL);

  puzzle = ipuz_puzzle_new_from_file (puzzle_path, &tmp_error);
  if (puzzle == NULL || tmp_error)
    {
      /* We don't delete the file, as we may want to look at it later */
      g_warning ("%s is not a valid puzzle\n%s", puzzle_path, tmp_error?tmp_error->message:"");
      g_propagate_error (error, tmp_error);
      return;
    }

  source = g_file_new_for_path (puzzle_path);
  /* delete_when_done being TRUE tells us the puzzle basename is a
   * mkstmp-created string, and not useful */
  if (delete_when_done)
    puzzle_name = build_puzzle_name_from_puzzle (puzzle, &tmp_error);
  else
    puzzle_name = g_file_get_basename (source);

  destination = g_file_new_build_filename (g_get_user_data_dir (),
                                           "gnome-crosswords",
                                           DOMAIN_DOWNLOADS,
                                           puzzle_set_config_get_id (self->config),
                                           puzzle_name,
                                           NULL);

  /* The Tuple takes ownership of all thesevalues */
  tuple = g_new (PuzzleInstallTuple, 1);
  tuple->model = self;
  tuple->puzzle_path = g_file_get_path (destination);
  tuple->puzzle_name = puzzle_name;
  tuple->puzzle = puzzle;
  tuple->delete_when_done = delete_when_done;
  tuple->src_path = g_strdup (puzzle_path);
  tuple->import_done_func = import_done_func;
  tuple->user_data = user_data;
  tuple->destroy_func = destroy_func;

  g_file_copy_async (source,
                     destination,
                     G_FILE_COPY_NONE,
                     G_PRIORITY_DEFAULT,
                     NULL,
                     NULL, NULL,
                     (GAsyncReadyCallback) copy_ready_cb,
                     tuple);
  return;
}

static void
remove_guesses (const gchar *id,
                const gchar *puzzle_name)
{
  g_autoptr(GFile) saved_file = NULL;
  g_autofree gchar *save_puzzle_name = NULL;

  g_assert (id != NULL);
  g_assert (puzzle_name != NULL);

  save_puzzle_name = g_strdup_printf ("%s.saved", puzzle_name);
  saved_file = g_file_new_build_filename (g_get_user_data_dir (),
                                          "gnome-crosswords",
                                          DOMAIN_SAVED_PUZZLES,
                                          id,
                                          save_puzzle_name,
                                          NULL);

  /* This may not actually exist yet. */
  g_file_delete (saved_file, NULL, NULL);
}

static void
remove_puzzle_row (PuzzleSetModel    *self,
                   PuzzleSetModelRow *row,
                   guint              position)
{
  gboolean won;

  g_autoptr(GFile) file = NULL;
  g_autoptr(GError) error = NULL;

  won = row->won;
  file = g_file_new_for_path (row->file_path);
  /* FIXME(error): We should let the user know when we can't remove a
   * file */
  if (!g_file_delete (file, NULL, &error))
    {
      g_warning ("Failed to delete %s: %s",
                 g_file_peek_path (file), error->message);
      return;
    }

  g_list_store_remove (G_LIST_STORE (self->puzzles), position);
  remove_guesses (puzzle_set_config_get_id (self->config), row->puzzle_name);

  update_won_count (row->model,
                    won ? -1 : 0);

}

void
puzzle_set_model_remove_puzzle (PuzzleSetModel *self,
                                const gchar    *puzzle_name)
{
  g_autoptr (PuzzleSetModelRow) temp_row = NULL;
  guint position = 0;
  PuzzleSetModelRow *row = NULL;

  g_return_if_fail (PUZZLE_IS_SET_MODEL (self));

  temp_row = puzzle_set_model_row_new (self);
  puzzle_set_model_row_set_puzzle_name (temp_row, puzzle_name);

  if (g_list_store_find_with_equal_func (G_LIST_STORE (self->puzzles), temp_row,
                                         (GEqualFunc) find_row_equal, &position))
    row = g_list_model_get_item (self->puzzles, position);

  /* we can't delete files from resoutces yet */
  if (row == NULL || row->readonly)
    return;

  remove_puzzle_row (self, row, position);
}

guint
puzzle_set_model_get_n_won (PuzzleSetModel *self)
{
  g_return_val_if_fail (PUZZLE_IS_SET_MODEL (self), 0);

  return self->n_won;
}

void
puzzle_set_model_print (PuzzleSetModel *self)
{
  g_print ("PuzzleSetModel: %s\n", puzzle_set_config_get_id (self->config));
  for (guint i = 0; i < g_list_model_get_n_items (G_LIST_MODEL (self)); i++)
    {
      gchar *title = NULL;
      PuzzleSetModelRow *row;
      g_autofree gchar *time_str = NULL;
      row = g_list_model_get_item (G_LIST_MODEL (self), i);
      g_object_get (row->puzzle,
                    "title", &title,
                    NULL);
      if (row->puzzle_mtime)
        time_str = g_date_time_format (row->puzzle_mtime, " — %F %T");
      g_print ("\tPuzzle %u: %s%s\n", i, title,
               time_str ? time_str :"");
    }
  g_print ("\n");
}

/* ModelRow */

static void
puzzle_set_model_row_init (PuzzleSetModelRow *self)
{
  /* PASS */
}

static void
puzzle_set_model_row_class_init (PuzzleSetModelRowClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = puzzle_set_model_row_dispose;

  row_obj_signals[ROW_CHANGED] =
    g_signal_new ("changed",
                  PUZZLE_TYPE_SET_MODEL_ROW,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION | G_SIGNAL_DETAILED,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);
}

static void
puzzle_set_model_row_dispose (GObject *object)
{
  PuzzleSetModelRow *self;

  self = PUZZLE_SET_MODEL_ROW (object);
  g_clear_object (&self->puzzle);
  g_clear_pointer (&self->puzzle_mtime, g_date_time_unref);
  g_clear_pointer (&self->file_path, g_free);
  g_clear_pointer (&self->puzzle_name, g_free);
  g_clear_pointer (&self->last_saved_checksum, g_free);
  g_clear_pointer (&self->loading_error, g_error_free);

  G_OBJECT_CLASS (puzzle_set_model_row_parent_class)->dispose (object);
}

static PuzzleSetModelRow *
puzzle_set_model_row_new (PuzzleSetModel *model)
{
  PuzzleSetModelRow *row;

  row = g_object_new (PUZZLE_TYPE_SET_MODEL_ROW, NULL);

  row->model = model;

  return row;
}

static void
puzzle_set_model_row_set_puzzle(PuzzleSetModelRow *self,
                                IpuzPuzzle        *puzzle)
{
  g_object_ref (puzzle);
  g_clear_object (&self->puzzle);
  self->puzzle = puzzle;

  g_signal_emit (self, row_obj_signals [ROW_CHANGED], g_quark_from_static_string ("puzzle"));
}

static void
puzzle_set_model_row_set_file_path (PuzzleSetModelRow *self,
                                    const gchar       *file_path)
{
  g_clear_pointer (&self->file_path, g_free);
  self->file_path = g_strdup (file_path);
}

static void
puzzle_set_model_row_set_puzzle_mtime (PuzzleSetModelRow *self,
                                       GDateTime         *puzzle_mtime)
{
  g_date_time_ref (puzzle_mtime);
  g_clear_pointer (&self->puzzle_mtime, g_date_time_unref);
  self->puzzle_mtime = puzzle_mtime;
}
static void
puzzle_set_model_row_set_puzzle_name (PuzzleSetModelRow *self,
                                      const gchar       *puzzle_name)
{
  g_clear_pointer (&self->puzzle_name, g_free);
  self->puzzle_name = g_strdup (puzzle_name);

  g_signal_emit (self, row_obj_signals [ROW_CHANGED],  g_quark_from_static_string ("puzzle-name"));
}

static void
puzzle_set_model_row_set_checksum (PuzzleSetModelRow *self,
                                   const gchar       *last_saved_checksum)
{
  gboolean emit_checksum = FALSE;

  if (g_strcmp0 (last_saved_checksum, self->last_saved_checksum))
    emit_checksum = TRUE;

  g_clear_pointer (&self->last_saved_checksum, g_free);
  self->last_saved_checksum = g_strdup (last_saved_checksum);

  if (emit_checksum) g_signal_emit (self, row_obj_signals [ROW_CHANGED], g_quark_from_static_string ("last-saved-checksum"));
}

static void
puzzle_set_model_row_set_readonly (PuzzleSetModelRow *self,
                                   gboolean           readonly)
{
  gboolean emit_readonly = FALSE;

  readonly = !!readonly;
  if (readonly != self->readonly)
    emit_readonly = TRUE;

  self->readonly = readonly;

  if (emit_readonly) g_signal_emit (self, row_obj_signals [ROW_CHANGED], g_quark_from_static_string ("readonly"));
}

static void
puzzle_set_model_row_set_mark (PuzzleSetModelRow *self,
                               gboolean           mark)
{
  g_return_if_fail (self != NULL);

  self->mark = !! mark;
}

/* public methods */

IpuzPuzzle *
puzzle_set_model_row_get_puzzle (PuzzleSetModelRow *row)
{
  g_return_val_if_fail (PUZZLE_IS_SET_MODEL_ROW (row), NULL);

  return row->puzzle;
}

const gchar *
puzzle_set_model_row_get_puzzle_name (PuzzleSetModelRow *row)
{
  g_return_val_if_fail (PUZZLE_IS_SET_MODEL_ROW (row), NULL);

  return row->puzzle_name;
}

const gchar *
puzzle_set_model_row_get_last_saved_checksum (PuzzleSetModelRow *row)
{
  g_return_val_if_fail (PUZZLE_IS_SET_MODEL_ROW (row), NULL);

  return row->last_saved_checksum;
}

gboolean
puzzle_set_model_row_get_readonly (PuzzleSetModelRow *row)
{
  g_return_val_if_fail (PUZZLE_IS_SET_MODEL_ROW (row), FALSE);

  return row->readonly;
}

static void
row_save_guesses (const gchar       *id,
                  PuzzleSetModelRow *row)
{
  IpuzGuesses *guesses;
  g_autofree gchar *guesses_dir_path = NULL;
  g_autofree gchar *filename = NULL;
  g_autofree gchar *savename = NULL;

  g_return_if_fail (id != NULL);

  guesses_dir_path = g_build_filename (g_get_user_data_dir (),
                                       "gnome-crosswords",
                                       DOMAIN_SAVED_PUZZLES,
                                       id, NULL);
  assure_directory (guesses_dir_path);

  guesses = ipuz_crossword_get_guesses (IPUZ_CROSSWORD (row->puzzle));
  savename = g_strdup_printf ("%s.saved", row->puzzle_name);
  filename = g_build_filename (guesses_dir_path, savename, NULL);

  /* FIXME(error): I'm not sure how best to handle errors in this
   * function. */
  ipuz_guesses_save_to_file (guesses, filename, NULL);
}

void
puzzle_set_model_row_set_guesses (PuzzleSetModelRow *row,
                                  IpuzGuesses       *guesses)
{
  gfloat new_percent;
  gfloat new_won;
  gboolean emit_won = FALSE;
  gboolean emit_percent = FALSE;

  g_autofree gchar *checksum = NULL;
  g_return_if_fail (PUZZLE_IS_SET_MODEL_ROW (row));
  g_return_if_fail (row->puzzle != NULL);

  if (!ipuz_crossword_set_guesses (IPUZ_CROSSWORD (row->puzzle),
                                   guesses))
    {
      /* the board changed. Set a clear one */
      guesses = ipuz_guesses_new_from_board (ipuz_crossword_get_board (IPUZ_CROSSWORD (row->puzzle)), FALSE);
      ipuz_crossword_set_guesses (IPUZ_CROSSWORD (row->puzzle), guesses);
      ipuz_guesses_unref (guesses);
    }

  new_percent = ipuz_guesses_get_percent (guesses);
  if (new_percent != row->percent)
    {
      row->percent = new_percent;
      emit_percent = TRUE;
    }

  checksum = ipuz_guesses_get_checksum (guesses, NULL);
  if (g_strcmp0 (checksum, row->last_saved_checksum) != 0)
    {
      row_save_guesses (puzzle_set_config_get_id (row->model->config), row);
      puzzle_set_model_row_set_checksum (row, checksum);
    }

  new_won = ipuz_crossword_game_won (IPUZ_CROSSWORD (row->puzzle));
  if (new_won != row->won)
    {
      row->won = new_won;
      emit_won = TRUE;

      update_won_count (row->model,
                        row->won?1:-1);
    }

  if (emit_won) g_signal_emit (row, row_obj_signals [ROW_CHANGED],  g_quark_from_static_string ("won"));
  if (emit_percent) g_signal_emit (row, row_obj_signals [ROW_CHANGED],  g_quark_from_static_string ("percent"));
  g_signal_emit (row, row_obj_signals [ROW_CHANGED],  g_quark_from_static_string ("guesses"));
}

gfloat
puzzle_set_model_row_get_percent (PuzzleSetModelRow *row)
{
  g_return_val_if_fail (PUZZLE_IS_SET_MODEL_ROW (row), 0.0);

  return row->percent;
}

gboolean
puzzle_set_model_row_won (PuzzleSetModelRow *row)
{
  g_return_val_if_fail (PUZZLE_IS_SET_MODEL_ROW (row), FALSE);

  return row->won;
}

/* PuzzleSetModelFilter */

G_DEFINE_TYPE (PuzzleSetModelFilter, puzzle_set_model_filter, GTK_TYPE_FILTER);

static void
puzzle_set_model_filter_init (PuzzleSetModelFilter *self)
{
  /* Pass */
}

static void
puzzle_set_model_filter_class_init (PuzzleSetModelFilterClass *klass)
{
  GObjectClass *object_class;
  GtkFilterClass *filter_class;

  object_class = G_OBJECT_CLASS (klass);
  filter_class = GTK_FILTER_CLASS (klass);

  object_class->dispose = puzzle_set_model_filter_dispose;
  filter_class->match = puzzle_set_model_filter_match;
  filter_class->get_strictness = puzzle_set_model_filter_get_strictness;
}

static void
puzzle_set_model_filter_dispose (GObject *object)
{
  PuzzleSetModelFilter *self;

  self = PUZZLE_SET_MODEL_FILTER (object);

  g_clear_signal_handler (&self->hide_solved_puzzles_handler,
                          self->settings);
  g_clear_object (&self->settings);

  G_OBJECT_CLASS (puzzle_set_model_filter_parent_class)->dispose (object);
}


static void
hide_solved_puzzles_changed_cb (PuzzleSetModelFilter *self)
{
  gboolean hide_solved_puzzles;

  hide_solved_puzzles = g_settings_get_boolean (self->settings, "hide-solved-puzzles");

  gtk_filter_changed (GTK_FILTER (self),
                      hide_solved_puzzles?GTK_FILTER_CHANGE_MORE_STRICT:GTK_FILTER_CHANGE_LESS_STRICT);
}

static gboolean
match_solved (PuzzleSetModelRow *row,
              GSettings         *settings)
{
  if (row->puzzle == NULL)
    return FALSE;

  if (! g_settings_get_boolean (settings, "hide-solved-puzzles"))
    return FALSE;

  if (ipuz_crossword_game_won (IPUZ_CROSSWORD (row->puzzle)))
    return TRUE;

  return FALSE;
}

/* This */
static gboolean
puzzle_set_model_filter_match (GtkFilter *filter,
                               gpointer   item)
{
  PuzzleSetModelFilter *self;
  PuzzleSetModelRow *row;

  self = PUZZLE_SET_MODEL_FILTER (filter);
  row = PUZZLE_SET_MODEL_ROW (item);

  if (self->type & PUZZLE_SET_MODEL_FILTER_SOLVED)
    {
      if (match_solved (row, self->settings))
        return FALSE;
    }

  return TRUE;
}

static GtkFilterMatch
puzzle_set_model_filter_get_strictness (GtkFilter *filter)
{
  return GTK_FILTER_MATCH_SOME;
}

/* Public Functions */

GtkFilter *
puzzle_set_model_filter_new (PuzzleSetModelFilterType type)
{
  PuzzleSetModelFilter *filter;

  filter = g_object_new (PUZZLE_TYPE_SET_MODEL_FILTER, NULL);

  filter->type = type;
  filter->settings = g_settings_new ("org.gnome.Crosswords");
  filter->hide_solved_puzzles_handler =
    g_signal_connect_swapped (filter->settings,
                              "changed::hide-solved-puzzles",
                              G_CALLBACK (hide_solved_puzzles_changed_cb),
                              filter);

  return (GtkFilter *) filter;
}
