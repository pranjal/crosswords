/* play-clue-list.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include "crosswords-enums.h"
#include "crosswords-misc.h"
#include "play-clue-list.h"
#include "play-clue-row.h"
#include <libipuz/libipuz.h>

enum
{
  PROP_0,
  PROP_MODE,
  PROP_DIRECTION,
  PROP_START,
  PROP_END,
  N_PROPS
};

enum {
  CLUE_SELECTED,
  CLUE_CELL_SELECTED,
  CLUE_CELL_GUESS,
  CLUE_CELL_GUESS_AT_CELL,
  CLUE_CELL_DO_COMMAND,
  N_SIGNALS
};

static GParamSpec *obj_props[N_PROPS] =  {NULL, };
static guint obj_signals[N_SIGNALS] = { 0 };


struct _PlayClueList
{
  GtkWidget parent_instance;

  PlayClueListMode mode;

  IpuzClueDirection direction;
  gint start;
  gint end;
  guint selected_id;

  ZoomLevel zoom_level;
  GtkWidget *title;
  GtkWidget *swindow;
  GtkWidget *list_box;
  guint n_rows;

  GtkSizeGroup *size_group;
};


static void play_clue_list_init            (PlayClueList      *self);
static void play_clue_list_class_init      (PlayClueListClass *klass);
static void play_clue_list_set_property    (GObject           *object,
                                            guint              prop_id,
                                            const GValue      *value,
                                            GParamSpec        *pspec);
static void play_clue_list_get_property    (GObject           *object,
                                            guint              prop_id,
                                            GValue            *value,
                                            GParamSpec        *pspec);
static void play_clue_list_dispose         (GObject           *object);
static void play_clue_list_row_selected_cb (PlayClueList      *clue_list,
                                            GtkListBoxRow     *row,
                                            GtkListBox        *list_box);
static void clue_cell_selected_cb          (PlayClueRow       *clue_row,
		                            guint              row,
					    guint              column,
					    PlayClueList      *self);
static void clue_cell_guess_cb             (PlayClueRow       *clue_row,
		                            const gchar       *guess,
					    PlayClueList      *self);
static void clue_cell_guess_at_cell_cb     (PlayClueRow       *clue_row,
		                            const gchar       *guess,
					    guint              row,
					    guint              column,
					    PlayClueList      *self);
static void clue_cell_do_command_cb        (PlayClueRow       *clue_row,
		                            GridCmdKind        kind,
					    PlayClueList      *self);



G_DEFINE_TYPE (PlayClueList, play_clue_list, GTK_TYPE_WIDGET);


static void
play_clue_list_init (PlayClueList *self)
{
  GtkLayoutManager *box_layout = gtk_widget_get_layout_manager (GTK_WIDGET (self));

  gtk_orientable_set_orientation (GTK_ORIENTABLE (box_layout), GTK_ORIENTATION_VERTICAL);
  gtk_box_layout_set_spacing (GTK_BOX_LAYOUT (box_layout), 16);

  self->zoom_level = ZOOM_UNSET;
  self->direction = IPUZ_CLUE_DIRECTION_ACROSS;
  self->start = 0;
  self->end = -1;
  self->n_rows = 0;

  self->title = g_object_new (GTK_TYPE_LABEL,
                              "css-name", "header",
                              "xalign", 0.0,
                              NULL);
  gtk_widget_insert_after (self->title, GTK_WIDGET (self), NULL);

  self->swindow = gtk_scrolled_window_new ();
  gtk_widget_set_vexpand (self->swindow, TRUE);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (self->swindow),
                                  GTK_POLICY_NEVER,
                                  GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_has_frame (GTK_SCROLLED_WINDOW (self->swindow), FALSE);

  self->list_box = gtk_list_box_new ();
  gtk_list_box_set_selection_mode (GTK_LIST_BOX (self->list_box), GTK_SELECTION_BROWSE);

  gtk_widget_insert_after (self->swindow, GTK_WIDGET (self), self->title);
  gtk_scrolled_window_set_child (GTK_SCROLLED_WINDOW (self->swindow), self->list_box);

  self->size_group = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);
}

static void
play_clue_list_class_init (PlayClueListClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->set_property = play_clue_list_set_property;
  object_class->get_property = play_clue_list_get_property;
  object_class->dispose = play_clue_list_dispose;

  obj_props[PROP_MODE] = g_param_spec_enum ("mode",
                                            NULL, NULL,
                                            PLAY_TYPE_CLUE_LIST_MODE,
                                            PLAY_CLUE_LIST_CLUES,
                                            G_PARAM_READWRITE);
  obj_props[PROP_DIRECTION] = g_param_spec_enum ("direction",
                                                 "Direction",
                                                 "Direction of the clues",
                                                 IPUZ_TYPE_CLUE_DIRECTION,
                                                 IPUZ_CLUE_DIRECTION_ACROSS,
                                                 G_PARAM_READWRITE);
  obj_props[PROP_START] = g_param_spec_int ("start",
                                             "Start",
                                             "First clue index to include",
                                            -1, 200, -1,
                                             G_PARAM_READWRITE);
  obj_props[PROP_END] = g_param_spec_int ("end",
                                          "End",
                                          "Final clue index include, or -1 to go to the end of the clues",
                                          -1, 200, -1,
                                          G_PARAM_READWRITE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);

  obj_signals [CLUE_SELECTED] =
    g_signal_new ("clue-selected",
                  PLAY_TYPE_CLUE_LIST,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 2,
                  IPUZ_TYPE_CLUE_DIRECTION, /* FIXME(ClueID): Make this a Boxed sometime */
                  G_TYPE_INT);

  obj_signals [CLUE_CELL_SELECTED] =
    g_signal_new ("clue-cell-selected",
                  PLAY_TYPE_CLUE_LIST,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 2,
                  G_TYPE_UINT, G_TYPE_UINT);

  obj_signals [CLUE_CELL_GUESS] =
    g_signal_new ("clue-cell-guess",
                  PLAY_TYPE_CLUE_LIST,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  G_TYPE_STRING);

  obj_signals [CLUE_CELL_GUESS_AT_CELL] =
    g_signal_new ("clue-cell-guess-at-cell",
                  PLAY_TYPE_CLUE_LIST,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 3,
                  G_TYPE_STRING,
		  G_TYPE_UINT, G_TYPE_UINT);

  obj_signals [CLUE_CELL_DO_COMMAND] =
    g_signal_new ("clue-cell-do-command",
                  PLAY_TYPE_CLUE_LIST,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  GRID_TYPE_CMD_KIND);

  gtk_widget_class_set_css_name (GTK_WIDGET_CLASS (klass), "cluelist");
  gtk_widget_class_set_layout_manager_type (GTK_WIDGET_CLASS (klass),
                                            GTK_TYPE_BOX_LAYOUT);
}

static void
play_clue_list_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  PlayClueList *self = PLAY_CLUE_LIST (object);

  switch (prop_id)
    {
    case PROP_MODE:
      self->mode = g_value_get_enum (value);
      if (self->mode == PLAY_CLUE_LIST_CLUES)
        {
          gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (self->swindow),
                                          GTK_POLICY_NEVER,
                                          GTK_POLICY_AUTOMATIC);
        }
      else
        {
          gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (self->swindow),
                                          GTK_POLICY_AUTOMATIC,
                                          GTK_POLICY_AUTOMATIC);
        }
      break;
    case PROP_DIRECTION:
      play_clue_list_set_clue_range (self,
                                     g_value_get_enum (value),
                                     self->start,
                                     self->end);
      break;
    case PROP_START:
      play_clue_list_set_clue_range (self,
                                     self->direction,
                                     g_value_get_int (value),
                                     self->end);
      break;
    case PROP_END:
      play_clue_list_set_clue_range (self,
                                     self->direction,
                                     self->start,
                                     g_value_get_int (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
play_clue_list_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  PlayClueList *self = PLAY_CLUE_LIST (object);

  switch (prop_id)
    {
    case PROP_MODE:
      g_value_set_enum (value, self->mode);
      break;
    case PROP_DIRECTION:
      g_value_set_enum (value, self->direction);
      break;
    case PROP_START:
      g_value_set_int (value, self->start);
      break;
    case PROP_END:
      g_value_set_int (value, self->end);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}


static void
play_clue_list_dispose (GObject *object)
{
  PlayClueList *self;
  GtkWidget *child;

  self = PLAY_CLUE_LIST (object);

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  g_clear_object (&self->size_group);

  G_OBJECT_CLASS (play_clue_list_parent_class)->dispose (object);
}

static void
play_clue_list_row_selected_cb (PlayClueList  *clue_list,
                                GtkListBoxRow *row,
                                GtkListBox    *list_box)
{
  GtkWidget *clue_row;
  IpuzClueId clue_id;

  /* unselections aren't interesting */
  if (row == NULL)
    return;

  clue_row = gtk_list_box_row_get_child (row);
  clue_id = play_clue_row_get_clue_id (PLAY_CLUE_ROW (clue_row));

  g_signal_emit (clue_list,
                 obj_signals[CLUE_SELECTED],
                 0,
                 clue_id.direction, clue_id.index);
}

static void
clue_cell_selected_cb (PlayClueRow  *clue_row,
		       guint         row,
		       guint         column,
		       PlayClueList *self)
{
  g_signal_emit (self,
		  obj_signals[CLUE_CELL_SELECTED],
		  0,
		  row, column);
};

static void
clue_cell_guess_cb (PlayClueRow  *clue_row,
		    const gchar  *guess,
		    PlayClueList *self)
{
  g_signal_emit (self,
		  obj_signals[CLUE_CELL_GUESS],
		  0,
		  guess);
};

static void
clue_cell_guess_at_cell_cb (PlayClueRow  *clue_row,
		            const gchar  *guess,
			    guint         row,
			    guint         column,
			    PlayClueList *self)
{
  g_signal_emit (self,
		 obj_signals[CLUE_CELL_GUESS_AT_CELL],
		 0,
		 guess,
		 row, column);
};

static void
clue_cell_do_command_cb (PlayClueRow  *clue_row,
		         GridCmdKind   kind,
			 PlayClueList *self)
{
  g_signal_emit (self,
		 obj_signals[CLUE_CELL_DO_COMMAND],
		 0,
		 kind);
};

/* Public metthods */

GtkWidget *
play_clue_list_new (void)
{
  return (GtkWidget *) g_object_new (PLAY_TYPE_CLUE_LIST, NULL);
}


void
play_clue_list_set_header_size_group (PlayClueList      *clue_list,
                                      GtkSizeGroup      *size_group)
{
  g_return_if_fail (PLAY_IS_CLUE_LIST (clue_list));
  g_return_if_fail (GTK_IS_SIZE_GROUP (size_group));

  gtk_size_group_add_widget (size_group, clue_list->title);
}

/**
 * play_clue_list_set_clue_range:
 * @clue_list: A `PlayClueList`
 * @direction: The direction of clues to display
 * @start: The index of the first clue
 * @end: The index of the last clue, or -1
 *
 * Sets the range of clues to display. Note that this range is the
 * index of the list of clues, and does not reference the clues'
 * number or label. if @end is -1, then @clue_list display all the
 * clues in @direction.
 **/
void
play_clue_list_set_clue_range (PlayClueList      *clue_list,
                               IpuzClueDirection  direction,
                               gint               start,
                               gint               end)
{
  g_return_if_fail (PLAY_IS_CLUE_LIST (clue_list));
  g_return_if_fail (start >= 0);
  g_return_if_fail (end >= -1);
  if (end != -1)
    g_return_if_fail (start < end);

  /* FIXME(reflow): update the list, instead of just updating these
   * values */
  clue_list->direction = direction;
  clue_list->start = start;
  clue_list->end = end;
}

void
play_clue_list_update (PlayClueList *clue_list,
                       GridState    *state,
                       ZoomLevel     zoom_level)
{
  GArray *clue_array;
  gboolean showenumerations;
  guint start, end;
  gboolean changed;

  g_return_if_fail (PLAY_IS_CLUE_LIST (clue_list));

  /* We hide ourself if we don't have any clues to show. Maybe a
   * little goofy */
  clue_array = ipuz_crossword_get_clues (state->xword, clue_list->direction);
  if (clue_array == NULL ||
      clue_array->len == 0)
    {
      gtk_widget_set_visible (GTK_WIDGET (clue_list), FALSE);
      return;
    }
  else
    gtk_widget_set_visible (GTK_WIDGET (clue_list), TRUE);

  /* NOTE: This queue resize is to fix a bug in GTK. If we change the
   * CSS of a widget, it appears to not invalidate the size cache of
   * widgets and queueing its own resize. We have to manually queue a
   * resize, otherwise we won't appear even if there's room on a zoom
   * change.
   *
   * It's pretty confusing. Basically, don't remove this queue_resize
   * unless GTK fixes it.
   */
  if (clue_list->zoom_level != zoom_level)
    {
      gtk_widget_queue_resize (GTK_WIDGET (clue_list));
      clue_list->zoom_level = zoom_level;
    }

  /* Set up the title label */
  if (clue_list->start == 0)
    {
      const gchar *dirstr = NULL;

      dirstr = ipuz_crossword_clue_set_get_label (state->xword, clue_list->direction);
      set_zoom_level_css_class (clue_list->title, NULL, zoom_level);
      gtk_label_set_label (GTK_LABEL (clue_list->title), dirstr);
      gtk_widget_set_visible (clue_list->title, TRUE);
    }
  else
    {
      gtk_widget_set_visible (clue_list->title, FALSE);
    }

  /* Set up the clues. */
  start = (guint) clue_list->start;
  if (clue_list->end == -1)
    end = clue_array->len;
  else
    end = clue_list->end;

  g_object_get (G_OBJECT (state->xword),
                "showenumerations", &showenumerations,
                NULL);

  g_return_if_fail (end <= clue_array->len);
  g_return_if_fail (end > start);

  /* If we don't have enough items in our list, we add them.
   * If we have too many, we remove them. */
  /* Note, weirdly, GtkListBox doesn't have a way to get the number of
   * rows in it. We store that in n_rows */

  /* This is a heuristic to scroll to the start if the number of rows
   * changes. It's really not perfect: we should really compare the
   * labels or something, but it will work most of the time */
  changed = FALSE;
  if ((end - start) > clue_list->n_rows)
    {
      for (guint i = 0; i < (end - start)-clue_list->n_rows; i++)
        {
          GtkWidget *clue_row;
          GtkWidget *list_row;

          clue_row = play_clue_row_new (clue_list->size_group);
	  g_signal_connect (G_OBJECT (clue_row), "clue-cell-selected", G_CALLBACK (clue_cell_selected_cb), clue_list);
	  g_signal_connect (G_OBJECT (clue_row), "clue-cell-guess", G_CALLBACK (clue_cell_guess_cb), clue_list);
	  g_signal_connect (G_OBJECT (clue_row), "clue-cell-guess-at-cell", G_CALLBACK (clue_cell_guess_at_cell_cb), clue_list);
	  g_signal_connect (G_OBJECT (clue_row), "clue-cell-do-command", G_CALLBACK (clue_cell_do_command_cb), clue_list);

          list_row = gtk_list_box_row_new ();
          gtk_list_box_row_set_child (GTK_LIST_BOX_ROW (list_row), clue_row);

          /* This is so that the keyboard focus stays with the grid
           * when the user selects a clue.*/
          gtk_widget_set_focus_on_click (list_row, FALSE);

          gtk_list_box_append (GTK_LIST_BOX (clue_list->list_box), list_row);
        }
      changed = TRUE;
    }
  else if ((end - start < clue_list->n_rows))
    {
      for (guint i = 0; i < clue_list->n_rows - (end - start); i++)
        {
          GtkListBoxRow *list_row;

          list_row = gtk_list_box_get_row_at_index (GTK_LIST_BOX (clue_list->list_box), 0);
          gtk_list_box_remove (GTK_LIST_BOX (clue_list->list_box), GTK_WIDGET (list_row));
        }
      changed = TRUE;
    }
  clue_list->n_rows = end - start;

  for (guint i = start; i < end; i++)
    {
      GtkListBoxRow *list_row;
      GtkWidget *clue_row;
      IpuzClue *clue;
      IpuzClueId clue_id;

      clue = g_array_index (clue_array, IpuzClue *, i);
      clue_id.direction = clue_list->direction;
      clue_id.index = i;

      list_row = gtk_list_box_get_row_at_index (GTK_LIST_BOX (clue_list->list_box), i);
      clue_row = gtk_list_box_row_get_child (list_row);

      /* Unhighlight the row if was highlighted */
      gtk_widget_remove_css_class (GTK_WIDGET (list_row), "highlighted");

      if (clue_list->mode == PLAY_CLUE_LIST_EDIT)
        play_clue_row_set_mode (PLAY_CLUE_ROW (clue_row), PLAY_CLUE_ROW_EDIT);
      else if (IPUZ_IS_ACROSTIC (state->xword))
        play_clue_row_set_mode (PLAY_CLUE_ROW (clue_row), PLAY_CLUE_ROW_PLAY_ACROSTIC);
      else
        play_clue_row_set_mode (PLAY_CLUE_ROW (clue_row), PLAY_CLUE_ROW_PLAY);
      play_clue_row_update (PLAY_CLUE_ROW (clue_row), state,
                            clue, clue_id,
                            showenumerations, zoom_level);
    }

  /* We check to see if there's a cursor. If not, clear the selection
   * and block the handler */
  if (! GRID_STATE_CURSOR_SET (state))
    {
      if (clue_list->selected_id)
        {
          g_signal_handler_disconnect (clue_list->list_box, clue_list->selected_id);
          clue_list->selected_id = 0;
        }
      gtk_list_box_set_selection_mode (GTK_LIST_BOX (clue_list->list_box), GTK_SELECTION_NONE);
      /* We don't need to scroll below */
      return;
    }
  else if (clue_list->selected_id == 0)
    {
      clue_list->selected_id = g_signal_connect_swapped (clue_list->list_box, "row-selected",
                                                         G_CALLBACK (play_clue_list_row_selected_cb),
                                                         clue_list);
    }

  /* Avoid triggering the handler while we play with the selection */
  g_signal_handler_block (clue_list->list_box, clue_list->selected_id);

  /* Force clear the selection. */
  /* This is something that might break in the future?  We always want
   * only one row selected across all the PlayClueLists, which is
   * basically a global BROWSE mode. But since the list is spread out
   * across a number of lists boxes, we can have it empty in practice.
   */
  gtk_list_box_set_selection_mode (GTK_LIST_BOX (clue_list->list_box), GTK_SELECTION_NONE);
  gtk_list_box_set_selection_mode (GTK_LIST_BOX (clue_list->list_box), GTK_SELECTION_BROWSE);

  if (state->clue.direction == clue_list->direction &&
      state->clue.index >= (guint) clue_list->start &&
      state->clue.index < (guint) clue_list->end)
    {
      GtkListBoxRow *row;

      row = gtk_list_box_get_row_at_index (GTK_LIST_BOX (clue_list->list_box),
                                           state->clue.index - clue_list->start);

      if (!ACROSTIC_MAIN_GRID_HAS_FOCUS (state))
        gtk_list_box_select_row (GTK_LIST_BOX (clue_list->list_box), row);

      /* Scroll to the selected row */
      if (changed)
        {
          GtkAdjustment *adj;

          /* We can't scroll to the selected widget, as we haven't
           * recalculated positions yet. We scroll to the top of the list,
           * under the assumption that it's probably right. */
          adj = gtk_list_box_get_adjustment (GTK_LIST_BOX (clue_list->list_box));
          gtk_adjustment_clamp_page (adj, 0.0, 0.0);
        }
      else
        {
          GtkAdjustment *adj;
          graphene_point_t from_point = {
            .x = 0.0,
            .y = 0.0
          };
          graphene_point_t to_point = {
            .x = 0.0,
            .y = 0.0
          };

          /* Scroll to the row. This is waaay harder than it should be */
          if (gtk_widget_compute_point (GTK_WIDGET (row), clue_list->list_box, &from_point, &to_point))
            {
              adj = gtk_list_box_get_adjustment (GTK_LIST_BOX (clue_list->list_box));
              if (adj)
                {
                  graphene_rect_t bounds;

                  if (gtk_widget_compute_bounds (GTK_WIDGET (row), clue_list->list_box, &bounds))
                    gtk_adjustment_clamp_page (adj, to_point.y - bounds.size.height, to_point.y + bounds.size.height);
                }
            }
        }
    }

  /* Semi-highlight the toggle row */
  if (state->clue.direction != clue_list->direction)
    {
      GtkListBoxRow *row;
      const IpuzClue *clue = NULL;
      IpuzClueId clue_id;
      IpuzCell *cursor_cell;

      cursor_cell = ipuz_board_get_cell (ipuz_crossword_get_board (state->xword),
                                         &state->cursor);

      clue = ipuz_cell_get_clue (cursor_cell,
                                 ipuz_clue_direction_switch (state->clue.direction));
      if (clue)
        {
          ipuz_crossword_get_id_by_clue (state->xword, clue, &clue_id);
          row = gtk_list_box_get_row_at_index (GTK_LIST_BOX (clue_list->list_box),
                                               clue_id.index - clue_list->start);

          gtk_widget_add_css_class (GTK_WIDGET (row), "highlighted");

          /* Scroll up/down to the row */
          GtkAdjustment *adj;
          graphene_point_t from_point = {
            .x = 0.0,
            .y = 0.0
          };
          graphene_point_t to_point = {
            .x = 0.0,
            .y = 0.0
          };

          if (gtk_widget_compute_point (GTK_WIDGET (row), clue_list->list_box, &from_point, &to_point))
            {
              adj = gtk_list_box_get_adjustment (GTK_LIST_BOX (clue_list->list_box));
              if (adj)
                {
                  graphene_rect_t bounds;

                  if (gtk_widget_compute_bounds (GTK_WIDGET (row), clue_list->list_box, &bounds))
                    gtk_adjustment_clamp_page (adj, to_point.y - bounds.size.height, to_point.y + bounds.size.height);
                }
            }
        }
    }

  g_signal_handler_unblock (clue_list->list_box, clue_list->selected_id);
}

GtkWidget *
play_clue_list_get_selected_grid (PlayClueList *clue_list)
{
  GtkWidget *clue_row;
  GtkListBoxRow *list_row;

  list_row = gtk_list_box_get_selected_row(GTK_LIST_BOX (clue_list->list_box));
  clue_row = gtk_list_box_row_get_child (list_row);

  return play_clue_row_get_selected_grid (PLAY_CLUE_ROW (clue_row));
};
