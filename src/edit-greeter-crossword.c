/* edit-greeter_crossword-crossword.c
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include "basic-templates.h"
#include "crosswords-misc.h"
#include "edit-greeter-crossword.h"
#include "edit-greeter-details.h"
#include "play-grid.h"


/* This is used to increment the "My Crossword-%u" title string */
static guint crossword_instance_count = 0;
static guint cryptic_instance_count = 0;
static guint barred_instance_count = 0;


enum {
  PROP_0,
  PROP_VALID,
  PROP_SPACING,
  PROP_PUZZLE_KIND,
  PROP_ORIENTATION,
  N_PROPS = PROP_ORIENTATION
};

static GParamSpec *obj_props[N_PROPS] = { NULL, };


static void        edit_greeter_crossword_init         (EditGreeterCrossword        *self);
static void        edit_greeter_details_interface_init (EditGreeterDetailsInterface *iface);
static void        edit_greeter_crossword_class_init   (EditGreeterCrosswordClass   *klass);
static void        edit_greeter_crossword_set_property (GObject                     *object,
                                                        guint                        prop_id,
                                                        const GValue                *value,
                                                        GParamSpec                  *pspec);
static void        edit_greeter_crossword_get_property (GObject                     *object,
                                                        guint                        prop_id,
                                                        GValue                      *value,
                                                        GParamSpec                  *pspec);
static void        edit_greeter_crossword_dispose      (GObject                     *object);
static IpuzPuzzle *edit_greeter_crossword_get_puzzle   (EditGreeterDetails          *details);
static void        set_puzzle_kind                     (EditGreeterCrossword        *self,
                                                        IpuzPuzzleKind               puzzle_kind);
static void        size_dialog_response_cb             (EditGreeterCrossword        *self,
                                                        gint                         response_id,
                                                        GtkDialog                   *dialog);
static void        update_presets                      (EditGreeterCrossword        *self);
static void        size_selected_cb                    (EditGreeterCrossword        *self,
                                                        GParamSpec                  *pspec,
                                                        AdwComboRow                 *action_row);


struct _EditGreeterCrossword
{
  GtkWidget parent_instance;

  gboolean valid;
  IpuzPuzzleKind puzzle_kind;
  BasicSize size;
  guint custom_width;
  guint custom_height;

  GtkWidget *preferences_group;
  GtkWidget *template_grid_view;

  /* Dialog */
  GtkWidget *size_dialog;
  GtkWidget *size_row;
  GtkWidget *title_row;
  GtkWidget *size_width_spin;
  GtkWidget *size_height_spin;
};


G_DEFINE_TYPE_WITH_CODE (EditGreeterCrossword, edit_greeter_crossword, GTK_TYPE_WIDGET,
                         G_IMPLEMENT_INTERFACE (GTK_TYPE_ORIENTABLE, NULL)
                         G_IMPLEMENT_INTERFACE (EDIT_TYPE_GREETER_DETAILS, edit_greeter_details_interface_init));


static void
setup_listitem_cb (GtkListItemFactory *factory,
                   GtkListItem        *list_item)
{
  GtkWidget *image;

  image = gtk_image_new ();
  gtk_widget_set_hexpand (image, FALSE);
  gtk_widget_set_size_request (image, 120, 120);

  gtk_list_item_set_child (list_item, image);
}

static void
bind_listitem_cb (GtkListItemFactory *factory,
                  GtkListItem        *list_item)
{
  GtkWidget *image;
  BasicTemplateRow *row;
  g_autoptr (GdkTexture) texture = NULL;

  image = gtk_list_item_get_child (list_item);
  row = (BasicTemplateRow *) gtk_list_item_get_item (list_item);
  texture = gdk_texture_new_for_pixbuf (basic_template_row_get_pixbuf (row));
  gtk_image_set_from_paintable (GTK_IMAGE (image),
                                GDK_PAINTABLE (texture));
}

static void
edit_greeter_crossword_init (EditGreeterCrossword *self)
{
  g_autoptr (GtkListItemFactory) template_factory = NULL;

  gtk_widget_init_template (GTK_WIDGET (self));

  /* We're valid at the start */
  self->valid = TRUE;
  self->puzzle_kind = IPUZ_PUZZLE_UNKNOWN;
  self->size = BASIC_SIZE_LARGE;
  adw_combo_row_set_selected (ADW_COMBO_ROW (self->size_row), BASIC_SIZE_LARGE);

  /* Set up the template grid view */
  template_factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (template_factory, "setup", G_CALLBACK (setup_listitem_cb), NULL);
  g_signal_connect (template_factory, "bind", G_CALLBACK (bind_listitem_cb), NULL);
  gtk_grid_view_set_factory (GTK_GRID_VIEW (self->template_grid_view),
                             template_factory);
}

static void
edit_greeter_crossword_class_init (EditGreeterCrosswordClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->set_property = edit_greeter_crossword_set_property;
  object_class->get_property = edit_greeter_crossword_get_property;
  object_class->dispose = edit_greeter_crossword_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-greeter-crossword.ui");

  gtk_widget_class_bind_template_child (widget_class, EditGreeterCrossword, preferences_group);
  gtk_widget_class_bind_template_child (widget_class, EditGreeterCrossword, template_grid_view);
  gtk_widget_class_bind_template_child (widget_class, EditGreeterCrossword, size_row);
  gtk_widget_class_bind_template_child (widget_class, EditGreeterCrossword, title_row);
  gtk_widget_class_bind_template_child (widget_class, EditGreeterCrossword, size_dialog);
  gtk_widget_class_bind_template_child (widget_class, EditGreeterCrossword, size_width_spin);
  gtk_widget_class_bind_template_child (widget_class, EditGreeterCrossword, size_height_spin);

  gtk_widget_class_bind_template_callback (widget_class, size_dialog_response_cb);
  gtk_widget_class_bind_template_callback (widget_class, size_selected_cb);
  gtk_widget_class_bind_template_callback (widget_class, edit_greeter_details_activated);

  g_object_class_override_property (object_class,
                                    PROP_ORIENTATION,
                                    "orientation");
  obj_props[PROP_VALID] =
    g_param_spec_boolean ("valid", NULL, NULL,
                          FALSE,
                          G_PARAM_READWRITE|G_PARAM_EXPLICIT_NOTIFY);
  obj_props[PROP_SPACING] =
    g_param_spec_uint ("spacing", NULL, NULL,
                       0, G_MAXUINT, 0,
                       G_PARAM_READWRITE|G_PARAM_EXPLICIT_NOTIFY);
  obj_props[PROP_PUZZLE_KIND] =
    g_param_spec_enum ("puzzle-kind", NULL, NULL,
                       IPUZ_TYPE_PUZZLE_KIND,
                       IPUZ_PUZZLE_CROSSWORD,
                       G_PARAM_READWRITE|G_PARAM_CONSTRUCT_ONLY);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BOX_LAYOUT);
  gtk_widget_class_set_accessible_role (widget_class, GTK_ACCESSIBLE_ROLE_GROUP);
}

static void
edit_greeter_details_interface_init (EditGreeterDetailsInterface *iface)
{
  iface->get_puzzle = edit_greeter_crossword_get_puzzle;
}

static void
edit_greeter_crossword_set_property (GObject      *object,
                                     guint         prop_id,
                                     const GValue *value,
                                     GParamSpec   *pspec)
{
  EditGreeterCrossword *self;
  GtkBoxLayout *box_layout;
  guint spacing;
  IpuzPuzzleKind kind;

  self = EDIT_GREETER_CROSSWORD (object);
  box_layout = GTK_BOX_LAYOUT (gtk_widget_get_layout_manager (GTK_WIDGET (object)));

  switch (prop_id)
    {
    case PROP_VALID:
      {
      gboolean valid = !!g_value_get_boolean (value);
      if (self->valid != valid)
        {
          self->valid = valid;
          g_object_notify_by_pspec (object, pspec);
        }
      }
      break;
    case PROP_ORIENTATION:
      {
        GtkOrientation orientation = g_value_get_enum (value);
        if (gtk_orientable_get_orientation (GTK_ORIENTABLE (box_layout)) != orientation)
          {
            gtk_orientable_set_orientation (GTK_ORIENTABLE (box_layout), orientation);
            g_object_notify_by_pspec (object, pspec);
          }
      }
      break;
    case PROP_SPACING:
      spacing = g_value_get_uint (value);
      if (spacing != gtk_box_layout_get_spacing (box_layout))
        {
          gtk_box_layout_set_spacing (box_layout, spacing);
          g_object_notify_by_pspec (object, pspec);
        }
      break;
    case PROP_PUZZLE_KIND:
      kind = g_value_get_enum (value);
      if (kind != IPUZ_PUZZLE_CROSSWORD &&
          kind != IPUZ_PUZZLE_CRYPTIC &&
          kind != IPUZ_PUZZLE_BARRED)
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      else
        set_puzzle_kind (self, kind);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
edit_greeter_crossword_get_property (GObject    *object,
                                     guint       prop_id,
                                     GValue     *value,
                                     GParamSpec *pspec)
{
  EditGreeterCrossword *self = EDIT_GREETER_CROSSWORD (object);
  GtkBoxLayout *box_layout = GTK_BOX_LAYOUT (gtk_widget_get_layout_manager (GTK_WIDGET (self)));

  switch (prop_id)
    {
    case PROP_VALID:
      g_value_set_boolean (value, self->valid);
      break;
    case PROP_ORIENTATION:
      g_value_set_enum (value, gtk_orientable_get_orientation (GTK_ORIENTABLE (box_layout)));
      break;
    case PROP_SPACING:
      g_value_set_uint (value, gtk_box_layout_get_spacing (box_layout));
      break;
    case PROP_PUZZLE_KIND:
      g_value_set_enum (value, self->puzzle_kind);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
edit_greeter_crossword_dispose (GObject *object)
{
  GtkWidget *child;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  G_OBJECT_CLASS (edit_greeter_crossword_parent_class)->dispose (object);
}

static IpuzPuzzle *
edit_greeter_crossword_get_puzzle (EditGreeterDetails *details)
{
  EditGreeterCrossword *self;
  GtkSelectionModel *model;
  BasicTemplateRow *row;
  IpuzPuzzle *template;
  IpuzPuzzle *puzzle = NULL;

  self = EDIT_GREETER_CROSSWORD (details);

  model = gtk_grid_view_get_model (GTK_GRID_VIEW (self->template_grid_view));
  row = gtk_single_selection_get_selected_item (GTK_SINGLE_SELECTION (model));
  template = basic_template_row_get_template (row);

  if (template)
    {
      puzzle = ipuz_puzzle_deep_copy (template);
    }

  if (puzzle)
    {
      const gchar *title_str = NULL;

      title_str = gtk_editable_get_text (GTK_EDITABLE (self->title_row));
      g_object_set (puzzle,
                    "showenumerations", (self->puzzle_kind == IPUZ_PUZZLE_CRYPTIC),
                    "title", title_str,
                    NULL);
    }

  /* Make sure we have clues on the new puzzle */
  ipuz_crossword_fix_all (IPUZ_CROSSWORD (puzzle), NULL);

  if (self->puzzle_kind == IPUZ_PUZZLE_BARRED)
    barred_instance_count ++;
  else if (self->puzzle_kind == IPUZ_PUZZLE_CROSSWORD)
    crossword_instance_count ++;
  else if (self->puzzle_kind == IPUZ_PUZZLE_CRYPTIC)
    cryptic_instance_count ++;

  return puzzle;
}

static void
set_puzzle_kind (EditGreeterCrossword *self,
                 IpuzPuzzleKind        puzzle_kind)
{
  const gchar *pref_title;
  g_autofree gchar *title_text = NULL;

  g_assert (EDIT_IS_GREETER_CROSSWORD (self));

  if (self->puzzle_kind == puzzle_kind)
    return;

  self->puzzle_kind = puzzle_kind;

  switch (puzzle_kind)
    {
    case IPUZ_PUZZLE_CROSSWORD:
      pref_title = _("Standard Crossword");
      if (crossword_instance_count == 0)
        /* translator: this is the default title of crosswords */
        title_text = g_strdup (_("My Crossword"));
      else
        title_text = g_strdup_printf (_("My Crossword-%u"), crossword_instance_count);
      break;
    case IPUZ_PUZZLE_CRYPTIC:
      pref_title = _("Cryptic Crossword");
      if (cryptic_instance_count == 0)
        /* translator: this is the default title of cryptic crosswords */
        title_text = g_strdup (_("My Cryptic Crossword"));
      else
        title_text = g_strdup_printf (_("My Cryptic Crossword-%u"), cryptic_instance_count);
      break;
    case IPUZ_PUZZLE_BARRED:
      pref_title = _("Barred Crossword");
      if (barred_instance_count == 0)
        /* translator: this is the default title of barred crosswords */
        title_text = g_strdup (_("My Barred Crossword"));
      else
        title_text = g_strdup_printf (_("My Barred Crossword-%u"), barred_instance_count);
      break;
    default:
      g_assert_not_reached ();
    }

  adw_preferences_group_set_title (ADW_PREFERENCES_GROUP (self->preferences_group), pref_title);
  gtk_editable_set_text (GTK_EDITABLE (self->title_row), title_text);
  update_presets (self);
}

static void
size_dialog_response_cb (EditGreeterCrossword *self,
                         gint                  response_id,
                         GtkDialog            *dialog)
{
  if (response_id == GTK_RESPONSE_APPLY)
    {
      guint new_width, new_height;

      new_width = (guint) gtk_spin_button_get_value (GTK_SPIN_BUTTON (self->size_width_spin));
      new_height = (guint) gtk_spin_button_get_value (GTK_SPIN_BUTTON (self->size_height_spin));

      if (new_width == SMALL_SIZE &&
          new_height == SMALL_SIZE)
        {
          self->size = BASIC_SIZE_SMALL;
        }
      else if (new_width == MEDIUM_SIZE &&
               new_height == MEDIUM_SIZE)
        {
          self->size = BASIC_SIZE_MEDIUM;
        }
      else if (new_width == LARGE_SIZE &&
               new_height == LARGE_SIZE)
        {
          self->size = BASIC_SIZE_LARGE;

        }
      else if (new_width == X_LARGE_SIZE &&
               new_height == X_LARGE_SIZE)
        {
          self->size = BASIC_SIZE_X_LARGE;
        }
      else
        {
          self->size = BASIC_SIZE_CUSTOM;
          self->custom_width = new_width;
          self->custom_height = new_height;
        }

      update_presets (self);
    }
  gtk_widget_set_visible (GTK_WIDGET (dialog), FALSE);
}

static void
update_presets (EditGreeterCrossword *self)
{
  GListModel *model;
  g_autoptr (GtkSelectionModel) selection_model = NULL;

  if (self->size == BASIC_SIZE_CUSTOM)
    model = basic_template_create_custom_model (self->puzzle_kind,
                                                self->custom_width,
                                                self->custom_height);
  else
    model = basic_template_get_model (self->puzzle_kind, self->size);

  /* The g_object_ref() here is needed because
   * gtk_single_selection_new() takes ownership of the model, and will
   * free it when cleared. */
  selection_model = (GtkSelectionModel *) gtk_single_selection_new (g_object_ref (model));
  gtk_grid_view_set_model (GTK_GRID_VIEW (self->template_grid_view),
                           selection_model);
  gtk_selection_model_select_item (selection_model,
                                   0, TRUE);
}

static void
size_selected_cb  (EditGreeterCrossword *self,
                   GParamSpec           *pspec,
                   AdwComboRow          *action_row)
{
  BasicSize new_size = adw_combo_row_get_selected (action_row);
  if (new_size == BASIC_SIZE_CUSTOM)
    {
      GtkWindow *window;

      window = (GtkWindow *) gtk_widget_get_root (GTK_WIDGET (self));
      gtk_window_set_transient_for (GTK_WINDOW (self->size_dialog),
                                    window);

      gtk_widget_set_visible (self->size_dialog, TRUE);
      return;
    }
  if (self->size != new_size)
    {
      self->size = new_size;
      update_presets (self);
    }
}
