/* crosswords-quirks.h
 *
 * Copyright 2022 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include <libipuz/libipuz.h>


G_BEGIN_DECLS


typedef enum
{
  QUIRKS_GUESS_ADVANCE_ADJACENT = 0,     /* @ Advance to the next cell @ */
  QUIRKS_GUESS_ADVANCE_OPEN = 1,         /* @ Advance to the next open cell @ */
  QUIRKS_GUESS_ADVANCE_OPEN_IN_CLUE = 2, /* @ Advance to the next open cell within a clue @ */
} QuirksGuessAdvance;

typedef enum
{
  QUIRKS_FOCUS_LOCATION_NONE,            /* @ Non-Acrostic and nothing is focused @ */
  QUIRKS_FOCUS_LOCATION_MAIN_GRID,       /* @ Acrostic and main grid is focused @ */
  QUIRKS_FOCUS_LOCATION_CLUE_GRID,       /* @ Acrostic and clue grid is focused @ */
} QuirksFocusLocation;


#define CROSSWORDS_TYPE_QUIRKS (crosswords_quirks_get_type())
G_DECLARE_FINAL_TYPE (CrosswordsQuirks, crosswords_quirks, CROSSWORDS, QUIRKS, GObject);


CrosswordsQuirks    *crosswords_quirks_new                     (IpuzPuzzle          *puzzle);
gboolean             crosswords_quirks_get_ij_digraph          (CrosswordsQuirks    *quirks);
QuirksGuessAdvance   crosswords_quirks_get_guess_advance       (CrosswordsQuirks    *quirks);
gboolean             crosswords_quirks_get_switch_on_move      (CrosswordsQuirks    *quirks);
IpuzSymmetry         crosswords_quirks_get_symmetry            (CrosswordsQuirks    *quirks);
void                 crosswords_quirks_set_symmetry            (CrosswordsQuirks    *quirks,
                                                                IpuzSymmetry         symmetry);
QuirksFocusLocation  crosswords_quirks_get_focus_location      (CrosswordsQuirks    *quirks);
void                 crosswords_quirks_set_focus_location      (CrosswordsQuirks    *quirks,
                                                                QuirksFocusLocation  location);

/* For testing purposes */
CrosswordsQuirks    *crosswords_quirks_test_new                (IpuzPuzzle          *puzzle);
void                 crosswords_quirks_test_set_ij_digraph     (CrosswordsQuirks    *quirks,
                                                                gboolean             ij_digraph);
void                 crosswords_quirks_test_set_guess_advance  (CrosswordsQuirks    *quirks,
                                                                QuirksGuessAdvance   guess_advance);
void                 crosswords_quirks_test_set_switch_on_move (CrosswordsQuirks    *quirks,
                                                                gboolean             switch_on_move);



G_END_DECLS
