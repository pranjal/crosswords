/* edit-entry-row.c
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <libipuz/libipuz.h>
#include <glib/gi18n-lib.h>
#include "crosswords-enums.h"
#include "edit-entry-row.h"


enum
{
  PROP_0,
  PROP_VALIDATION_MODE,
  N_PROPS,
};

enum
{
  COMMITTED,
  N_SIGNALS
};

static GParamSpec *obj_props[N_PROPS] =  {NULL, };
static guint obj_signals[N_SIGNALS] = { 0 };


struct _EditEntryRow
{
  AdwEntryRow parent_instance;

  EditEntryRowValidationMode mode;
  gchar *saved_text;

  gboolean message_set;
  gchar *saved_title;
};


G_DEFINE_FINAL_TYPE (EditEntryRow, edit_entry_row, ADW_TYPE_ENTRY_ROW);


static void edit_entry_row_init         (EditEntryRow      *self);
static void edit_entry_row_class_init   (EditEntryRowClass *klass);
static void edit_entry_row_set_property (GObject           *object,
                                         guint              prop_id,
                                         const GValue      *value,
                                         GParamSpec        *pspec);
static void edit_entry_row_get_property (GObject           *object,
                                         guint              prop_id,
                                         GValue            *value,
                                         GParamSpec        *pspec);
static void edit_entry_row_dispose      (GObject           *self);
static void edit_entry_row_changed_cb   (EditEntryRow      *self);


static void
edit_entry_row_class_init (EditEntryRowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->set_property = edit_entry_row_set_property;
  object_class->get_property = edit_entry_row_get_property;
  object_class->dispose = edit_entry_row_dispose;

  obj_props [PROP_VALIDATION_MODE] = g_param_spec_enum ("validation-mode",
                                                        NULL, NULL,
                                                        EDIT_TYPE_ENTRY_ROW_VALIDATION_MODE,
                                                        EDIT_ENTRY_ROW_HTML,
                                                        G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);

  obj_signals [COMMITTED] =
    g_signal_new ("committed",
                  EDIT_TYPE_ENTRY_ROW,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_BOOLEAN, 0);
}

static void
edit_entry_row_init (EditEntryRow *self)
{
  self->saved_text = g_strdup ("");
  adw_entry_row_set_show_apply_button (ADW_ENTRY_ROW (self), TRUE);
  gtk_editable_set_enable_undo (gtk_editable_get_delegate (GTK_EDITABLE (self)), FALSE);
  g_signal_connect (self, "apply", G_CALLBACK (edit_entry_row_commit), NULL);
  g_signal_connect (self, "entry-activated", G_CALLBACK (edit_entry_row_commit), NULL);
  g_signal_connect (self, "move-focus", G_CALLBACK (edit_entry_row_commit), NULL);
  g_signal_connect (self, "changed", G_CALLBACK (edit_entry_row_changed_cb), NULL);
}

static void
edit_entry_row_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  EditEntryRow *self = EDIT_ENTRY_ROW (object);

  switch (prop_id)
    {
    case PROP_VALIDATION_MODE:
      edit_entry_row_set_validation_mode (self, g_value_get_enum (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
edit_entry_row_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  EditEntryRow *self = EDIT_ENTRY_ROW (object);

  switch (prop_id)
    {
    case PROP_VALIDATION_MODE:
      g_value_set_enum (value, self->mode);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
edit_entry_row_dispose (GObject *object)
{
  EditEntryRow *self;

  self = EDIT_ENTRY_ROW (object);

  g_clear_pointer (&self->saved_text, g_free);
  g_clear_pointer (&self->saved_title, g_free);

  G_OBJECT_CLASS (edit_entry_row_parent_class)->dispose (object);
}

static void
edit_entry_row_changed_cb (EditEntryRow *self)
{
  if (! self->message_set)
    return;

  adw_preferences_row_set_title (ADW_PREFERENCES_ROW (self), self->saved_title);
  gtk_widget_remove_css_class (GTK_WIDGET (self), "warning");
  gtk_widget_remove_css_class (GTK_WIDGET (self), "error");

  g_clear_pointer (&self->saved_title, g_free);
  self->message_set = FALSE;
}

/* Public functions */

void
edit_entry_row_set_text (EditEntryRow *self,
                         const gchar  *text)
{
  g_return_if_fail (EDIT_IS_ENTRY_ROW (self));

  if (text == NULL)
    text = "";

  if (! g_strcmp0 (text, self->saved_text))
    return;

  g_free (self->saved_text);
  self->saved_text = g_strdup (text);
  gtk_editable_set_text (GTK_EDITABLE (self), text);

  /* Why do we need this? */
  adw_entry_row_set_show_apply_button (ADW_ENTRY_ROW (self), FALSE);
  adw_entry_row_set_show_apply_button (ADW_ENTRY_ROW (self), TRUE);
}

void
edit_entry_row_set_warning (EditEntryRow *self,
                            const gchar  *message)
{
  g_return_if_fail (EDIT_IS_ENTRY_ROW (self));

  if (self->message_set)
    return;

  g_free (self->saved_title);
  self->saved_title =
    g_strdup (adw_preferences_row_get_title (ADW_PREFERENCES_ROW (self)));

  gtk_widget_remove_css_class (GTK_WIDGET (self), "error");
  gtk_widget_add_css_class (GTK_WIDGET (self), "warning");
  adw_preferences_row_set_title (ADW_PREFERENCES_ROW (self),
                                 message);
  self->message_set = TRUE;
}

void
edit_entry_row_set_error (EditEntryRow *self,
                          const gchar  *message)
{
  g_return_if_fail (EDIT_IS_ENTRY_ROW (self));

  if (self->message_set)
    return;

  g_free (self->saved_title);
  self->saved_title =
    g_strdup (adw_preferences_row_get_title (ADW_PREFERENCES_ROW (self)));

  gtk_widget_remove_css_class (GTK_WIDGET (self), "warning");
  gtk_widget_add_css_class (GTK_WIDGET (self), "error");
  adw_preferences_row_set_title (ADW_PREFERENCES_ROW (self),
                                 message);
  self->message_set = TRUE;
}

void
edit_entry_row_commit (EditEntryRow *self)
{
  const gchar *text;
  gboolean handled = FALSE;

  g_return_if_fail (EDIT_IS_ENTRY_ROW (self));

  text = gtk_editable_get_text (GTK_EDITABLE (self));

  /* We short-circuit this if there's no change */
  if (! g_strcmp0 (text, self->saved_text))
    return;

  g_signal_emit (self, obj_signals [COMMITTED], 0, &handled);

  if (! handled)
    return;

  g_free (self->saved_text);
  self->saved_text = g_strdup (text);
}

/* Filter out any non-enumeration characters */
static void
enumeration_insert_text_cb (GtkEditable *editable,
                            const char  *text,
                            int          length,
                            int         *position,
                            gpointer     data)
{
  const gchar *ptr;
  gboolean valid = TRUE;

  for (ptr = text;
       (ptr - text) < length;
       ptr = g_utf8_next_char (ptr))
    {
      if (! ipuz_enumeration_valid_char (ptr[0]))
        {
          valid = FALSE;
          break;
        }
    }

  if (valid)
    {
      g_signal_handlers_block_by_func (editable,
                                       (gpointer) enumeration_insert_text_cb, data);
      gtk_editable_insert_text (editable, text, length, position);
      g_signal_handlers_unblock_by_func (editable,
                                         (gpointer) enumeration_insert_text_cb, data);
    }

  g_signal_stop_emission_by_name (editable, "insert_text");
}

void
edit_entry_row_set_validation_mode (EditEntryRow               *self,
                                    EditEntryRowValidationMode  mode)
{
  g_return_if_fail (EDIT_IS_ENTRY_ROW (self));

  self->mode = mode;

  if (mode == EDIT_ENTRY_ROW_ENUMERATION)
    {
      g_signal_connect (gtk_editable_get_delegate (GTK_EDITABLE (self)),
                        "insert-text", G_CALLBACK (enumeration_insert_text_cb), self);
    }
}
