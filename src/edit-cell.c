/* edit-cell.c
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <libipuz/libipuz.h>
#include <glib/gi18n-lib.h>
#include <adwaita.h>
#include "edit-cell.h"
#include "edit-grid.h"
#include "edit-symmetry.h"
#include "play-cell.h"
#include "grid-state.h"


enum
{
  SIDE_CHANGED,
  CELL_TYPE_CHANGED,
  INITIAL_VAL_CHANGED,
  N_SIGNALS
};

static guint obj_signals[N_SIGNALS] = { 0 };


struct _EditCell
{
  GtkWidget parent_instance;

  LayoutConfig config;

  GtkWidget *sidebar_logo_cell;

  GtkWidget *cell_type_row;

  GtkWidget *bars_box;
  GtkWidget *top_toggle;
  GtkWidget *left_toggle;
  GtkWidget *right_toggle;
  GtkWidget *bottom_toggle;

  GtkWidget *cell_type_box;
  GtkWidget *normal_toggle;
  GtkWidget *block_toggle;
  GtkWidget *null_toggle;

  gboolean initial_val_set;
  GtkWidget *initial_val_switch;
};


G_DEFINE_FINAL_TYPE (EditCell, edit_cell, GTK_TYPE_WIDGET);


static void edit_cell_init              (EditCell      *self);
static void edit_cell_class_init        (EditCellClass *klass);
static void edit_cell_dispose           (GObject       *object);
static void barred_toggled_cb           (EditCell      *self,
                                         GtkWidget     *toggle);
static void cell_type_toggle_changed_cb (EditCell      *self,
                                         GtkWidget     *toggle);
static void initial_val_activated_cb    (EditCell      *self);


static void
edit_cell_init (EditCell *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

static void
edit_cell_class_init (EditCellClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = edit_cell_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-cell.ui");

  gtk_widget_class_bind_template_child (widget_class, EditCell, cell_type_row);
  gtk_widget_class_bind_template_child (widget_class, EditCell, sidebar_logo_cell);

  gtk_widget_class_bind_template_child (widget_class, EditCell, bars_box);
  gtk_widget_class_bind_template_child (widget_class, EditCell, top_toggle);
  gtk_widget_class_bind_template_child (widget_class, EditCell, left_toggle);
  gtk_widget_class_bind_template_child (widget_class, EditCell, right_toggle);
  gtk_widget_class_bind_template_child (widget_class, EditCell, bottom_toggle);

  gtk_widget_class_bind_template_child (widget_class, EditCell, cell_type_box);
  gtk_widget_class_bind_template_child (widget_class, EditCell, normal_toggle);
  gtk_widget_class_bind_template_child (widget_class, EditCell, block_toggle);
  gtk_widget_class_bind_template_child (widget_class, EditCell, null_toggle);

  gtk_widget_class_bind_template_child (widget_class, EditCell, initial_val_switch);

  gtk_widget_class_bind_template_callback (widget_class, barred_toggled_cb);
  gtk_widget_class_bind_template_callback (widget_class, cell_type_toggle_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, initial_val_activated_cb);

  /**
   * EditCell::side-changed:
   * @gobject: object which emitted the signal
   * @side: `IpuzStyleSides` with a single bit set, with the side that changed.
   *
   * This signal is emitted when the user changes one of the selected sides
   * by clicking on the respective toggle button.  Note that @side has only
   * a single bit set from the sides available in `IpuzStyleSides`.  To query the whole
   * state of the `EditCell`, use edit_cell_get_sides().
   */
  obj_signals[SIDE_CHANGED] =
    g_signal_new ("side-changed",
                  EDIT_TYPE_CELL,
                  G_SIGNAL_RUN_FIRST,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  IPUZ_TYPE_STYLE_SIDES);

  obj_signals[CELL_TYPE_CHANGED] =
    g_signal_new ("cell-type-changed",
                  EDIT_TYPE_CELL,
                  G_SIGNAL_RUN_FIRST,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  IPUZ_TYPE_CELL_TYPE);

  obj_signals[INITIAL_VAL_CHANGED] =
    g_signal_new ("initial-val-changed",
                  EDIT_TYPE_CELL,
                  G_SIGNAL_RUN_FIRST,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  G_TYPE_BOOLEAN);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BOX_LAYOUT);

  gtk_widget_class_set_css_name (widget_class, "edit-cell");
}

static void
edit_cell_dispose (GObject *object)
{
  GtkWidget *child;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  G_OBJECT_CLASS (edit_cell_parent_class)->dispose (object);
}

static void
barred_toggled_cb (EditCell  *self,
                   GtkWidget *toggle)
{
  IpuzStyleSides side;

  if (toggle == self->left_toggle)
    side = IPUZ_STYLE_SIDES_LEFT;
  else if (toggle == self->right_toggle)
    side = IPUZ_STYLE_SIDES_RIGHT;
  else if (toggle == self->top_toggle)
    side = IPUZ_STYLE_SIDES_TOP;
  else if (toggle == self->bottom_toggle)
    side = IPUZ_STYLE_SIDES_BOTTOM;
  else
    g_assert_not_reached ();

  g_signal_emit (self, obj_signals[SIDE_CHANGED], 0, side);
}

static void
cell_type_toggle_changed_cb (EditCell  *self,
                             GtkWidget *toggle)
{
  IpuzCellType cell_type;

  if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle)))
    return;

  if (toggle == self->normal_toggle)
    cell_type = IPUZ_CELL_NORMAL;
  else if (toggle == self->block_toggle)
    cell_type = IPUZ_CELL_BLOCK;
  else if (toggle == self->null_toggle)
    cell_type = IPUZ_CELL_NULL;
  else
    g_assert_not_reached ();

  g_signal_emit (self, obj_signals[CELL_TYPE_CHANGED], 0, cell_type);
}

static void
initial_val_activated_cb (EditCell *self)
{
  if (adw_switch_row_get_active (ADW_SWITCH_ROW (self->initial_val_switch)) == self->initial_val_set)
    return;

  g_signal_emit (self, obj_signals[INITIAL_VAL_CHANGED], 0,
                 adw_switch_row_get_active (ADW_SWITCH_ROW (self->initial_val_switch)));
}

static void
set_toggle (EditCell  *self,
            GtkWidget *toggle,
            gboolean   is_sensitive,
            gboolean   is_active)
{
  gtk_widget_set_sensitive (toggle, is_sensitive);

  g_signal_handlers_block_by_func (toggle, barred_toggled_cb, self);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (toggle), is_active);
  g_signal_handlers_unblock_by_func (toggle, barred_toggled_cb, self);
}

static void
update_bars (EditCell  *self,
             GridState *grid_state)
{
  IpuzStyleSides bars = 0;
  guint width, height;

  gtk_widget_set_visible (self->bars_box, IPUZ_IS_BARRED (grid_state->xword));
  if (!IPUZ_IS_BARRED (grid_state->xword))
      return;

  width = ipuz_crossword_get_width (grid_state->xword);
  height = ipuz_crossword_get_height (grid_state->xword);

  bars = ipuz_barred_get_cell_bars (IPUZ_BARRED (grid_state->xword),
                                    &grid_state->cursor);

  set_toggle (self, self->top_toggle,
              grid_state->cursor.row > 0,
              IPUZ_STYLE_SIDES_HAS_TOP (bars));
  set_toggle (self, self->left_toggle,
              grid_state->cursor.column > 0,
              IPUZ_STYLE_SIDES_HAS_LEFT (bars));
  set_toggle (self, self->bottom_toggle,
              grid_state->cursor.row + 1 < height,
              IPUZ_STYLE_SIDES_HAS_BOTTOM (bars));
  set_toggle (self, self->right_toggle,
              grid_state->cursor.column + 1 < width,
              IPUZ_STYLE_SIDES_HAS_RIGHT (bars));
}

static void
update_sidebar_logo (EditCell  *self,
                     GridState *grid_state)
{
  LayoutCell layout = {0, };
  IpuzCell *cell;
  IpuzStyle *style;

  cell = ipuz_crossword_get_cell (grid_state->xword,
                                  &grid_state->cursor);
  style = ipuz_cell_get_style (cell);

  layout.cell_type = ipuz_cell_get_cell_type (cell);
  layout.main_text = ipuz_cell_get_solution (cell);
  layout.foreground_css_class = LAYOUT_ITEM_FOREGROUND_STYLE_NORMAL;

  if (IPUZ_CELL_IS_BLOCK (cell))
    layout.css_class = LAYOUT_ITEM_STYLE_BLOCK;
  else if (IPUZ_CELL_IS_NULL (cell))
    layout.css_class = LAYOUT_ITEM_STYLE_NULL;
  else
    layout.css_class = LAYOUT_ITEM_STYLE_NORMAL;

  if (ipuz_cell_get_initial_val (cell) != NULL)
    {
      layout.main_text = ipuz_cell_get_initial_val (cell);
      layout.css_class = LAYOUT_ITEM_STYLE_INITIAL_VAL;
    }
  if (style)
    {
      const gchar *color;
      layout.divided = ipuz_style_get_divided (style);
      color = ipuz_style_get_bg_color (style);
      if (color)
        {
          layout.bg_color_set = TRUE;
          gdk_rgba_parse (&layout.bg_color, color);
        }

      color = ipuz_style_get_text_color (style);
      if (color)
        {
          layout.text_color_set = TRUE;
          gdk_rgba_parse (&layout.text_color, color);
        }
    }
  layout.editable = FALSE;
  play_cell_update_config (PLAY_CELL (self->sidebar_logo_cell), self->config);
  play_cell_update (PLAY_CELL (self->sidebar_logo_cell), &layout);
}

static void
update_cell_type (EditCell  *self,
                  GridState *grid_state)
{
  IpuzCell *cell;
  const gchar *subtitle = NULL;

  gtk_widget_set_visible (self->cell_type_box, !IPUZ_IS_BARRED (grid_state->xword));
  if (IPUZ_IS_BARRED (grid_state->xword))
      return;

  cell = ipuz_crossword_get_cell (grid_state->xword,
                                  &grid_state->cursor);

  g_signal_handlers_block_by_func (self->normal_toggle, cell_type_toggle_changed_cb, self);
  g_signal_handlers_block_by_func (self->block_toggle, cell_type_toggle_changed_cb, self);
  g_signal_handlers_block_by_func (self->null_toggle, cell_type_toggle_changed_cb, self);

  if (IPUZ_CELL_IS_BLOCK (cell))
    {
      subtitle = _("Block");

      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (self->block_toggle), TRUE);
    }
  else if (IPUZ_CELL_IS_NULL (cell))
    {
      subtitle = _("Null");
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (self->null_toggle), TRUE);
    }
  else
    {
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (self->normal_toggle), TRUE);
      subtitle = _("Regular");
    }

  g_signal_handlers_unblock_by_func (self->normal_toggle, cell_type_toggle_changed_cb, self);
  g_signal_handlers_unblock_by_func (self->block_toggle, cell_type_toggle_changed_cb, self);
  g_signal_handlers_unblock_by_func (self->null_toggle, cell_type_toggle_changed_cb, self);

  adw_action_row_set_subtitle (ADW_ACTION_ROW (self->cell_type_row), subtitle);
}

static void
update_initial_val (EditCell  *self,
                    GridState *grid_state)
{
  IpuzCell *cell;

  cell = ipuz_crossword_get_cell (grid_state->xword,
                                  &grid_state->cursor);

  g_signal_handlers_block_by_func (self->initial_val_switch, initial_val_activated_cb, self);
  adw_switch_row_set_active (ADW_SWITCH_ROW (self->initial_val_switch), FALSE);
  gtk_widget_set_sensitive (self->initial_val_switch, FALSE);
  self->initial_val_set = FALSE;

  if (IPUZ_CELL_IS_NORMAL (cell))
    {
      const gchar *initial_val;
      const gchar *solution;

      initial_val = ipuz_cell_get_initial_val (cell);
      solution = ipuz_cell_get_solution (cell);

      if (initial_val)
        {
          adw_switch_row_set_active (ADW_SWITCH_ROW (self->initial_val_switch), TRUE);
          gtk_widget_set_sensitive (self->initial_val_switch, TRUE);
          self->initial_val_set = TRUE;
        }
      else if (solution)
        {
          gtk_widget_set_sensitive (self->initial_val_switch, TRUE);
        }
    }
  g_signal_handlers_unblock_by_func (self->initial_val_switch, initial_val_activated_cb, self);
}

/* Public methods */

void
edit_cell_update (EditCell     *self,
                  GridState    *grid_state,
                  LayoutConfig  logo_config)
{

  g_assert (grid_state->xword != NULL);

  self->config = logo_config;

  update_sidebar_logo (self, grid_state);
  update_cell_type (self, grid_state);
  update_bars (self, grid_state);
  update_initial_val (self, grid_state);
}
