/* edit-preview.c
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <libipuz/libipuz.h>
#include <glib/gi18n-lib.h>
#include "cell-preview.h"
#include "play-border.h"
#include "play-cell.h"
#include "play-style.h"
#include "grid-state.h"

struct _CellPreview
{
  GtkWidget parent_instance;

  GtkWidget *tl_border;
  GtkWidget *t_border;
  GtkWidget *tr_border;
  GtkWidget *l_border;
  GtkWidget *cell;
  GtkWidget *r_border;
  GtkWidget *bl_border;
  GtkWidget *b_border;
  GtkWidget *br_border;
};


G_DEFINE_FINAL_TYPE (CellPreview, cell_preview, GTK_TYPE_WIDGET);


static void cell_preview_init       (CellPreview      *self);
static void cell_preview_class_init (CellPreviewClass *klass);
static void cell_preview_dispose    (GObject          *object);


static void
cell_preview_init (CellPreview *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

static void
cell_preview_class_init (CellPreviewClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = cell_preview_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/cell-preview.ui");

  gtk_widget_class_bind_template_child (widget_class, CellPreview, tl_border);
  gtk_widget_class_bind_template_child (widget_class, CellPreview, t_border);
  gtk_widget_class_bind_template_child (widget_class, CellPreview, tr_border);
  gtk_widget_class_bind_template_child (widget_class, CellPreview, l_border);
  gtk_widget_class_bind_template_child (widget_class, CellPreview, cell);
  gtk_widget_class_bind_template_child (widget_class, CellPreview, r_border);
  gtk_widget_class_bind_template_child (widget_class, CellPreview, bl_border);
  gtk_widget_class_bind_template_child (widget_class, CellPreview, b_border);
  gtk_widget_class_bind_template_child (widget_class, CellPreview, br_border);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BOX_LAYOUT);
  gtk_widget_class_set_css_name (widget_class, "cell-preview");
}

static void
cell_preview_dispose (GObject *object)
{
  GtkWidget *child;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  G_OBJECT_CLASS (cell_preview_parent_class)->dispose (object);
}


/* Public methods */

static void
update_cell (CellPreview  *self,
             GridState    *state,
             LayoutConfig  config)
{
  LayoutCell layout = {0, };
  IpuzCell *cell;
  IpuzStyle *style;

  cell = ipuz_crossword_get_cell (state->xword,
                                  &state->cursor);
  style = ipuz_cell_get_style (cell);

  layout.cell_type = ipuz_cell_get_cell_type (cell);
  layout.main_text = ipuz_cell_get_solution (cell);
  layout.foreground_css_class = LAYOUT_ITEM_FOREGROUND_STYLE_NORMAL;

  if (IPUZ_CELL_IS_BLOCK (cell))
    layout.css_class = LAYOUT_ITEM_STYLE_BLOCK;
  else if (IPUZ_CELL_IS_NULL (cell))
    layout.css_class = LAYOUT_ITEM_STYLE_NULL;
  else
    layout.css_class = LAYOUT_ITEM_STYLE_NORMAL;

  if (ipuz_cell_get_initial_val (cell) != NULL)
    {
      layout.main_text = ipuz_cell_get_initial_val (cell);
      layout.css_class = LAYOUT_ITEM_STYLE_INITIAL_VAL;
    }

  if (style)
    {
      const gchar *color;
      layout.divided = ipuz_style_get_divided (style);
      color = ipuz_style_get_bg_color (style);
      if (color)
        {
          layout.bg_color_set = TRUE;
          gdk_rgba_parse (&layout.bg_color, color);
        }

      color = ipuz_style_get_text_color (style);
      if (color)
        {
          layout.text_color_set = TRUE;
          gdk_rgba_parse (&layout.text_color, color);
        }
    }
  layout.editable = FALSE;
  play_cell_update_config (PLAY_CELL (self->cell), config);
  play_cell_update (PLAY_CELL (self->cell), &layout);
}

static void
update_borders (CellPreview  *self,
                GridState    *state,
                LayoutConfig  config)
{
  LayoutItemBorderStyle css_style = LAYOUT_BORDER_STYLE_NORMAL;
  gboolean filled = TRUE;
  GdkRGBA bg_color = { 0 };
  gboolean bg_color_set = FALSE;
  IpuzCell *cell;
  IpuzStyle *style;

  cell = ipuz_crossword_get_cell (state->xword,
                                  &state->cursor);
  style = ipuz_cell_get_style (cell);
  
  if (IPUZ_CELL_IS_BLOCK (cell))
    css_style = LAYOUT_BORDER_STYLE_DARK;
  else if (ipuz_cell_get_initial_val (cell) != NULL)
    css_style = LAYOUT_BORDER_STYLE_INITIAL_VAL;
  
  if (style)
    {
      const gchar *color;
      color = ipuz_style_get_bg_color (style);
      if (color)
        {
          bg_color_set = TRUE;
          gdk_rgba_parse (&bg_color, color);
          darken_color (&bg_color);
        }
    }

  play_border_update_config (PLAY_BORDER (self->tl_border), config);
  play_border_update_state (PLAY_BORDER (self->tl_border), filled, css_style, bg_color_set, bg_color);

  play_border_update_config (PLAY_BORDER (self->t_border), config);
  play_border_update_state (PLAY_BORDER (self->t_border), filled, css_style, bg_color_set, bg_color);

  play_border_update_config (PLAY_BORDER (self->tr_border), config);
  play_border_update_state (PLAY_BORDER (self->tr_border), filled, css_style, bg_color_set, bg_color);

  play_border_update_config (PLAY_BORDER (self->l_border), config);
  play_border_update_state (PLAY_BORDER (self->l_border), filled, css_style,bg_color_set, bg_color);

  play_border_update_config (PLAY_BORDER (self->r_border), config);
  play_border_update_state (PLAY_BORDER (self->r_border), filled, css_style,bg_color_set, bg_color);

  play_border_update_config (PLAY_BORDER (self->bl_border), config);
  play_border_update_state (PLAY_BORDER (self->bl_border), filled, css_style, bg_color_set, bg_color);

  play_border_update_config (PLAY_BORDER (self->b_border), config);
  play_border_update_state (PLAY_BORDER (self->b_border), filled, css_style, bg_color_set, bg_color);

  play_border_update_config (PLAY_BORDER (self->br_border), config);
  play_border_update_state (PLAY_BORDER (self->br_border), filled, css_style,bg_color_set, bg_color);
}

void
cell_preview_update_from_cursor (CellPreview  *self,
                                 GridState    *state,
                                 LayoutConfig  config)
{
  update_cell (self, state, config);
  update_borders (self, state, config);
}
