/* edit-preview-window.c
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include <glib/gi18n.h>

#include "edit-app.h"
#include "edit-preview-window.h"
#include "play-clue-list.h"
#include "play-grid.h"

struct _EditPreviewWindow
{
  AdwWindow parent_instance;

  /* Template widgets */
  GtkWidget *xword_column;
  GtkWidget *intro_label;
  GtkWidget *play_grid;
  GtkWidget *notes_label;
  GtkWidget *clue_label;
  GtkWidget *primary_clues;
  GtkWidget *secondary_clues;
  GtkWidget *action_bar;
  GtkWidget *across_toggle;
  GtkWidget *down_toggle;
  GtkSizeGroup *spacer_size_group;
};


static void edit_preview_window_init          (EditPreviewWindow      *self);
static void edit_preview_window_class_init    (EditPreviewWindowClass *klass);
static void edit_preview_window_actions_close (EditPreviewWindow      *self,
                                               const gchar            *action_name,
                                               GVariant               *param);


G_DEFINE_TYPE (EditPreviewWindow, edit_preview_window, ADW_TYPE_WINDOW);


static void
edit_preview_window_init (EditPreviewWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

#if DEVELOPMENT_BUILD
  gtk_widget_add_css_class (GTK_WIDGET (self), "devel");
#endif

}

static void
edit_preview_window_class_init (EditPreviewWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-preview-window.ui");

  gtk_widget_class_bind_template_child (widget_class, EditPreviewWindow, xword_column);
  gtk_widget_class_bind_template_child (widget_class, EditPreviewWindow, intro_label);
  gtk_widget_class_bind_template_child (widget_class, EditPreviewWindow, play_grid);
  gtk_widget_class_bind_template_child (widget_class, EditPreviewWindow, notes_label);
  gtk_widget_class_bind_template_child (widget_class, EditPreviewWindow, clue_label);
  gtk_widget_class_bind_template_child (widget_class, EditPreviewWindow, primary_clues);
  gtk_widget_class_bind_template_child (widget_class, EditPreviewWindow, secondary_clues);
  gtk_widget_class_bind_template_child (widget_class, EditPreviewWindow, action_bar);
  gtk_widget_class_bind_template_child (widget_class, EditPreviewWindow, across_toggle);
  gtk_widget_class_bind_template_child (widget_class, EditPreviewWindow, down_toggle);
  gtk_widget_class_bind_template_child (widget_class, EditPreviewWindow, spacer_size_group);

  gtk_widget_class_install_action (widget_class, "win.close", NULL,
                                   (GtkWidgetActionActivateFunc) edit_preview_window_actions_close);

  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_Escape, 0, "win.close", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_w, GDK_CONTROL_MASK, "win.close", NULL);
}

static void
edit_preview_window_actions_close (EditPreviewWindow *self,
                                   const gchar       *action_name,
                                   GVariant          *param)
{
  gtk_window_close (GTK_WINDOW (self));
}

/* Public functions */

GtkWidget *
edit_preview_window_new (void)
{
  GtkWidget *window;

  window = g_object_new (EDIT_TYPE_PREVIEW_WINDOW, NULL);

  return window;
}

void
edit_preview_window_update (EditPreviewWindow *self,
                            GridState         *state)
{
  const gchar *str;

  play_grid_update_state (PLAY_GRID (self->play_grid),
                          state,
                          layout_config_default (IPUZ_PUZZLE_CROSSWORD));
  play_clue_list_update (PLAY_CLUE_LIST (self->primary_clues), state, ZOOM_NORMAL);
  play_clue_list_update (PLAY_CLUE_LIST (self->secondary_clues), state, ZOOM_NORMAL);

  str = ipuz_puzzle_get_intro (IPUZ_PUZZLE (state->xword));
  if (str == NULL)
    str = "";
  gtk_label_set_markup (GTK_LABEL (self->intro_label), str);

  str = ipuz_puzzle_get_intro (IPUZ_PUZZLE (state->xword));
  gtk_label_set_markup (GTK_LABEL (self->intro_label), str);

  str = ipuz_puzzle_get_notes (IPUZ_PUZZLE (state->xword));
  gtk_label_set_markup (GTK_LABEL (self->notes_label), str);
}
