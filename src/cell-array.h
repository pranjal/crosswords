/* cell-array.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once


#include <libipuz/libipuz.h>


G_BEGIN_DECLS


/* Array to manage lists of cells. Acs like a set, with each cell
 * either being in or not in the array. It can be converted to a list
 * of Cells when done. It's just a GArray, but we make a mini-type out
 * of it to capture behavior.
 */
typedef GArray CellArray;

CellArray *cell_array_new         (void);
CellArray *cell_array_copy        (CellArray     *src);
void       cell_array_unref       (CellArray     *cell_array);
void       cell_array_add         (CellArray     *cell_array,
                                   IpuzCellCoord  coord);
void       cell_array_remove      (CellArray     *cell_array,
                                   IpuzCellCoord  coord);
void       cell_array_toggle      (CellArray     *cell_array,
                                   IpuzCellCoord  coord);
gboolean   cell_array_find        (CellArray     *cell_array,
                                   IpuzCellCoord  coord,
                                   guint         *out);
void       cell_array_print       (CellArray     *cell_array);

#define cell_array_len(ca)        (((GArray*) ca)->len)
#define cell_array_index(ca,i)    (g_array_index((GArray*)ca,IpuzCellCoord,i))
#define cell_array_ref(ca)        (g_array_ref((GArray*)ca))
#define cell_array_set_size(ca,s) (g_array_set_size((GArray*)ca,s))


G_END_DECLS
