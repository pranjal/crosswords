/* gen-word-list.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <glib.h>
#include <glib/gi18n.h>
#include <glib/gstdio.h>
#include <gio/gio.h>
#include <libipuz/libipuz.h>
#include "gen-word-list.h"

G_BEGIN_DECLS


typedef enum
{
  BRODA_SCORED,
  BRODA_FULL,
} WordListImporter;

typedef struct
{
  GArray *words;
  GArray *letters;
  GArray *word_index;
  GArray *anagram_list;
  GArray *anagram_fragments;
  GArray *corresponding_hash;
  GArray *enumerations;

  IpuzCharset *charset;
  gint threshold;
  gint min_length;
  gint max_length;
  gssize letter_list_offset;
  gssize letter_index_offset;
  gssize enumerations_offset;
  gssize anagram_word_list_offset;
  gssize anagram_hash_index_offset;
  WordListImporter importer;
} GenWordList;


GenWordList *gen_word_list_new                   (gint              min_length,
                                                  gint              max_length,
                                                  gint              threshold,
                                                  WordListImporter  importer);
void         gen_word_list_free                  (GenWordList      *word_list);
void         gen_word_list_add_test_word         (GenWordList      *word_list,
                                                  const char       *word,
                                                  glong             priority);
gboolean     gen_word_list_parse                 (GenWordList      *word_list,
                                                  GInputStream     *stream);
void         gen_word_list_build_enumerations    (GenWordList      *word_list);
void         gen_word_list_sort                  (GenWordList      *word_list);
void         gen_word_list_remove_duplicates     (GenWordList      *word_list);
void         gen_word_list_build_charset         (GenWordList      *word_list);
void         gen_word_list_anagram_table         (GenWordList      *word_list);
void         gen_word_list_anagram_dump          (GenWordList      *word_list);
void         gen_word_list_calculate_offsets     (GenWordList      *word_list);
void         create_anagram_fragments            (GenWordList      *word_list);
void         gen_word_list_write                 (GenWordList      *word_list,
                                                  const char       *display_name,
                                                  const char       *output_filename);
void         gen_word_list_write_to_stream       (GenWordList      *word_list,
                                                  const char       *display_name,
                                                  GOutputStream    *stream);
void         gen_word_list_write_index           (GenWordList      *word_list,
                                                  const char       *display_name,
                                                  const char       *output_filename);
void         gen_word_list_write_index_to_stream (GenWordList      *word_list,
                                                  const char       *display_name,
                                                  GOutputStream    *stream);



/* For debugging */
void         gen_word_list_print_stats        (GenWordList      *word_list);
void         gen_word_list_dump               (GenWordList      *word_list);


G_END_DECLS
