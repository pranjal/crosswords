/* edit-word-list.c
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <libipuz/libipuz.h>
#include <glib/gi18n-lib.h>
#include <adwaita.h>
#include "crosswords-enums.h"
#include "edit-grid.h"
#include "edit-word-list.h"
#include "play-grid.h"
#include "word-array-model.h"
#include "word-list.h"
#include "word-list-model.h"


enum
{
  WORD_ACTIVATED,
  N_SIGNALS
};

static guint obj_signals[N_SIGNALS] = { 0 };


struct _EditWordList
{
  GtkWidget parent_instance;

  GtkFilter *filter;

  WordList *word_list;
  WordArrayModel *across_list_model;
  WordArrayModel *down_list_model;

  GtkWidget *across_word_list_view;
  GtkWidget *down_word_list_view;

  GtkWidget *show_multi_switch_row;
  gboolean multi_check;

  gchar *across_clue_string;
  gchar *down_clue_string;
  guint across_pos;
  guint down_pos;
};


G_DEFINE_TYPE (EditWordList, edit_word_list, GTK_TYPE_WIDGET);


static void edit_word_list_init         (EditWordList      *self);
static void edit_word_list_class_init   (EditWordListClass *klass);
static void edit_word_list_dispose      (GObject           *object);
static void across_word_activate_cb     (EditWordList      *self,
                                         guint              position);
static void down_word_activate_cb       (EditWordList      *self,
                                         guint              position);
static void show_multi_row_activated_cb (EditWordList      *self);
static void edit_word_list_recalculate  (EditWordList      *self);


static void
setup_listitem_cb (GtkListItemFactory *factory,
                   GtkListItem        *list_item)
{
  GtkWidget *label;

  label = gtk_label_new (NULL);
  gtk_label_set_xalign (GTK_LABEL (label), 0.0);
  gtk_list_item_set_child (list_item, label);
}

static void
combine_word_and_enumeration_src (GString     *label_str,
                                  const gchar *word,
                                  const gchar *enumeration_src)
{
  const gchar *word_ptr;
  const gchar *ptr;

  g_assert (word);
  g_assert (enumeration_src);

  word_ptr = word;
  ptr = enumeration_src;
  while (ptr[0])
    {
      if (g_ascii_isdigit (ptr[0]))
        {
          guint64 num;
          gchar *endptr = NULL;
          num = g_ascii_strtoll (ptr, &endptr, 10);

          for (guint i = 0; i < num; i++)
            {
              g_assert (word_ptr[i]);
              g_string_append_unichar (label_str, word_ptr[i]);
            }
          word_ptr += num;
          ptr = endptr;
        }
      else
        {
          g_string_append_unichar (label_str, ptr[0]);
          ptr++;
        }
    }
}

static void
row_changed (WordListModelRow *row,
             GtkWidget        *label)
{
  GString *label_str = NULL;
  const gchar *enumeration_src;

  label_str = g_string_new (NULL);
  enumeration_src = word_list_model_row_get_enumeration_src (row);

  if (enumeration_src == NULL)
    {
      g_string_printf (label_str,
                       "%s — (%d)",
                       word_list_model_row_get_word (row),
                       word_list_model_row_get_priority (row));
    }
  else
    {
      combine_word_and_enumeration_src (label_str,
                                        word_list_model_row_get_word (row),
                                        enumeration_src);
      g_string_append_printf (label_str,
                              " - (%d)",
                              word_list_model_row_get_priority (row));
    }

  gtk_label_set_text (GTK_LABEL (label), label_str->str);
  g_string_free (label_str, TRUE);
}

static void
bind_listitem_cb (GtkListItemFactory *factory,
                  GtkListItem        *list_item)
{
  GtkWidget *label;
  WordListModelRow *row;

  label = gtk_list_item_get_child (list_item);
  row = (WordListModelRow *) gtk_list_item_get_item (list_item);

  g_signal_connect (row, "changed", G_CALLBACK (row_changed), label);
  row_changed (row, label);
}

static void
unbind_listitem_cb (GtkListItemFactory *factory,
                    GtkListItem        *list_item)
{
  GtkWidget *label;
  WordListModelRow *row;

  label = gtk_list_item_get_child (list_item);
  row = (WordListModelRow *) gtk_list_item_get_item (list_item);

  g_signal_handlers_disconnect_by_func (row, G_CALLBACK (row_changed), label);
}


gboolean
show_multi_filter (WordListModelRow *row,
                   EditWordList     *self)
{
  if (self->multi_check)
    return TRUE;

  return (word_list_model_row_get_enumeration_src (row) == NULL);
}

static void
edit_word_list_init (EditWordList *self)
{

  GListModel *filter_model;
  GListModel *selection_model;
  g_autoptr (GtkListItemFactory) factory = NULL;

  gtk_widget_init_template (GTK_WIDGET (self));
  self->multi_check = TRUE;

  self->word_list = word_list_new ();
  g_signal_connect_swapped (self->word_list, "resource-changed",
                            G_CALLBACK (edit_word_list_recalculate),
                            self);
  self->across_list_model = word_array_model_new (self->word_list);
  self->down_list_model = word_array_model_new (self->word_list);
  self->filter = (GtkFilter *)
    gtk_custom_filter_new ((GtkCustomFilterFunc)show_multi_filter,
                           self, NULL);
  factory = gtk_signal_list_item_factory_new ();
  g_signal_connect (factory, "setup", G_CALLBACK (setup_listitem_cb), NULL);
  g_signal_connect (factory, "bind", G_CALLBACK (bind_listitem_cb), NULL);
  g_signal_connect (factory, "unbind", G_CALLBACK (unbind_listitem_cb), NULL);

  /* Across */
  filter_model = (GListModel *)
    gtk_filter_list_model_new (G_LIST_MODEL (self->across_list_model),
                               self->filter);
  selection_model = (GListModel *) gtk_no_selection_new (filter_model);
  g_object_set (self->across_word_list_view,
                "model", selection_model,
                "factory", factory,
                NULL);

  filter_model = (GListModel *)
    gtk_filter_list_model_new (G_LIST_MODEL (self->down_list_model),
                               self->filter);
  selection_model = (GListModel *) gtk_no_selection_new (filter_model);
  g_object_set (self->down_word_list_view,
                "model", selection_model,
                "factory", factory,
                NULL);
}

static void
edit_word_list_class_init (EditWordListClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = edit_word_list_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-word-list.ui");

  gtk_widget_class_bind_template_callback (widget_class, across_word_activate_cb);
  gtk_widget_class_bind_template_callback (widget_class, down_word_activate_cb);
  gtk_widget_class_bind_template_callback (widget_class, show_multi_row_activated_cb);

  gtk_widget_class_bind_template_child (widget_class, EditWordList, across_word_list_view);
  gtk_widget_class_bind_template_child (widget_class, EditWordList, down_word_list_view);
  gtk_widget_class_bind_template_child (widget_class, EditWordList, show_multi_switch_row);

  obj_signals [WORD_ACTIVATED] =
    g_signal_new ("word-activated",
                  EDIT_TYPE_WORD_LIST,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 3,
                  IPUZ_TYPE_CLUE_DIRECTION,
                  G_TYPE_STRING,
                  G_TYPE_STRING);

  gtk_widget_class_set_css_name (widget_class, "wordlist");
  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
}

static void
edit_word_list_dispose (GObject *object)
{
  EditWordList *self;
  GtkWidget *child;

  self = EDIT_WORD_LIST (object);

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  g_clear_object (&self->filter);
  g_clear_object (&self->word_list);
  g_clear_object (&self->across_list_model);
  g_clear_object (&self->down_list_model);
  g_clear_pointer (&self->across_clue_string, g_free);
  g_clear_pointer (&self->down_clue_string, g_free);

  G_OBJECT_CLASS (edit_word_list_parent_class)->dispose (object);
}

static void
word_activate (EditWordList      *self,
               guint              position,
               IpuzClueDirection  direction)
{
  GListModel *model;
  WordListModelRow *row;
  const gchar *word;
  const gchar *enumeration_src;

  if (direction == IPUZ_CLUE_DIRECTION_ACROSS)
    model = (GListModel *) gtk_list_view_get_model (GTK_LIST_VIEW (self->across_word_list_view));
  else
    model = (GListModel *) gtk_list_view_get_model (GTK_LIST_VIEW (self->down_word_list_view));

  row = g_list_model_get_item (model, position);
  word = word_list_model_row_get_word (row);
  enumeration_src = word_list_model_row_get_enumeration_src (row);
  g_assert (word);

  g_signal_emit (self, obj_signals [WORD_ACTIVATED], 0, direction, word, enumeration_src);
}

static void
across_word_activate_cb (EditWordList *self,
                         guint         position)
{
  word_activate (self, position, IPUZ_CLUE_DIRECTION_ACROSS);
}

static void
down_word_activate_cb (EditWordList *self,
                       guint         position)
{
  word_activate (self, position, IPUZ_CLUE_DIRECTION_DOWN);
}

static void
show_multi_row_activated_cb (EditWordList *self)
{
  gboolean multi_check;

  multi_check = adw_switch_row_get_active (ADW_SWITCH_ROW (self->show_multi_switch_row));
  if (multi_check != self->multi_check)
    {
      GtkFilterChange change;
      self->multi_check = multi_check;

      if (multi_check)
        change = GTK_FILTER_CHANGE_LESS_STRICT;
      else
        change = GTK_FILTER_CHANGE_MORE_STRICT;

      gtk_filter_changed (self->filter, change);
    }
}


static guint
find_cursor_in_clue_clue (IpuzClue      *clue,
                          IpuzCellCoord  cursor)
{
  const GArray *cells;

  cells = ipuz_clue_get_cells (clue);
  for (guint i = 0; i < cells->len; i++)
    {
      IpuzCellCoord clue_coord = g_array_index (cells, IpuzCellCoord, i);
      if (clue_coord.row == cursor.row && clue_coord.column == cursor.column)
        return i;
    }

  g_assert_not_reached ();
}

static void
edit_word_list_recalculate (EditWordList *self)
{
  g_autoptr (IpuzCharset) intersecting_chars = NULL;
  g_autoptr (WordArray) across_array = NULL;
  g_autoptr (WordArray) down_array = NULL;

  word_list_find_intersection (self->word_list,
                               self->across_clue_string,
                               self->across_pos,
                               self->down_clue_string,self->down_pos,
                               &intersecting_chars,
                               &across_array,
                               &down_array);
  word_array_model_set_array (self->across_list_model, across_array);
  word_array_model_set_array (self->down_list_model, down_array);
  gtk_filter_changed (self->filter, GTK_FILTER_CHANGE_DIFFERENT);
}


void
edit_word_list_update (EditWordList *self,
                       GridState    *edit_state)
{
  IpuzClue *across_clue;
  IpuzClue *down_clue;

  g_return_if_fail (EDIT_IS_WORD_LIST (self));

  self->across_pos = 0;
  self->down_pos = 0;
  g_clear_pointer (&self->across_clue_string, g_free);
  g_clear_pointer (&self->down_clue_string, g_free);

  if (edit_state->clue.direction == IPUZ_CLUE_DIRECTION_NONE)
    {
      word_array_model_set_array (self->across_list_model, NULL);
      word_array_model_set_array (self->down_list_model, NULL);
      return;
    }

  across_clue = ipuz_crossword_find_clue_by_coord (edit_state->xword,
                                                   IPUZ_CLUE_DIRECTION_ACROSS,
                                                   &edit_state->cursor);
  down_clue = ipuz_crossword_find_clue_by_coord (edit_state->xword,
                                                 IPUZ_CLUE_DIRECTION_DOWN,
                                                 &edit_state->cursor);

  if (across_clue)
    {
      IpuzClueId clue_id;

      ipuz_crossword_get_id_by_clue (edit_state->xword, across_clue, &clue_id);
      self->across_clue_string = ipuz_crossword_get_clue_string_by_id (edit_state->xword, &clue_id);
      self->across_pos = find_cursor_in_clue_clue (across_clue, edit_state->cursor);
    }

  if (down_clue)
    {
      IpuzClueId clue_id;

      ipuz_crossword_get_id_by_clue (edit_state->xword, down_clue, &clue_id);
      self->down_clue_string = ipuz_crossword_get_clue_string_by_id (edit_state->xword, &clue_id);
      self->down_pos = find_cursor_in_clue_clue (down_clue, edit_state->cursor);
    }

  edit_word_list_recalculate (self);
}
