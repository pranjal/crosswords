/* acrostic-generator.c
 *
 * Copyright 2023 Tanmay Patil <tanmaynpatil105@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include "acrostic-generator.h"
#include "word-list.h"


G_DEFINE_QUARK (acrostic-generator-error-quark, acrostic_generator_error)

#define MIN_WORD_SIZE 3
#define MAX_WORD_SIZE 20

enum
{
  PROP_0,
  PROP_RANDOM_SEED,
  PROP_MIN_WORD_SIZE,
  PROP_MAX_WORD_SIZE,
  PROP_QUOTE_STR,
  PROP_SOURCE_STR,
  PROP_CHARSET,
  PROP_WORD_LIST,
  N_PROPS
};

static GParamSpec *obj_props[N_PROPS] = {NULL, };


struct _AcrosticGenerator
{
  GObject parent_object;
  guint64 counter;
  guint32 random_seed;
  IpuzCharset *charset;

  gchar *quote_str;
  gchar *source_str;

  /* Clues */
  GArray *clues;

  /* Word List */
  WordList *word_list;

  /* Word size */
  guint max_word_size;
  guint min_word_size;

  /* Grid */
  guint n_rows;
  guint n_columns;
};

static void acrostic_generator_init         (AcrosticGenerator      *self);
static void acrostic_generator_class_init   (AcrosticGeneratorClass *klass);
static void acrostic_generator_set_property (GObject                *object,
                                             guint                   prop_id,
                                             const GValue           *value,
                                             GParamSpec             *pspec);
static void acrostic_generator_get_property (GObject                *object,
                                             guint                   prop_id,
                                             GValue                 *value,
                                             GParamSpec             *pspec);
static void acrostic_generator_finalize     (GObject                *object);


G_DEFINE_TYPE (AcrosticGenerator, acrostic_generator, G_TYPE_OBJECT);

static void
acrostic_generator_init (AcrosticGenerator *self)
{
  self->max_word_size = MAX_WORD_SIZE;
  self->min_word_size = MIN_WORD_SIZE;
  self->counter = 0;
}

static void
acrostic_generator_class_init (AcrosticGeneratorClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  object_class->set_property = acrostic_generator_set_property;
  object_class->get_property = acrostic_generator_get_property;
  object_class->finalize = acrostic_generator_finalize;

  obj_props[PROP_RANDOM_SEED] = g_param_spec_uint ("random-seed",
                                                   "Random seed",
                                                   "Seed value for rand()",
                                                   0,
                                                   G_MAXUINT32,
                                                   0,
                                                   G_PARAM_READWRITE);

  obj_props[PROP_MIN_WORD_SIZE] = g_param_spec_uint ("min-word-size",
                                                     "Minimum word size",
                                                     "Minimum size of a clue word",
                                                     MIN_WORD_SIZE,
                                                     MAX_WORD_SIZE,
                                                     MIN_WORD_SIZE,
                                                     G_PARAM_READWRITE);

  obj_props[PROP_MAX_WORD_SIZE] = g_param_spec_uint ("max-word-size",
                                                     "Maximum word size",
                                                     "Maximum size of a clue word",
                                                     MIN_WORD_SIZE,
                                                     MAX_WORD_SIZE,
                                                     MAX_WORD_SIZE,
                                                     G_PARAM_READWRITE);

  obj_props[PROP_QUOTE_STR] = g_param_spec_string ("quote-string",
                                                   "Quote string",
                                                   "Quote string",
                                                   NULL,
                                                   G_PARAM_READWRITE);

  obj_props[PROP_SOURCE_STR] = g_param_spec_string ("source-string",
                                                    "Source string",
                                                    "Source string",
                                                    NULL,
                                                    G_PARAM_READWRITE);

  obj_props[PROP_CHARSET] = g_param_spec_boxed ("charset",
                                                NULL, NULL,
                                                IPUZ_TYPE_CHARSET,
                                                G_PARAM_READWRITE);

  obj_props[PROP_WORD_LIST] = g_param_spec_object ("word-list",
                                                   NULL, NULL,
                                                   WORD_TYPE_LIST,
                                                   G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  g_object_class_install_properties (object_class, N_PROPS, obj_props);
}

static void
acrostic_generator_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  AcrosticGenerator *self;

  self = ACROSTIC_GENERATOR (object);

  switch (prop_id)
    {
    case PROP_RANDOM_SEED:
      self->random_seed = g_value_get_uint (value);
      break;
    case PROP_MIN_WORD_SIZE:
      self->min_word_size = g_value_get_uint (value);
      break;
    case PROP_MAX_WORD_SIZE:
      self->max_word_size = g_value_get_uint (value);
      break;
    case PROP_QUOTE_STR:
      g_clear_pointer (&self->quote_str, g_free);
      self->quote_str = g_value_dup_string (value);
      break;
    case PROP_SOURCE_STR:
      g_clear_pointer (&self->source_str, g_free);
      self->source_str = g_value_dup_string (value);
      break;
    case PROP_CHARSET:
      if (self->charset != NULL)
        ipuz_charset_unref (self->charset);
      self->charset = g_value_dup_boxed (value);
      break;
    case PROP_WORD_LIST:
      g_clear_object (&self->word_list);
      self->word_list = g_value_dup_object (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
acrostic_generator_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  AcrosticGenerator *self;

  g_return_if_fail (object != NULL);

  self = ACROSTIC_GENERATOR (object);

  switch (prop_id)
    {
    case PROP_RANDOM_SEED:
      g_value_set_uint (value, self->random_seed);
      break;
    case PROP_MIN_WORD_SIZE:
      g_value_set_uint (value, self->min_word_size);
      break;
    case PROP_MAX_WORD_SIZE:
      g_value_set_uint (value, self->max_word_size);
      break;
    case PROP_QUOTE_STR:
      g_value_set_string (value, self->quote_str);
      break;
    case PROP_SOURCE_STR:
      g_value_set_string (value, self->source_str);
      break;
    case PROP_CHARSET:
      g_value_set_boxed (value, self->charset);
      break;
    case PROP_WORD_LIST:
      g_value_set_object (value, self->word_list);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
acrostic_generator_finalize (GObject *object)
{
  AcrosticGenerator *self;

  self = ACROSTIC_GENERATOR (object);

  g_clear_pointer (&self->quote_str, g_free);
  g_clear_pointer (&self->source_str, g_free);
  g_clear_pointer (&self->clues, g_array_unref);
  g_clear_object (&self->word_list);
  ipuz_charset_unref (self->charset);

  G_OBJECT_CLASS (acrostic_generator_parent_class)->finalize (object);
}

/* Public methods */

AcrosticGenerator*
acrostic_generator_new (void)
{
  GObject *object;
  IpuzCharsetBuilder *builder;
  IpuzCharset *charset;
  g_autoptr (WordList) word_list = NULL;
  GRand *g_rand;
  guint32 random_seed;

  g_rand = g_rand_new ();
  random_seed = g_rand_int (g_rand);

  /* FIXME: use the default charset */
  builder = ipuz_charset_builder_new_for_language ("C");
  charset = ipuz_charset_builder_build (builder);
  builder = NULL;
  word_list = word_list_new ();

  object = g_object_new (ACROSTIC_TYPE_GENERATOR,
                         "random-seed", random_seed,
                         "charset", charset,
                         "word-list", word_list,
                         NULL);

  g_rand_free (g_rand);

  return ACROSTIC_GENERATOR (object);
}

void
acrostic_generator_set_min_word_size (AcrosticGenerator *self,
                                      guint              new_size)
{
  g_return_if_fail (ACROSTIC_IS_GENERATOR (self));

  self->min_word_size = new_size;
}

void
acrostic_generator_set_max_word_size (AcrosticGenerator *self,
                                      guint              new_size)
{
  g_return_if_fail (ACROSTIC_IS_GENERATOR (self));

  self->max_word_size = new_size;
}

static gboolean
check_if_possible (AcrosticGenerator *self,
                   const gchar       *quote_str,
                   const gchar       *source_str,
                   GError           **error)
{
  IpuzCharsetIter *iter;
  g_autoptr (IpuzCharset) quote_charset = NULL;
  g_autoptr (IpuzCharset) source_charset = NULL;

  quote_charset = ipuz_charset_deserialize (quote_str);
  source_charset = ipuz_charset_deserialize (source_str);

  if ((ipuz_charset_get_size (source_charset) * self->min_word_size) > ipuz_charset_get_size (quote_charset))
    {
       /*
        *  QUOTE : LIFE IS TOO SHORT
        *  SOURCE : TOLSTOI
        *
        *  min_word_size = 3 (minimun word size for each clue)
        *
        *  T _ _
        *  O _ _
        *  L _ _               quote_len = 14
        *  S _ _               source_len = 7
        *  T _ _               total_min_clue_text_len = source_len * min_word_size = 7 * 3 = 21
        *  O _ _               Since, clue_text_len > quote_len => Invalid
        *  I _ _
        *
        */
      *error = g_error_new (ACROSTIC_GENERATOR_ERROR, ACROSTIC_GENERATOR_ERROR_SOURCE_TOO_SHORT,
                            "Source text is too short to accomodate the specificed minimum word size for each clue");
      return FALSE;
    }

  for (size_t index = 0;
       index < ipuz_charset_get_n_values (source_charset);
       index++)
    {
      IpuzCharsetIterValue value;
      value = ipuz_charset_get_value (source_charset, index);

      if (value.count > ipuz_charset_get_char_count (quote_charset, value.c))
        {
          *error = g_error_new (ACROSTIC_GENERATOR_ERROR, ACROSTIC_GENERATOR_ERROR_SOURCE_NOT_IN_QUOTE,
                                "Source characters are missing in the provided quote");
          return FALSE;
        }
    }

  return TRUE;
}

/*
 * normalise_input
 *
 * Remove spaces, punctuations from the string
 * and convert it to uppercase
 */
static gchar*
normalise_input (const gchar *str,
                 IpuzCharset *charset)
{
  GString *normalised;
  const char *p;

  normalised = g_string_new (NULL);

  for (p = str; p[0]; p = g_utf8_next_char (p))
    {
      gunichar c = g_unichar_toupper (g_utf8_get_char (p));

      if (ipuz_charset_get_char_count (charset, c) > 0)
        g_string_append_unichar (normalised, c);
    }

  return g_string_free_and_steal (normalised);
}

gboolean
acrostic_generator_set_text (AcrosticGenerator *self,
                             const gchar       *quote_str,
                             const gchar       *source_str,
                             GError           **error)
{
  /* normalised quote and source strings */
  gchar *n_quote_str, *n_source_str;

  n_quote_str = normalise_input (quote_str, self->charset);
  n_source_str = normalise_input (source_str, self->charset);

  if (!check_if_possible (self, n_quote_str, n_source_str, error))
    {
      g_free (n_quote_str);
      g_free (n_source_str);

      /* callee function sets the error */
      return FALSE;
    }

  /* clear old values */
  g_clear_pointer (&self->quote_str, g_free);
  g_clear_pointer (&self->source_str, g_free);

  self->quote_str = n_quote_str;
  self->source_str = n_source_str;

  return TRUE;
}

void
acrostic_generator_set_seed (AcrosticGenerator *self,
                             guint32            seed)
{
  g_return_if_fail (ACROSTIC_IS_GENERATOR (self));

  self->random_seed = seed;
}

typedef struct {
  gunichar leading_char;
  const gchar *letters;
  guint word_length;
} ClueEntry;

static void
shuffle_array (GArray *arr)
{
  if (arr->len == 0)
    return;

  for (guint i = 0; i < arr->len - 1; i++)
    {
      guint j = i + rand() / (RAND_MAX / (arr->len - i) + 1);
      gushort swap = g_array_index (arr, gushort, j);
      g_array_index (arr, gushort, j) = g_array_index (arr, gushort, i);
      g_array_index (arr, gushort, i) = swap;
    }
}

static GArray*
generate_random_lookup (guint size)
{
  GArray *array;

  array = g_array_new (FALSE, TRUE, sizeof (gushort));

  g_array_set_size (array, size);

  for (guint i = 0; i < size; i++)
    g_array_index (array, gushort, i) = (gushort) i;

  shuffle_array (array);

  return array;
}

static gchar *
generate_filter (gunichar leading_char,
                 guint    len,
                 guint    min_word_size,
                 guint    max_word_size)
{
  GString *filter;

  g_assert (len >= min_word_size && len <= max_word_size);

  filter = g_string_sized_new (len);

  g_string_append_unichar (filter, leading_char);

  for (guint i = 1; i < len; i++)
    g_string_append_c (filter, '?');

  return g_string_free_and_steal (filter);
}

static void
dump_clues (GArray *clues)
{
  GString *dump_str = g_string_new (NULL);
  char ESC=27;

  for (guint i = 0; i < clues->len; i++)
    {
      ClueEntry clue_entry;

      clue_entry = g_array_index (clues, ClueEntry, i);
      if (clue_entry.letters)
        {
          g_string_append_printf (dump_str, "[ %s ] ", clue_entry.letters);
        }
      else
        {
          g_string_append_printf (dump_str, "[ %c[1m", ESC);
          g_string_append_unichar (dump_str, clue_entry.leading_char);
          g_string_append_printf (dump_str, "%c[0m", ESC);
          if (clue_entry.word_length > 0)
            {
              for (guint j = 0; j < clue_entry.word_length - 1; j++)
                g_string_append_c (dump_str, '*');
            }

          g_string_append (dump_str, " ]");
          if (i < clues->len - 1)
            g_string_append (dump_str, ", ");
        }
    }
  g_print ("%s", dump_str->str);
  g_print ("\n");
  g_string_free (dump_str, TRUE);
}

static gboolean
acrostic_generator_helper (AcrosticGenerator  *self,
                           GArray             *clues,
                           guint               index,
                           IpuzCharsetBuilder *remaining_letters)
{
  ClueEntry *clue_entry;

  g_autofree gchar *filter = NULL;
  g_autoptr (GArray) rand_offset = NULL;

  if (index == clues->len)
    return TRUE;

  clue_entry = &(g_array_index (clues, ClueEntry, index));

  filter = generate_filter (clue_entry->leading_char, clue_entry->word_length,
                            self->min_word_size,
                            self->max_word_size);

  word_list_set_filter (self->word_list, filter, WORD_LIST_MATCH);

  rand_offset = generate_random_lookup (word_list_get_n_items (self->word_list));

  for (guint i = 0; i < word_list_get_n_items (self->word_list); i++)
    {
      const gchar *word;
      self->counter++;
      word = word_list_get_word (self->word_list, g_array_index (rand_offset, gushort, i));

      clue_entry->letters = word;

      /* Give some progress */
      if (self->counter % 1000000 == 0)
        dump_clues (clues);

      if (ipuz_charset_builder_remove_text (remaining_letters, word + 1))
        {
          if (acrostic_generator_helper (self, clues, index + 1, remaining_letters))
            return TRUE;

          ipuz_charset_builder_add_text (remaining_letters, word + 1);
          clue_entry->letters = NULL;
          word_list_set_filter (self->word_list, filter, WORD_LIST_MATCH);
        }

    }

  clue_entry->letters = NULL;
  
  return FALSE;
}

static GArray*
initialize_clue_entries (const char *text)
{
  const char *p;
  GArray *clues;

  clues = g_array_new (FALSE, TRUE, sizeof (ClueEntry));

  for (p = text; p[0]; p = g_utf8_next_char (p))
    {
      gunichar c;
      ClueEntry clue_entry;

      c = g_utf8_get_char (p);

      clue_entry.leading_char = c;
      clue_entry.letters = NULL;
      clue_entry.word_length = 0;

      g_array_append_val (clues, clue_entry);
    }

  return clues;
}

static void
generate_random_lengths (GArray *clues,
                         guint   number,
                         guint   min_word_size,
                         guint   max_word_size)
{
  if ((clues->len * max_word_size) < number)
    return;

  guint sum = 0;

  for (guint i = 0; i < clues->len; i++)
    {
      ClueEntry *clue_entry;
      guint len;
      guint max_len = MAX (min_word_size,
                           MIN (max_word_size, number - sum));

      len = rand() % (max_len - min_word_size + 1) + min_word_size;
      sum += len;

      clue_entry = &(g_array_index (clues, ClueEntry, i));
      clue_entry->word_length = len;
    }

  if (sum != number)
    generate_random_lengths (clues, number, min_word_size, max_word_size);
}

static guint
minimise_max_word_size (guint quote_len,
                        guint source_len,
                        guint min_word_size,
                        guint max_word_size)
{
  guint minimised;

  minimised = quote_len - (source_len - 1) * min_word_size;

  return MIN (max_word_size, minimised);
}

void
acrostic_generator_run (AcrosticGenerator *self)
{
  guint max_word_size;
  GArray *clues;
  IpuzCharsetBuilder *quote_builder, *source_builder;
  g_autoptr (IpuzCharset) quote_charset = NULL;
  g_autoptr (IpuzCharset) source_charset = NULL;

  g_return_if_fail (ACROSTIC_IS_GENERATOR (self));

  quote_builder = ipuz_charset_builder_new_from_text (self->quote_str);
  source_builder = ipuz_charset_builder_new_from_text (self->source_str);

  quote_charset = ipuz_charset_builder_build (quote_builder);
  source_charset = ipuz_charset_builder_build (source_builder);
  source_builder = NULL;

  srand(self->random_seed);

  clues = initialize_clue_entries (self->source_str);

  max_word_size = minimise_max_word_size (ipuz_charset_get_size (quote_charset),
                                          ipuz_charset_get_size (source_charset),
                                          self->min_word_size, self->max_word_size);

  generate_random_lengths (clues, ipuz_charset_get_size (quote_charset),
                           self->min_word_size,
                           max_word_size);

  quote_builder = ipuz_charset_builder_new ();
  ipuz_charset_builder_add_text (quote_builder, self->quote_str);

  /*
   * Remove SOURCE string from charset in advance
   */
  if (!ipuz_charset_builder_remove_text (quote_builder, self->source_str))
    {
      /* Ideally, this code block is unreachable since we verify the condition during `_set_text` */
      g_clear_pointer (&clues, g_array_unref);
      ipuz_charset_unref (ipuz_charset_builder_build (quote_builder));
      return;
    }

  if (!acrostic_generator_helper (self, clues, 0, quote_builder))
    {
      g_print ("Didn't find a clue: counter (%lu)\n", self->counter);
      dump_clues (clues);
      g_clear_pointer (&clues, g_array_unref);
      ipuz_charset_unref (ipuz_charset_builder_build (quote_builder));
      return;
    }
  else
    {
      g_print ("Success!\n");
      dump_clues (clues);
      ipuz_charset_unref (ipuz_charset_builder_build (quote_builder));
    }

  self->clues = clues;
}
