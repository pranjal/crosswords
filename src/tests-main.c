#include <glib.h>
#include <locale.h>
#include "grid-layout.h"
#include "layout-tests.h"
#include "test-utils.h"

/* Tests for a complete puzzle */
#define FOCUS_TEST_FILENAME "tests/focus.ipuz"

/* Tests for a puzzle with no clues */
#define NO_CLUES_TEST_FILENAME "tests/focus-noclues.ipuz"

/* Tests for a puzzle with no focusable cells */
#define NO_FOCUSABLE_CELLS_TEST_FILENAME "tests/focus-no-focusable-cells.ipuz"

/* Tests for a puzzle where not all cells have clues */
#define SOME_CLUES_TEST_FILENAME "tests/focus-someclues.ipuz"

/* Tests for a acrostic puzzle */
#define ACROSTIC_TEST_FILENAME "tests/acrostic.ipuz"

static void
assert_state (const GridState *state, gboolean has_cursor, guint row, guint column, IpuzClueDirection direction, guint clue_index)
{
  GridState test = {
    .xword = state->xword,
    .clue.index = clue_index,
    .clue.direction = direction,
    .cursor.row = row,
    .cursor.column = column,
    .quirks = state->quirks,
    .behavior = grid_state_mode_to_behavior (GRID_STATE_SOLVE),
  };

  grid_state_assert_equal (state, &test);
}

static void
initial_state (void)
{
  GridState *state = load_state (FOCUS_TEST_FILENAME, GRID_STATE_SOLVE);
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  grid_state_free (state);
}

static void
can_select_valid_cells (void)
{
  GridState *state = load_state (FOCUS_TEST_FILENAME, GRID_STATE_SOLVE);
  IpuzCellCoord coord;

  coord.row = 1;
  coord.column = 0;
  state = grid_state_replace (state, grid_state_cell_selected (state, coord));
  assert_state (state, TRUE, 1, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);

  coord.row = 0;
  coord.column = 1;
  state = grid_state_replace (state, grid_state_cell_selected (state, coord));
  assert_state (state, TRUE, 0, 1, IPUZ_CLUE_DIRECTION_ACROSS, 0);

  grid_state_free (state);
}

static void
selecting_cell_changes_direction (void)
{
  GridState *state = load_state (FOCUS_TEST_FILENAME, GRID_STATE_SOLVE);
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_ACROSS, 0);

  IpuzCellCoord coord = {
    .row = 0,
    .column = 0,
  };
  state = grid_state_replace (state, grid_state_cell_selected (state, coord));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);

  grid_state_free (state);
}

G_GNUC_WARN_UNUSED_RESULT static GridState *
assert_cell_is_not_selectable (GridState *state, guint row, guint column, IpuzCellType cell_type)
{
  IpuzCellCoord old_coord = state->cursor;
  guint old_direction = state->clue.direction;
  guint old_index = state->clue.index;
  IpuzCell *cell;
  IpuzCellCoord coord = { .row = row, .column = column };

  /* Check the cell type */
  cell = ipuz_crossword_get_cell (state->xword, &coord);
  g_assert (cell != NULL);
  g_assert_cmpint (ipuz_cell_get_cell_type (cell), ==, cell_type);

  /* And it must not be selectable, and the state must remain unchanged */
  state = grid_state_replace (state, grid_state_cell_selected (state, coord));
  assert_state (state, TRUE, old_coord.row, old_coord.column, old_direction, old_index);
  return state;
}

static void
wont_select_null_cells (void)
{
  GridState *state = load_state (FOCUS_TEST_FILENAME, GRID_STATE_SOLVE);
  state = assert_cell_is_not_selectable (state, 1, 1, IPUZ_CELL_NULL);

  grid_state_free (state);
}

static void
wont_select_block_cells (void)
{
  GridState *state = load_state (FOCUS_TEST_FILENAME, GRID_STATE_SOLVE);
  state = assert_cell_is_not_selectable (state, 2, 1, IPUZ_CELL_BLOCK);

  grid_state_free (state);
}

static IpuzCellCoord
coord_for_cmd_kind (IpuzCellCoord coord, GridCmdKind kind)
{
  switch (kind)
    {
    case GRID_CMD_KIND_UP:
      coord.row -= 1;
      break;

    case GRID_CMD_KIND_DOWN:
      coord.row += 1;
      break;

    case GRID_CMD_KIND_LEFT:
      coord.column -= 1;
      break;

    case GRID_CMD_KIND_RIGHT:
      coord.column += 1;
      break;

    default:
      g_assert_not_reached ();
      break;

    }

  return coord;
}


static void
assert_focuses_within_clue (guint start_row,
                            guint start_col,
                            guint num_cells,
                            GridCmdKind focus_forward,
                            GridCmdKind focus_back,
                            IpuzClueDirection direction)
{
  GridState *state = load_state (FOCUS_TEST_FILENAME, GRID_STATE_SOLVE);
  IpuzCellCoord coord = {
    .row = start_row,
    .column = start_col,
  };
  guint i;

  /* Select the starting cell and ensure we are in the correct direction */

  state = grid_state_replace (state, grid_state_cell_selected (state, coord));
  if (state->clue.direction != direction)
    {
      state = grid_state_replace (state, grid_state_cell_selected (state, coord));
    }

  gint clue_index = state->clue.index;
  assert_state (state, TRUE, coord.row, coord.column, direction, clue_index);

  /* Go forward... */
  for (i = 0; i < num_cells; i++)
    {
      state = grid_state_replace (state, grid_state_do_command (state, focus_forward));
      coord = coord_for_cmd_kind (coord, focus_forward);
      assert_state (state, TRUE, coord.row, coord.column, direction, clue_index);
    }

  /* Go back... */
  for (i = 0; i < num_cells; i++)
    {
      state = grid_state_replace (state, grid_state_do_command (state, focus_back));
      coord = coord_for_cmd_kind (coord, focus_back);
      assert_state (state, TRUE, coord.row, coord.column, direction, clue_index);
    }

  grid_state_free (state);
}

static void
focuses_within_clue_across (void)
{
  assert_focuses_within_clue (0, 0, 3, GRID_CMD_KIND_RIGHT, GRID_CMD_KIND_LEFT, IPUZ_CLUE_DIRECTION_ACROSS);
}

static void
focuses_within_clue_down (void)
{
  assert_focuses_within_clue (0, 0, 3, GRID_CMD_KIND_DOWN, GRID_CMD_KIND_UP, IPUZ_CLUE_DIRECTION_DOWN);
}

static void
assert_next_prev_empty_cell_dir (GridState        **state,
                                 IpuzClueDirection  direction,
                                 gint               incr)
{
  GridCmdKind cmd = GRID_CMD_KIND_FORWARD_EMPTYCELL;
  GArray *clues = ipuz_crossword_get_clues ((*state)->xword, direction);

  if (incr == -1)
    cmd = GRID_CMD_KIND_BACK_EMPTYCELL;

  for (guint i = 0; i < clues->len; i++)
    {
      guint clue_index = i;
      IpuzClue *i_clue;
      IpuzClueId i_clue_id;
      IpuzCellCoord i_clue_coord;

      if (incr == -1)
        clue_index = clues->len - 1 - i;

      i_clue = g_array_index (clues, IpuzClue *, clue_index);
      ipuz_crossword_get_id_by_clue ((*state)->xword, i_clue, &i_clue_id);
      ipuz_clue_get_first_cell (i_clue, &i_clue_coord);
      assert_state (*state, TRUE, i_clue_coord.row, i_clue_coord.column, i_clue_id.direction, i_clue_id.index);

      *state = grid_state_replace (*state, grid_state_do_command (*state, cmd));
    }
}

static void
fill_cell (IpuzCrossword       *xword,
           IpuzCell            *cell,
           const IpuzCellCoord *coord,
           gpointer             data)
{
  if (cell->cell_type != IPUZ_CELL_NORMAL)
    return;

  IpuzGuesses *guesses = ipuz_crossword_get_guesses (xword);
  ipuz_guesses_set_guess (guesses, coord, "X");
}

static void
focuses_next_prev_empty_cell (void)
{
  GridState *state = load_state (FOCUS_TEST_FILENAME, GRID_STATE_SOLVE);
  IpuzGuesses *guesses = ipuz_guesses_new_from_board (ipuz_crossword_get_board (state->xword), FALSE);
  IpuzCellCoord coord = {
    .row = 0,
    .column = 0,
  };

  ipuz_crossword_set_guesses (state->xword, guesses);

  state = grid_state_replace (state, grid_state_cell_selected (state, coord));
  if (state->clue.direction != IPUZ_CLUE_DIRECTION_ACROSS)
    state = grid_state_replace (state, grid_state_cell_selected (state, coord));

  /* forward direction */
  assert_next_prev_empty_cell_dir (&state, IPUZ_CLUE_DIRECTION_ACROSS, 1);
  assert_next_prev_empty_cell_dir (&state, IPUZ_CLUE_DIRECTION_DOWN, 1);

  /* loop completed */
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_ACROSS, 0);

  /* reverse direction */
  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_BACK_EMPTYCELL));
  assert_next_prev_empty_cell_dir (&state, IPUZ_CLUE_DIRECTION_DOWN, -1);
  assert_next_prev_empty_cell_dir (&state, IPUZ_CLUE_DIRECTION_ACROSS, -1);
  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_FORWARD_EMPTYCELL));

  /* loop completed */
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_ACROSS, 0);

  /* check whether action is a no-op with full grid */
  ipuz_crossword_foreach_cell (state->xword, fill_cell, NULL);
  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_FORWARD_EMPTYCELL));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_ACROSS, 0);

  grid_state_free (state);
}

static void
advance_next_empty_cell (void)
{
  /* See https://gitlab.gnome.org/jrb/crosswords/-/issues/206 */

  /* Set up filled grid */
  GridState *state = load_state_with_quirks (FOCUS_TEST_FILENAME, GRID_STATE_SOLVE);
  IpuzGuesses *guesses = ipuz_guesses_new_from_board (ipuz_crossword_get_board (state->xword), FALSE);
  IpuzCellCoord coord = {
    .row = 0,
    .column = 0,
  };

  state = grid_state_replace (state, grid_state_cell_selected (state, coord));
  if (state->clue.direction != IPUZ_CLUE_DIRECTION_ACROSS)
    state = grid_state_replace (state, grid_state_cell_selected (state, coord));

  ipuz_crossword_set_guesses (state->xword, guesses);
  ipuz_crossword_foreach_cell (state->xword, fill_cell, NULL);

  /* Remove empty cells for test */
  ipuz_guesses_set_guess(guesses, &coord, NULL);
  coord.row = 1;
  ipuz_guesses_set_guess(guesses, &coord, NULL);

  /* check that guessing sends the cursor to the next empty cell */
  crosswords_quirks_test_set_guess_advance(state->quirks, QUIRKS_GUESS_ADVANCE_OPEN);
  state = grid_state_replace (state, grid_state_guess (state, "X"));
  assert_state (state, TRUE, 1, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);

  grid_state_free (state);
}

static void
do_circuit (void)
{
  GridState *state = load_state (FOCUS_TEST_FILENAME, GRID_STATE_SOLVE);

  /* Do a complete circuit along the test board; this will test all the easy
   * cases for switching direction..
   *
   * First go down.
   */

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_DOWN));
  assert_state (state, TRUE, 1, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_DOWN));
  assert_state (state, TRUE, 2, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_DOWN));
  assert_state (state, TRUE, 3, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_DOWN)); /* try going out of bounds */
  assert_state (state, TRUE, 3, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);

  /* Then go right */

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_RIGHT));
  assert_state (state, TRUE, 3, 1, IPUZ_CLUE_DIRECTION_ACROSS, 1);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_RIGHT));
  assert_state (state, TRUE, 3, 2, IPUZ_CLUE_DIRECTION_ACROSS, 1);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_RIGHT));
  assert_state (state, TRUE, 3, 3, IPUZ_CLUE_DIRECTION_ACROSS, 1);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_RIGHT)); /* try going out of bounds */
  assert_state (state, TRUE, 3, 3, IPUZ_CLUE_DIRECTION_ACROSS, 1);

  /* Then go up */

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_UP));
  assert_state (state, TRUE, 2, 3, IPUZ_CLUE_DIRECTION_DOWN, 1);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_UP));
  assert_state (state, TRUE, 1, 3, IPUZ_CLUE_DIRECTION_DOWN, 1);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_UP));
  assert_state (state, TRUE, 0, 3, IPUZ_CLUE_DIRECTION_DOWN, 1);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_UP)); /* try going out of bounds */
  assert_state (state, TRUE, 0, 3, IPUZ_CLUE_DIRECTION_DOWN, 1);

  /* Then go left */

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_LEFT));
  assert_state (state, TRUE, 0, 2, IPUZ_CLUE_DIRECTION_ACROSS, 0);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_LEFT));
  assert_state (state, TRUE, 0, 1, IPUZ_CLUE_DIRECTION_ACROSS, 0);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_LEFT));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_ACROSS, 0);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_LEFT)); /* try going out of bounds */
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_ACROSS, 0);
}

static void
assert_skips_unselectable_cells (IpuzCellCoord start,
                                 IpuzClueDirection start_direction,
                                 gint start_clue_index,
                                 IpuzCellCoord end,
                                 IpuzClueDirection end_direction,
                                 gint end_clue_index,
                                 GridCmdKind focus_forward,
                                 GridCmdKind focus_back)
{
  GridState *state = load_state (FOCUS_TEST_FILENAME, GRID_STATE_SOLVE);

  state = grid_state_replace (state, grid_state_cell_selected (state, start));
  assert_state (state, TRUE, start.row, start.column, start_direction, start_clue_index);

  state = grid_state_replace (state, grid_state_do_command (state, focus_forward));
  assert_state (state, TRUE, end.row, end.column, end_direction, end_clue_index);

  state = grid_state_replace (state, grid_state_do_command (state, focus_back));
  assert_state (state, TRUE, start.row, start.column, start_direction, start_clue_index);

  grid_state_free (state);
}

static void
skips_unselectable_cells_across (void)
{
  IpuzCellCoord start = {
    .row = 1,
    .column = 0,
  };

  IpuzCellCoord end = {
    .row = 1,
    .column = 3,
  };

  assert_skips_unselectable_cells (start, IPUZ_CLUE_DIRECTION_DOWN, 0,
                                   end, IPUZ_CLUE_DIRECTION_DOWN, 1,
                                   GRID_CMD_KIND_RIGHT,
                                   GRID_CMD_KIND_LEFT);
}

static void
skips_unselectable_cells_down (void)
{
  IpuzCellCoord start = {
    .row = 0,
    .column = 1,
  };

  IpuzCellCoord end = {
    .row = 3,
    .column = 1,
  };

  assert_skips_unselectable_cells (start, IPUZ_CLUE_DIRECTION_ACROSS, 0,
                                   end, IPUZ_CLUE_DIRECTION_ACROSS, 1,
                                   GRID_CMD_KIND_DOWN,
                                   GRID_CMD_KIND_UP);
}

G_GNUC_WARN_UNUSED_RESULT static GridState *
assert_stays_the_same_when_navigating (GridState *state, guint row, guint column, GridCmdKind cmd_kind)
{
  IpuzCellCoord coord = {
    .row = row,
    .column = column,
  };
  state = grid_state_replace (state, grid_state_cell_selected (state, coord));

  IpuzCellCoord old_coord = state->cursor;
  guint old_direction = state->clue.direction;
  guint old_index = state->clue.index;

  state = grid_state_replace (state, grid_state_do_command (state, cmd_kind));
  assert_state (state, TRUE, old_coord.row, old_coord.column, old_direction, old_index);
  return state;
}

static void
stops_at_bounds (void)
{
  GridState *state = load_state (FOCUS_TEST_FILENAME, GRID_STATE_SOLVE);
  state = assert_stays_the_same_when_navigating (state, 1, 0, GRID_CMD_KIND_LEFT);
  state = assert_stays_the_same_when_navigating (state, 1, 3, GRID_CMD_KIND_RIGHT);
  state = assert_stays_the_same_when_navigating (state, 0, 1, GRID_CMD_KIND_UP);
  state = assert_stays_the_same_when_navigating (state, 3, 1, GRID_CMD_KIND_DOWN);
  grid_state_free (state);
}

static void
select_clue (void)
{
  GridState *state = load_state (FOCUS_TEST_FILENAME, GRID_STATE_SOLVE);
  IpuzClue *clue;
  IpuzClueId clue_id = {
    .direction = IPUZ_CLUE_DIRECTION_DOWN,
    .index = 1,
  };

  clue = ipuz_crossword_get_clue_by_id (state->xword, &clue_id);
  state = grid_state_replace (state, grid_state_clue_selected (state, clue));
  assert_state (state, TRUE, 0, 3, IPUZ_CLUE_DIRECTION_DOWN, 1);

  clue_id.direction = IPUZ_CLUE_DIRECTION_ACROSS;
  clue = ipuz_crossword_get_clue_by_id (state->xword, &clue_id);
  state = grid_state_replace (state, grid_state_clue_selected (state, clue));
  assert_state (state, TRUE, 3, 0, IPUZ_CLUE_DIRECTION_ACROSS, 1);

  grid_state_free (state);
}

static void
seleting_same_clue_leaves_cursor_unchanged (void)
{
  GridState *state = load_state (FOCUS_TEST_FILENAME, GRID_STATE_SOLVE);
  IpuzClue *clue;

  IpuzCellCoord coord = {
    .row = 1,
    .column = 3,
  };

  IpuzClueId clue_id = {
    .direction = IPUZ_CLUE_DIRECTION_DOWN,
    .index = 1,
  };
  state = grid_state_replace (state, grid_state_cell_selected (state, coord));
  assert_state (state, TRUE, 1, 3, IPUZ_CLUE_DIRECTION_DOWN, 1);

  clue = ipuz_crossword_get_clue_by_id (state->xword, &clue_id);
  state = grid_state_replace (state, grid_state_clue_selected (state, clue));
  assert_state (state, TRUE, 1, 3, IPUZ_CLUE_DIRECTION_DOWN, 1);

  grid_state_free (state);
}

static void
no_focusable_cells_can_load (void)
{
  GridState *state = load_state (NO_FOCUSABLE_CELLS_TEST_FILENAME, GRID_STATE_SOLVE);
  assert_state (state, FALSE, 0, 0, IPUZ_CLUE_DIRECTION_NONE, 0);
  grid_state_free (state);
}

static void
no_clues_can_load (void)
{
  GridState *state = load_state (NO_CLUES_TEST_FILENAME, GRID_STATE_SOLVE);
  assert_state (state, TRUE, 0, 1, IPUZ_CLUE_DIRECTION_NONE, 0);
  grid_state_free (state);
}

#if 0
static void
no_clues_can_select_cells (void)
{
  GridState *state = load_state (NO_CLUES_TEST_FILENAME, GRID_STATE_SOLVE);
  IpuzCellCoord coord;

  coord.row = 0;
  coord.column = 1;
  state = grid_state_replace (state, grid_state_cell_selected (state, coord));
  assert_state (state, TRUE, 0, 1, IPUZ_CLUE_DIRECTION_NONE, 0);

  coord.row = 1;
  coord.column = 0;
  state = grid_state_replace (state, grid_state_cell_selected (state, coord));
  assert_state (state, TRUE, 1, 0, IPUZ_CLUE_DIRECTION_NONE, 0);

  /* not selectable, position stays the same */
  coord.row = 1;
  coord.column = 1;
  state = grid_state_replace (state, grid_state_cell_selected (state, coord));
  assert_state (state, TRUE, 1, 0, IPUZ_CLUE_DIRECTION_NONE, 0);

  grid_state_free (state);
}

static void
no_clues_can_focus (void)
{
  GridState *state = load_state (NO_CLUES_TEST_FILENAME, GRID_STATE_SOLVE);
  IpuzCellCoord coord;

  coord.row = 0;
  coord.column = 1;
  state = grid_state_replace (state, grid_state_cell_selected (state, coord));
  assert_state (state, TRUE, 0, 1, IPUZ_CLUE_DIRECTION_NONE, 0);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_LEFT));
  assert_state (state, TRUE, 0, 1, IPUZ_CLUE_DIRECTION_NONE, 0);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_RIGHT));
  assert_state (state, TRUE, 0, 2, IPUZ_CLUE_DIRECTION_NONE, 0);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_DOWN));
  assert_state (state, TRUE, 3, 2, IPUZ_CLUE_DIRECTION_NONE, 0);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_UP));
  assert_state (state, TRUE, 0, 2, IPUZ_CLUE_DIRECTION_NONE, 0);

  /* With direction = NONE, going forward/back does nothing since there is nowhere to go */
  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_FORWARD));
  assert_state (state, TRUE, 0, 2, IPUZ_CLUE_DIRECTION_NONE, 0);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_BACK));
  assert_state (state, TRUE, 0, 2, IPUZ_CLUE_DIRECTION_NONE, 0);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_BACKSPACE));
  assert_state (state, TRUE, 0, 2, IPUZ_CLUE_DIRECTION_NONE, 0);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_SWITCH));
  assert_state (state, TRUE, 0, 2, IPUZ_CLUE_DIRECTION_NONE, 0);

  grid_state_free (state);
}

static void
some_clues_can_load (void)
{
  GridState *state = load_state (SOME_CLUES_TEST_FILENAME, GRID_STATE_SOLVE);
  assert_state (state, TRUE, 0, 1, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  grid_state_free (state);
}

static void
some_clues_can_select_cells (void)
{
  GridState *state = load_state (SOME_CLUES_TEST_FILENAME, GRID_STATE_SOLVE);
  IpuzCellCoord coord;

  assert_state (state, TRUE, 0, 1, IPUZ_CLUE_DIRECTION_ACROSS, 0);

  /* Select a cell with no clue */
  coord.row = 3;
  coord.column = 1;
  state = grid_state_replace (state, grid_state_cell_selected (state, coord));
  assert_state (state, TRUE, 3, 1, IPUZ_CLUE_DIRECTION_NONE, 0);

  /* Select a cell with a clue again */
  coord.row = 1;
  coord.column = 0;
  state = grid_state_replace (state, grid_state_cell_selected (state, coord));
  assert_state (state, TRUE, 1, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);

  grid_state_free (state);
}

#endif

static void
assert_guess (IpuzGuesses *guesses, guint row, guint column, const char *expected)
{
  IpuzCellCoord coord = {
    .row = row,
    .column = column,
  };
  const char *guess = ipuz_guesses_get_guess (guesses, &coord);
  g_assert_cmpstr (guess, ==, expected);
}

static void
guesses_enters_complete_guess_across (void)
{
  GridState *state = load_state (FOCUS_TEST_FILENAME, GRID_STATE_SOLVE);
  g_autoptr (IpuzGuesses) guesses = NULL;

  guesses = ipuz_guesses_new_from_board (ipuz_crossword_get_board (state->xword), FALSE);
  ipuz_crossword_set_guesses (state->xword, guesses);
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  assert_guess (guesses, 0, 0, NULL);

  /* enter three guesses, advancing the cursor at each... */
  state = grid_state_replace (state, grid_state_guess (state, "A"));
  assert_state (state, TRUE, 0, 1, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  assert_guess (guesses, 0, 0, "A");

  state = grid_state_replace (state, grid_state_guess (state, "B"));
  assert_state (state, TRUE, 0, 2, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  assert_guess (guesses, 0, 1, "B");

  state = grid_state_replace (state, grid_state_guess (state, "C"));
  assert_state (state, TRUE, 0, 3, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  assert_guess (guesses, 0, 2, "C");

  /* ... but upon entering the last guess of the clue, leave the cursor in place */
  state = grid_state_replace (state, grid_state_guess (state, "D"));
  assert_state (state, TRUE, 0, 3, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  assert_guess (guesses, 0, 3, "D");

  grid_state_free (state);
}

static void
guesses_enters_complete_guess_down (void)
{
  GridState *state = load_state (FOCUS_TEST_FILENAME, GRID_STATE_SOLVE);
  g_autoptr (IpuzGuesses) guesses = NULL;

  guesses = ipuz_guesses_new_from_board (ipuz_crossword_get_board (state->xword), FALSE);
  ipuz_crossword_set_guesses (state->xword, guesses);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_SWITCH));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);
  assert_guess (guesses, 0, 0, NULL);

  /* enter three guesses, advancing the cursor at each... */
  state = grid_state_replace (state, grid_state_guess (state, "A"));
  assert_state (state, TRUE, 1, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);
  assert_guess (guesses, 0, 0, "A");

  state = grid_state_replace (state, grid_state_guess (state, "B"));
  assert_state (state, TRUE, 2, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);
  assert_guess (guesses, 1, 0, "B");

  state = grid_state_replace (state, grid_state_guess (state, "C"));
  assert_state (state, TRUE, 3, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);
  assert_guess (guesses, 2, 0, "C");

  /* ... but upon entering the last guess of the clue, leave the cursor in place */
  state = grid_state_replace (state, grid_state_guess (state, "D"));
  assert_state (state, TRUE, 3, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);
  assert_guess (guesses, 3, 0, "D");

  grid_state_free (state);
}

static void
guesses_backspace_across (void)
{
  GridState *state = load_state (FOCUS_TEST_FILENAME, GRID_STATE_SOLVE);
  g_autoptr (IpuzGuesses) guesses = NULL;

  guesses = ipuz_guesses_new_from_board (ipuz_crossword_get_board (state->xword), FALSE);
  ipuz_crossword_set_guesses (state->xword, guesses);
  assert_guess (guesses, 0, 0, NULL);

  state = grid_state_replace (state, grid_state_guess (state, "A"));
  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_LEFT));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  assert_guess (guesses, 0, 0, "A");

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_BACKSPACE));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  assert_guess (guesses, 0, 0, NULL);

  state = grid_state_replace (state, grid_state_guess (state, "A"));
  state = grid_state_replace (state, grid_state_guess (state, "B"));
  state = grid_state_replace (state, grid_state_guess (state, "C"));
  state = grid_state_replace (state, grid_state_guess (state, "D"));
  assert_state (state, TRUE, 0, 3, IPUZ_CLUE_DIRECTION_ACROSS, 0);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_BACKSPACE));
  assert_state (state, TRUE, 0, 2, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  assert_guess (guesses, 0, 3, NULL);
  assert_guess (guesses, 0, 2, "C");

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_BACKSPACE));
  assert_state (state, TRUE, 0, 1, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  assert_guess (guesses, 0, 2, NULL);
  assert_guess (guesses, 0, 1, "B");

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_BACKSPACE));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  assert_guess (guesses, 0, 1, NULL);
  assert_guess (guesses, 0, 0, "A");

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_BACKSPACE));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_ACROSS, 0);
  assert_guess (guesses, 0, 0, NULL);
}

static void
guesses_backspace_down (void)
{
  GridState *state = load_state (FOCUS_TEST_FILENAME, GRID_STATE_SOLVE);
  g_autoptr (IpuzGuesses) guesses = NULL;

  guesses = ipuz_guesses_new_from_board (ipuz_crossword_get_board (state->xword), FALSE);
  ipuz_crossword_set_guesses (state->xword, guesses);
  assert_guess (guesses, 0, 0, NULL);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_SWITCH));

  state = grid_state_replace (state, grid_state_guess (state, "A"));
  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_UP));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);
  assert_guess (guesses, 0, 0, "A");

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_BACKSPACE));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);
  assert_guess (guesses, 0, 0, NULL);

  state = grid_state_replace (state, grid_state_guess (state, "A"));
  state = grid_state_replace (state, grid_state_guess (state, "B"));
  state = grid_state_replace (state, grid_state_guess (state, "C"));
  state = grid_state_replace (state, grid_state_guess (state, "D"));
  assert_state (state, TRUE, 3, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_BACKSPACE));
  assert_state (state, TRUE, 2, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);
  assert_guess (guesses, 3, 0, NULL);
  assert_guess (guesses, 2, 0, "C");

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_BACKSPACE));
  assert_state (state, TRUE, 1, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);
  assert_guess (guesses, 2, 0, NULL);
  assert_guess (guesses, 1, 0, "B");

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_BACKSPACE));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);
  assert_guess (guesses, 1, 0, NULL);
  assert_guess (guesses, 0, 0, "A");

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_BACKSPACE));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_DOWN, 0);
  assert_guess (guesses, 0, 0, NULL);
}

void
acrostic_main_grid_guess_backspace (void)
{
  g_autoptr (IpuzPuzzle) puzzle_a = NULL;
  GridState *state = load_state_with_quirks (ACROSTIC_TEST_FILENAME, GRID_STATE_SOLVE);
  IpuzCellCoord coord = {
    .row = 0,
    .column = 4,
  };

  crosswords_quirks_set_focus_location (state->quirks, QUIRKS_FOCUS_LOCATION_MAIN_GRID);
  state = grid_state_replace (state, grid_state_cell_selected (state, coord));

  state = grid_state_replace (state, grid_state_guess (state, "A"));

  /* (0,4) -> (1,0) */
  assert_state (state, TRUE, 1, 0, IPUZ_CLUE_DIRECTION_CLUES, 1);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_BACKSPACE));

  /* (1,0) -> (0,4) */
  assert_state (state, TRUE, 0, 4, IPUZ_CLUE_DIRECTION_CLUES, 0);

  grid_state_free (state);
}

void
acrostic_clue_grid_guess_backspace (void)
{
  g_autoptr (IpuzPuzzle) puzzle_a = NULL;
  GridState *state = load_state_with_quirks (ACROSTIC_TEST_FILENAME, GRID_STATE_SOLVE);
  IpuzCellCoord coord = {
    .row = 1,
    .column = 0,
  };

  crosswords_quirks_set_focus_location (state->quirks, QUIRKS_FOCUS_LOCATION_CLUE_GRID);
  state = grid_state_replace (state, grid_state_cell_selected (state, coord));

  state = grid_state_replace (state, grid_state_guess (state, "A"));

  /* (1,0) -> (1,1) */
  assert_state (state, TRUE, 1, 1, IPUZ_CLUE_DIRECTION_CLUES, 1);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_BACKSPACE));

  /* (1,1) -> (1,0) */
  assert_state (state, TRUE, 1, 0, IPUZ_CLUE_DIRECTION_CLUES, 1);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_BACKSPACE));

  /* (1,0) -> (1,0) */
  assert_state (state, TRUE, 1, 0, IPUZ_CLUE_DIRECTION_CLUES, 1);

  grid_state_free (state);
}

void
acrostic_clue_grid_focus_up_down (void)
{
  GridState *state = load_state_with_quirks (ACROSTIC_TEST_FILENAME, GRID_STATE_SOLVE);
  IpuzClue *clue;
  IpuzClueId clue_id = {
    .direction = IPUZ_CLUE_DIRECTION_CLUES,
    .index = 0,
  };

  clue = ipuz_crossword_get_clue_by_id (state->xword, &clue_id);

  crosswords_quirks_set_focus_location (state->quirks, QUIRKS_FOCUS_LOCATION_CLUE_GRID);
  state = grid_state_replace (state, grid_state_clue_selected (state, clue));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_CLUES, 0);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_DOWN));
  assert_state (state, TRUE, 1, 0, IPUZ_CLUE_DIRECTION_CLUES, 1);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_UP));
  assert_state (state, TRUE, 0, 0, IPUZ_CLUE_DIRECTION_CLUES, 0);

  grid_state_free (state);
}

void
acrostic_skip_block_cells (void)
{
  g_autoptr (IpuzPuzzle) puzzle_a = NULL;
  GridState *state = load_state_with_quirks (ACROSTIC_TEST_FILENAME, GRID_STATE_SOLVE);
  IpuzCellCoord coord = {
    .row = 1,
    .column = 1,
  };

  crosswords_quirks_set_focus_location (state->quirks, QUIRKS_FOCUS_LOCATION_MAIN_GRID);
  state = grid_state_replace (state, grid_state_cell_selected (state, coord));

  state = grid_state_replace (state, grid_state_guess (state, "A"));

  /* (1,1) -> (1,3) */
  assert_state (state, TRUE, 1, 3, IPUZ_CLUE_DIRECTION_CLUES, 1);

  state = grid_state_replace (state, grid_state_do_command (state, GRID_CMD_KIND_BACKSPACE));

  /* (1,3) -> (1,1) */
  assert_state (state, TRUE, 1, 1, IPUZ_CLUE_DIRECTION_CLUES, 1);

  grid_state_free (state);
}

/*
 * QuirksFocusLocation Test
 *
 * @ QUIRKS_FOCUS_LOCATION_NONE      : We won't advance (for an acrostic)
 * @ QUIRKS_FOCUS_LOCATION_MAIN_GRID : We advance on the main grid based on the quote_clue
 * @ QUIRKS_FOCUS_LOCATION_CLUE_GRID : We advance on the clue grid based on the respective clue
 */
void
acrostic_focus_quirk_test (void)
{
  g_autoptr (IpuzPuzzle) puzzle_a = NULL;
  GridState *state = load_state_with_quirks (ACROSTIC_TEST_FILENAME, GRID_STATE_SOLVE);
  IpuzCellCoord coord = {
    .row = 0,
    .column = 4,
  };

  state = grid_state_replace (state, grid_state_cell_selected (state, coord));

  /*
   * FOCUS = NONE
   * (0,4) -> (0,4)
   * We don't advance
   */
  crosswords_quirks_set_focus_location (state->quirks, QUIRKS_FOCUS_LOCATION_NONE);
  state = grid_state_replace (state, grid_state_guess (state, "A"));
  assert_state (state, TRUE, 0, 4, IPUZ_CLUE_DIRECTION_CLUES, 0);

  /*
   * FOCUS = MAIN_GRID
   * (0,4) -> (1,0)
   * We advance on the main grid
   */
  crosswords_quirks_set_focus_location (state->quirks, QUIRKS_FOCUS_LOCATION_MAIN_GRID);
  state = grid_state_replace (state, grid_state_guess (state, "A"));
  assert_state (state, TRUE, 1, 0, IPUZ_CLUE_DIRECTION_CLUES, 1);

  /*
   * FOCUS = CLUE_GRID
   * (1,0) -> (1,1)
   * We advance on the clue grid
   */
  crosswords_quirks_set_focus_location (state->quirks, QUIRKS_FOCUS_LOCATION_CLUE_GRID);
  state = grid_state_replace (state, grid_state_guess (state, "A"));
  assert_state (state, TRUE, 1, 1, IPUZ_CLUE_DIRECTION_CLUES, 1);

  grid_state_free (state);
}

int
main (int argc, char **argv)
{
  setlocale(LC_ALL, "en_US.utf8");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/focus/initial_state", initial_state);
  g_test_add_func ("/focus/can_select_valid_cells", can_select_valid_cells);
  g_test_add_func ("/focus/selecting_cell_changes_direction", selecting_cell_changes_direction);
  g_test_add_func ("/focus/wont_select_null_cells", wont_select_null_cells);
  g_test_add_func ("/focus/wont_select_block_cells", wont_select_block_cells);
  g_test_add_func ("/focus/focuses_within_clue_across", focuses_within_clue_across);
  g_test_add_func ("/focus/focuses_within_clue_down", focuses_within_clue_down);
  g_test_add_func ("/focus/focuses_next_prev_empty_cell", focuses_next_prev_empty_cell);
  g_test_add_func ("/focus/advance_next_empty_cell", advance_next_empty_cell);
  g_test_add_func ("/focus/do_circuit", do_circuit);
  g_test_add_func ("/focus/skips_unselectable_cells_across", skips_unselectable_cells_across);
  g_test_add_func ("/focus/skips_unselectable_cells_down", skips_unselectable_cells_down);
  g_test_add_func ("/focus/stops_at_bounds", stops_at_bounds);

  g_test_add_func ("/focus/select_clue", select_clue);
  g_test_add_func ("/focus/seleting_same_clue_leaves_cursor_unchanged", seleting_same_clue_leaves_cursor_unchanged);

  g_test_add_func ("/no_clues/no_focusable_cells_can_load", no_focusable_cells_can_load);
  g_test_add_func ("/no_clues/can_load", no_clues_can_load);

  /* FIXME(noclues): Disable these tests for now.  We don't really
   * have a strategy for what to do when we have a puzzle with no
   * clues, or no clues associated with cells
   */
#if 0
  g_test_add_func ("/no_clues/can_select_cells", no_clues_can_select_cells);
  g_test_add_func ("/no_clues/can_focus", no_clues_can_focus);

  g_test_add_func ("/some_clues/can_load", some_clues_can_load);
  g_test_add_func ("/some_clues/can_select_cells", some_clues_can_select_cells);
#endif

  g_test_add_func ("/guesses/enters_complete_guess_across", guesses_enters_complete_guess_across);
  g_test_add_func ("/guesses/enters_complete_guess_down", guesses_enters_complete_guess_down);
  g_test_add_func ("/guesses/backspace_across", guesses_backspace_across);
  g_test_add_func ("/guesses/backspace_down", guesses_backspace_down);

  g_test_add_func ("/layout/sets_main_text_for_solve_mode", layout_sets_main_text_for_solve_mode);
  g_test_add_func ("/layout/sets_main_text_for_browse_mode", layout_sets_main_text_for_browse_mode);
  g_test_add_func ("/layout/sets_main_text_for_edit_mode", layout_sets_main_text_for_edit_mode);
  g_test_add_func ("/layout/sets_main_text_for_edit_browse_mode", layout_sets_main_text_for_edit_browse_mode);
  g_test_add_func ("/layout/sets_main_text_for_select_mode", layout_sets_main_text_for_select_mode);
  g_test_add_func ("/layout/sets_main_text_for_view_mode", layout_sets_main_text_for_view_mode);
  g_test_add_func ("/layout/sets_main_text_to_initial_val", layout_sets_main_text_to_initial_val);
  g_test_add_func ("/layout/loads_barred", layout_loads_barred);

  g_test_add_func ("/acrostic/main_grid_guess_backspace", acrostic_main_grid_guess_backspace);
  g_test_add_func ("/acrostic/clue_grid_guess_backspace", acrostic_clue_grid_guess_backspace);
  g_test_add_func ("/acrostic/clue_grid_focus_up_down", acrostic_clue_grid_focus_up_down);
  g_test_add_func ("/acrostic/skip_block_cells", acrostic_skip_block_cells);
  g_test_add_func ("/acrostic/focus_quirk_test", acrostic_focus_quirk_test);

  return g_test_run ();
}
