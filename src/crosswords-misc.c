/* crosswords-misc.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include <locale.h>
#include <glib/gi18n-lib.h>

#include <adwaita.h>

#include "crosswords-misc.h"
#include "grid-layout.h"
#include "play-grid.h"
#include "svg.h"

GdkPixbuf *
xwd_pixbuf_load_from_gresource (GResource  *resource,
                                const char *resource_path)
{
  GBytes *bytes;
  g_autoptr (GError) error = NULL;
  g_autoptr (GdkPixbufLoader) loader = NULL;
  GdkPixbuf *pixbuf;

  if (resource == NULL)
    bytes = g_resources_lookup_data (resource_path,
                                     G_RESOURCE_LOOKUP_FLAGS_NONE,
                                     NULL);
  else
    bytes = g_resource_lookup_data (resource, resource_path,
                                    G_RESOURCE_LOOKUP_FLAGS_NONE,
                                    NULL);
  loader = gdk_pixbuf_loader_new ();
  gdk_pixbuf_loader_write_bytes (loader, bytes, &error);
  gdk_pixbuf_loader_close (loader, NULL);

  pixbuf = gdk_pixbuf_loader_get_pixbuf (loader);
  if (pixbuf)
    g_object_ref (G_OBJECT (pixbuf));

  return pixbuf;
}

IpuzPuzzle *
xwd_load_puzzle_from_resource (GResource    *resource,
                               const gchar  *resource_path,
                               GError      **error)
{
  g_autoptr (GInputStream) stream = NULL;

  if (resource)
    stream = g_resource_open_stream (resource, resource_path, 0, error);
  else
    stream = g_resources_open_stream (resource_path, 0, error);

  if (stream == NULL)
    return NULL;

  return ipuz_puzzle_new_from_stream (stream, NULL, error);
}


GdkPixbuf *
xwd_thumbnail_puzzle (IpuzPuzzle *puzzle)
{
  GridLayout *layout;
  GridState *state;
  GdkPixbuf *pixbuf;

  state = grid_state_new (IPUZ_CROSSWORD (puzzle), NULL, GRID_STATE_VIEW);
  LayoutConfig config = {
    .border_size = 1,
    .base_size = 3,
  };
  layout = grid_layout_new (state, config);

  RsvgHandle *handle = svg_handle_new_from_layout (layout, coloring_from_adwaita ());
  pixbuf = rsvg_handle_get_pixbuf (handle);

  g_object_unref (handle);
  grid_layout_free (layout);
  grid_state_free (state);

  return pixbuf;
}

guint
utf8_n_clusters (const gchar  *text)
{
  if (text == NULL || *text == '\0')
    return 0;

  /* FIXME(i18n): Actually write this thing. I'm faking it to get the code working
   */
  return (guint) g_utf8_strlen (text, -1);
}

/**
 * utf8_get_next_cluster:
 * @text: a valid utf8 string
 * @end: an end pointer to the next utf8 character in text
 *
 * Returns a newly allocated string of the next text cluster in
 * text. #end is set to be the start of the next cluster in the
 * string.
 *
 * Returns: a newly allocated utf8 string
 **/
gchar *
utf8_get_next_cluster (const gchar  *text,
                       const gchar **end)
{
  const gchar *ptr;
  gchar *retval;
  gunichar c;

  if (text == NULL || *text == '\0')
    {
      *end = text;
      return NULL;
    }

  /* Move forward through any spaces or control characters until we find an uppercase character */
  ptr = text;
  c = g_utf8_get_char (ptr);
  while (ptr[0] && g_unichar_isspace (c))
    {
      ptr = g_utf8_next_char (ptr);
      c = g_utf8_get_char (ptr);
    }

  text = ptr;
  if (!text[0])
    {
      *end = text;
      return NULL;
   }

  /* Go through subsequent characters appending any marks */
  do
    {
      ptr = g_utf8_next_char (ptr);
      c = g_utf8_get_char (ptr);
    }
  while (ptr[0] && g_unichar_ismark (c));

  retval = g_strndup (text, ptr - text);
  *end = ptr;

  return retval;
}

static void
zoom_level_css_clear_or_set (GtkWidget   *widget,
                             const gchar *css_prefix,
                             const gchar *css_class,
                             gboolean     set_css)
{
  g_autofree gchar *real_css_class = NULL;
  gboolean has_css;

  if (css_prefix)
    real_css_class = g_strdup_printf ("%s%s", css_prefix, css_class);
  else
    real_css_class = g_strdup (css_class);

  has_css = gtk_widget_has_css_class (widget, real_css_class);

  /* GTK will blindly update the css class regardless of what's
   * set. Since we are calling this every keypress, we only remove/set
   * the css_class if it's different. */
  if (!has_css && set_css)
    gtk_widget_add_css_class (widget, real_css_class);
  else if (has_css && !set_css)
    gtk_widget_remove_css_class (widget, real_css_class);
}

void
set_zoom_level_css_class (GtkWidget   *widget,
                          const gchar *css_prefix,
                          ZoomLevel    zoom_level)
{
  g_return_if_fail (GTK_IS_WIDGET (widget));

  zoom_level_css_clear_or_set (widget, css_prefix, "zoom-xsmall",
                               (zoom_level == ZOOM_XSMALL));
  zoom_level_css_clear_or_set (widget, css_prefix, "zoom-small",
                               (zoom_level == ZOOM_SMALL));
  zoom_level_css_clear_or_set (widget, css_prefix, "zoom-normal",
                               (zoom_level == ZOOM_NORMAL));
  zoom_level_css_clear_or_set (widget, css_prefix, "zoom-large",
                               (zoom_level == ZOOM_LARGE));
  zoom_level_css_clear_or_set (widget, css_prefix, "zoom-xlarge",
                               (zoom_level == ZOOM_XLARGE));
}

Coloring
coloring_from_adwaita (void)
{
  Coloring coloring;

  if (adw_style_manager_get_dark (adw_style_manager_get_default ()))
    {
      coloring = COLORING_DARK;
    }
  else
    {
      coloring = COLORING_LIGHT;
    }

  return coloring;
}
