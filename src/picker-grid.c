/* picker-grid.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <adwaita.h>
#include "crosswords-misc.h"
#include "picker-grid.h"
#include "puzzle-button.h"
#include "puzzle-set.h"
#include "puzzle-set-config.h"


struct _PickerGrid
{
  PuzzlePicker parent_instance;

  guint width;
  guint height;
  gboolean show_progress;
  ConfigGridStyle grid_style;

  gulong n_won_handler;

  /* Bound widgets */
  GtkWidget *header_label;
  GtkWidget *button_grid;
};


static void     picker_grid_init               (PickerGrid      *self);
static void     picker_grid_class_init         (PickerGridClass *klass);
static void     picker_grid_dispose            (GObject         *object);
static void     picker_grid_load               (PuzzlePicker    *self);
static gboolean calculate_button_locked_status (PickerGrid      *self,
                                                guint            column,
                                                guint            row,
                                                gfloat           percent);


G_DEFINE_TYPE (PickerGrid, picker_grid, PUZZLE_TYPE_PICKER);


static void
picker_grid_init (PickerGrid *self)
{
  GtkLayoutManager *box_layout;
  gtk_widget_init_template (GTK_WIDGET (self));

  self->show_progress = TRUE;
  self->width = 0;
  self->height = 0;

  box_layout = gtk_widget_get_layout_manager (GTK_WIDGET (self));
  gtk_orientable_set_orientation (GTK_ORIENTABLE (box_layout), GTK_ORIENTATION_VERTICAL);
  gtk_box_layout_set_spacing (GTK_BOX_LAYOUT (box_layout), 16);
}

static void
picker_grid_class_init (PickerGridClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  PuzzlePickerClass *picker_class = PUZZLE_PICKER_CLASS (klass);

  object_class->dispose = picker_grid_dispose;
  picker_class->load = picker_grid_load;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/picker-grid.ui");
  gtk_widget_class_bind_template_child (widget_class, PickerGrid, header_label);
  gtk_widget_class_bind_template_child (widget_class, PickerGrid, button_grid);

  /* Ironically, we're a box not a grid */
  gtk_widget_class_set_layout_manager_type (widget_class,
                                            GTK_TYPE_BOX_LAYOUT);
}

static void
picker_grid_dispose (GObject *object)
{
  PickerGrid *self;
  GtkWidget *child;
  PuzzleSetModel *model;

  self = PICKER_GRID (object);

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  model = puzzle_picker_get_model (PUZZLE_PICKER (object));
  g_clear_signal_handler (&self->n_won_handler,
                          model);

  G_OBJECT_CLASS (picker_grid_parent_class)->dispose (object);
}

static void
picker_grid_load_header (PickerGrid *self)
{
  PuzzleSetConfig *config;
  const gchar *header = NULL;
  const gchar *header_face = NULL;
  PangoFontDescription *font_desc;
  PangoAttribute *attr;
  PangoAttrList *attrs;

  config = puzzle_picker_get_config (PUZZLE_PICKER (self));

  header = puzzle_set_config_get_header (config);
  header_face = puzzle_set_config_get_header_face (config);

  gtk_label_set_text (GTK_LABEL (self->header_label), header);

  /* Set the label style */
  /* FIXME(CSS): set some of this from CSS */
  attrs = pango_attr_list_new ();
  font_desc = pango_font_description_new ();
  pango_font_description_set_size (font_desc, 48 * PANGO_SCALE);
  pango_font_description_set_weight (font_desc, PANGO_WEIGHT_HEAVY);
  if (header_face)
    pango_font_description_set_family (font_desc, header_face);
  attr = pango_attr_font_desc_new (font_desc);
  pango_font_description_free (font_desc);
  pango_attr_list_insert (attrs, attr);
  gtk_label_set_attributes (GTK_LABEL (self->header_label), attrs);
  pango_attr_list_unref (attrs);
}


static void
button_clicked_cb (PuzzleButton     *button,
                   guint             puzzle_id,
                   PuzzleButtonMode  mode,
                   PickerGrid       *picker_grid)
{
  PuzzleSetModel *model;
  PuzzleSetModelRow *row;

  const gchar *puzzle_name;

  model = puzzle_picker_get_model (PUZZLE_PICKER (picker_grid));
  row = g_list_model_get_item (G_LIST_MODEL (model), puzzle_id - 1);
  g_assert (row != NULL);

  puzzle_name = puzzle_set_model_row_get_puzzle_name (row);
  g_assert (puzzle_name != NULL);

  if (mode == PUZZLE_BUTTON_MODE_SOLVED)
    {
      puzzle_picker_clear_puzzle (PUZZLE_PICKER (picker_grid),
                                  puzzle_name,
                                  _("Clear this puzzle?"),
                                  _("You have completed this puzzle. Do you want replay it? You will have to solve it again to make progress.\n\nYou can also view the completed puzzle."),
                                  TRUE, TRUE);
    }
  else
    {
      puzzle_picker_puzzle_selected (PUZZLE_PICKER (picker_grid), puzzle_name);
    }
}


/* We don't really work at more than an 8x8 puzzle size */
#define MAX_PICKER_GRID_SIZE 8

static void
picker_grid_load_buttons (PickerGrid *self)
{
  PuzzleSetConfig *config;
  guint row, column;
  g_autoptr (GtkSizeGroup) size_group = NULL;
  PuzzleSetModel *model;
  PuzzleSetModelRow *model_row;

  config = puzzle_picker_get_config (PUZZLE_PICKER (self));
  model = puzzle_picker_get_model (PUZZLE_PICKER (self));

  /* constrain ourselves to 8x8. Any larger won't make sense visually */
  self->width = puzzle_set_config_get_width (config);
  self->height = puzzle_set_config_get_height (config);

  self->width = MIN (MAX_PICKER_GRID_SIZE, self->width);
  self->height = MIN (MAX_PICKER_GRID_SIZE, self->height);
  size_group = gtk_size_group_new (GTK_SIZE_GROUP_VERTICAL);

  for (row = 0; row < self->height; row++)
    for (column = 0; column < self->width; column++)
      {
        guint index;
        IpuzPuzzle *puzzle;
        GtkWidget *button;

        index = row * self->width + column;

        model_row = g_list_model_get_item (G_LIST_MODEL (model), index);
        if (model_row == NULL)
          break;

        puzzle = puzzle_set_model_row_get_puzzle (model_row);
        g_assert (puzzle);

        button = puzzle_button_new (model_row);

        g_object_set (button,
                      "puzzle-id", index + 1,
                      "show-progress", self->show_progress,
                      "progress-fraction", 0.0,
                      "progress-size-group", size_group,
                      NULL);
        gtk_widget_set_size_request (button, 200, 200);

#if 0
        const gchar *thumbnail = NULL;

        /* If we decide to bring these back */
        thumbnail = puzzle_set_config_get_puzzle_thumb (config, puzzle_id - 1);
        if (thumbnail)
          {
            g_autoptr (GdkPixbuf) thumb_pixbuf = NULL;
            g_autofree gchar *thumbnail_path = NULL;

            thumbnail_path = g_build_filename (PUZZLE_SET_PREFIX,
                                               puzzle_set_config_get_id (config),
                                               thumbnail,
                                               NULL);
            thumb_pixbuf = xwd_pixbuf_load_from_gresource (puzzle_picker_get_resource (PUZZLE_PICKER (self)),
                                                           thumbnail_path);
            g_object_set (button,
                          "thumb-pixbuf", thumb_pixbuf,
                          NULL);
          }
#endif
        g_signal_connect (G_OBJECT (button), "puzzle-clicked",
                          G_CALLBACK (button_clicked_cb), self);
        gtk_grid_attach (GTK_GRID (self->button_grid),
                         button, column, row,
                         1, 1);
      }
}

/* Public Functions */


static gboolean
calculate_button_locked_status (PickerGrid *grid,
                                guint       column,
                                guint       row,
                                gfloat      percent)
{
  IpuzPuzzle *left_puzzle = NULL;
  IpuzPuzzle *above_puzzle = NULL;
  PuzzleSetModel *model;
  PuzzleSetModelRow *model_row;

  if (percent > 0.0)
    return FALSE;

  model = puzzle_picker_get_model (PUZZLE_PICKER (grid));

  /* Magic values */
  if (column > 0)
    {
      model_row = g_list_model_get_item (G_LIST_MODEL (model), row * grid->width + (column-1));
      left_puzzle = puzzle_set_model_row_get_puzzle (model_row);
    }

  if (row > 0)
    {
      model_row = g_list_model_get_item (G_LIST_MODEL (model), (row - 1) * grid->width + column);
      above_puzzle = puzzle_set_model_row_get_puzzle (model_row);
    }

  /* The top left button is always unlocked */
  if (row == 0 && column == 0)
    return FALSE;
  else if (left_puzzle && ipuz_crossword_game_won (IPUZ_CROSSWORD (left_puzzle)))
    return FALSE;
  else if (above_puzzle && ipuz_crossword_game_won (IPUZ_CROSSWORD (above_puzzle)))
    return FALSE;

  return TRUE;
}

static void
n_won_changed_cb (PickerGrid *self)
{
  guint row, column;
  PuzzleSetModel *model;
  PuzzleSetConfig *config;

  model = puzzle_picker_get_model (PUZZLE_PICKER (self));
  config = puzzle_picker_get_config (PUZZLE_PICKER (self));

  for (row = 0; row < self->height; row++)
    for (column = 0; column < self->width; column++)
      {
        PuzzleSetModelRow *model_row;
        GtkWidget *button;
        PuzzleButtonMode mode;
        gfloat percent;

        model_row = g_list_model_get_item (G_LIST_MODEL (model), row * self->width + column);
        if (model_row == NULL)
          break;

        button = gtk_grid_get_child_at (GTK_GRID (self->button_grid), column, row);
        percent = puzzle_set_model_row_get_percent (model_row);

        mode = PUZZLE_BUTTON_MODE_NORMAL;
        if (puzzle_set_config_get_grid_style (config) == CONFIG_GRID_LOCK_AND_IMAGE &&
            calculate_button_locked_status (self, column, row, percent))
          mode = PUZZLE_BUTTON_MODE_LOCKED;
        else if (puzzle_set_model_row_won (model_row))
          mode = PUZZLE_BUTTON_MODE_SOLVED;

        g_object_set (G_OBJECT (button),
                      "mode", mode,
                      "progress-fraction", percent,
                      "show-progress", (mode == PUZZLE_BUTTON_MODE_NORMAL),
                      NULL);
      }
}

/* Called after both config and model are set */
static void
picker_grid_load (PuzzlePicker *picker)
{
  PickerGrid *self;
  PuzzleSetModel *model;

  self = PICKER_GRID (picker);

  PUZZLE_PICKER_CLASS (picker_grid_parent_class)->load (picker);

  model = puzzle_picker_get_model (picker);

  picker_grid_load_header (PICKER_GRID (self));
  picker_grid_load_buttons (PICKER_GRID (self));

  self->n_won_handler = g_signal_connect_swapped (G_OBJECT (model), "won-changed", G_CALLBACK (n_won_changed_cb), self);
  n_won_changed_cb (self);
}
