/* word-solver.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "word-solver.h"
#include "word-solver-task.h"
#include "word-list.h"
#include "word-list-resources.h"


enum {
  FINISHED,
  N_SIGNALS
};

static guint obj_signals[N_SIGNALS] = { 0 };


struct _WordSolver
{
  GObject parent_object;
  WordSolverState state;

  gulong resource_changed_handler;

  /* List of the initial words in a puzzle. Currently, a hashtable of
   * gchar * as we are dealing with the strings lexically instead of
   * as WordIndexes. If we ever add a trie to WordIndex and we can
   * look up words in constant time, we can replace this with a
   * WordArray and use that universally.*/
  GHashTable *initial_word_table;

  /* These are the only structures modifiable by tasks. They persist
   * for the life of the word-solver. */
  gint count; /* Only use with g_atomic_int_* */
  GMutex solutions_mutex; /* For accessing solutions */
  GArray *solutions;

  /* These are set for each run and cleared when completed */
  GCancellable *cancellable;
  gboolean clear_on_finish; /* We've been reset, and no need to keep results around */
  GTask *task;

};


static void word_solver_init       (WordSolver      *self);
static void word_solver_class_init (WordSolverClass *klass);
static void word_solver_dispose    (GObject         *object);
static void word_solver_finalize   (GObject         *object);
static void resource_changed_cb    (WordSolver      *self);


G_DEFINE_TYPE (WordSolver, word_solver, G_TYPE_OBJECT);


static void
word_solver_init (WordSolver *self)
{
  self->state = WORD_SOLVER_READY;
  self->resource_changed_handler =
    g_signal_connect_swapped (WORD_LIST_RESOURCES_DEFAULT,
                              "resource-changed",
                              G_CALLBACK (resource_changed_cb),
                              self);

  g_mutex_init (&self->solutions_mutex);
}

static void
word_solver_class_init (WordSolverClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = word_solver_dispose;
  object_class->finalize = word_solver_finalize;

  obj_signals [FINISHED] =
    g_signal_new ("finished",
                  WORD_TYPE_SOLVER,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);
}

static void
word_solver_dispose (GObject *object)
{
  WordSolver *self;

  self = WORD_SOLVER (object);

  /* FIXME(threading): If we're running, we will not dispose
   * correctly */
  g_clear_object (&self->task);
  g_clear_object (&self->cancellable);
  g_clear_pointer (&self->initial_word_table, g_hash_table_unref);
  g_clear_signal_handler (&self->resource_changed_handler, WORD_LIST_RESOURCES_DEFAULT);

  G_OBJECT_CLASS (word_solver_parent_class)->dispose (object);
}

static void
word_solver_finalize (GObject *object)
{
  WordSolver *self;

  self = WORD_SOLVER (object);

  g_mutex_clear (&self->solutions_mutex);
  if (self->solutions)
    {
      g_array_free (self->solutions, TRUE);
      self->solutions = NULL;
    }

  G_OBJECT_CLASS (word_solver_parent_class)->finalize (object);
}

/* We call this if we are running and someone happens to change the
 * word-list resource under us. It's safe — the task will never hit a
 * mainloop in its thread so its word-list will never swap the data
 * under it. Nevertheless, we don't want to keep running on the old
 * data anyway.
*/
static void
resource_changed_cb (WordSolver *self)
{
  if (self->state == WORD_SOLVER_RUNNING)
    word_solver_cancel (self);
}

WordSolver *
word_solver_new (void)
{
  return g_object_new (WORD_TYPE_SOLVER, NULL);
}

static void
word_solver_task_complete (WordSolver   *solver,
                           GAsyncResult *res,
                           gpointer      user_data)
{
  if (solver->clear_on_finish)
    {
      g_array_set_size (solver->solutions, 0);
      solver->clear_on_finish = FALSE;
      solver->state = WORD_SOLVER_READY;
    }
  else
    {
      solver->state = WORD_SOLVER_COMPLETE;
    }
  g_clear_object (&solver->task);
  g_clear_object (&solver->cancellable);
  g_clear_pointer (&solver->initial_word_table, g_hash_table_unref);

  g_signal_emit (solver, obj_signals [FINISHED], 0);
}

static void
solutions_clear (IpuzGuesses **solution)
{
  g_clear_pointer (solution, ipuz_guesses_unref);
}

static void
initial_word_table_foreach (IpuzCrossword     *xword,
                            IpuzClueDirection  direction,
                            IpuzClue          *clue,
                            IpuzClueId        *clue_id,
                            gpointer           user_data)
{
  const GArray *cells;
  GHashTable *initial_word_table = (GHashTable *)user_data;
  GString *word;

  cells = ipuz_clue_get_cells (clue);
  if (cells == NULL)
    return;

  word = g_string_new (NULL);

  for (guint i = 0; i < cells->len; i++)
    {
      IpuzCellCoord coord;
      IpuzCell *cell;
      const gchar *solution;

      coord = g_array_index (cells, IpuzCellCoord, i);
      cell = ipuz_crossword_get_cell (xword, &coord);
      solution = ipuz_cell_get_solution (cell);
      if (solution == NULL || solution[0] == '\0')
        {
          g_string_free (word, TRUE);
          return;
        }
      g_string_append (word, solution);
    }

  g_hash_table_insert (initial_word_table,
                       g_string_free_and_steal (word),
                       GINT_TO_POINTER (TRUE));
}

static GHashTable *
calculate_initial_word_table (IpuzCrossword *xword)
{
  GHashTable *initial_word_table;

  initial_word_table =
    g_hash_table_new_full (g_str_hash, g_str_equal,
                           g_free, NULL);

  ipuz_crossword_foreach_clue (xword, initial_word_table_foreach, initial_word_table);

  return initial_word_table;
}

void
word_solver_run (WordSolver    *self,
                 IpuzCrossword *xword,
                 CellArray     *cell_array,
                 WordArray     *skip_list,
                 gint           max_solutions,
                 gint           min_priority)
{
  WordSolverTask *solver_task;

  g_return_if_fail (WORD_IS_SOLVER (self));
  g_return_if_fail (self->state != WORD_SOLVER_RUNNING);

  if (self->state == WORD_SOLVER_COMPLETE)
    word_solver_reset (self);

  self->state = WORD_SOLVER_RUNNING;
  self->count = 0;

  /* temporary assertions to make sure our assumptions are right */
  g_assert (self->initial_word_table == NULL);
  g_assert (self->solutions == NULL);
  g_assert (self->cancellable == NULL);
  g_assert (self->task == NULL);

  self->initial_word_table = calculate_initial_word_table (xword);

  self->solutions = g_array_new (FALSE, FALSE, sizeof (IpuzGuesses *));
  g_array_set_clear_func (self->solutions, (GDestroyNotify) solutions_clear);
  self->cancellable = g_cancellable_new ();

  solver_task = word_solver_task_new (&self->count,
                                      &self->solutions_mutex,
                                      self->solutions,
                                      xword,
                                      cell_array,
                                      self->initial_word_table,
                                      skip_list,
                                      max_solutions,
                                      min_priority);
  self->task = g_task_new (self, self->cancellable,
                           (GAsyncReadyCallback) word_solver_task_complete,
                           NULL);
  g_task_set_task_data (self->task, solver_task, g_object_unref);
  g_task_run_in_thread (self->task, (GTaskThreadFunc) word_solver_task_run);
}

void
word_solver_cancel (WordSolver *solver)
{
  g_return_if_fail (WORD_IS_SOLVER (solver));
  g_return_if_fail (solver->state == WORD_SOLVER_RUNNING);

  g_cancellable_cancel (solver->cancellable);
  solver->state = WORD_SOLVER_COMPLETE;
}

void
word_solver_reset (WordSolver *solver)
{
  g_return_if_fail (WORD_IS_SOLVER (solver));

  /* Make sure we cancel it */
  if (word_solver_get_state (solver) == WORD_SOLVER_RUNNING)
    {
      solver->clear_on_finish = TRUE;
      word_solver_cancel (solver);
    }
  else
    {
      g_clear_pointer (&solver->solutions, g_array_unref);
      solver->state = WORD_SOLVER_READY;
    }
}

WordSolverState
word_solver_get_state (WordSolver *solver)
{
  g_return_val_if_fail (WORD_IS_SOLVER (solver), WORD_SOLVER_READY);

  return solver->state;
}

gint
word_solver_get_count (WordSolver *solver)
{
  g_return_val_if_fail (WORD_IS_SOLVER (solver), 0);

  return g_atomic_int_get (&(solver->count));
}

guint
word_solver_get_n_solutions (WordSolver *solver)
{
  guint len;
  g_return_val_if_fail (WORD_IS_SOLVER (solver), 0);

  g_mutex_lock (&solver->solutions_mutex);
  if (solver->solutions == NULL)
    len = 0;
  else
    len = solver->solutions->len;
  g_mutex_unlock (&solver->solutions_mutex);

  return len;
}

IpuzGuesses *
word_solver_get_solution (WordSolver *solver,
                          guint       index)
{
  IpuzGuesses *guesses = NULL;

  g_return_val_if_fail (WORD_IS_SOLVER (solver), NULL);

  g_mutex_lock (&solver->solutions_mutex);
  if (index < solver->solutions->len)
    guesses = g_array_index (solver->solutions, IpuzGuesses *, index);
  g_mutex_unlock (&solver->solutions_mutex);

  return guesses;
}
