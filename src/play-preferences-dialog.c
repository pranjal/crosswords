/* play-preferences-dialog.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include <glib/gi18n.h>

#include "contrib/gnome-languages.h"
#include "crosswords-app.h"
#include "crosswords-quirks.h"
#include "play-preferences-dialog.h"
#include "puzzle-set.h"
#include "puzzle-set-list.h"


#define SOURCES_ROW_DATA "sources-row-data"
#define SHOWN_SWITCH_DATA "shown-switch-data"

enum
{
  DELETE_NEVER,
  DELETE_DAY,
  DELETE_WEEK,
  DELETE_MONTH,
};

typedef enum
{
  TAG_TYPE_LANGUAGE,
  TAG_TYPE_SIZE,
  TAG_TYPE_PUZZLE_TYPE,
  TAG_TYPE_AUTO_DOWNLOAD,
} TagType;

struct _PlayPreferencesDialog
{
  AdwPreferencesDialog parent_instance;

  GSettings *settings;

  /* Template widgets */
  GtkWidget *guess_behavior_row;
  GtkWidget *switch_on_move_switch;
  GtkWidget *hide_solved_puzzles_switch;
  GtkWidget *delete_solved_puzzles_row;
  GtkWidget *show_all_languages_switch;
  GtkWidget *shown_sources_list_box;
  GtkWidget *auto_download_switch;
};


static void       play_preferences_dialog_init          (PlayPreferencesDialog      *self);
static void       play_preferences_dialog_class_init    (PlayPreferencesDialogClass *klass);
static void       play_preferences_dialog_dispose       (GObject                    *object);
static gboolean   guess_behavior_row_get_mapping        (GValue                     *value,
                                                         GVariant                   *variant,
                                                         gpointer                    user_data);
static GVariant * guess_behavior_row_set_mapping        (const GValue               *value,
                                                         const GVariantType         *expected_type,
                                                         gpointer                    user_data);
static gboolean   delete_solved_puzzles_row_get_mapping (GValue                     *value,
                                                         GVariant                   *variant,
                                                         gpointer                    user_data);
static GVariant * delete_solved_puzzles_row_set_mapping (const GValue               *value,
                                                         const GVariantType         *expected_type,
                                                         gpointer                    user_data);
static void       shown_sources_row_activated_cb        (GtkListBox                 *self,
                                                         GtkListBoxRow              *row,
                                                         PlayPreferencesDialog      *dialog);
static GtkWidget* create_shown_sources_row              (PuzzleSet                  *puzzle_set,
                                                         PlayPreferencesDialog      *self);


G_DEFINE_TYPE (PlayPreferencesDialog, play_preferences_dialog, ADW_TYPE_PREFERENCES_DIALOG);


static void
play_preferences_dialog_init (PlayPreferencesDialog *self)
{
  GListModel *puzzle_set_list;
  GtkFilter *shown_sources_filter;
  g_autoptr (GtkFilterListModel) shown_sources_filter_model = NULL;

  gtk_widget_init_template (GTK_WIDGET (self));

#if DEVELOPMENT_BUILD
  gtk_widget_add_css_class (GTK_WIDGET (self), "devel");
#endif

  self->settings = g_settings_new ("org.gnome.Crosswords");

  /* Game Behavior */
  g_settings_bind_with_mapping (self->settings, "guess-advance-type",
                                self->guess_behavior_row, "selected",
                                G_SETTINGS_BIND_DEFAULT,
                                guess_behavior_row_get_mapping,
                                guess_behavior_row_set_mapping,
                                NULL, NULL);
  g_settings_bind (self->settings, "switch-on-move",
                   self->switch_on_move_switch, "active",
                   G_SETTINGS_BIND_DEFAULT);

  /* Solved Puzzles */
  g_settings_bind (self->settings, "hide-solved-puzzles",
                   self->hide_solved_puzzles_switch, "active",
                   G_SETTINGS_BIND_DEFAULT);
  g_settings_bind_with_mapping (self->settings, "delete-downloaded-puzzles",
                                self->delete_solved_puzzles_row, "selected",
                                G_SETTINGS_BIND_DEFAULT,
                                delete_solved_puzzles_row_get_mapping,
                                delete_solved_puzzles_row_set_mapping,
                                NULL, NULL);

  /* Puzzle Sources */
  g_settings_bind (self->settings, "show-all-languages",
                   self->show_all_languages_switch, "active",
                   G_SETTINGS_BIND_DEFAULT);

  puzzle_set_list = puzzle_set_list_get ();
  shown_sources_filter = puzzle_set_filter_new (PUZZLE_SET_FILTER_LANGUAGE,
                                                self->settings);
  /* Strangely, the filter_model takes ownership of the model and
   * filter, so we have to ref puzzle_set_list or it gets reaped.*/
  shown_sources_filter_model = gtk_filter_list_model_new (g_object_ref (puzzle_set_list),
                                                          shown_sources_filter);

  gtk_list_box_bind_model (GTK_LIST_BOX (self->shown_sources_list_box),
                           G_LIST_MODEL (shown_sources_filter_model),
                           (GtkListBoxCreateWidgetFunc) create_shown_sources_row,
                           self, NULL);

  g_settings_bind (self->settings, "auto-download-puzzle-sets",
                   self->auto_download_switch, "active",
                   G_SETTINGS_BIND_DEFAULT);
}

static void
play_preferences_dialog_class_init (PlayPreferencesDialogClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = play_preferences_dialog_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/play-preferences-dialog.ui");

  gtk_widget_class_bind_template_child (widget_class, PlayPreferencesDialog, guess_behavior_row);
  gtk_widget_class_bind_template_child (widget_class, PlayPreferencesDialog, switch_on_move_switch);
  gtk_widget_class_bind_template_child (widget_class, PlayPreferencesDialog, hide_solved_puzzles_switch);
  gtk_widget_class_bind_template_child (widget_class, PlayPreferencesDialog, delete_solved_puzzles_row);
  gtk_widget_class_bind_template_child (widget_class, PlayPreferencesDialog, show_all_languages_switch);
  gtk_widget_class_bind_template_child (widget_class, PlayPreferencesDialog, shown_sources_list_box);
  gtk_widget_class_bind_template_child (widget_class, PlayPreferencesDialog, auto_download_switch);

  gtk_widget_class_bind_template_callback (widget_class, shown_sources_row_activated_cb);
}

static void
play_preferences_dialog_dispose (GObject *object)
{
  PlayPreferencesDialog *self;

  self = PLAY_PREFERENCES_DIALOG (object);

  g_clear_object (&self->settings);

  G_OBJECT_CLASS (play_preferences_dialog_parent_class)->dispose (object);
}

static gboolean
guess_behavior_row_get_mapping (GValue   *value,
                                GVariant *variant,
                                gpointer  user_data)
{
  const gchar *advance;

  advance = g_variant_get_string (variant, NULL);

  if (! g_strcmp0 (advance, "adjacent"))
    g_value_set_uint (value, (guint) QUIRKS_GUESS_ADVANCE_ADJACENT);
  else if (! g_strcmp0 (advance, "open"))
    g_value_set_uint (value, (guint) QUIRKS_GUESS_ADVANCE_OPEN);
  else if (! g_strcmp0 (advance, "open-in-clue"))
    g_value_set_uint (value, (guint) QUIRKS_GUESS_ADVANCE_OPEN_IN_CLUE);
  else
    {
      g_warning ("Unknown advance type: %s", advance);
      return FALSE;
    }

  return TRUE;
}

static GVariant *
guess_behavior_row_set_mapping (const GValue       *value,
                                const GVariantType *expected_type,
                                gpointer            user_data)
{
  GVariant *retval = NULL;

  /* FIXME(enum): Maybe we should use the enum nick for this */
  switch (g_value_get_uint (value))
    {
    case QUIRKS_GUESS_ADVANCE_ADJACENT:
      retval = g_variant_new_string ("adjacent");
      break;
    case QUIRKS_GUESS_ADVANCE_OPEN:
      retval = g_variant_new_string ("open");
      break;
    case QUIRKS_GUESS_ADVANCE_OPEN_IN_CLUE:
      retval = g_variant_new_string ("open-in-clue");
      break;
    default:
      g_assert_not_reached ();
    }

  return retval;
}


static gboolean
delete_solved_puzzles_row_get_mapping (GValue   *value,
                                       GVariant *variant,
                                       gpointer  user_data)
{
  const gchar *delete_solved_puzzles;

  delete_solved_puzzles = g_variant_get_string (variant, NULL);

  if (! g_strcmp0 (delete_solved_puzzles, "never"))
    g_value_set_uint (value, (guint) DELETE_NEVER);
  else if (! g_strcmp0 (delete_solved_puzzles, "day"))
    g_value_set_uint (value, (guint) DELETE_DAY);
  else if (! g_strcmp0 (delete_solved_puzzles, "week"))
    g_value_set_uint (value, (guint) DELETE_WEEK);
  else if (! g_strcmp0 (delete_solved_puzzles, "month"))
    g_value_set_uint (value, (guint) DELETE_MONTH);
  else
    {
      g_warning ("Unknown delete_solved_puzzles type: %s", delete_solved_puzzles);
      return FALSE;
    }

  return TRUE;
}

static GVariant *
delete_solved_puzzles_row_set_mapping (const GValue       *value,
                                       const GVariantType *expected_type,
                                       gpointer            user_data)
{
  GVariant *retval = NULL;

  /* FIXME(enum): Maybe we should use the enum nick for this */
  switch (g_value_get_uint (value))
    {
    case DELETE_NEVER:
      retval = g_variant_new_string ("never");
      break;
    case DELETE_DAY:
      retval = g_variant_new_string ("day");
      break;
    case DELETE_WEEK:
      retval = g_variant_new_string ("week");
      break;
    case DELETE_MONTH:
      retval = g_variant_new_string ("month");
      break;
    default:
      g_assert_not_reached ();
    }

  return retval;
}

static void
shown_switch_toggled (GtkWidget             *shown_switch,
                      GParamSpec            *pspec,
                      PlayPreferencesDialog *self)
{
  GVariantBuilder builder;
  g_autoptr (GVariant) old_show_list = NULL;
  gboolean shown;
  const gchar *id;
  gboolean found_id = FALSE;

  old_show_list = g_settings_get_value (self->settings, "shown-puzzle-sets");
  shown = gtk_switch_get_state (GTK_SWITCH (shown_switch));
  id = g_object_get_data (G_OBJECT (shown_switch), SHOWN_SWITCH_DATA);

  g_variant_builder_init (&builder, G_VARIANT_TYPE_STRING_ARRAY);

  for (guint i = 0; i < g_variant_n_children (old_show_list); i++)
    {
      g_autoptr (GVariant) child = NULL;

      child = g_variant_get_child_value (old_show_list, i);
      if (g_strcmp0 (id, g_variant_get_string (child, NULL)) == 0)
        {
          if (!shown)
            found_id = TRUE;
          continue;
        }
      g_variant_builder_add (&builder, "s", g_variant_get_string (child, NULL));
    }
  /* We use found_id to guard against accidentally adding this
   * twice. */
  if (shown && !found_id)
    g_variant_builder_add (&builder, "s", id);

  g_settings_set_value (self->settings, "shown-puzzle-sets",
                        g_variant_builder_end (&builder));
}

void
add_tag_widget (GtkWidget *box,
                const gchar *label,
                TagType type)
{
  GtkWidget *tag;

  tag = g_object_new (GTK_TYPE_LABEL,
                      "label", label,
                      "css-name", "button",
                      "ellipsize", PANGO_ELLIPSIZE_END,
                      NULL);
  gtk_widget_add_css_class (tag, "tag");

  switch (type)
  {
    case TAG_TYPE_LANGUAGE:
      gtk_widget_add_css_class (tag, "language_tag");
      break;
    case TAG_TYPE_SIZE:
      gtk_widget_add_css_class (tag, "size_tag");
      break;
    case TAG_TYPE_PUZZLE_TYPE:
      gtk_widget_add_css_class (tag, "puzzle_type_tag");
      break;
    case TAG_TYPE_AUTO_DOWNLOAD:
      gtk_widget_add_css_class (tag, "auto_download_tag");
      break;
  }

  gtk_box_append (GTK_BOX(box), tag);
}

static GtkWidget *
create_subtitle_widget (PuzzleSet *puzzle_set)
{
  const char *locale;
  g_autofree gchar *language = NULL;
  ConfigSetTags tags;
  GtkWidget *hbox;

  hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 2);

  locale = puzzle_set_get_locale (puzzle_set);
  language = gnome_get_country_from_locale (locale, NULL);
  tags = puzzle_set_get_tags (puzzle_set);

  /* Set tag for language */
  if (language)
    add_tag_widget (hbox, language, TAG_TYPE_LANGUAGE);

  /* Set tags for size */
  if (tags & CONFIG_SET_TAGS_MINI)
    add_tag_widget (hbox, _("Mini"), TAG_TYPE_SIZE);

  if (tags & CONFIG_SET_TAGS_REGULAR)
    {
      if (tags & (CONFIG_SET_TAGS_MINI | CONFIG_SET_TAGS_JUMBO))
        add_tag_widget (hbox, _("Regular"), TAG_TYPE_SIZE);
    }

  if (tags & CONFIG_SET_TAGS_JUMBO)
    add_tag_widget (hbox, _("Jumbo"), TAG_TYPE_SIZE);

  /* Set tags for puzzle type */

  if (tags & CONFIG_SET_TAGS_ACROSTIC)
    add_tag_widget (hbox, _("Acrostic Puzzles"), TAG_TYPE_PUZZLE_TYPE);

  if (tags & CONFIG_SET_TAGS_ARROWWORD)
    add_tag_widget (hbox, _("Arrowword Puzzles"), TAG_TYPE_PUZZLE_TYPE);

  if (tags & CONFIG_SET_TAGS_BARRED)
    add_tag_widget (hbox, _("Barred Puzzles"), TAG_TYPE_PUZZLE_TYPE);

  if (tags & CONFIG_SET_TAGS_CROSSWORD)
    add_tag_widget (hbox, _("Crosswords"), TAG_TYPE_PUZZLE_TYPE);

  if (tags & CONFIG_SET_TAGS_CRYPTIC)
    add_tag_widget (hbox, _("Cryptic Crosswords"), TAG_TYPE_PUZZLE_TYPE);

  if (tags & CONFIG_SET_TAGS_FILIPPINE)
    add_tag_widget (hbox, _("Filippine"), TAG_TYPE_PUZZLE_TYPE);

  /* Create tag for auto download */
  if (puzzle_set_get_auto_download (puzzle_set))
    add_tag_widget (hbox, _("Automatic Downloader"), TAG_TYPE_AUTO_DOWNLOAD);

  if (gtk_widget_get_first_child (hbox) == NULL)
    {
      g_object_ref_sink (G_OBJECT (hbox));
      g_object_unref (G_OBJECT (hbox));
      return NULL;
    }
  return hbox;
}

static void
shown_sources_row_activated_cb (GtkListBox            *self,
                                GtkListBoxRow         *row,
                                PlayPreferencesDialog *dialog)
{
  GtkSwitch *shown_switch;

  shown_switch = (GtkSwitch *) g_object_get_data (G_OBJECT (row), SOURCES_ROW_DATA);

  gtk_switch_set_active (shown_switch, ! gtk_switch_get_active (shown_switch));
}

static GtkWidget *
create_shown_sources_row (PuzzleSet             *puzzle_set,
                          PlayPreferencesDialog *self)
{
  GtkWidget *hbox, *vbox;
  GtkWidget *shown_switch;
  GtkWidget *row;
  GtkWidget *label;
  GtkWidget *subtitle_widget;

  hbox = g_object_new (GTK_TYPE_BOX,
                       "orientation", GTK_ORIENTATION_HORIZONTAL,
                       "hexpand", TRUE,
                       "spacing", 8,
                       NULL);
  gtk_widget_add_css_class (hbox, "header");

  /* Set up the title and the tags*/
  vbox = g_object_new (GTK_TYPE_BOX,
                       "orientation", GTK_ORIENTATION_VERTICAL,
                       "hexpand", TRUE,
                       NULL);
  gtk_widget_add_css_class (vbox, "title");
  label = gtk_label_new (NULL);
  gtk_widget_set_vexpand (label, TRUE);
  gtk_label_set_xalign (GTK_LABEL (label), 0.0);
  gtk_widget_add_css_class (label, "title");
  gtk_box_append (GTK_BOX (vbox), label);
  subtitle_widget = create_subtitle_widget (puzzle_set);
  if (subtitle_widget)
    gtk_box_append (GTK_BOX (vbox), create_subtitle_widget (puzzle_set));
  gtk_box_append (GTK_BOX (hbox), vbox);

  /* set up the toggle switch */
  shown_switch = gtk_switch_new ();
  if (puzzle_set_get_shown (puzzle_set))
    {
      gtk_switch_set_state (GTK_SWITCH (shown_switch), TRUE);
      gtk_switch_set_active (GTK_SWITCH (shown_switch), TRUE);
    }
  gtk_widget_set_can_focus (shown_switch, FALSE);
  gtk_widget_set_valign (shown_switch, GTK_ALIGN_CENTER);
  g_object_set_data (G_OBJECT (shown_switch), SHOWN_SWITCH_DATA, (gpointer) puzzle_set_get_id (puzzle_set));
  g_signal_connect (shown_switch, "notify::state", G_CALLBACK (shown_switch_toggled), self);
  gtk_box_append (GTK_BOX (hbox), shown_switch);

  /* Set up the row */
  row = g_object_new (ADW_TYPE_PREFERENCES_ROW,
                      "title", puzzle_set_get_short_name (puzzle_set),
                      "activatable", true,
                      "child", hbox,
                      NULL);
  g_object_set_data (G_OBJECT (row), SOURCES_ROW_DATA, shown_switch);
  g_object_bind_property (row, "title",
                          label, "label",
                          G_BINDING_SYNC_CREATE);

  return row;
}

/* Public functions */

GtkWidget *
play_preferences_dialog_new (gboolean   display_shown_sources_page)
{
  GtkWidget *dialog;

  dialog = g_object_new (PLAY_TYPE_PREFERENCES_DIALOG, NULL);
  if (display_shown_sources_page)
    adw_preferences_dialog_set_visible_page_name (ADW_PREFERENCES_DIALOG (dialog),
                                                  "sources");

  return dialog;
}
