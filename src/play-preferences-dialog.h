/* play-preferences-dialog.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#include <adwaita.h>

G_BEGIN_DECLS


#define PLAY_TYPE_PREFERENCES_DIALOG (play_preferences_dialog_get_type())
G_DECLARE_FINAL_TYPE (PlayPreferencesDialog, play_preferences_dialog, PLAY, PREFERENCES_DIALOG, AdwPreferencesWindow);


GtkWidget *play_preferences_dialog_new (gboolean display_shown_sources_page);


G_END_DECLS
