/* grid-state.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once


#include <gtk/gtk.h>
#include <libipuz/libipuz.h>

#include "cell-array.h"
#include "crosswords-quirks.h"

G_BEGIN_DECLS

#define GRID_STATE_BLOCK_CHARS ".#"
#define GRID_STATE_SKIP_CHARS  "?"

#define GRID_STATE_DEHYDRATED(state) (state && state->xword == NULL)
#define GRID_STATE_HAS_USE_CURSOR(state) (state&&state->behavior&GRID_BEHAVIOR_USE_CURSOR)
#define GRID_STATE_HAS_SHOW_GUESS(state) (state&&state->behavior&GRID_BEHAVIOR_SHOW_GUESS)
#define GRID_STATE_HAS_SELECTABLE(state) (state&&state->behavior&GRID_BEHAVIOR_SELECTABLE)
#define GRID_STATE_HAS_EDIT_CELLS(state) (state&&state->behavior&GRID_BEHAVIOR_EDIT_CELLS)
#define GRID_STATE_HAS_NORMAL_ONLY(state) (state&&state->behavior&GRID_BEHAVIOR_NORMAL_ONLY)
#define GRID_STATE_HAS_QUIRKS_ADVANCE(state) (state&&state->behavior&GRID_BEHAVIOR_QUIRKS_ADVANCE)

#define GRID_STATE_CURSOR_SET(state) (GRID_STATE_HAS_USE_CURSOR(state) && \
                                      ((GRID_STATE_HAS_EDIT_CELLS(state) && !GRID_STATE_HAS_SHOW_GUESS(state)) || \
                                       state->clue.direction!=IPUZ_CLUE_DIRECTION_NONE))

#define NONE_HAS_FOCUS(state)               ((state != NULL) &&         \
		                             (state->quirks != NULL) && \
		                             (crosswords_quirks_get_focus_location(state->quirks)==QUIRKS_FOCUS_LOCATION_NONE))
#define ACROSTIC_MAIN_GRID_HAS_FOCUS(state) ((state != NULL) && \
		                             (IPUZ_IS_ACROSTIC (state->xword)) && \
		                             (state->quirks != NULL) && \
					     ((crosswords_quirks_get_focus_location(state->quirks)==QUIRKS_FOCUS_LOCATION_MAIN_GRID)))
#define ACROSTIC_CLUE_GRID_HAS_FOCUS(state) ((state != NULL) && \
		                             (IPUZ_IS_ACROSTIC (state->xword)) && \
		                             (state->quirks != NULL) && \
		                             (crosswords_quirks_get_focus_location(state->quirks)==QUIRKS_FOCUS_LOCATION_CLUE_GRID))
#define GET_QUOTE_CLUE(xword)               (IPUZ_IS_ACROSTIC(xword)?ipuz_acrostic_get_quote_clue(IPUZ_ACROSTIC(xword)):NULL)
#define CMD_KIND_DESTRUCTIVE(kind)          (kind==GRID_CMD_KIND_DELETE||kind==GRID_CMD_KIND_BACKSPACE)


typedef enum
{
  GRID_CMD_KIND_NONE,
  GRID_CMD_KIND_UP,
  GRID_CMD_KIND_DOWN,
  GRID_CMD_KIND_LEFT,
  GRID_CMD_KIND_RIGHT,
  GRID_CMD_KIND_FORWARD,
  GRID_CMD_KIND_BACK,
  GRID_CMD_KIND_FORWARD_EMPTYCELL,
  GRID_CMD_KIND_BACK_EMPTYCELL,
  GRID_CMD_KIND_FORWARD_CLUE,
  GRID_CMD_KIND_BACK_CLUE,
  GRID_CMD_KIND_BACKSPACE,
  GRID_CMD_KIND_DELETE,
  GRID_CMD_KIND_SWITCH,
} GridCmdKind;

typedef enum
{
  GRID_STATE_SOLVE,       /* @ Solve a crossword @ */
  GRID_STATE_BROWSE,      /* @ Browse through a board without modifying it @ */
  GRID_STATE_EDIT,        /* @ Edit the grid @ */
  GRID_STATE_EDIT_BROWSE, /* @ Browse through an editable board without modifying it @ */
  GRID_STATE_SELECT,      /* @ Select cells @ */
  GRID_STATE_VIEW,        /* @ Display a board with no interaction @ */
} GridStateMode;

typedef enum
{
  GRID_BEHAVIOR_USE_CURSOR = 1 << 0,
  GRID_BEHAVIOR_SHOW_GUESS = 1 << 1,
  GRID_BEHAVIOR_SELECTABLE = 1 << 2,
  GRID_BEHAVIOR_EDIT_CELLS = 1 << 3,
  GRID_BEHAVIOR_NORMAL_ONLY = 1 << 4,
  GRID_BEHAVIOR_QUIRKS_ADVANCE = 1 << 4,
} GridStateBehavior;


/* This defines each state mode in terms of the behavior it implements */
#define GRID_STATE_SOLVE_FLAGS (GRID_BEHAVIOR_USE_CURSOR|\
                                GRID_BEHAVIOR_SHOW_GUESS|\
                                GRID_BEHAVIOR_EDIT_CELLS|\
                                GRID_BEHAVIOR_NORMAL_ONLY|\
                                GRID_BEHAVIOR_QUIRKS_ADVANCE)
#define GRID_STATE_BROWSE_FLAGS (GRID_BEHAVIOR_USE_CURSOR|\
                                 GRID_BEHAVIOR_SHOW_GUESS|\
                                 GRID_BEHAVIOR_NORMAL_ONLY)
#define GRID_STATE_EDIT_FLAGS (GRID_BEHAVIOR_USE_CURSOR|\
                               GRID_BEHAVIOR_EDIT_CELLS)
#define GRID_STATE_EDIT_BROWSE_FLAGS (GRID_BEHAVIOR_USE_CURSOR|\
                                      GRID_BEHAVIOR_NORMAL_ONLY)
#define GRID_STATE_SELECT_FLAGS (GRID_BEHAVIOR_USE_CURSOR|\
                                 GRID_BEHAVIOR_SELECTABLE|\
                                 GRID_BEHAVIOR_NORMAL_ONLY)
#define GRID_STATE_VIEW_FLAGS (0)


typedef enum
{
  GRID_REVEAL_NONE,         /* @ Reveal nothing. Display the crossword as normal @ */
  GRID_REVEAL_ERRORS_BOARD, /* @ Reveal all incorrect guesses on the board @ */
  GRID_REVEAL_ERRORS_CLUE,  /* @ Reveal all incorrect guesses in the current clue @ */
  GRID_REVEAL_ERRORS_CELL,  /* @ Reveal all incorrect guesses in the current cell @ */
  GRID_REVEAL_ALL,          /* @ Reveal all unsolved letters. Effectively give up @ */
} GridRevealMode;

typedef enum
{
  GRID_SELECTION_SELECT,    /* @ Select a cell @ */
  GRID_SELECTION_TOGGLE,    /* @ Toggle selection @ */
  GRID_SELECTION_EXTEND,    /* @ Extend the selection @ */
} GridSelectionMode;

typedef struct
{
  GridStateBehavior behavior;
  IpuzCrossword *xword;

  /* Current keyboard state */
  IpuzCellCoord cursor;
  IpuzClueId clue; /* Current selected direction and clue ID, or NONE */

  /* Modifiers to the current display and behavior */
  GridRevealMode reveal_mode;
  CellArray *saved_selection;
  CellArray *selected_cells;
  CrosswordsQuirks *quirks;
} GridState;


GridState         *grid_state_new                (IpuzCrossword      *xword,
                                                  CrosswordsQuirks   *quirks,
                                                  GridStateMode       mode) G_GNUC_WARN_UNUSED_RESULT;
GridState         *grid_state_new_from_behavior  (IpuzCrossword      *xword,
                                                  CrosswordsQuirks   *quirks,
                                                  GridStateBehavior   behavior) G_GNUC_WARN_UNUSED_RESULT;
void               grid_state_free               (GridState         *state);
GridState         *grid_state_dehydrate          (GridState          *state);
GridState         *grid_state_hydrate            (GridState          *state,
                                                  IpuzCrossword      *xword,
                                                  CrosswordsQuirks   *quirk);
GridState         *grid_state_swap_xword         (GridState          *state,
                                                  IpuzCrossword      *xword);
GridState         *grid_state_replace            (GridState          *old_state,
                                                  GridState          *new_state) G_GNUC_WARN_UNUSED_RESULT;

GridState         *grid_state_change_mode        (GridState          *state,
                                                  GridStateMode       mode) G_GNUC_WARN_UNUSED_RESULT;
GridState         *grid_state_change_behavior    (GridState          *state,
                                                  GridStateBehavior   behavior) G_GNUC_WARN_UNUSED_RESULT;
GridState         *grid_state_clue_selected      (GridState          *state,
                                                  IpuzClue           *clue) G_GNUC_WARN_UNUSED_RESULT;
GridState         *grid_state_cell_selected      (GridState          *state,
                                                  IpuzCellCoord       coord) G_GNUC_WARN_UNUSED_RESULT;
GridState         *grid_state_do_command         (GridState          *state,
                                                  GridCmdKind         kind) G_GNUC_WARN_UNUSED_RESULT;
GridState         *grid_state_guess              (GridState          *state,
                                                  const gchar        *guess) G_GNUC_WARN_UNUSED_RESULT;
GridState         *grid_state_guess_at_cell      (GridState          *state,
                                                  const gchar        *guess,
                                                  IpuzCellCoord       coord) G_GNUC_WARN_UNUSED_RESULT;
GridState         *grid_state_guess_word         (GridState          *state,
                                                  IpuzCellCoord       coord,
                                                  IpuzClueDirection   direction,
                                                  const gchar        *word) G_GNUC_WARN_UNUSED_RESULT;
GridState         *grid_state_commit_pencil      (GridState          *state,
                                                  gboolean           *state_changed) G_GNUC_WARN_UNUSED_RESULT;
GridState         *grid_state_set_cell_type      (GridState          *state,
                                                  IpuzCellCoord       coord,
                                                  IpuzCellType        cell_type) G_GNUC_WARN_UNUSED_RESULT;
GridState         *grid_state_select_cell        (GridState          *state,
                                                  IpuzCellCoord       coord) G_GNUC_WARN_UNUSED_RESULT;
GridState         *grid_state_select_toggle      (GridState          *state,
                                                  IpuzCellCoord       coord) G_GNUC_WARN_UNUSED_RESULT;
GridState         *grid_state_select_drag_start  (GridState          *state,
                                                  IpuzCellCoord       anchor_coord,
                                                  IpuzCellCoord       new_coord,
                                                  GridSelectionMode   mode) G_GNUC_WARN_UNUSED_RESULT;
GridState         *grid_state_select_drag_update (GridState          *state,
                                                  IpuzCellCoord       anchor_coord,
                                                  IpuzCellCoord       new_coord,
                                                  GridSelectionMode   mode) G_GNUC_WARN_UNUSED_RESULT;
GridState         *grid_state_select_drag_end    (GridState          *state) G_GNUC_WARN_UNUSED_RESULT;
GridState         *grid_state_select_all         (GridState          *state) G_GNUC_WARN_UNUSED_RESULT;
GridState         *grid_state_select_none        (GridState          *state) G_GNUC_WARN_UNUSED_RESULT;
GridState         *grid_state_select_invert      (GridState          *state) G_GNUC_WARN_UNUSED_RESULT;
GridState         *grid_state_set_guesses        (GridState          *state,
                                                  IpuzGuesses        *guesses) G_GNUC_WARN_UNUSED_RESULT;
GridState         *grid_state_set_reveal_mode    (GridState          *state,
                                                  GridRevealMode      reveal_mode) G_GNUC_WARN_UNUSED_RESULT;
GridState         *grid_state_set_bars           (GridState          *state,
                                                  IpuzCellCoord       coord,
                                                  IpuzStyleSides      bars,
                                                  gboolean            update_cursor) G_GNUC_WARN_UNUSED_RESULT;
GridState         *grid_state_ensure_lone_style  (GridState          *state,
                                                  IpuzCellCoord       coord) G_GNUC_WARN_UNUSED_RESULT;


/* Helper functions */
GridStateBehavior  grid_state_mode_to_behavior   (GridStateMode mode);
void               grid_state_assert_equal       (const GridState   *a,
                                                  const GridState   *b);

void               grid_state_print              (const GridState   *state,
                                                  gboolean           print_members);

G_END_DECLS
