/* word-solver-misc.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>


G_BEGIN_DECLS


/* Size of the priority and enumeration sections in the mmapped file */
#define NIL_SIZE         1 /* bytes */
#define PRIORITY_SIZE    1
#define ENUMERATION_SIZE 2
#define WORD_OFFSET      (PRIORITY_SIZE+ENUMERATION_SIZE)


/* A quick reference into a WordList of any given word/priority. It's
 * not meaningful on its own, but it's convenient when manipulating a
 * lot of words. */
typedef struct
{
  gint length; /* Length of the word, in characters */
  gint index;  /* Index within word list length */
} WordIndex;


WordIndex *word_index_copy (WordIndex *word_index);
void       word_index_free (WordIndex *word_index);


/* A description of a filter fragment. Used to efficiently describe
 * something like "??A?. For this example, length is 4, position is 2,
 * and char_index is the index of A within the corresponding
 * Charset (0 in English) */

typedef struct
{
  gint length;      /* Length of the word */
  gint position;    /* Position of the character within the word */
  gint char_index;  /* location within the charset of the word (eg, 0-25 in English) */
} FilterFragment;

gsize word_index_fragment_index         (gint           min_word_length,
                                         gsize          charset_len,
                                         FilterFragment fragment);
guint word_index_calculate_letters_size (gint           min_word_length,
                                         gint           max_word_length,
                                         gsize          charset_len);


/* The hash function used for anagrams */
guint word_list_hash_func (const gchar *str);


/* WordArrays are a simple list of words represented as
 * WordIndex. They are unique: inserting a word multiple times results
 * in the word only existing once, and the array is always sorted. It
 * is used to keep a list of words we don't want to search through, as
 * well as used internally within the word-list.
 *
 * It's possible to use the GArray functions instead of the WordArray
 * functions, but if you do that, you need to keep the
 * uniqueness/sorted invariant true. The WordList does this directly
 * at times.
 *
 * It's not recommended that you use this structure unless you really
 * know what you're doing. It's not a user-friendly structure. But it
 * is useful.
 */
typedef GArray WordArray;

WordArray *word_array_new      (void);
WordArray *word_array_copy     (WordArray *src);
void       word_array_unref    (WordArray *word_array);
gboolean   word_array_add      (WordArray *word_array,
                                WordIndex  word_index);
gboolean   word_array_remove   (WordArray *word_array,
                                WordIndex  word_index);
gboolean   word_array_find     (WordArray *word_array,
                                WordIndex  word_index,
                                guint     *out);
#define word_array_len(wa)     (((GArray*)wa)->len)
#define word_array_index(wa,i) (g_array_index((GArray*)wa,WordIndex,i))
#define word_array_ref(wa)     (g_array_ref((GArray*)wa))

G_DEFINE_AUTOPTR_CLEANUP_FUNC(WordArray, word_array_unref)


G_END_DECLS
