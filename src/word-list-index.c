/* word-list-index.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include "word-list-index.h"

WordListIndex *
word_list_index_new (void)
{
  WordListIndex *index;

  index = g_new0 (WordListIndex, 1);

  index->words = g_array_new (FALSE, TRUE, sizeof (WordListSection));

  return index;
}


static void
word_list_index_append_section (WordListIndex *index,
                                JsonNode      *node)
{
  JsonArray *array;
  WordListSection section;

  array = json_node_get_array (node);

  section.word_len = json_array_get_int_element (array, 0);
  section.stride = json_array_get_int_element (array, 1);
  section.offset = json_array_get_int_element (array, 2);
  section.count = json_array_get_int_element (array, 3);

  g_array_append_val (index->words, section);
}

WordListIndex *
word_list_index_new_from_json (JsonNode *root)
{
  WordListIndex *index;
  JsonObject *obj;
  JsonArray *array;
  JsonNode *element;
  guint len;

  g_return_val_if_fail (root != NULL, NULL);
  g_return_val_if_fail (JSON_NODE_HOLDS_OBJECT (root), NULL);

  index = word_list_index_new ();
  obj = json_node_get_object (root);

  element = json_object_get_member (obj, "display-name");
  g_assert (element != NULL);
  index->display_name = json_node_dup_string (element);

  element = json_object_get_member (obj, "word-count");
  g_assert (element != NULL);
  index->word_count = (guint) json_node_get_int (element);

  element = json_object_get_member (obj, "charset");
  g_assert (element != NULL);
  const char *str = json_node_get_string (element);
  index->charset = ipuz_charset_deserialize (str);

  element = json_object_get_member (obj, "filterchar");
  g_assert (element != NULL);
  index->filterchar = json_node_dup_string (element);

  element = json_object_get_member (obj, "min-length");
  g_assert (element != NULL);
  index->min_length = json_node_get_int (element);

  element = json_object_get_member (obj, "max-length");
  g_assert (element != NULL);
  index->max_length = json_node_get_int (element);

  element = json_object_get_member (obj, "threshold");
  g_assert (element != NULL);
  index->threshold = json_node_get_int (element);

  element = json_object_get_member (obj, "letter-list-offset");
  g_assert (element != NULL);
  index->letter_list_offset = json_node_get_int (element);

  element = json_object_get_member (obj, "letter-index-offset");
  g_assert (element != NULL);
  index->letter_index_offset = json_node_get_int (element);

  element = json_object_get_member (obj, "enumerations-offset");
  g_assert (element != NULL);
  index->enumerations_offset = json_node_get_int (element);

  element = json_object_get_member (obj, "anagram-word-list-offset");
  g_assert (element != NULL);
  index->anagram_word_list_offset = json_node_get_int (element);

  element = json_object_get_member (obj, "anagram-hash-index-offset");
  g_assert (element != NULL);
  index->anagram_hash_index_offset = json_node_get_int (element);

  element = json_object_get_member (obj, "anagram-hash-index-length");
  g_assert (element != NULL);
  index->anagram_hash_index_length = json_node_get_int (element);

  element = json_object_get_member (obj, "words");
  g_assert (JSON_NODE_HOLDS_ARRAY (element));

  array = json_node_get_array (element);
  len = json_array_get_length (array);

  for (guint i = 0; i < len; i++)
    {
      JsonNode *section_node;

      section_node = json_array_get_element (array, i);
      g_assert (JSON_NODE_HOLDS_ARRAY (section_node));
      word_list_index_append_section (index, section_node);
    }

  return index;
}

void
word_list_index_free (WordListIndex *index)
{
  if (index == NULL)
    return;

  ipuz_charset_unref (index->charset);
  g_free (index->filterchar);
  g_array_unref (index->words);
  g_free (index);
}

WordListSection
word_list_index_get_section (WordListIndex *index,
                             gint           word_len)
{
  WordListSection error_section = {0, };
  error_section.word_len = -1;
  error_section.count = 0;

  for (guint i = 0; i < index->words->len; i++)
    {
      WordListSection section = g_array_index (index->words, WordListSection, i);
      if (section.word_len == word_len)
        return section;
    }

  return error_section;
}
