/* edit-acrostic-details.c
 *
 * Copyright 2024 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include <adwaita.h>
#include "crosswords-misc.h"
#include "edit-acrostic-details.h"


struct _EditAcrosticDetails
{
  GtkWidget parent_instance;
};


static void     edit_acrostic_details_init           (EditAcrosticDetails      *self);
static void     edit_acrostic_details_class_init     (EditAcrosticDetailsClass *klass);
static void     edit_acrostic_details_dispose        (GObject                  *object);


G_DEFINE_TYPE (EditAcrosticDetails, edit_acrostic_details, GTK_TYPE_WIDGET);


static void
edit_acrostic_details_init (EditAcrosticDetails *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

static void
edit_acrostic_details_class_init (EditAcrosticDetailsClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = edit_acrostic_details_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-acrostic-details.ui");

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
}

static void
edit_acrostic_details_dispose (GObject *object)
{
  GtkWidget *child;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  G_OBJECT_CLASS (edit_acrostic_details_parent_class)->dispose (object);
}

void
edit_acrostic_details_update (EditAcrosticDetails *self,
                              GridState           *grid_state)
{
}

void
edit_acrostic_details_commit_changes (EditAcrosticDetails *self)
{
}

