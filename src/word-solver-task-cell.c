/* word-solver-task.c
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "word-solver-task.h"
#include "word-list.h"


typedef struct
{
  IpuzCellCoord coord;
  guint level;
} WordSolverCellInfo;

struct _WordSolverTask
{
  GObject parent_object;

  WordList *word_list;

  IpuzCrossword *xword;
  gint max_solutions;
  gint min_priority;
  GCancellable *cancellable;
  gboolean clear_on_finish;

  gboolean backtrack;
  IpuzCellCoord backtrack_coord;

  GArray *open_cells;
  IpuzGuesses *overlay;
  GHashTable *skip_table;

  /* Owned by the WordSolver. */
  gint *count;  /* (atomic) */
  GMutex *solutions_mutex;
  GArray *solutions;
};


static void word_solver_task_init       (WordSolverTask      *self);
static void word_solver_task_class_init (WordSolverTaskClass *klass);
static void word_solver_task_dispose    (GObject             *object);


G_DEFINE_TYPE (WordSolverTask, word_solver_task, G_TYPE_OBJECT);


static void
word_solver_task_init (WordSolverTask *self)
{
  self->word_list = word_list_new ();
  self->max_solutions = -1;
  self->min_priority = 50;
}

static void
word_solver_task_class_init (WordSolverTaskClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = word_solver_task_dispose;
}

static void
word_solver_task_dispose (GObject *object)
{
  WordSolverTask *self;

  self = WORD_SOLVER_TASK (object);

  g_clear_object (&self->word_list);
  g_clear_object (&self->xword);
  g_clear_object (&self->cancellable);
  g_clear_pointer (&self->open_cells, g_array_unref);
  g_clear_pointer (&self->overlay, ipuz_guesses_unref);

  G_OBJECT_CLASS (word_solver_task_parent_class)->dispose (object);
}

static IpuzGuesses *
create_initial_overlay (IpuzCrossword *xword)
{
  IpuzGuesses *overlay;
  IpuzBoard *board;
  guint row, column;

  board = ipuz_crossword_get_board (xword);
  overlay = ipuz_guesses_new_from_board (board, FALSE);

  for (row = 0; row < ipuz_guesses_get_height (overlay); row++)
    {
      for (column = 0; column < ipuz_guesses_get_width (overlay); column++)
        {
          IpuzCellCoord coord = { .row = row, .column = column };
          IpuzCell *board_cell = ipuz_board_get_cell (board, &coord);
          const gchar *solution;

          if (IPUZ_CELL_IS_GUESSABLE (board_cell))
            {
              solution = ipuz_cell_get_solution (board_cell);
              if (solution)
                ipuz_guesses_set_guess (overlay,
                                        &coord,
                                        solution);
            }
          /* FIXME: Copy over the initial val */
        }
    }

  return overlay;
}

/* Unused for now */
void
shuffle_array (GArray *arr)
{
  if (arr->len == 0)
    return;

  for (guint i = 0; i < arr->len - 1; i++)
    {
      guint j = i + rand() / (RAND_MAX / (arr->len - i) + 1);
      WordSolverCellInfo swap = g_array_index (arr, WordSolverCellInfo, j);
      g_array_index (arr, WordSolverCellInfo, j) = g_array_index (arr, WordSolverCellInfo, i);
      g_array_index (arr, WordSolverCellInfo, i) = swap;
    }
}

static gint
open_cells_cmp (gconstpointer a,
                gconstpointer b)
{
  WordSolverCellInfo *info_a = (WordSolverCellInfo *) a;
  WordSolverCellInfo *info_b = (WordSolverCellInfo *) b;

  if (info_a->coord.column == info_b->coord.column)
    return info_a->coord.row - info_b->coord.row;
  return info_a->coord.column - info_b->coord.column;
}

/* Get a list of the cells in cell_array that still need filling */
static CellArray *
calculate_open_cells (CellArray   *cell_array,
                      IpuzGuesses *overlay)
{
  GArray *open_cells = g_array_new (FALSE, FALSE, sizeof (WordSolverCellInfo));

  for (guint i = 0; i < cell_array->len; i++)
    {
      IpuzCellCoord coord = cell_array_index (cell_array, i);
      WordSolverCellInfo cell_info = {
        .level = i,
      };
      const gchar *guess;

      guess = ipuz_guesses_get_guess (overlay, &coord);
      if (guess == NULL || guess[0] == '\0')
        {
          cell_info.coord = coord;
          g_array_append_val (open_cells, cell_info);
        }
    }

  g_array_sort (open_cells, open_cells_cmp);
  //shuffle_array (open_cells);

  return open_cells;
}

static gchar *
get_filter_for_cells (WordSolverTask *self,
                      const GArray   *cells,
                      IpuzCellCoord   intersecting_coord,
                      guint           *intersect_pos)
{
  GString *str;

  g_assert (intersect_pos != NULL);
  str = g_string_new (NULL);

  *intersect_pos = 0;

  for (guint i = 0; i < cells->len; i++)
    {
      IpuzCellCoord coord = g_array_index (cells, IpuzCellCoord, i);
      const gchar *guess;

      guess = ipuz_guesses_get_guess (self->overlay, &coord);
      if (guess == NULL)
        g_string_append (str, "?");
      else
        g_string_append (str, guess);

      if (ipuz_cell_coord_equal (&coord, &intersecting_coord))
        *intersect_pos = i;
    }
  return g_string_free (str, FALSE);
}

static void
calculate_backtrack_coord (WordSolverTask *self,
                           const GArray   *across_cells,
                           const GArray   *down_cells,
                           IpuzCellCoord   current_coord)
{
  WordSolverCellInfo target_cell_info;
  gint max_level = 10000;

  if (across_cells)
    {
      for (guint i = 0; i < across_cells->len; i++)
        {
          guint out;
          target_cell_info.coord = g_array_index (across_cells, IpuzCellCoord, i);
          if (ipuz_cell_coord_equal (&current_coord, &target_cell_info.coord))
            continue;

          if (ipuz_guesses_get_guess (self->overlay, &target_cell_info.coord) == NULL)
            continue;

          /*FIXME: We should store this in the guesses*/
          if (g_array_binary_search (self->open_cells,
                                     &target_cell_info,
                                     open_cells_cmp,
                                     &out))
            {
              WordSolverCellInfo found_cell_info =
                g_array_index (self->open_cells, WordSolverCellInfo, out);

              if ((gint) found_cell_info.level < max_level)
                {
                  max_level = found_cell_info.level;
                  self->backtrack = TRUE;
                  self->backtrack_coord = found_cell_info.coord;
                }
            }
        }
    }

  if (down_cells)
    {
      for (guint i = 0; i < down_cells->len; i++)
        {
          guint out;
          target_cell_info.coord = g_array_index (down_cells, IpuzCellCoord, i);
          if (ipuz_cell_coord_equal (&current_coord, &target_cell_info.coord))
            continue;

          if (ipuz_guesses_get_guess (self->overlay, &target_cell_info.coord) == NULL)
            continue;

          /*FIXME: We should store this in the guesses*/
          if (g_array_binary_search (self->open_cells,
                                     &target_cell_info,
                                     open_cells_cmp,
                                     &out))
            {
              WordSolverCellInfo found_cell_info =
                g_array_index (self->open_cells, WordSolverCellInfo, out);

              if ((gint) found_cell_info.level > max_level)
                {
                  max_level = found_cell_info.level;
                  self->backtrack = TRUE;
                  self->backtrack_coord = found_cell_info.coord;
                }
            }
        }
    }
#if 0
  if (self->backtrack)
    {
      ipuz_guesses_print (self->overlay);
      g_print ("found_cell_info: (%u %u)\n", self->backtrack_coord.row, self->backtrack_coord.column);
    }
#endif
}

static gchar *
add_guess_to_filter (const gchar *filter,
                     const gchar *guess)
{
  GString *str;
  const gchar *ptr;
  const gchar *wildcard = NULL;

  if (filter == NULL)
    return NULL;

  g_assert (guess != NULL);

  for (ptr = filter; *ptr; ptr = g_utf8_next_char (ptr))
    {
      if (*ptr == '?')
        {
          if (wildcard)
            return NULL;
          wildcard = ptr;
        }
    }

  if (wildcard == NULL)
    return NULL;

  /* wildcard now points to the '?' and ptr to the end of the string */
  str = g_string_new (NULL);
  g_string_append_len (str, filter, (gssize) (wildcard - filter));
  g_string_append (str, guess);
  g_string_append_len (str, wildcard + 1, (gssize) (ptr - wildcard));

  return g_string_free_and_steal (str);
}

/* Returns TRUE if we should skip the word, FALSE, otherwise. */
static gboolean
add_or_skip_word (WordSolverTask *self,
                  const gchar    *word)
{
  if (word == NULL)
    return FALSE;

  if (g_hash_table_contains (self->skip_table, word))
    return TRUE;

  g_hash_table_insert (self->skip_table, (gpointer)word, GINT_TO_POINTER (TRUE));
  return FALSE;
}

static void
clean_up_word (WordSolverTask *self,
               const gchar    *word)
{
  if (word == NULL)
    return;

  g_hash_table_remove (self->skip_table, word);
}

static gint
charset_array_sort_func (IpuzCharsetIterValue *a,
                         IpuzCharsetIterValue *b)
{
  return ((gint)b->count) - ((gint)a->count);
}

static void
word_solver_task_run_helper (WordSolverTask *self,
                             guint           current_cell)
{
  WordSolverCellInfo cell_info;
  IpuzCell *cell;
  IpuzClue *across_clue;
  IpuzClue *down_clue;
  g_autofree gchar *across_filter = NULL;
  g_autofree gchar *down_filter = NULL;
  g_autoptr (IpuzCharset) intersect_chars = NULL;
  guint across_pos = 0;
  guint down_pos = 0;
  g_autoptr (GArray) charset_array_hack = NULL;
  const GArray *across_cells = NULL;
  const GArray *down_cells = NULL;
  IpuzCharsetIter *iter;

  g_assert (self != NULL);
  g_assert (current_cell <= self->open_cells->len);
  /* if we need to backtrack, we shouldn't be calling this function */
  g_assert (!self->backtrack);

  if (g_cancellable_is_cancelled (self->cancellable))
    return;

  /* We found a solution! Add the current overlay to the solutions
   * queue. */
  if (current_cell == self->open_cells->len)
    {
      gint len; /* int, not guint */

      g_atomic_int_inc (self->count);

      g_mutex_lock (self->solutions_mutex);
      if (!g_cancellable_is_cancelled (self->cancellable))
        {
          IpuzGuesses *new_overlay;
          new_overlay = ipuz_guesses_copy (self->overlay);
          g_array_append_val (self->solutions, new_overlay);
        }
      len = (gint) self->solutions->len;
      if (self->max_solutions >= 0 &&
          self->max_solutions <= len)
        g_cancellable_cancel (self->cancellable);
      g_mutex_unlock (self->solutions_mutex);

      return;
    }

  cell_info = g_array_index (self->open_cells, WordSolverCellInfo, current_cell);
  /* REMOVE WHEN CONFIDENT */
  g_assert (ipuz_guesses_get_guess (self->overlay, &cell_info.coord) == NULL);

  cell = ipuz_crossword_get_cell (self->xword, &cell_info.coord);

  across_clue = (IpuzClue *) ipuz_cell_get_clue (cell, IPUZ_CLUE_DIRECTION_ACROSS);
  if (across_clue)
    across_cells = ipuz_clue_get_cells (across_clue);
  down_clue = (IpuzClue *) ipuz_cell_get_clue (cell, IPUZ_CLUE_DIRECTION_DOWN);
  if (down_clue)
    down_cells = ipuz_clue_get_cells (down_clue);

  if (across_clue)
    across_filter = get_filter_for_cells (self, across_cells,
                                          cell_info.coord, &across_pos);
  if (down_clue)
    down_filter = get_filter_for_cells (self, down_cells,
                                        cell_info.coord, &down_pos);

  word_list_find_intersection(self->word_list,
                              across_filter, across_pos,
                              down_filter, down_pos,
                              &intersect_chars,
                              NULL, NULL);

#if 0
  // Debug printing
  for (guint i = 0; i < current_cell; i++)
    g_print (" ");
  g_print ("(%u, %u) %s × %s\n",
           cell_info.coord.row, cell_info.coord.column,
           across_filter, down_filter);
  for (guint i = 0; i < current_cell; i++)
    g_print (" ");
  size_t index = 0;
  while (index < ipuz_charset_get_n_values (intersect_chars))
    {
      IpuzCharsetIterValue val;

      val = ipuz_charset_get_value (intersect_chars, index);
      g_print ("(%c,%u)", (char) val.c, val.count);
      index++;
      if (index < ipuz_charset_get_n_values (intersect_chars))
        g_print (", ");
    }
  g_print ("\n");
#endif

  /* We hit a dead end. We need to backtrack to the intersecting cell
   * that's the oldest */
  if (ipuz_charset_get_n_chars (intersect_chars) == 0)
    {
      calculate_backtrack_coord (self, across_cells, down_cells, cell_info.coord);
      g_atomic_int_inc (self->count);
      return;
    }

  /* Temporary code to sort the charset by count. This should be done
   * in the charset itself */
  charset_array_hack = g_array_new (FALSE, FALSE, sizeof (IpuzCharsetIterValue));
  for (size_t index = 0;
       index < ipuz_charset_get_n_values (intersect_chars);
       index++)
    {
      IpuzCharsetIterValue value = ipuz_charset_get_value (intersect_chars, index);
      g_array_append_val (charset_array_hack, value);
    }

  g_array_sort (charset_array_hack,
                (GCompareFunc) charset_array_sort_func);


  /* This is the loop that tries all the characters */
  for (guint i = 0; i < charset_array_hack->len; i++)
    {
      IpuzCharsetIterValue val;
      gchar guess[7];
      gint len;
      g_autofree gchar *across_word = NULL;
      g_autofree gchar *down_word = NULL;

      if (g_cancellable_is_cancelled (self->cancellable))
        return;

      val = g_array_index (charset_array_hack, IpuzCharsetIterValue, i);

      /* figure out what character we want to try */
      len = g_unichar_to_utf8 (val.c, guess);
      guess[len] = '\0';

      /* Check to see if placing this character finishes a word. If it
       * does, we need to either add it to our skip_word_list, or  */
      across_word = add_guess_to_filter (across_filter, guess);
      down_word = add_guess_to_filter (down_filter, guess);

      if (add_or_skip_word (self, across_word))
        continue;
      if (add_or_skip_word (self, down_word))
        continue;

      /* Try this guess and recurse */
      ipuz_guesses_set_guess (self->overlay, &cell_info.coord, guess);
      word_solver_task_run_helper (self, current_cell + 1);

      /* Now that we've tried that word, remove it */
      clean_up_word (self, across_word);
      clean_up_word (self, down_word);

      /* Do we need to backtrack ? */
      if (self->backtrack)
        {
          ipuz_guesses_set_guess (self->overlay, &cell_info.coord, NULL);

          if (! ipuz_cell_coord_equal (&cell_info.coord, &self->backtrack_coord))
            {
              return;
            }
          self->backtrack = FALSE;
        }
    }

  /* Undo what we did */
  ipuz_guesses_set_guess (self->overlay, &cell_info.coord, NULL);
}

static GHashTable *
create_skip_table (WordSolverTask *self,
                   GHashTable     *initial_word_table,
                   WordArray      *skip_list)
{
  GHashTable *skip_table;
  GHashTableIter iter;
  gpointer key, value;

  /* NOTE: All the strings we add to the hash table are owned
   * externally. The strings from the parent word-solver hashtable are
   * never cleared while the task is running. The words from the
   * skip_list come from the WordList. And the transient words are
   * used then cleared before being freed. Kinda weird, but means we
   * don't need a g_hash_table_new_full() at this time. If we ever
   * start bouncing around the stack or any other weirdness, we should
   * reassess.
   */
  skip_table =
    g_hash_table_new (g_str_hash, g_str_equal);

  g_hash_table_iter_init (&iter, initial_word_table);
  while (g_hash_table_iter_next (&iter, &key, &value))
    g_hash_table_insert (skip_table, key, value);

  for (guint i = 0; i < skip_list->len; i++)
    {
      WordIndex index;
      const gchar *key;

      index = word_array_index (skip_list, i);
      key = word_list_get_indexed_word (self->word_list, index);
      
      g_hash_table_insert (skip_table,
                           (gpointer) key,
                           GINT_TO_POINTER (TRUE));
    }

  return skip_table;
}


WordSolverTask *
word_solver_task_new (gint           *count,
                      GMutex         *solutions_mutex,
                      GArray         *solutions,
                      IpuzCrossword  *xword,
                      CellArray      *selected_cells,
                      GHashTable     *initial_word_table,
                      WordArray      *skip_list,
                      gint            max_solutions,
                      gint            min_priority)
{
  WordSolverTask *self;

  self = g_object_new (WORD_TYPE_SOLVER_TASK, NULL);
  self->count = count;
  self->solutions_mutex = solutions_mutex;
  self->solutions = solutions;
  self->max_solutions = max_solutions;
  self->min_priority = min_priority;

  /* We count on the xword not changing while we're running. */
  self->xword = g_object_ref (xword);
  self->overlay = create_initial_overlay (self->xword);
  self->open_cells = calculate_open_cells (selected_cells, self->overlay);
  self->skip_table = create_skip_table (self, initial_word_table, skip_list);

  return self;
}

/* This is run in a separate thread */
void
word_solver_task_run (GTask          *task,
                      WordSolver     *solver,
                      WordSolverTask *self,
                      GCancellable   *cancellable)
{
  g_return_if_fail (WORD_IS_SOLVER_TASK (self));

  self->cancellable = g_object_ref (cancellable);

  /* We only run if we have any cells to solve for */
  if (self->open_cells->len > 0)
    word_solver_task_run_helper (self, 0);

  g_task_return_pointer (task, NULL, NULL);
}
