/* edit-window-private.h
 *
 * Copyright 2023 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <adwaita.h>
#include "edit-state.h"
#include "edit-window.h"
#include "puzzle-stack.h"


G_BEGIN_DECLS


struct _EditWindow
{
  AdwApplicationWindow parent_instance;

  PuzzleStack *puzzle_stack;
  gulong changed_id;
  gchar *uri;

  EditState *edit_state;

  GtkWidget *split_view;
  GtkWidget *modified_label;
  GtkWidget *subtitle_label;
  GtkWidget *edit_menu_button;
  GtkWidget *grid_toggle;
  GtkWidget *clues_toggle;
  GtkWidget *style_toggle;
  GtkWidget *metadata_toggle;
  GtkWidget *main_paned;
  GtkWidget *main_stack;
  GtkWidget *sidebar_stack;
  GtkWidget *info_stack;

  /* Grid */
  GtkWidget *grid_switcher_bar;
  GtkWidget *grid_stack;
  GtkWidget *main_grid;
  GtkWidget *grid_preview;
  GtkWidget *symmetry_widget;
  GtkWidget *cell_widget;
  GtkWidget *word_list_widget;
  GtkWidget *autofill_details;
  GtkWidget *acrostic_details;
  GtkWidget *edit_grid_widget;
  GtkWidget *letter_histogram_group;
  GtkWidget *letter_histogram_widget;
  GtkWidget *cluelen_histogram_group;
  GtkWidget *cluelen_histogram_widget;

  /* Clues */
  GtkWidget *clue_details_widget;
  GtkWidget *clue_list_widget;
  GtkWidget *clue_info_anagram;
  GtkWidget *clue_info_odd;
  GtkWidget *clue_info_even;

  /* Style */
  GtkWidget *style_preview;
  GtkWidget *style_shapebg_row;
  GtkWidget *style_foreground_color_row;
  GtkWidget *style_background_color_row;

  /* Metadata */
  GtkWidget *edit_metadata;
};


void _edit_window_class_actions_init   (GtkWidgetClass        *widget_class);
void _edit_window_class_controls_init  (GtkWidgetClass        *widget_class);
void _edit_window_puzzle_changed_cb    (EditWindow            *self,
                                        PuzzleStackOperation   operation,
                                        PuzzleStackChangeType  change_type,
                                        PuzzleStack           *puzzle_stack);
void _win_grid_actions_commit_pencil   (EditWindow            *self,
                                        const gchar           *action_name,
                                        GVariant              *param);
void _edit_window_control_update_stage (EditWindow            *self);
void _edit_window_set_stage            (EditWindow            *self,
                                        PuzzleStackStage       stage);


G_END_DECLS
