/* puzzle-set.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <adwaita.h>
#include <libipuz/libipuz.h>

#include "crosswords-misc.h"
#include "puzzle-picker.h"
#include "puzzle-set.h"


enum
{
  PROP_0,
  PROP_CONFIG,
  PROP_MODEL,
  N_PROPS
};

enum {
  PUZZLE_SELECTED,
  START_DOWNLOAD,
  N_SIGNALS
};

enum {
  RESPONSE_CANCEL,
  RESPONSE_VIEW,
  RESPONSE_CLEAR,
};

static GParamSpec *obj_props[N_PROPS] = {NULL, };
static guint obj_signals[N_SIGNALS] = { 0 };


#define DOMAIN_DOWNLOADS     "downloads"
#define DOMAIN_SAVED_PUZZLES "saved-puzzles"


typedef struct
{
  PuzzleSetConfig *config;
  PuzzleSetModel *model;

  /* Clear dialog information */
  gchar *clear_puzzle;
  gboolean clear_play_when_done;
} PuzzlePickerPrivate;


static void         puzzle_picker_init                      (PuzzlePicker       *self);
static void         puzzle_picker_class_init                (PuzzlePickerClass  *klass);
static void         puzzle_picker_set_property              (GObject            *object,
                                                             guint               prop_id,
                                                             const GValue       *value,
                                                             GParamSpec         *pspec);
static void         puzzle_picker_get_property              (GObject            *object,
                                                             guint               prop_id,
                                                             GValue             *value,
                                                             GParamSpec         *pspec);
static void         puzzle_picker_dispose                   (GObject            *object);
static void         puzzle_picker_real_load                 (PuzzlePicker       *picker);
static const gchar *puzzle_picker_real_get_title            (PuzzlePicker       *picker);
static void         puzzle_picker_real_set_downloader_state (PuzzlePicker       *picker,
                                                             DownloaderState     state);


G_DEFINE_TYPE_WITH_PRIVATE (PuzzlePicker, puzzle_picker, GTK_TYPE_WIDGET);


static void
puzzle_picker_init (PuzzlePicker *self)
{
  /* Pass */
}

static void
puzzle_picker_class_init (PuzzlePickerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = puzzle_picker_get_property;
  object_class->set_property = puzzle_picker_set_property;
  object_class->dispose = puzzle_picker_dispose;
  klass->load = puzzle_picker_real_load;
  klass->get_title = puzzle_picker_real_get_title;
  klass->set_downloader_state = puzzle_picker_real_set_downloader_state;

  obj_props[PROP_CONFIG] =
    g_param_spec_object ("config",
                         "Config",
                         "Config information",
                         PUZZLE_TYPE_SET_CONFIG,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT);

  obj_props[PROP_MODEL] =
    g_param_spec_object ("model",
                         "Model",
                         "Puzzle Set Model",
                         PUZZLE_TYPE_SET_MODEL,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);

  obj_signals[PUZZLE_SELECTED] =
    g_signal_new ("puzzle-selected",
                  PUZZLE_TYPE_PICKER,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  G_TYPE_STRING);

  obj_signals[START_DOWNLOAD] =
    g_signal_new ("start-download",
                  PUZZLE_TYPE_PICKER,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);
}

static void
puzzle_picker_set_property (GObject           *object,
                            guint              prop_id,
                            const GValue      *value,
                            GParamSpec        *pspec)
{
  PuzzlePickerPrivate *priv;

  priv = puzzle_picker_get_instance_private (PUZZLE_PICKER (object));

  switch (prop_id)
    {
    case PROP_CONFIG:
      priv->config = (PuzzleSetConfig *) g_value_dup_object (value);
      if (priv->model)
        puzzle_picker_load (PUZZLE_PICKER (object));
      break;
    case PROP_MODEL:
      priv->model = (PuzzleSetModel *) g_value_dup_object (value);
      if (priv->config)
        puzzle_picker_load (PUZZLE_PICKER (object));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }

}

static void
puzzle_picker_get_property (GObject           *object,
                            guint              prop_id,
                            GValue            *value,
                            GParamSpec        *pspec)
{
  PuzzlePickerPrivate *priv;

  priv = puzzle_picker_get_instance_private (PUZZLE_PICKER (object));

  switch (prop_id)
    {
    case PROP_CONFIG:
      g_value_set_object (value, priv->config);
      break;
    case PROP_MODEL:
      g_value_set_object (value, priv->model);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }

}

static void
puzzle_picker_dispose (GObject *object)
{
  PuzzlePickerPrivate *priv;

  priv = puzzle_picker_get_instance_private (PUZZLE_PICKER (object));

  g_clear_object (&priv->config);
  g_clear_object (&priv->model);
  g_clear_pointer (&priv->clear_puzzle, g_free);

  G_OBJECT_CLASS (puzzle_picker_parent_class)->dispose (object);
}

static void
puzzle_picker_real_load (PuzzlePicker *picker)
{
  /* Pass */
}

static const gchar *
puzzle_picker_real_get_title (PuzzlePicker *picker)
{
  /* Maybe customize later, but for now use the ShortName */
  return NULL;
}

static void
puzzle_picker_real_set_downloader_state (PuzzlePicker    *self,
                                         DownloaderState  state)
{
  /* Pass */
}

/* Public Methods */

const gchar *
puzzle_picker_get_title (PuzzlePicker *picker)
{
  PuzzlePickerClass *klass;

  g_return_val_if_fail (PUZZLE_IS_PICKER (picker), NULL);

  klass = (PuzzlePickerClass *) G_OBJECT_GET_CLASS (picker);

  return (klass)->get_title (picker);
}

static void
restart_puzzle_dialog_response (GtkDialog    *dialog,
                                const gchar  *response,
                                PuzzlePicker *picker)
{
  PuzzlePickerPrivate *priv;

  priv = puzzle_picker_get_instance_private (picker);
  g_return_if_fail (priv->clear_puzzle != NULL);

  if (g_strcmp0 (response, "clear") == 0)
    {
      guint index = 0;
      PuzzleSetModelRow *row;
      IpuzPuzzle *puzzle;
      IpuzGuesses *guesses;
      IpuzBoard *board;

      if (! puzzle_set_model_find_by_puzzle_name (priv->model,
                                                  priv->clear_puzzle,
                                                  &index))
        {
          g_warning ("Trying to clear a puzzle that's no longer in the model: %s\n", priv->clear_puzzle);
          return;
        }

      row = g_list_model_get_item (G_LIST_MODEL (priv->model),
                                   index);
      puzzle = puzzle_set_model_row_get_puzzle (row);
      board = ipuz_crossword_get_board (IPUZ_CROSSWORD (puzzle));
      guesses = ipuz_guesses_new_from_board (board, FALSE);
      puzzle_set_model_row_set_guesses (row, guesses);

      if (priv->clear_play_when_done)
        puzzle_picker_puzzle_selected (picker, priv->clear_puzzle);
    }
  else if (g_strcmp0 (response, "view") == 0)
    {
      priv->clear_play_when_done = FALSE;
      puzzle_picker_puzzle_selected (picker, priv->clear_puzzle);
    }
  else if (g_strcmp0 (response, "cancel") == 0)
    {
      priv->clear_play_when_done = FALSE;
    }

  g_clear_pointer (&priv->clear_puzzle, g_free);
  gtk_window_destroy (GTK_WINDOW (dialog));
}

void
puzzle_picker_clear_puzzle (PuzzlePicker *picker,
                            const gchar  *puzzle_name,
                            const gchar  *primary,
                            const gchar  *secondary,
                            gboolean      play_when_done,
                            gboolean      allow_view)
{
  PuzzlePickerPrivate *priv;
  GtkWidget *dialog;
  GtkRoot *root;

  priv = puzzle_picker_get_instance_private (picker);

  if (primary == NULL)
    primary = _("Clear puzzle?");
  root = gtk_widget_get_root (GTK_WIDGET (picker));

  dialog = adw_message_dialog_new (GTK_WINDOW (root),
                                   primary,
                                   secondary);
  adw_message_dialog_set_body_use_markup (ADW_MESSAGE_DIALOG (dialog), TRUE);
  adw_message_dialog_add_response (ADW_MESSAGE_DIALOG (dialog),
                                   "cancel", _("_Cancel"));
  if (allow_view)
    adw_message_dialog_add_response (ADW_MESSAGE_DIALOG (dialog),
                                     "view", _("_View"));
  adw_message_dialog_add_response (ADW_MESSAGE_DIALOG (dialog),
                                   "clear", _("_Clear"));

  adw_message_dialog_set_response_appearance (ADW_MESSAGE_DIALOG (dialog), "clear", ADW_RESPONSE_DESTRUCTIVE);

  adw_message_dialog_set_default_response (ADW_MESSAGE_DIALOG (dialog), "cancel");
  adw_message_dialog_set_close_response (ADW_MESSAGE_DIALOG (dialog), "cancel");

  g_clear_pointer (&priv->clear_puzzle, g_free);
  priv->clear_puzzle = g_strdup (puzzle_name);
  priv->clear_play_when_done = play_when_done;

  g_signal_connect (dialog, "response",
                    G_CALLBACK (restart_puzzle_dialog_response),
                    picker);

  gtk_window_present (GTK_WINDOW (dialog));
}

/* Public Methods */

void
puzzle_picker_load (PuzzlePicker *picker)
{
  PuzzlePickerClass *klass;

  g_return_if_fail (PUZZLE_IS_PICKER (picker));

  klass = (PuzzlePickerClass *) G_OBJECT_GET_CLASS (picker);

  (klass)->load (picker);
}

void
puzzle_picker_set_downloader_state (PuzzlePicker    *picker,
                                    DownloaderState  state)
{
  PuzzlePickerClass *klass;

  g_return_if_fail (PUZZLE_IS_PICKER (picker));

  klass = (PuzzlePickerClass *) G_OBJECT_GET_CLASS (picker);

  (klass)->set_downloader_state (picker, state);
}

void
puzzle_picker_puzzle_selected (PuzzlePicker *picker,
                               const gchar  *puzzle_name)
{
  g_return_if_fail (PUZZLE_IS_PICKER (picker));

  g_signal_emit (picker, obj_signals[PUZZLE_SELECTED], 0,
                 puzzle_name);
}

void
puzzle_picker_start_download (PuzzlePicker *picker)
{
  g_return_if_fail (PUZZLE_IS_PICKER (picker));

  g_signal_emit (picker, obj_signals[START_DOWNLOAD], 0);
}

PuzzleSetConfig *
puzzle_picker_get_config (PuzzlePicker *picker)
{
  PuzzlePickerPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_PICKER (picker), NULL);

  priv = puzzle_picker_get_instance_private (picker);

  return priv->config;
}

PuzzleSetModel *
puzzle_picker_get_model (PuzzlePicker *picker)
{
  PuzzlePickerPrivate *priv;

  g_return_val_if_fail (PUZZLE_IS_PICKER (picker), NULL);

  priv = puzzle_picker_get_instance_private (picker);

  return priv->model;
}
