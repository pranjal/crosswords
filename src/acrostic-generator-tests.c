/* acrostic-generator-tests.c
 *
 * Copyright 2023 Tanmay Patil <tanmaynpatil105@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include <locale.h>
#include <libipuz/libipuz.h>
#include "acrostic-generator.h"
#include "test-words-resources.h"
#include "word-list.h"


static AcrosticGenerator *
create_test_generator (void)
{
  GResource *resource = wordlist_get_resource ();
  g_autoptr (WordList) word_list = NULL;
  g_autoptr (GBytes) bytes = NULL;
  IpuzCharsetBuilder *builder;
  g_autoptr (IpuzCharset) charset;

  bytes = g_resource_lookup_data (resource,
                                  "/org/gnome/Crosswords/word-list/test-words.dict",
                                  0, NULL);
  g_assert (bytes);
  word_list = word_list_new_from_bytes (bytes);

  builder = ipuz_charset_builder_new_for_language ("C");
  charset = ipuz_charset_builder_build (builder);

  return (AcrosticGenerator *) g_object_new (ACROSTIC_TYPE_GENERATOR,
                                             "random-seed", (guint) 1,
                                             "charset", charset,
                                             "word-list", word_list,
                                             NULL);
}

static void
test_acrostic_set_text (void)
{
  g_autoptr (AcrosticGenerator) generator = NULL;
  g_autoptr (GError) error = NULL;

  generator = create_test_generator ();

  g_assert (acrostic_generator_set_text (generator, "ABCDEFIAFF", "ADE", &error));

  g_assert (acrostic_generator_set_text (generator, "CARNEGIEVISITEDPRINCETONANDTOLDWILSONWHATHISYOUNGMENNEEDEDWASNOTALAWSCHOOLBUTALAKETOROWONINADDITIONTOBEINGASPORTTHATBUILTCHARACTERANDWOULDLETTHEUNDERGRADUATESRELAXROWINGWOULDKEEPTHEMFROMPLAYINGFOOTBALLAROUGHNECKSPORTCARNEGIEDETESTED", "DAVIDHALBERSTAMTHEAMATEURS", &error));

  g_assert (!acrostic_generator_set_text (generator, "CARNEGIEISITEDPRINCETONANDTOLDWILSONWHATHISYOUNGMENNEEDEDWASNOTALAWSCHOOLBUTALAKETOROWONINADDITIONTOBEINGASPORTTHATBUILTCHARACTERANDWOULDLETTHEUNDERGRADUATESRELAXROWINGWOULDKEEPTHEMFROMPLAYINGFOOTBALLAROUGHNECKSPORTCARNEGIEDETESTED", "DAVIDHALBERSTAMTHEAMATEURS", &error));
}

#if 0
static void
test_acrostic_run (void)
{
  g_autoptr (AcrosticGenerator) generator = NULL;
  g_autoptr (GError) error = NULL;

  generator = create_test_generator ();

  acrostic_generator_set_seed (generator, 0);

  g_assert (acrostic_generator_set_text (generator, "EVERY DAY BRINGS NEW CHOICES.", "EDBNC", &error));

  acrostic_generator_run (generator);
}

static void
test_acrostic_word_size (void)
{
  g_autoptr (AcrosticGenerator) generator = NULL;
  g_autoptr (GError) error = NULL;

  generator = create_test_generator ();

  acrostic_generator_set_min_word_size (generator, 5);
  acrostic_generator_set_max_word_size (generator, 6);
  g_assert (acrostic_generator_set_text (generator, "Every day brings new choices.", "EDBN", &error));

  acrostic_generator_run (generator);
}

static void
test_acrostic_error (void)
{
  g_autoptr (AcrosticGenerator) generator = NULL;
  g_autoptr (GError) error = NULL;

  generator = create_test_generator ();

  g_assert (!acrostic_generator_set_text (generator, "LIFE IS TOO SHORT", "TOLSTOI", &error));
  g_assert (g_error_matches (error, ACROSTIC_GENERATOR_ERROR, ACROSTIC_GENERATOR_ERROR_SOURCE_TOO_SHORT));

  g_assert (!acrostic_generator_set_text (generator, "CARNEGIEISITEDPRINCETONANDTOLDWILSONWHATHISYOUNGMENNEEDEDWASNOTALAWSCHOOLBUTALAKETOROWONINADDITIONTOBEINGASPORTTHATBUILTCHARACTERANDWOULDLETTHEUNDERGRADUATESRELAXROWINGWOULDKEEPTHEMFROMPLAYINGFOOTBALLAROUGHNECKSPORTCARNEGIEDETESTED", "DAVIDHALBERSTAMTHEAMATEURS", &error));
  g_assert (g_error_matches (error, ACROSTIC_GENERATOR_ERROR, ACROSTIC_GENERATOR_ERROR_SOURCE_NOT_IN_QUOTE));
}
#endif

int
main (int argc, char **argv)
{
  setlocale (LC_ALL, "en_US.utf8");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/acrostic-generator/acrostic_set_text", test_acrostic_set_text);
#if 0
  g_test_add_func ("/acrostic-generator/acrostic_run", test_acrostic_run);
  g_test_add_func ("/acrostic-generator/acrostic_word_size", test_acrostic_word_size);
  g_test_add_func ("/acrostic-generator/acrostic_error", test_acrostic_error);
#endif

  return g_test_run ();
}
