/* play-grid.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include "crosswords-enums.h"
#include "crosswords-misc.h"
#include "play-border.h"
#include "play-cell.h"
#include "grid-layout.h"
#include "play-grid.h"
#include "play-marshaller.h"
#include "svg.h"


enum
{
  PROP_0,
  PROP_SIZE_MODE,
  PROP_MIN_BASE_SIZE,
  PROP_MAX_BASE_SIZE,
  N_PROPS,
};

enum
{
  GUESS,
  GUESS_AT_CELL,
  DO_COMMAND,
  CELL_SELECTED,
  SELECT_DRAG_START,
  SELECT_DRAG_UPDATE,
  SELECT_DRAG_END,
  N_SIGNALS
};

static GParamSpec *obj_props[N_PROPS] =  {NULL, };
static guint obj_signals[N_SIGNALS] = { 0 };


typedef struct _PlayGridPrivate
{
  GtkWidget parent_instance;

  /* vbox that holds all the widgets */
  GtkWidget *vbox;

  PlayGridSizeMode size_mode;
  guint min_base_size;
  guint max_base_size;

  gchar *details_source_svg;
  RsvgHandle *details_handle;

  /* See grid-layout.h for the meaning of board_rows vs. grid_rows / etc. */
  guint board_rows;
  guint board_columns;
  guint grid_rows;
  guint grid_columns;

  /* 2D array of PlayCell* or PlayBorder*, stored as grid_rows*grid_columns elements, row
   * major, rowstride=grid_columns */
  GPtrArray *children;

  /* Array with one GtkBox per grid_row */
  GPtrArray *rows;

  /* Layout configuration for the current set of widgets */
  GridLayout *layout;
  LayoutConfig config;

  /* Cache the last allocated config so we know if we have to update the widgets */
  LayoutConfig last_allocated_config;

  /* Whether we are populated yet.  The fields above are valid only if loaded == TRUE. */

  /* motion info */
  GtkGesture *drag_gesture;

  /* Selection information */
  GtkGesture *click_gesture;
  GtkEventController *motion_controller;
  gboolean selection_enabled;
  IpuzCellCoord anchor_coord;
  IpuzCellCoord last_update_coord;
  GridSelectionMode selection_mode;
  guint loaded : 1;
  guint anchor_coord_set : 1;
  guint last_update_coord_set : 1;
} PlayGridPrivate;


static void               play_grid_init             (PlayGrid       *self);
static void               play_grid_class_init       (PlayGridClass  *klass);
static void               play_grid_dispose          (GObject        *object);
static void               play_grid_set_property     (GObject        *object,
                                                      guint           prop_id,
                                                      const GValue   *value,
                                                      GParamSpec     *pspec);
static void               play_grid_get_property     (GObject        *object,
                                                      guint           prop_id,
                                                      GValue         *value,
                                                      GParamSpec     *pspec);
static void               play_grid_size_allocate    (GtkWidget      *widget,
                                                      int             width,
                                                      int             height,
                                                      int             baseline);
static void               play_grid_measure          (GtkWidget      *widget,
                                                      GtkOrientation  orientation,
                                                      int             for_size,
                                                      int            *minimum,
                                                      int            *natural,
                                                      int            *minimum_baseline,
                                                      int            *natural_baseline);
static void               play_grid_snapshot         (GtkWidget      *widget,
                                                      GtkSnapshot    *snapshot);
static GtkSizeRequestMode play_grid_get_request_mode (GtkWidget      *widget);
static IpuzCellCoord      play_grid_pos_to_coord     (PlayGrid       *self,
                                                      double          widget_x,
                                                      double          widget_y);
static GtkWidget         *play_grid_pos_to_cell      (PlayGrid       *self,
                                                      double          widget_x,
                                                      double          widget_y);
static void               click_gesture_pressed_cb   (PlayGrid       *self,
                                                      int             n_press,
                                                      double          widget_x,
                                                      double          widget_y,
                                                      GtkGesture     *gesture);
static void               drag_gesture_begin_cb      (PlayGrid       *self,
                                                      double          start_x,
                                                      double          start_y,
                                                      GtkGestureDrag *gesture);
static void               drag_gesture_update_cb     (PlayGrid       *self,
                                                      double          start_x,
                                                      double          start_y,
                                                      GtkGestureDrag *gesture);

static void               drag_gesture_end_cb        (PlayGrid       *self,
                                                      double          start_x,
                                                      double          start_y,
                                                      GtkGestureDrag *gesture);



G_DEFINE_TYPE_WITH_CODE (PlayGrid, play_grid, GTK_TYPE_WIDGET,
		         G_ADD_PRIVATE (PlayGrid));


static GtkGesture *
create_drag_gesture (PlayGrid *self)
{
  GtkGesture *gesture;

  gesture = gtk_gesture_drag_new ();
  g_signal_connect_swapped (gesture, "drag-begin",
                            G_CALLBACK (drag_gesture_begin_cb), self);
  g_signal_connect_swapped (gesture, "drag-update",
                            G_CALLBACK (drag_gesture_update_cb), self);
  g_signal_connect_swapped (gesture, "drag-end",
                            G_CALLBACK (drag_gesture_end_cb), self);
  gtk_gesture_single_set_exclusive (GTK_GESTURE_SINGLE (gesture), TRUE);
  gtk_event_controller_set_propagation_phase (GTK_EVENT_CONTROLLER (gesture),
                                              GTK_PHASE_CAPTURE);

  return gesture;
}

static GtkGesture *
create_click_gesture (PlayGrid *self)
{
  GtkGesture *gesture;

  gesture = gtk_gesture_click_new ();
  g_signal_connect_swapped (gesture, "pressed",
                            G_CALLBACK (click_gesture_pressed_cb), self);
  gtk_gesture_single_set_button (GTK_GESTURE_SINGLE (gesture), 1);
  gtk_event_controller_set_propagation_phase (GTK_EVENT_CONTROLLER (gesture),
                                              GTK_PHASE_CAPTURE);

  return gesture;
}

static void
play_grid_init (PlayGrid *self)
{
  PlayGridPrivate *priv;

  priv = play_grid_get_instance_private (self);

  gtk_widget_set_can_focus (GTK_WIDGET (self), TRUE);
  priv->vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
  gtk_widget_insert_before (priv->vbox, GTK_WIDGET (self), NULL);


  /* Keep these values in sync with layout_config_at_zoom_level(), as
   * well as class_init() */
  priv->min_base_size = 7;
  priv->max_base_size = 36;
  priv->size_mode = PLAY_GRID_FIXED;

  priv->board_rows = 0;
  priv->board_columns = 0;
  priv->grid_rows = 0;
  priv->grid_columns = 0;
  priv->children = NULL;
  priv->rows = NULL;
  priv->config = layout_config_default (IPUZ_PUZZLE_CROSSWORD);
  priv->loaded = FALSE;
  priv->selection_enabled = FALSE;

  /* Set up gestures and events */
  priv->drag_gesture = NULL;
  priv->click_gesture = create_click_gesture (self);

  gtk_widget_add_controller (GTK_WIDGET (self), GTK_EVENT_CONTROLLER (priv->click_gesture));
}

static void
play_grid_class_init (PlayGridClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->set_property = play_grid_set_property;
  object_class->get_property = play_grid_get_property;
  object_class->dispose = play_grid_dispose;

  widget_class->size_allocate = play_grid_size_allocate;
  widget_class->measure = play_grid_measure;
  widget_class->snapshot = play_grid_snapshot;
  widget_class->get_request_mode = play_grid_get_request_mode;

  obj_signals [GUESS] =
    g_signal_new ("guess",
                  PLAY_TYPE_GRID,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  G_TYPE_STRING);

  obj_signals [GUESS_AT_CELL] =
    g_signal_new ("guess-at-cell",
                  PLAY_TYPE_GRID,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 3,
                  G_TYPE_STRING,
                  G_TYPE_UINT, G_TYPE_UINT);

  obj_signals [CELL_SELECTED] =
    g_signal_new ("cell-selected",
                  PLAY_TYPE_GRID,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 2,
                  G_TYPE_UINT, G_TYPE_UINT);

  obj_signals [DO_COMMAND] =
    g_signal_new ("do-command",
                  PLAY_TYPE_GRID,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  GRID_TYPE_CMD_KIND);

  obj_signals [SELECT_DRAG_START] =
    g_signal_new ("select-drag-start",
                  PLAY_TYPE_GRID,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 3,
                  IPUZ_TYPE_CELL_COORD,
                  IPUZ_TYPE_CELL_COORD,
                  GRID_TYPE_SELECTION_MODE);

  obj_signals [SELECT_DRAG_UPDATE] =
    g_signal_new ("select-drag-update",
                  PLAY_TYPE_GRID,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 3,
                  IPUZ_TYPE_CELL_COORD,
                  IPUZ_TYPE_CELL_COORD,
                  GRID_TYPE_SELECTION_MODE);

  obj_signals [SELECT_DRAG_END] =
    g_signal_new ("select-drag-end",
                  PLAY_TYPE_GRID,
                  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0);

  obj_props[PROP_SIZE_MODE] = g_param_spec_enum ("size-mode",
                                                 "Size Mode",
                                                 "Resize behavior of the grid",
                                                 PLAY_TYPE_GRID_SIZE_MODE,
                                                 PLAY_GRID_FIXED,
                                                 G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
  obj_props[PROP_MIN_BASE_SIZE] = g_param_spec_uint ("min-base-size",
                                                     "Minimum base size",
                                                     "Smallest possible base size",
                                                     MIN_CELL_BASE_SIZE,
                                                     MAX_CELL_BASE_SIZE,
                                                     7,
                                                     G_PARAM_READWRITE);
  obj_props[PROP_MAX_BASE_SIZE] = g_param_spec_uint ("max-base-size",
                                                     "Maximum base size",
                                                     "Largest possible base_size",
                                                     MIN_CELL_BASE_SIZE,
                                                     MAX_CELL_BASE_SIZE,
                                                     48,
                                                     G_PARAM_READWRITE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);

  gtk_widget_class_set_css_name (widget_class, "play-grid");
}

static void
play_grid_clear (PlayGrid *self)
{
  PlayGridPrivate *priv;

  priv = play_grid_get_instance_private (self);

  if (priv->loaded)
    {
      for (guint i = 0; i < priv->grid_rows; i++)
        {
          GtkWidget **row = (GtkWidget **) &g_ptr_array_index (priv->rows, i);
          gtk_widget_unparent (*row);
          *row = NULL;
        }

      g_ptr_array_free (priv->rows, TRUE);
      g_ptr_array_free (priv->children, TRUE);

      priv->board_rows = 0;
      priv->board_columns = 0;
      priv->grid_rows = 0;
      priv->grid_columns = 0;
      priv->children = NULL;
      priv->rows = NULL;
      priv->loaded = FALSE;
    }
}

static void
play_grid_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  PlayGridPrivate *priv;
  guint new_size;

  priv = play_grid_get_instance_private (PLAY_GRID (object));

  switch (prop_id)
    {
    case PROP_SIZE_MODE:
      priv->size_mode = g_value_get_enum (value);
      break;
    case PROP_MIN_BASE_SIZE:
      new_size = g_value_get_uint (value);
      if (priv->last_allocated_config.base_size < new_size)
        gtk_widget_queue_resize (GTK_WIDGET (object));
      priv->min_base_size = new_size;
      break;
    case PROP_MAX_BASE_SIZE:
      new_size = g_value_get_uint (value);
      if (priv->last_allocated_config.base_size > new_size)
        gtk_widget_queue_resize (GTK_WIDGET (object));
      priv->max_base_size = g_value_get_uint (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
play_grid_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  PlayGridPrivate *priv;

  g_return_if_fail (object != NULL);

  priv = play_grid_get_instance_private (PLAY_GRID (object));

  switch (prop_id)
    {
    case PROP_SIZE_MODE:
      g_value_set_enum (value, priv->size_mode);
      break;
    case PROP_MIN_BASE_SIZE:
      g_value_set_boolean (value, priv->min_base_size);
      break;
    case PROP_MAX_BASE_SIZE:
      g_value_set_boolean (value, priv->max_base_size);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
play_grid_dispose (GObject *object)
{
  PlayGridPrivate *priv;
  GtkWidget *child;

  priv = play_grid_get_instance_private (PLAY_GRID (object));

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  g_clear_pointer (&priv->layout, grid_layout_free);
  g_clear_pointer (&priv->details_source_svg, g_free);
  g_clear_object (&priv->details_handle);

  G_OBJECT_CLASS (play_grid_parent_class)->dispose (object);
}

static void
play_grid_size_allocate (GtkWidget *widget,
                         int        width,
                         int        height,
                         int        baseline)
{
  PlayGridPrivate *priv;
  GtkWidget *child;
  LayoutConfig config;
  guint calc_width;
  guint calc_height;

  priv = play_grid_get_instance_private (PLAY_GRID (widget));

  if (priv->size_mode == PLAY_GRID_SHRINKABLE)
    {
      gboolean valid = FALSE;
      config = layout_config_within_bounds (priv->min_base_size,
                                            priv->max_base_size,
                                            width, height,
                                            priv->board_columns,
                                            priv->board_rows,
                                            &valid);
      if (!valid)
        {
          g_warning ("PlayGrid has been allocated a size outside its bounds");
          return;
        }
    }
  else
    {
      config = priv->config;
    }

  /* If the layout has changed since last time we've updated it, we will need to  */
  if (!layout_config_equal (&config, &priv->last_allocated_config))
    {
      guint grid_row, grid_column;

      for (grid_row = 0; grid_row < priv->grid_rows; grid_row++)
        {
          for (grid_column = 0; grid_column < priv->grid_columns; grid_column++)
            {
              GtkWidget *child = g_ptr_array_index (priv->children, grid_row * priv->grid_columns + grid_column);

              if (PLAY_IS_CELL (child))
                play_cell_update_config (PLAY_CELL (child), config);
              else if (PLAY_IS_BORDER (child))
                play_border_update_config (PLAY_BORDER (child), config);
            }
          priv->last_allocated_config = config;
        }
    }

  calc_width = ((config.base_size * 3) + config.border_size) * priv->board_columns + config.border_size;
  calc_height = ((config.base_size * 3) + config.border_size) * priv->board_rows + config.border_size;

  int child_min = 0;
  int child_nat = 0;
  int child_min_baseline = -1;
  int child_nat_baseline = -1;

  /* GTK will complain if we just allocate the overlay without remeasuring it */
  gtk_widget_measure (priv->vbox, GTK_ORIENTATION_VERTICAL, -1,
                      &child_min, &child_nat,
                      &child_min_baseline, &child_nat_baseline);
  gtk_widget_measure (priv->vbox, GTK_ORIENTATION_HORIZONTAL, -1,
                      &child_min, &child_nat,
                      &child_min_baseline, &child_nat_baseline);

  for (child = gtk_widget_get_first_child (widget);
       child != NULL;
       child = gtk_widget_get_next_sibling (child))
    {
      GtkAllocation allocation = {
        .x = (width - calc_width)/2,
        .y = (height - calc_height)/2,
        .width = calc_width,
        .height = calc_height
      };
      if (gtk_widget_should_layout (child))
        gtk_widget_size_allocate (child, &allocation, baseline);
    }
}

static void
play_grid_measure (GtkWidget      *widget,
                   GtkOrientation  orientation,
                   int             for_size,
                   int            *minimum,
                   int            *natural,
                   int            *minimum_baseline,
                   int            *natural_baseline)
{
  PlayGridPrivate *priv;
  GtkWidget *child;
  LayoutConfig config;
  gint new_width = 0;
  gint new_height = 0;

  priv = play_grid_get_instance_private (PLAY_GRID (widget));

  /* If we are fixed, then self->config determines the size of the widget */
  if (priv->size_mode == PLAY_GRID_FIXED)
    {
      config = priv->config;
      if (orientation == GTK_ORIENTATION_HORIZONTAL)
        *minimum = priv->board_columns *
          (get_play_cell_size (config.base_size) + config.border_size) +
          config.border_size;
      else
        *minimum = priv->board_rows *
          (get_play_cell_size (config.base_size) + config.border_size) +
          config.border_size;
      *natural = *minimum;
    }
  else
    {
      /* First, set the minimum. */
      layout_config_size_at_base_size (priv->min_base_size,
                                       priv->board_columns,
                                       priv->board_rows,
                                       &new_width, &new_height);
      if (orientation == GTK_ORIENTATION_HORIZONTAL)
        *minimum = new_width;
      else
        *minimum = new_height;

      /* Now set the natural. */
      if (for_size == -1)
        {
          layout_config_size_at_base_size (priv->max_base_size,
                                           priv->board_columns,
					   priv->board_rows,
                                           &new_width, &new_height);
          if (orientation == GTK_ORIENTATION_HORIZONTAL)
            *natural = new_width;
          else
            *natural = new_height;
        }
      else
        {
          gboolean valid;

          if (orientation == GTK_ORIENTATION_HORIZONTAL)
            {
              config = layout_config_within_bounds (priv->min_base_size,
                                                    priv->max_base_size,
                                                    -1, for_size,
                                                    priv->board_columns,
                                                    priv->board_rows,
                                                    &valid);
            }
          else if (orientation == GTK_ORIENTATION_VERTICAL)
            {
              config = layout_config_within_bounds (priv->min_base_size,
                                                    priv->max_base_size,
                                                    for_size, -1,
                                                    priv->board_columns,
                                                    priv->board_rows,
                                                    &valid);

            }
          else
            g_assert_not_reached ();

          layout_config_size_at_base_size (config.base_size,
                                           priv->board_columns,
                                           priv->board_rows,
                                           &new_width, &new_height);
          if (orientation == GTK_ORIENTATION_HORIZONTAL)
            *natural = new_width;
          else
            *natural = new_height;
        }
    }

  /* This is just the vbox widget, though we keep the loop just in
   * case the layout changes. It will also call measure() on all the
   * subwidgets, which will keep gtk happy. */
  for (child = gtk_widget_get_first_child (widget);
       child != NULL;
       child = gtk_widget_get_next_sibling (child))
    {
      if (gtk_widget_should_layout (child))
        {
          int child_min = 0;
          int child_nat = 0;
          int child_min_baseline = -1;
          int child_nat_baseline = -1;

          gtk_widget_measure (child, orientation, for_size,
                              &child_min, &child_nat,
                              &child_min_baseline, &child_nat_baseline);
        }
    }

#if 0
  g_print ("Measure:\n\tOrientation: %s\n\tfor_size: %d\n\tminimum:%d\n\tnatural:%d\n",
           (orientation==GTK_ORIENTATION_VERTICAL)?"vertical":"horizontal",
           for_size, *minimum, *natural);
#endif
}

static void
play_grid_snapshot (GtkWidget   *widget,
                    GtkSnapshot *snapshot)
{
  PlayGridPrivate *priv;
  cairo_t *cr;
  GError *error = NULL;
  RsvgRectangle viewport = {
    .x = 0,
    .y = 0,
  };
  gint width, height;

  GTK_WIDGET_CLASS (play_grid_parent_class)->snapshot (widget, snapshot);

  priv = play_grid_get_instance_private (PLAY_GRID (widget));

  if (priv->details_handle == NULL)
    return;

  layout_config_size (priv->last_allocated_config,
                      priv->board_columns,
                      priv->board_rows,
                      &width,
                      &height);
  viewport.width = (double) width;
  viewport.height = (double) height;

  cr= gtk_snapshot_append_cairo (snapshot, &GRAPHENE_RECT_INIT (0, 0, viewport.width, viewport.height));
  if (!rsvg_handle_render_document (priv->details_handle, cr, &viewport, &error))
    {
      g_warning ("could not render SVG: %s", error->message);
      g_error_free (error);
    }
}

static GtkSizeRequestMode
play_grid_get_request_mode (GtkWidget *widget)
{
  PlayGridPrivate *priv;

  priv = play_grid_get_instance_private (PLAY_GRID (widget));

  if (priv->size_mode == PLAY_GRID_SHRINKABLE)
    return GTK_SIZE_REQUEST_HEIGHT_FOR_WIDTH;

  return GTK_SIZE_REQUEST_CONSTANT_SIZE;
}

static IpuzCellCoord
play_grid_pos_to_coord (PlayGrid *self,
                        double    widget_x,
                        double    widget_y)
{
  PlayGridPrivate *priv;
  guint cell_size;
  gfloat border_offset;
  IpuzCellCoord coord = {0, 0};
  graphene_rect_t out_bounds;
  guint calc_width, calc_height;

  priv = play_grid_get_instance_private (PLAY_GRID (self));

  if (! priv->loaded)
    return coord;

  cell_size = priv->last_allocated_config.base_size*3 + priv->last_allocated_config.border_size;
  border_offset = priv->last_allocated_config.border_size / 2.0;

  /* this should never fail, but we get a warning if we don't check the value */
  if (! gtk_widget_compute_bounds (GTK_WIDGET (self), GTK_WIDGET (self), &out_bounds))
    return coord;
  calc_width = ((priv->last_allocated_config.base_size * 3) +
                priv->last_allocated_config.border_size) * priv->board_columns + priv->last_allocated_config.border_size;
  calc_height = ((priv->last_allocated_config.base_size * 3) +
                 priv->last_allocated_config.border_size) * priv->board_rows + priv->last_allocated_config.border_size;

  widget_x -= (out_bounds.size.width - calc_width)/2;
  widget_y -= (out_bounds.size.height - calc_height)/2;

  widget_x = MAX (border_offset, widget_x);
  widget_y = MAX (border_offset, widget_y);

  coord.row = (guint) (widget_y - border_offset) / cell_size;
  coord.column = (guint) (widget_x - border_offset) / cell_size;
  coord.row = MIN (coord.row, priv->board_rows - 1);
  coord.column = MIN (coord.column, priv->board_columns - 1);

  return coord;
}

static GtkWidget *
play_grid_pos_to_cell (PlayGrid *self,
                       double    widget_x,
                       double    widget_y)
{
  PlayGridPrivate *priv;
  IpuzCellCoord coord;
  guint grid_row, grid_column;

  priv = play_grid_get_instance_private (PLAY_GRID (self));

  if (! priv->loaded)
    return NULL;

  coord = play_grid_pos_to_coord (self, widget_x, widget_y);

  grid_row = 2 * coord.row + 1;
  grid_column = 2 * coord.column + 1;

  return (GtkWidget *) g_ptr_array_index (priv->children, grid_row * priv->grid_columns + grid_column);
}

static void
click_gesture_pressed_cb (PlayGrid   *self,
                          int         n_press,
                          double      widget_x,
                          double      widget_y,
                          GtkGesture *gesture)
{
  GtkWidget *cell;

  g_assert (PLAY_IS_GRID (self));

  cell = play_grid_pos_to_cell (self, widget_x, widget_y);
  if (cell != NULL)
    {
      g_assert (PLAY_IS_CELL (cell));
      play_cell_select (PLAY_CELL (cell));
    }
}

static void
drag_gesture_begin_cb (PlayGrid       *self,
                       double          start_x,
                       double          start_y,
                       GtkGestureDrag *gesture)
{
  PlayGridPrivate *priv;
  GdkEventSequence *sequence;
  GdkEvent *event;
  GdkModifierType state_mask;
  IpuzCellCoord new_coord;

  priv = play_grid_get_instance_private (PLAY_GRID (self));

  sequence = gtk_gesture_single_get_current_sequence (GTK_GESTURE_SINGLE (gesture));
  event = gtk_gesture_get_last_event (GTK_GESTURE (gesture), sequence);
  state_mask = gdk_event_get_modifier_state (event);
  new_coord = play_grid_pos_to_coord (self, start_x, start_y);
  if (state_mask & GDK_CONTROL_MASK)
    {
      priv->selection_mode = GRID_SELECTION_TOGGLE;
      priv->anchor_coord = new_coord;
    }
  else if (state_mask & GDK_SHIFT_MASK)
    {
      priv->selection_mode = GRID_SELECTION_EXTEND;
      if (! priv->anchor_coord_set)
        priv->anchor_coord = new_coord;
    }
  else
    {
      priv->selection_mode = GRID_SELECTION_SELECT;
      priv->anchor_coord = new_coord;
    }
  priv->anchor_coord_set = TRUE;

  g_signal_emit (self, obj_signals [SELECT_DRAG_START], 0,
                 &priv->anchor_coord,
                 &new_coord,
                 priv->selection_mode);
}

static void
drag_gesture_update_cb (PlayGrid       *self,
                        double          offset_x,
                        double          offset_y,
                        GtkGestureDrag *gesture)
{
  PlayGridPrivate *priv;
  GdkEventSequence *sequence;
  double x, y;
  IpuzCellCoord coord;
  gboolean emit_signal = FALSE;

  priv = play_grid_get_instance_private (PLAY_GRID (self));

  sequence = gtk_gesture_single_get_current_sequence (GTK_GESTURE_SINGLE (gesture));
  gtk_gesture_get_point (GTK_GESTURE (gesture), sequence, &x, &y);

  coord = play_grid_pos_to_coord (self, x, y);
  if (priv->last_update_coord_set)
    {
      if (! ipuz_cell_coord_equal (&coord, &priv->last_update_coord))
        emit_signal = TRUE;
    }
  else
    emit_signal = TRUE;

  priv->last_update_coord = coord;
  priv->last_update_coord_set = TRUE;


  if (emit_signal)
    g_signal_emit (self, obj_signals [SELECT_DRAG_UPDATE], 0,
                   &priv->anchor_coord,
                   &priv->last_update_coord,
                   priv->selection_mode);
}

static void
drag_gesture_end_cb (PlayGrid       *self,
                     double          offset_x,
                     double          offset_y,
                     GtkGestureDrag *gesture)

{
  PlayGridPrivate *priv;

  priv = play_grid_get_instance_private (PLAY_GRID (self));

  priv->anchor_coord_set = TRUE;
  priv->anchor_coord = priv->last_update_coord;
  priv->last_update_coord_set = FALSE;
  priv->selection_mode = GRID_SELECTION_SELECT;

  g_signal_emit (self, obj_signals [SELECT_DRAG_END], 0);
}

static void
guess_cb (GObject     *cell,
          const gchar *guess,
          PlayGrid    *self)
{
  /* reemit the signal */
  g_signal_emit (G_OBJECT (self), obj_signals [GUESS], 0, guess);
}

static void
guess_at_cell_cb (GObject     *cell,
                  const gchar *guess,
                  guint        row,
                  guint        column,
                  PlayGrid    *self)
{
  /* reemit the signal */
  g_signal_emit (G_OBJECT (self), obj_signals [GUESS_AT_CELL], 0, guess, row, column);
}

static void
do_command_cb (GObject     *cell,
               GridCmdKind  kind,
               PlayGrid    *self)
{
  /* reemit the signal */
  g_signal_emit (G_OBJECT (self), obj_signals [DO_COMMAND], 0, kind);
}

static void
cell_selected_cb (GObject *cell,
                  guint row,
                  guint column,
                  PlayGrid *self)
{
  /* reemit the signal */
  g_signal_emit (G_OBJECT (self), obj_signals [CELL_SELECTED], 0, row, column);
}

static GtkWidget *
create_cell_widget (PlayGrid      *self,
                    IpuzCellCoord  coord,
                    LayoutConfig   config)
{
  GtkWidget *cell = play_cell_new (coord, config);
  g_signal_connect (G_OBJECT (cell), "guess", G_CALLBACK (guess_cb), self);
  g_signal_connect (G_OBJECT (cell), "guess-at-cell", G_CALLBACK (guess_at_cell_cb), self);
  g_signal_connect (G_OBJECT (cell), "do-command", G_CALLBACK (do_command_cb), self);
  g_signal_connect (G_OBJECT (cell), "cell-selected", G_CALLBACK (cell_selected_cb), self);
  return cell;
}

static GtkWidget *
create_border_widget (PlayBorderKind kind, LayoutConfig config)
{
  GtkWidget *border = play_border_new (kind, config);
  return border;
}

/* Public Functions */

GtkWidget *
play_grid_new (PlayGridSizeMode size_mode)
{
  GObject *object;

  object = g_object_new (PLAY_TYPE_GRID,
                         "size-mode", size_mode,
                         NULL);

  return GTK_WIDGET (object);
}

PlayGridSizeMode
play_grid_get_size_mode (PlayGrid *grid)
{
  PlayGridPrivate *priv;
  g_return_val_if_fail (PLAY_IS_GRID (grid), PLAY_GRID_FIXED);

  priv = play_grid_get_instance_private (grid);

  return priv->size_mode;
}


static PlayBorderKind
layout_item_kind_to_border_kind (LayoutItemKind kind)
{
  switch (kind)
    {
    case LAYOUT_ITEM_KIND_INTERSECTION:
      return PLAY_BORDER_KIND_INTERSECTION;

    case LAYOUT_ITEM_KIND_BORDER_HORIZONTAL:
      return PLAY_BORDER_KIND_HORIZONTAL;

    case LAYOUT_ITEM_KIND_BORDER_VERTICAL:
      return PLAY_BORDER_KIND_VERTICAL;

    default:
      g_assert_not_reached ();
    }
}

static void
create_widgets (PlayGrid       *grid,
                GridLayout     *layout,
                IpuzCrossword  *xword,
                LayoutConfig    config)
{
  PlayGridPrivate *priv;
  guint grid_rows, grid_columns;
  guint grid_row, grid_column;

  priv = play_grid_get_instance_private (grid);

  g_assert (!priv->loaded);

  grid_rows = layout->grid_rows;
  grid_columns = layout->grid_columns;

  priv->board_rows = layout->board_rows;
  priv->board_columns = layout->board_columns;
  priv->grid_rows = grid_rows;
  priv->grid_columns = grid_columns;
  priv->children = g_ptr_array_sized_new (grid_rows * grid_columns);
  priv->rows = g_ptr_array_sized_new (grid_rows);

  for (grid_row = 0; grid_row < grid_rows; grid_row++)
    {
      GtkWidget *row = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
      g_ptr_array_add (priv->rows, row);

      for (grid_column = 0; grid_column < grid_columns; grid_column++)
        {
          GridCoord grid_coord = { .row = grid_row, .column = grid_column };
          LayoutItemKind kind = grid_layout_get_kind (layout, grid_coord);

          switch (kind) {
          case LAYOUT_ITEM_KIND_INTERSECTION:
          case LAYOUT_ITEM_KIND_BORDER_HORIZONTAL:
          case LAYOUT_ITEM_KIND_BORDER_VERTICAL: {
            PlayBorderKind border_kind = layout_item_kind_to_border_kind (kind);
            GtkWidget *border = create_border_widget (border_kind, config);
            gtk_box_append (GTK_BOX (row), border);
            g_ptr_array_add (priv->children, border);
            break;
          }
          case LAYOUT_ITEM_KIND_CELL:
          case LAYOUT_ITEM_KIND_CLUE_BLOCK_CELL:{
            IpuzCellCoord coord = { .row = grid_row / 2, .column = grid_column / 2 };
            GtkWidget *cell;

            cell = create_cell_widget (grid, coord, config);
            gtk_box_append (GTK_BOX (row), cell);
            g_ptr_array_add (priv->children, cell);
            break;
          }

          default:
            g_assert_not_reached ();
            return;
          }
        }

      gtk_widget_insert_before (row, priv->vbox, NULL);
    }

  priv->loaded = TRUE;
}

static void
update_widgets (PlayGrid       *grid,
                GridLayout     *layout,
                IpuzCrossword  *xword)
{
  PlayGridPrivate *priv;
  IpuzBoard *board;
  guint grid_row, grid_column;

  priv = play_grid_get_instance_private (grid);

  board = ipuz_crossword_get_board (xword);

  g_assert (priv->board_rows == layout->board_rows);
  g_assert (priv->board_columns == layout->board_columns);
  g_assert (priv->grid_rows == layout->grid_rows);
  g_assert (priv->grid_columns == layout->grid_columns);

  for (grid_row = 0; grid_row < priv->grid_rows; grid_row++)
    {
      for (grid_column = 0; grid_column < priv->grid_columns; grid_column++)
        {
          GridCoord grid_coord = { .row = grid_row, .column = grid_column };
          LayoutItemKind kind = grid_layout_get_kind (layout, grid_coord);
          GtkWidget *child = g_ptr_array_index (priv->children, grid_row * priv->grid_columns + grid_column);

          switch (kind) {
          case LAYOUT_ITEM_KIND_INTERSECTION: {
            LayoutIntersection item = grid_layout_get_intersection (layout, grid_coord);
            PlayBorder *border = PLAY_BORDER (child);
            play_border_update_state (border, item.filled, item.css_class, item.bg_color_set, item.bg_color);
            break;
          }

          case LAYOUT_ITEM_KIND_BORDER_HORIZONTAL: {
            LayoutBorderHorizontal item = grid_layout_get_border_horizontal (layout, grid_coord);
            PlayBorder *border = PLAY_BORDER (child);
            play_border_update_state (border, item.filled, item.css_class, item.bg_color_set, item.bg_color);
            break;
          }

          case LAYOUT_ITEM_KIND_BORDER_VERTICAL: {
            LayoutBorderVertical item = grid_layout_get_border_vertical (layout, grid_coord);
            PlayBorder *border = PLAY_BORDER (child);
            play_border_update_state (border, item.filled, item.css_class, item.bg_color_set, item.bg_color);
            break;
          }

          case LAYOUT_ITEM_KIND_CELL: {
            PlayCell *cell = PLAY_CELL (child);
            LayoutCell cell_layout = grid_layout_get_cell (layout, grid_coord);

            play_cell_update (cell, &cell_layout);
            break;
          }
          case LAYOUT_ITEM_KIND_CLUE_BLOCK_CELL: {
            PlayCell *cell = PLAY_CELL (child);
            IpuzCellCoord coord = { .row = grid_row / 2, .column = grid_column / 2 };
            LayoutClueBlockCell clue_block_layout = grid_layout_get_clue_block_cell (layout, grid_coord);
            IpuzCell *puz_cell = ipuz_board_get_cell (board, &coord);
            play_cell_load_clue_block_state (cell, puz_cell, &clue_block_layout);
            break;
          }
          default:
            g_assert_not_reached ();
            return;
          }
        }
    }
}

static gboolean
should_reload (PlayGrid     *self,
               GridLayout   *layout)
{
  PlayGridPrivate *priv;

  priv = play_grid_get_instance_private (self);

  return (!priv->loaded
          || priv->board_rows != layout->board_rows
          || priv->board_columns != layout->board_columns);

}

static LayoutConfig
normalize_config (PlayGrid     *self,
                  LayoutConfig  config)
{
  PlayGridPrivate *priv;

  priv = play_grid_get_instance_private (self);

  if (priv->size_mode == PLAY_GRID_SHRINKABLE)
    /* Normalize this to the base_size, and ignore any custom border
     * widths */
    return layout_config_at_base_size (config.base_size);
  return config;
}

static void
ensure_select_info (PlayGrid *self)
{
  PlayGridPrivate *priv;

  priv = play_grid_get_instance_private (self);

  if (priv->selection_enabled)
    return;

  if (priv->click_gesture)
    {
      gtk_widget_remove_controller (GTK_WIDGET (self), GTK_EVENT_CONTROLLER (priv->click_gesture));
      priv->click_gesture = NULL;
    }

  if (priv->drag_gesture == NULL)
    {
      priv->drag_gesture = create_drag_gesture (self);
      gtk_widget_add_controller (GTK_WIDGET (self), GTK_EVENT_CONTROLLER (priv->drag_gesture));
    }

  priv->selection_enabled = TRUE;
}

static void
disable_select_info (PlayGrid *self)
{
  PlayGridPrivate *priv;

  priv = play_grid_get_instance_private (self);

  if (!priv->selection_enabled)
    return;

  if (priv->drag_gesture)
    {
      gtk_widget_remove_controller (GTK_WIDGET (self), GTK_EVENT_CONTROLLER (priv->drag_gesture));
      priv->drag_gesture = NULL;
    }

  if (priv->click_gesture == NULL)
    {
      priv->click_gesture = create_click_gesture (self);
      gtk_widget_add_controller (GTK_WIDGET (self), GTK_EVENT_CONTROLLER (priv->click_gesture));
    }

  priv->selection_enabled = FALSE;
}

static void
update_overlay (PlayGrid *self)
{
  PlayGridPrivate *priv;
  GString *new_source_svg = NULL;

  priv = play_grid_get_instance_private (self);
  g_assert (priv->layout);

  new_source_svg = svg_overlays_from_layout (priv->layout);
  /* Update the overlay */
  if (g_strcmp0 (priv->details_source_svg, new_source_svg->str) != 0)
    {
      g_clear_pointer (&priv->details_source_svg, g_free);
      g_clear_object (&priv->details_handle);

      priv->details_handle = svg_handle_from_string (new_source_svg,
                                                     coloring_from_adwaita ());
      priv->details_source_svg = g_string_free_and_steal (new_source_svg);
      gtk_widget_queue_draw (GTK_WIDGET (self));
    }
  else
    {
      g_string_free (new_source_svg, TRUE);
    }
}

void
play_grid_update_state (PlayGrid     *self,
                        GridState    *state,
                        LayoutConfig  config)
{
  PlayGridPrivate *priv;

  g_return_if_fail (PLAY_IS_GRID (self));
  g_return_if_fail (state != NULL);

  priv = play_grid_get_instance_private (self);

  config = normalize_config (self, config);

  g_clear_pointer (&priv->layout, grid_layout_free);
  priv->layout = grid_layout_new (state, config);

  /* as an optimization, we keep old widgets around if the board
   * hasn't changed configuration. Otherwise, we just recreate them
   * all. */
  if (should_reload (self, priv->layout))
    {
      play_grid_clear (self);
      create_widgets (self, priv->layout, state->xword, config);
    }
  update_overlay (self);
  play_grid_set_layout_config (self, config);
  update_widgets (self, priv->layout, state->xword);

  if (GRID_STATE_HAS_SELECTABLE (state))
    ensure_select_info (self);
  else
    disable_select_info (self);
}

void
play_grid_focus_cell (PlayGrid      *self,
                      IpuzCellCoord  coord)
{
  PlayGridPrivate *priv;

  g_return_if_fail (PLAY_IS_GRID (self));

  priv = play_grid_get_instance_private (self);

  g_assert (priv->children != NULL);
  g_assert (priv->board_rows > 0);
  g_assert (priv->board_columns > 0);

  g_assert (coord.row < priv->board_rows);
  g_assert (coord.column < priv->board_columns);

  guint grid_row = 2 * coord.row + 1;
  guint grid_column = 2 * coord.column + 1;

  GtkWidget *cell = g_ptr_array_index (priv->children, grid_row * priv->grid_columns + grid_column);
  gtk_widget_grab_focus (cell);
}

void
play_grid_set_layout_config (PlayGrid     *self,
                             LayoutConfig  config)
{
  PlayGridPrivate *priv;

  g_return_if_fail (PLAY_IS_GRID (self));

  priv = play_grid_get_instance_private (self);

  config = normalize_config (self, config);

  if (! layout_config_equal (&config, &priv->config))
    gtk_widget_queue_resize (GTK_WIDGET (self));
  priv->config = config;

  if (priv->size_mode == PLAY_GRID_SHRINKABLE &&
      priv->max_base_size != config.base_size)
    {
      priv->max_base_size = config.base_size;
      g_object_notify_by_pspec (G_OBJECT (self), obj_props [PROP_MAX_BASE_SIZE]);
    }
}
