/* picker-list.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <adwaita.h>
#include "crosswords-misc.h"
#include "picker-list.h"
#include "picker-list-row.h"
#include "puzzle-button.h"
#include "puzzle-downloader.h"
#include "puzzle-set.h"
#include "puzzle-picker.h"

#define LIST_ROW_USER_DATA "puzzle"

struct _PickerList
{
  PuzzlePicker parent_instance;

  gboolean show_progress;
  GtkFilter *filter;

  /* Bound widgets */
  GtkWidget *header_label;
  GtkWidget *picker_button;
  GtkWidget *url_link_button;
  GtkWidget *list_box;
  GtkWidget *subheader_label;
  GtkWidget *puzzle_list;
};


static void picker_list_init                 (PickerList      *self);
static void picker_list_class_init           (PickerListClass *klass);
static void picker_list_dispose              (GObject         *object);
static void picker_list_load                 (PuzzlePicker    *picker);
static void picker_list_items_changed        (PickerList      *picker,
                                              guint            position,
                                              guint            removed,
                                              guint            added,
                                              GListModel      *list);
static void picker_list_set_downloader_state (PuzzlePicker    *picker,
                                              DownloaderState  state);


G_DEFINE_TYPE (PickerList, picker_list, PUZZLE_TYPE_PICKER);


static void
picker_list_init (PickerList *self)
{
  GtkLayoutManager *box_layout;
  gtk_widget_init_template (GTK_WIDGET (self));

  self->show_progress = TRUE;

  box_layout = gtk_widget_get_layout_manager (GTK_WIDGET (self));
  gtk_orientable_set_orientation (GTK_ORIENTABLE (box_layout), GTK_ORIENTATION_VERTICAL);
  gtk_box_layout_set_spacing (GTK_BOX_LAYOUT (box_layout), 16);
}

static void
picker_list_class_init (PickerListClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  PuzzlePickerClass *picker_class = PUZZLE_PICKER_CLASS (klass);

  object_class->dispose = picker_list_dispose;
  picker_class->load = picker_list_load;
  picker_class->set_downloader_state = picker_list_set_downloader_state;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/picker-list.ui");
  gtk_widget_class_bind_template_child (widget_class, PickerList, header_label);
  gtk_widget_class_bind_template_child (widget_class, PickerList, list_box);
  gtk_widget_class_bind_template_child (widget_class, PickerList, subheader_label);
  gtk_widget_class_bind_template_child (widget_class, PickerList, picker_button);
  gtk_widget_class_bind_template_child (widget_class, PickerList, url_link_button);
  gtk_widget_class_bind_template_child (widget_class, PickerList, puzzle_list);

  gtk_widget_class_bind_template_callback (widget_class, puzzle_picker_start_download);

  gtk_widget_class_set_layout_manager_type (widget_class,
                                            GTK_TYPE_BOX_LAYOUT);
}

static void
picker_list_dispose (GObject *object)
{
  GtkWidget *child;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  G_OBJECT_CLASS (picker_list_parent_class)->dispose (object);
}

static void
set_label_face (GtkWidget   *label,
                const gchar *face)
{
  PangoFontDescription *font_desc;
  PangoAttribute *attr;
  PangoAttrList *attrs;

  font_desc = pango_font_description_new ();
  attrs = pango_attr_list_new ();
  pango_font_description_set_family (font_desc, face);
  attr = pango_attr_font_desc_new (font_desc);
  pango_font_description_free (font_desc);
  pango_attr_list_insert (attrs, attr);
  gtk_label_set_attributes (GTK_LABEL (label), attrs);
  pango_attr_list_unref (attrs);
}


static void
picker_list_load_headers (PickerList *self)
{
  PuzzleSetConfig *config;
  const gchar *header = NULL;
  const gchar *header_face = NULL;
  const gchar *subheader = NULL;
  const gchar *subheader_face = NULL;
  const gchar *url = NULL;

  config = puzzle_picker_get_config (PUZZLE_PICKER (self));

  header = puzzle_set_config_get_header (config);
  header_face = puzzle_set_config_get_header_face (config);
  subheader = puzzle_set_config_get_subheader (config);
  subheader_face = puzzle_set_config_get_subheader_face (config);
  url = puzzle_set_config_get_url (config);

  gtk_label_set_text (GTK_LABEL (self->header_label), header);
  gtk_label_set_text (GTK_LABEL (self->subheader_label), subheader);

  /* set the URL */
  if (url && g_uri_is_valid (url, G_URI_FLAGS_NONE, NULL))
    {
      gtk_link_button_set_uri (GTK_LINK_BUTTON (self->url_link_button), url);
      gtk_button_set_label (GTK_BUTTON (self->url_link_button), url);
    }
  gtk_widget_set_visible (self->url_link_button, (url != NULL));

  /* Set the label style */
  /* FIXME(CSS): set some of this from CSS */
  if (header_face)
    set_label_face (self->header_label, header_face);
  if (subheader_face)
    set_label_face (self->subheader_label, subheader_face);
}

static void
picker_list_load_button (PickerList *self)
{
  PuzzleSetConfig *config;
  gboolean use_button = FALSE;
  const gchar *button_label = NULL;

  config = puzzle_picker_get_config (PUZZLE_PICKER (self));

  use_button = puzzle_set_config_get_use_button (config);
  if (! use_button)
    {
      gtk_widget_set_visible (self->picker_button, FALSE);
      return;
    }

  button_label = puzzle_set_config_get_button_label (config);
  gtk_button_set_label (GTK_BUTTON (self->picker_button), button_label);
}

static void
row_clear_clicked_cb (PickerListRow *list_row,
                      PuzzlePicker  *picker)
{
  g_autoptr (PuzzleSetModelRow) model_row = NULL;
  const gchar *puzzle_name;
  IpuzPuzzle *puzzle;
  IpuzGuesses *guesses;
  gfloat percent;

  g_object_get (list_row,
                "model-row", &model_row,
                NULL);

  g_assert (PUZZLE_IS_SET_MODEL_ROW (model_row));

  puzzle = puzzle_set_model_row_get_puzzle (model_row);
  puzzle_name = puzzle_set_model_row_get_puzzle_name (model_row);
  g_assert (puzzle != NULL);

  guesses = ipuz_crossword_get_guesses (IPUZ_CROSSWORD (puzzle));

  /* Only clear the puzzle if it has changes */
  percent = ipuz_guesses_get_percent (guesses);
  if (percent > 0)
    {
      puzzle_picker_clear_puzzle (picker, puzzle_name,
                                  _("Clear this puzzle?"),
                                  _("If you clear this puzzle, all your progress will be lost."),
                                  TRUE, FALSE);
    }
}

static void
remove_response_cb (PuzzlePicker *picker,
                    const gchar  *response,
                    GtkWindow    *dialog)
{
  const gchar *puzzle_name;
  GListModel *model;

  model = G_LIST_MODEL (puzzle_picker_get_model (picker));
  puzzle_name = g_object_get_data (G_OBJECT (dialog),
                                   LIST_ROW_USER_DATA);

  if (g_strcmp0 (response, "remove") == 0)
    puzzle_set_model_remove_puzzle (PUZZLE_SET_MODEL (model), puzzle_name);
}

static void
remove_button_clicked_cb (PickerListRow *list_row,
                          PuzzlePicker  *picker)
{
  g_autoptr (PuzzleSetModelRow) model_row = NULL;
  const gchar *puzzle_name;
  GtkWindow *parent_window;
  GtkWidget *dialog;
  IpuzPuzzle *puzzle;
  IpuzGuesses *guesses;
  const gchar *secondary_text;

  g_object_get (list_row,
                "model-row", &model_row,
                NULL);

  g_assert (PUZZLE_IS_SET_MODEL_ROW (model_row));

  puzzle = puzzle_set_model_row_get_puzzle (model_row);
  puzzle_name = puzzle_set_model_row_get_puzzle_name (model_row);

  g_assert (puzzle != NULL);

  parent_window = (GtkWindow *) gtk_widget_get_root (GTK_WIDGET (picker));

  guesses = ipuz_crossword_get_guesses (IPUZ_CROSSWORD (puzzle));
  if (ipuz_guesses_get_percent (guesses) > 0.0)
    secondary_text = _("If you remove this puzzle, you won't be able to play it again and all your progress will be lost.");
  else
    secondary_text = _("If you remove this puzzle, you won't be able to play it again.");

  dialog = adw_message_dialog_new (parent_window,
                                   _("Remove puzzle"),
                                   secondary_text);
  g_object_set_data_full (G_OBJECT (dialog), LIST_ROW_USER_DATA, g_strdup (puzzle_name), g_free);


  adw_message_dialog_add_responses (ADW_MESSAGE_DIALOG (dialog),
                                    "cancel",  _("_Cancel"),
                                    "remove", _("_Remove"),
                                    NULL);

  adw_message_dialog_set_response_appearance (ADW_MESSAGE_DIALOG (dialog), "remove", ADW_RESPONSE_DESTRUCTIVE);

  adw_message_dialog_set_default_response (ADW_MESSAGE_DIALOG (dialog), "cancel");
  adw_message_dialog_set_close_response (ADW_MESSAGE_DIALOG (dialog), "cancel");

  g_signal_connect_swapped (dialog, "response",
                            G_CALLBACK (remove_response_cb),
                            picker);

  gtk_widget_set_visible (dialog, TRUE);
}

static void
row_activated_cb (PickerListRow *list_row,
                  PuzzlePicker  *picker)
{
  g_autoptr (PuzzleSetModelRow) model_row = NULL;

  g_object_get (list_row,
                "model-row", &model_row,
                NULL);

  puzzle_picker_puzzle_selected (picker, puzzle_set_model_row_get_puzzle_name (model_row));
}

static void
model_row_changed_cb (PickerList *self)
{
  gtk_filter_changed (self->filter, GTK_FILTER_CHANGE_DIFFERENT);
}

static GtkWidget *
list_row_widget_new (PuzzleSetModelRow *model_row,
                     PuzzlePicker      *picker)
{
  GtkWidget *list_row;

  list_row = (GtkWidget *) g_object_new (PICKER_TYPE_LIST_ROW,
                                         "model-row", model_row,
                                         "activatable", TRUE,
                                         NULL);

  g_signal_connect (list_row, "clear-clicked",
                    G_CALLBACK (row_clear_clicked_cb), picker);
  g_signal_connect (list_row, "delete-clicked",
                    G_CALLBACK (remove_button_clicked_cb), picker);
  g_signal_connect (list_row, "activated",
                    G_CALLBACK (row_activated_cb), picker);
  g_signal_connect_swapped (list_row, "changed",
                            G_CALLBACK (model_row_changed_cb), picker);

  return list_row;
}


static void
picker_list_load_list (PickerList *self)
{
  g_autoptr (GListModel) filter_model = NULL;
  GListModel *model;

  self->filter = puzzle_set_model_filter_new (PUZZLE_SET_MODEL_FILTER_SOLVED);

  model = (GListModel *) puzzle_picker_get_model (PUZZLE_PICKER (self));
  filter_model = (GListModel *) gtk_filter_list_model_new (g_object_ref (model),
                                                           g_object_ref (self->filter));

  gtk_list_box_bind_model (GTK_LIST_BOX (self->puzzle_list),
                           filter_model,
                           (GtkListBoxCreateWidgetFunc) list_row_widget_new,
                           self,
                           NULL);
  g_signal_connect_swapped (G_OBJECT (filter_model), "items-changed", G_CALLBACK (picker_list_items_changed), self);

  /* These values are bogus, but they're also ignored by the
   * callback */
  picker_list_items_changed (self, 0, 0, 0, filter_model);
}

static void
picker_list_load (PuzzlePicker *picker)
{
  PickerList *self = PICKER_LIST (picker);
  PuzzleSetConfig *config;

  PUZZLE_PICKER_CLASS (picker_list_parent_class)->load (picker);

  config = puzzle_picker_get_config (picker);
  self->show_progress = puzzle_set_config_get_show_progress (config);

  picker_list_load_headers (self);
  picker_list_load_button (self);
  picker_list_load_list (self);
}

static void
picker_list_items_changed        (PickerList *picker,
                                  guint       position,
                                  guint       removed,
                                  guint       added,
                                  GListModel *list)
{
  guint n_puzzles;

  n_puzzles = g_list_model_get_n_items (list);
  gtk_widget_set_visible (picker->puzzle_list, n_puzzles > 0);
}

static void
picker_list_set_downloader_state (PuzzlePicker    *picker,
                                  DownloaderState  state)
{
  PickerList *self;

  PUZZLE_PICKER_CLASS (picker_list_parent_class)->set_downloader_state (picker, state);
  self = PICKER_LIST (picker);
 
  switch (state)
    {
    case DOWNLOADER_STATE_DISABLED:
      gtk_widget_set_sensitive (PICKER_LIST (picker)->picker_button, FALSE);
      gtk_widget_set_tooltip_text (self->picker_button,
                                   _("Downloader disabled"));
      break;
    case DOWNLOADER_STATE_WAITING_FOR_NETWORK:
      gtk_widget_set_sensitive (PICKER_LIST (picker)->picker_button, FALSE);
      gtk_widget_set_tooltip_text (self->picker_button,
                                   _("Network access is required to download a puzzle"));
      break;
    case DOWNLOADER_STATE_DOWNLOADING:
      gtk_widget_set_sensitive (PICKER_LIST (picker)->picker_button, FALSE);
      gtk_widget_set_tooltip_text (self->picker_button, NULL);
      break;
    case DOWNLOADER_STATE_READY:
      gtk_widget_set_sensitive (PICKER_LIST (picker)->picker_button, TRUE);
      gtk_widget_set_tooltip_text (self->picker_button, NULL);
      break;
    default:
      g_assert_not_reached ();
    }
}
