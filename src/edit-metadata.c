/* edit-metadata.c
 *
 * Copyright 2021 Jonathan Blandford
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crosswords-config.h"
#include <glib/gi18n-lib.h>
#include <libipuz/libipuz.h>
#include <adwaita.h>
#include "crosswords-misc.h"
#include "edit-metadata.h"


enum
{
  METADATA_CHANGED,
  SHOWENUMERATIONS_CHANGED,
  N_SIGNALS
};

static guint obj_signals[N_SIGNALS] = { 0 };


struct _EditMetadata
{
  GtkWidget parent_instance;

  /* Template widgets */
  GtkWidget *title_row;
  GtkWidget *copyright_row;
  GtkWidget *url_row;
  GtkWidget *publisher_row;
  GtkWidget *publication_row;
  GtkWidget *author_row;
  GtkWidget *editor_row;
  GtkWidget *date_row;
  GtkWidget *showenumerations_switch;
  GtkWidget *difficulty_row;
  GtkWidget *notes_text_view;

  gchar *title;
  gchar *copyright;
  gchar *url;
  gchar *publisher;
  gchar *publication;
  gchar *author;
  gchar *editor;
  gchar *date;
  gboolean showenumerations;
  gchar *difficulty;
  gchar *notes;
};


static void edit_metadata_init                   (EditMetadata          *self);
static void edit_metadata_class_init             (EditMetadataClass     *klass);
static void edit_metadata_dispose                (GObject               *object);
static void showenumerations_switch_activated_cb (EditMetadata          *self);


G_DEFINE_TYPE (EditMetadata, edit_metadata, GTK_TYPE_WIDGET);


static void
init_entry_row (EditMetadata *self,
                GtkWidget    *row)
{
  GtkEditable *text;

  text = gtk_editable_get_delegate (GTK_EDITABLE (row));
  g_signal_connect_swapped (G_OBJECT (text), "notify::has-focus", G_CALLBACK (edit_metadata_commit_changes), self);
  gtk_widget_action_set_enabled (GTK_WIDGET (text), "text.undo", FALSE);
  gtk_widget_action_set_enabled (GTK_WIDGET (text), "text.redo", FALSE);
}

static void
edit_metadata_init (EditMetadata *self)
{
  GtkLayoutManager *box_layout;

  gtk_widget_init_template (GTK_WIDGET (self));

  box_layout = gtk_widget_get_layout_manager (GTK_WIDGET (self));
  gtk_orientable_set_orientation (GTK_ORIENTABLE (box_layout), GTK_ORIENTATION_HORIZONTAL);
  gtk_box_layout_set_spacing (GTK_BOX_LAYOUT (box_layout), 25);
  gtk_widget_set_hexpand (GTK_WIDGET (self), TRUE);
  gtk_widget_set_vexpand (GTK_WIDGET (self), TRUE);

  init_entry_row (self, self->title_row);
  init_entry_row (self, self->copyright_row);
  init_entry_row (self, self->url_row);
  init_entry_row (self, self->publisher_row);
  init_entry_row (self, self->publication_row);
  init_entry_row (self, self->author_row);
  init_entry_row (self, self->editor_row);
  init_entry_row (self, self->date_row);
  init_entry_row (self, self->difficulty_row);
}

static void
edit_metadata_class_init (EditMetadataClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = edit_metadata_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Crosswords/edit-metadata.ui");
  gtk_widget_class_bind_template_child (widget_class, EditMetadata, title_row);
  gtk_widget_class_bind_template_child (widget_class, EditMetadata, copyright_row);
  gtk_widget_class_bind_template_child (widget_class, EditMetadata, url_row);
  gtk_widget_class_bind_template_child (widget_class, EditMetadata, publisher_row);
  gtk_widget_class_bind_template_child (widget_class, EditMetadata, publication_row);
  gtk_widget_class_bind_template_child (widget_class, EditMetadata, author_row);
  gtk_widget_class_bind_template_child (widget_class, EditMetadata, editor_row);
  gtk_widget_class_bind_template_child (widget_class, EditMetadata, date_row);
  gtk_widget_class_bind_template_child (widget_class, EditMetadata, showenumerations_switch);
  gtk_widget_class_bind_template_child (widget_class, EditMetadata, difficulty_row);
  gtk_widget_class_bind_template_child (widget_class, EditMetadata, notes_text_view);

  gtk_widget_class_bind_template_callback (widget_class, edit_metadata_commit_changes);
  gtk_widget_class_bind_template_callback (widget_class, showenumerations_switch_activated_cb);
  obj_signals[METADATA_CHANGED] =
    g_signal_new ("metadata-changed",
                  EDIT_TYPE_METADATA,
                  G_SIGNAL_RUN_FIRST,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 2,
                  G_TYPE_STRING,
                  G_TYPE_STRING);

  obj_signals[SHOWENUMERATIONS_CHANGED] =
    g_signal_new ("showenumerations-changed",
                  EDIT_TYPE_METADATA,
                  G_SIGNAL_RUN_FIRST,
                  0,
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  G_TYPE_BOOLEAN);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BOX_LAYOUT);
}

static void
edit_metadata_dispose (GObject *object)
{
  EditMetadata *self;
  GtkWidget *child;

  self = EDIT_METADATA (object);

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  g_clear_pointer (&self->title, g_free);
  g_clear_pointer (&self->copyright, g_free);
  g_clear_pointer (&self->url, g_free);
  g_clear_pointer (&self->publisher, g_free);
  g_clear_pointer (&self->publication, g_free);
  g_clear_pointer (&self->author, g_free);
  g_clear_pointer (&self->editor, g_free);
  g_clear_pointer (&self->date, g_free);
  g_clear_pointer (&self->difficulty, g_free);
  g_clear_pointer (&self->notes, g_free);

  G_OBJECT_CLASS (edit_metadata_parent_class)->dispose (object);
}

static void
copy_property_to_row (IpuzPuzzle   *puzzle,
                      const gchar  *property,
                      GtkWidget    *entry,
                      gchar       **cur_value)
{
  gchar *value = NULL;

  if (puzzle)
    g_object_get (G_OBJECT (puzzle),
                  property, &value,
                  NULL);

  if (g_strcmp0 (*cur_value, value) == 0)
    {
      g_free (value);
      return;
    }

  gtk_editable_set_text (GTK_EDITABLE (entry), value?value:"");
  g_clear_pointer (cur_value, g_free);
  *cur_value = value;
}

static void
showenumerations_switch_activated_cb (EditMetadata *self)
{
  g_assert (EDIT_IS_METADATA (self));

  g_signal_emit (self, obj_signals[SHOWENUMERATIONS_CHANGED], 0,
                 gtk_switch_get_active (GTK_SWITCH (self->showenumerations_switch)));
}


static void
copy_row_to_hash (EditMetadata  *self,
                  GHashTable    *changes,
                  const gchar   *property,
                  GtkWidget     *entry_row,
                  gchar        **old_value)
{
  const gchar *entry_text;

  entry_text = gtk_editable_get_text (GTK_EDITABLE (entry_row));

    /* value can be null, but the entry is only ever an empty string. We
   * don't support saving empty strings. */
  if (entry_text && entry_text[0] == '\0')
    entry_text = NULL;

  if (g_strcmp0 (entry_text, *old_value))
    {
      g_hash_table_insert (changes, (gpointer) property, (gpointer) entry_text);
      g_clear_pointer (old_value, g_free);
      *old_value = g_strdup (entry_text);
    }
}

/* Public Functions */

void
edit_metadata_commit_changes (EditMetadata *self)
{
  g_autoptr (GHashTable) changes = NULL;
  GHashTableIter iter;
  gpointer key, value;

  g_return_if_fail (EDIT_IS_METADATA (self));

  changes = g_hash_table_new (g_str_hash, g_str_equal);

  copy_row_to_hash (self, changes, "title", self->title_row, &self->title);
  copy_row_to_hash (self, changes, "title", self->title_row, &self->title);
  copy_row_to_hash (self, changes, "copyright", self->copyright_row, &self->copyright);
  copy_row_to_hash (self, changes, "url", self->url_row, &self->url);
  copy_row_to_hash (self, changes, "publisher", self->publisher_row, &self->publisher);
  copy_row_to_hash (self, changes, "publication", self->publication_row, &self->publication);
  copy_row_to_hash (self, changes, "author", self->author_row, &self->author);
  copy_row_to_hash (self, changes, "editor", self->editor_row, &self->editor);
  copy_row_to_hash (self, changes, "date", self->date_row, &self->date);
  copy_row_to_hash (self, changes, "difficulty", self->difficulty_row, &self->difficulty);
  //  copy_row_to_hash (self, changes, "notes", self->notes_text_view, &self->notes);

  g_hash_table_iter_init (&iter, changes);
  while (g_hash_table_iter_next (&iter, &key, &value))
    {
      g_signal_emit (self, obj_signals[METADATA_CHANGED], 0,
                     key, value);
    }
}

void
edit_metadata_update (EditMetadata *self,
                      IpuzPuzzle   *puzzle)
{
  gboolean showenumerations = FALSE;

  g_assert (EDIT_IS_METADATA (self));

  copy_property_to_row (puzzle, "title", self->title_row, &self->title);
  copy_property_to_row (puzzle, "copyright", self->copyright_row, &self->copyright);
  copy_property_to_row (puzzle, "url", self->url_row, &self->url);
  copy_property_to_row (puzzle, "publisher", self->publisher_row, &self->publisher);
  copy_property_to_row (puzzle, "publication", self->publication_row, &self->publication);
  copy_property_to_row (puzzle, "author", self->author_row, &self->author);
  copy_property_to_row (puzzle, "editor", self->editor_row, &self->editor);
  copy_property_to_row (puzzle, "date", self->date_row, &self->date);
  copy_property_to_row (puzzle, "difficulty", self->difficulty_row, &self->difficulty);

  /* showenumerations switch */
  g_object_get (puzzle, "showenumerations", &showenumerations, NULL);
  g_signal_handlers_block_by_func (self->showenumerations_switch, showenumerations_switch_activated_cb, self);
  gtk_switch_set_active (GTK_SWITCH (self->showenumerations_switch), showenumerations);
  g_signal_handlers_unblock_by_func (self->showenumerations_switch, showenumerations_switch_activated_cb, self);
}

