/* play-style.c
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <adwaita.h>
#include "play-style.h"


static PlayStyle *default_style = NULL;

struct _PlayStyle
{
  GObject parent_object;
  GdkRGBA colors[PLAY_N_COLORS];
};


static void play_style_init          (PlayStyle      *self);
static void play_style_class_init    (PlayStyleClass *klass);
static void play_style_update_colors (PlayStyle      *self);


G_DEFINE_TYPE (PlayStyle, play_style, G_TYPE_OBJECT);


static void
play_style_init (PlayStyle *self)
{
  AdwStyleManager *style_manager;

  style_manager = adw_style_manager_get_default ();

  g_signal_connect_object (style_manager, "notify::dark", G_CALLBACK (play_style_update_colors), self, G_CONNECT_SWAPPED);
  play_style_update_colors (self);
}

static void
play_style_class_init (PlayStyleClass *klass)
{
  /* Pass */
}

static void
load_light_colors (PlayStyle *self)
{
  gdk_rgba_parse (&self->colors[PLAY_COLOR_BLACK],     "#000000"); // BLACK
  gdk_rgba_parse (&self->colors[PLAY_COLOR_WHITE],     "#FFFFFF"); // WHITE
  gdk_rgba_parse (&self->colors[PLAY_COLOR_BLUE1],     "#99c1f1");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_GREEN1],    "#8ff0a4");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_YELLOW1],   "#f9f06b");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_ORANGE1],   "#ffbe6f");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_RED1],      "#f66151");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_PURPLE1],   "#dc8add");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_BROWN1],    "#cdab8f");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_BLUE3],     "#3584e4");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_GREEN3],    "#33d17a");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_YELLOW3],   "#f6d32d");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_ORANGE3],   "#ff7800");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_RED3],      "#e01b24");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_PURPLE3],   "#9141ac");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_BROWN3],    "#986a44");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_BLOCK],     "#3d3846"); // DARK 3
  gdk_rgba_parse (&self->colors[PLAY_COLOR_NORMAL],    "rgba(0.0,0.0,0.0,0.15)");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_HIGHLIGHT], "#f8e45c"); // YELLOW 2
  gdk_rgba_parse (&self->colors[PLAY_COLOR_FOCUS],     "#f5c211"); // YELLOW 4
}

static void
load_dark_colors (PlayStyle *self)
{
  gdk_rgba_parse (&self->colors[PLAY_COLOR_BLACK],     "#000000"); // BLACK
  gdk_rgba_parse (&self->colors[PLAY_COLOR_WHITE],     "#FFFFFF"); // WHITE
  gdk_rgba_parse (&self->colors[PLAY_COLOR_BLUE1],     "#99c1f1");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_GREEN1],    "#8ff0a4");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_YELLOW1],   "#f9f06b");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_ORANGE1],   "#ffbe6f");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_RED1],      "#f66151");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_PURPLE1],   "#dc8add");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_BROWN1],    "#cdab8f");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_BLUE3],     "#3584e4");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_GREEN3],    "#33d17a");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_YELLOW3],   "#f6d32d");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_ORANGE3],   "#ff7800");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_RED3],      "#e01b24");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_PURPLE3],   "#9141ac");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_BROWN3],    "#986a44");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_BLOCK],     "#3d3846"); // DARK 3
  gdk_rgba_parse (&self->colors[PLAY_COLOR_NORMAL],    "rgba(0.0,0.0,0.0,0.15)");
  gdk_rgba_parse (&self->colors[PLAY_COLOR_HIGHLIGHT], "#f8e45c"); // YELLOW 2
  gdk_rgba_parse (&self->colors[PLAY_COLOR_FOCUS],     "#f5c211"); // YELLOW 4
}

static void
play_style_update_colors (PlayStyle *self)
{
  AdwStyleManager *style_manager;

  style_manager = adw_style_manager_get_default ();

  if (adw_style_manager_get_dark (style_manager))
    load_dark_colors (self);
  else
    load_light_colors (self);
}

/* Public methods */

PlayStyle *
play_style_get (void)
{
  if (default_style == NULL)
    {
      default_style = (PlayStyle *) g_object_new (PLAY_TYPE_STYLE, NULL);
    }

  return default_style;
}

const GdkRGBA *
play_style_get_color (PlayStyle *style,
                      PlayColor  color)
{
  g_return_val_if_fail (PLAY_IS_STYLE (style), NULL);

  return &(style->colors[color]);
}

/* see https://en.wikipedia.org/wiki/Relative_luminance */
gfloat
play_style_color_luminance (GdkRGBA *color)
{
  g_return_val_if_fail (color != NULL, 0.0);

  return ((color->red * 0.2126) + (color->green * 0.7152) + (color->blue * 0.0722));
}

void
darken_color (GdkRGBA *color)
{
  color->red *= 0.8;
  color->green *= 0.8;
  color->blue *= 0.8;
}

void
lighten_color (GdkRGBA *color)
{
  CLAMP (color->red *= 1.2, 0.0, 1.0);
  CLAMP (color->green *= 1.2, 0.0, 1.0);
  CLAMP (color->blue *= 1.2, 0.0, 1.0);
}

