#include "grid-layout.h"
#include "test-utils.h"

static LayoutCell
default_cell_with_main_text (const char *main_text)
{
  LayoutCell cell = {
    .cell_type = IPUZ_CELL_NORMAL,
    .css_class = LAYOUT_ITEM_STYLE_NORMAL,
    .bg_color.red = 1.0,
    .bg_color.green = 1.0,
    .bg_color.blue = 1.0,
    .bg_color.alpha = 1.0,
    .bg_color_set = FALSE,
    .text_color.red = 0.0,
    .text_color.green = 0.0,
    .text_color.blue = 0.0,
    .text_color.alpha = 1.0,
    .text_color_set = FALSE,
    .main_text = main_text,
  };

  return cell;
}

static void
test_cell_has_main_text (const char *filename, GridStateMode mode, GridCoord coord, const char *set_guess, const char *main_text)
{
  GridState *state = load_state (filename, mode);
  IpuzGuesses *guesses = NULL;

  if (set_guess)
    {
      guesses = ipuz_guesses_new_from_board (ipuz_crossword_get_board (state->xword), FALSE);
      ipuz_crossword_set_guesses (state->xword, guesses);

      IpuzCellCoord cell_coord = {
        .row = coord.row / 2,
        .column = coord.column / 2,
      };

      ipuz_guesses_set_guess (guesses, &cell_coord, set_guess);
    }

  GridLayout *layout = grid_layout_new (state, layout_config_default (IPUZ_PUZZLE_CROSSWORD));
  LayoutCell cell = grid_layout_get_cell (layout, coord);

  LayoutCell expected = default_cell_with_main_text (main_text);
  g_assert (layout_cell_equal (&cell, &expected));

  if (guesses)
    {
      ipuz_guesses_unref (guesses);
    }

  grid_layout_free (layout);
  grid_state_free (state);
}

void
layout_sets_main_text_for_solve_mode (void)
{
  GridCoord coord = { .row = 7, .column = 1 };
  test_cell_has_main_text ("tests/focus.ipuz", GRID_STATE_SOLVE, coord, NULL, "");
  test_cell_has_main_text ("tests/focus.ipuz", GRID_STATE_SOLVE, coord, "X", "X");
}

void
layout_sets_main_text_for_browse_mode (void)
{
  GridCoord coord = { .row = 7, .column = 1 };
  test_cell_has_main_text ("tests/focus.ipuz", GRID_STATE_BROWSE, coord, NULL, "");
  test_cell_has_main_text ("tests/focus.ipuz", GRID_STATE_BROWSE, coord, "X", "X");
}

void
layout_sets_main_text_for_edit_mode (void)
{
  GridCoord coord = { .row = 7, .column = 1 };
  test_cell_has_main_text ("tests/focus.ipuz", GRID_STATE_EDIT, coord, NULL, "T");
}

void
layout_sets_main_text_for_edit_browse_mode (void)
{
  GridCoord coord = { .row = 7, .column = 1 };
  test_cell_has_main_text ("tests/focus.ipuz", GRID_STATE_EDIT_BROWSE, coord, NULL, "T");
}

void
layout_sets_main_text_for_select_mode (void)
{
  GridCoord coord = { .row = 7, .column = 1 };
  test_cell_has_main_text ("tests/focus.ipuz", GRID_STATE_SELECT, coord, NULL, "T");
}

void
layout_sets_main_text_for_view_mode (void)
{
  GridCoord coord = { .row = 7, .column = 1 };
  test_cell_has_main_text ("tests/focus.ipuz", GRID_STATE_VIEW, coord, NULL, "T");
}

void
layout_sets_main_text_to_initial_val (void)
{
  GridCoord coord = { .row = 5, .column = 3 };
  GridState *state = load_state ("tests/focus.ipuz", GRID_STATE_SOLVE);
  GridLayout *layout = grid_layout_new (state, layout_config_default (IPUZ_PUZZLE_CROSSWORD));
  LayoutCell cell = grid_layout_get_cell (layout, coord);

  g_assert_cmpstr (cell.main_text, ==, "I");

  grid_layout_free (layout);
  grid_state_free (state);
}

void
layout_loads_barred (void)
{
  GridState *state = load_state ("tests/barred.ipuz", GRID_STATE_SOLVE);
  GridLayout *layout = grid_layout_new (state, layout_config_default (IPUZ_PUZZLE_CROSSWORD));
  guint i;

  g_assert_cmpint (layout->overlays->len, ==, 2);

  for (i = 0; i < layout->overlays->len; i++)
    {
      LayoutOverlay allowed_1 = {
        .kind = LAYOUT_OVERLAY_KIND_BARRED,
        .u.barred.coord.column = 4,
        .u.barred.coord.row = 1,
      };

      LayoutOverlay allowed_2 = {
        .kind = LAYOUT_OVERLAY_KIND_BARRED,
        .u.barred.coord.column = 7,
        .u.barred.coord.row = 4,
      };

      LayoutOverlay o = g_array_index (layout->overlays, LayoutOverlay, i);

      g_assert (layout_overlay_equal (&o, &allowed_1) || layout_overlay_equal (&o, &allowed_2));
    }

  grid_layout_free (layout);
  grid_state_free (state);
}
