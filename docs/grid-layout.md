# Grid layout
**Status**: Implemented | 2022

The `GridLayout` system is used to encapsulate the appearance of a
crossword into a relatively light-weight structure.

It is calculated from a `XwordState`, and contains roughly the
following information within it:

* The shape and layout of the board
* The current string to display in each cell
* The current cell state, for setting CSS values
* Any decorations to include in the overlay (see overlay.md)
* The current sizing and zoom level to render

It can be used by `PlayGrid` and `PlayCell` to render themselves
appropriately. It can also be used to generate a pdf for printing
support.

## Types

## Grid layout for crosswords.

Consider a 3x3 board:

```
+-+-+-+
|C|C|C|
+-+-+-+
|C|C|C|
+-+-+-+
|C|C|C|
+-+-+-+
```

Each character there is a layout item: _**+**_ are intersections, _**|**_
are vertical vborders, _**-**_ are horizontal borders, and _**C**_ are
cells.

We have `(2 * board_rows + 1) * (2 * board_columns + 1)` items here.

For consistency, we distinguish between `board_rows` (rows of cells in
the crossword), and `grid_rows` (rows of layout items in the
GridLayout) — and similarly for `board_columns` vs. `grid_columns`.

So:

```
grid_rows = 2 * board_rows + 1
grid_columns = 2 * board_columns + 1
```

The drawing code can iterate linearly through the grid, and get a
`LayoutItemKind` for each position.  That way it knows whether it must
draw an intersection, a border, or a cell.  Then it can ask for each
particular item kind and get its details.

**Note:** For crossword-style puzzles, the type of each layout item is
completely predictable regardless of the puzzle. That is why
`grid_layout_get_kind` doesn't take a layout as an argument.

