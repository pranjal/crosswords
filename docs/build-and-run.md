## Build instructions

*Crosswords* and *Crossword Editor* build on most modern Linux
distributions and MacOS ([build hints](docs/macos-build.md)). It was
developed primarily on Fedora and OpenSUSE and should build easily on
those platforms with few add-ons. It has dependencies on GTK 4.10,
libadwaita-1.2, and libpanel-1.3 which will be built submodules if not
installed.

The easiest way to build and test Crosswords from source is by
building a flatpak. This command will build and install it:

```shell
$ flatpak-builder  --force-clean _flatpak/ org.gnome.Crosswords.Devel.json  --user --install
$ # To run the output:
$ flatpak run org.gnome.Crosswords.Devel
```

### Local build from a container image

An easy way to set up a development environment is to use `podman` to
download and run a container image.  This image already contains all
the dependencies for Crosswords, so you don't have to mess around with
your core system packages. `distrobox` is a useful wrapper around
podman.

Install `podman` and `distrobox` on your distro, and then:

```shell
cd crosswords   # wherever you did your "git clone"
bash ci/pull-container-image.sh
```

In the Crosswords source tree, ``ci/pull-container-image.sh`` is a script
that will invoke ``podman pull`` to download the container image that
you can use for development.  It is the same image that Crosswords uses
for its continuous integration pipeline (CI), so you can have exactly
the same setup on your own machine.

Then, the `pull-container-image.sh` script will give you instructions
on how to use `distrobox` to build and install Crosswords inside the
container:

```
Now run this:

  distrobox create --image $IMAGE_NAME --name crosswords
  distrobox enter crosswords

Once inside the container, you can build and install with this:

  sudo sh ci/build-and-install.sh

You can just run crosswords on the shell afterwards.  It is installed to
the overlay /usr filesystem, and will not interfere with your system:

  crosswords
```

Run those commands from the script with the correct $IMAGE_NAME.

Now you can build and install Crosswords while inside the container.
With the instructions above, the Crosswords program will get installed
to the container's overlay file system for `/usr` without affecting
your main system.

**Note:** When done with this, you can remove the image by calling
`distrobox rm crosswords`.

### Local build

Use standard meson tools to build it locally on Linux. You'll need
recent versions of most libraries to get this to build. In general, we
test crosswords on the latest released Fedora, openSUSE, and Ubuntu,
but it's not guaranteed to build on older versions.

```shell
$ meson setup _build -Dlocaledir=/usr/share/locale
$ ninja -C _build
```

**Note:** We set localedir just so that we can find translations of
language names. It's not necessary, but there will be a runtime
warning about missing translations without it.

**Note:** We require a relatively new version of meson to build
Crosswords. You can install a local version of meson that's
sufficiently new by running `pip3 install --user meson`.

### Load .puz files (Optional)

In order to use the convertor to load other crossword types, you need
to install some python dependencies. The easiest way to do this is to
use pip:

```shell
$ # Set up the virtualenv first (see note)
$ pip install -r requirements.txt
$ pip install --no-deps -r requirements.no-deps.txt
```

**Note:** If you're not running this in a VM, we _strongly_
recommended that you use `virtualenv` to setup a python environment
before using
pip. [Here's](https://developer.fedoraproject.org/tech/languages/python/python-installation.html#using-virtual-environments)
an example of how to do so on fedora.

## Running Crosswords without installation

Running it locally out of the `builddir` is a little more involved as
it requires some environment variables set to work. To make this
simple and avoid a full system installation, a `run` script is
included.

Use it as follows:

```shell
$ cd _build/

$ # To run the game
$ ./run ./src/crosswords

$ # To run the crossword editor
$ ./run ./src/crossword-editor

$ # To use the convertor
$ ./run ./tools/ipuz-convertor -i puzzle.puz -o /path/to/puzzle.ipuz

$ # To debug the game
$ ./run gdb ./src/crosswords

$ # To run crossword tests
$ ./run ninja test
```

## Loading Puzzle Sets

If you want to test the game with another puzzle-set without installation, you can use the `PUZZLE_SET_PATH` and `PATH` environment variables. As an example:

```shell
$ PATH=~/Projects/puzzle-sets-xword-dl/ PUZZLE_SET_PATH=~/Projects/puzzle-sets-xword-dl/_build/puzzle-sets/ ./run src/crosswords
```

## Common problems

### gschemas.compiled

Meson doesn't rebuild the compiled gschemas file when the source gschema file changes. If you see an error that looks similar to this after a rebuild:

```shell
$ ./run src/crosswords
(crosswords:100131): GLib-GIO-ERROR **: 09:46:51.527: Settings schema 'org.gnome.Crosswords' does not contain a key named 'hidden-puzzle-sets'
Trace/breakpoint trap (core dumped)
```

You'll have to recreate the compiled file. Assuming you're still in _build, this should fix it:

```shell
$ rm data/gschemas.compiled
$ ninja -C .
```
