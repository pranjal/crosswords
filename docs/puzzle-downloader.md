# Puzzle Downloader
**Status**: Implemented | 2022

Puzzle downloaders are a way to have an external program acquire
puzzles. It could be from a blog, potentially from the gnome.org site
later on, or from other sources. They are configured in the puzzle-set
puzzle.config file as part of a puzzle-set. See the `[Downloader]`
section of [puzzle-set-resource.md](puzzle-set-resource.md) for more
information on how to create one.

## Invocation

Puzzle downloaders are expected to be invoked based on the type of
downloader they are. The `Command` string in the config file will
determine how it's called, with the appropriate argument replaced with
the user value. So, for example, a date string with the command
`downloader --date %D` will be run with the date the user selected.

* **auto**: `$COMMAND`
* **date**: `$COMMAND %D`
* **entry**: `$COMMAND %S`
* **uri**: `$COMMAND %U`
* **number**: `COMMAND %N`

The downloader should download the puzzle, print it to stdout in .puz
or .ipuz format, and return 0 to indicate everything was
successful. Any other value indicates that the file was not downloaded
correctly. In addition, any string to stderr is passed to the user as
a user-visible error message.

**Note:** downloaders must support either .puz or .ipuz files in the
output and .puz files must have ConvertPuzToIpuz set to True. We do
not currently do any mime sniffing to allow both formats to be
supported simultaneously.

## Args
* **%D**: User selected date in ISO 8601 format (eg. `YYYY-MM-DD`)
* **%U**: A user entered URI
* **%N**: A user selected integer. It's between the limits configured,
  if they're set
* **%S**: The string in the entry field. No validation is done.

## Installation

TBD.

## Sandboxing

TBD.

