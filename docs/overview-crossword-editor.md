# Crossword Editor codebase overview

These documents describes the way the two applications are written. It
doesn't go into a lot of detail in each section, but gives a rough
overview and hints to understand the codebase better.

This page covers the crossword editor. In addition, it refers to code
from the [shared internals](shared-internals.md) section. There are
five major sections of code:

* Main application
* Edit board
* Edit clues
* Edit metadata
