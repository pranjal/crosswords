# Puzzle Set Tags
**Status**: Implemented | 2023

Puzzle sets can tag their puzzles to help the user decide if they want
to use them. This is done with the `Tags` key in the `[Puzzle Set]`
section of the `puzzle.config` file.

The following size tags are accepted:

* **mini**: Puzzles smaller than 8×8
* **regular**: Puzzles between 8×8 and 20×20
* **jumbo**: Puzzles 21×21 or greater

Sizes are approximate, and maybe give an indication of expected playing
time. A 6×11 puzzle might be considered a mini if its intended to be
solved quickly, while a set of 8×8 grids might be regular if they're uncommonly
hard.

In addition, the following puzzle type tags will be accepted. They
match the equivalent libipuz puzzle type:

* **acrostic**
* **arrowword**
* **barred**
* **crossword**
* **cryptic**
* **filippine**

Tags are cumulative. So if you support varying sizes or types of
puzzles within a puzzle set, simply include them all.

## Other indicators

* The `locale` key is used to indicate the puzzle set region and language
* The existence of an automatic downloader is used to indicate that
  the puzzle set can automatically download puzzles.


## Example:

As an example, consider the following values in a puzzle.config file.

```ini
[Puzzle Set]
...
Locale=en_US
Tags=mini;regular;crossword;cryptic;

[Picker List]
...
UseButton=true

[Picker Button]
Type=downloader

[Downloader]
...
Type=auto
```

This will produce the following tags for the user:

**United States (English) • Mini / Regular • Crosswords / Cryptic Crosswords • Automatic Downloader**
