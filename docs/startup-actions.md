# Startup Actions
**Status:** Draft | Unimplemented

We have a number of actions we want to do every time we start the game:

* Auto download puzzles
* Auto-clean old puzzles

This doc will list requirements and details for implementing this.


## Auto Download

The idea behind this feature is to download puzzles to play when
starting the song. This is triggered by the
`auto-download-puzzle-sets` gsettings key, and can be selected in the
preferences dialog.

### Implementation details

On startup, we should determine the list of puzzle sets the user has
subscribed to and see if they have auto downloaders. We would then run
through them serially and download each one.

Some considerations:

* If there's no network on startup, we shouldn't run the
  downloaders. If network becomes available, we could then trigger the
  download stage.

* The Downloader currently has a cancel dialog that pops up after a
  certain time. We will need to suppress that dialog, but probably
  cancel the operation if it takes too long. Maybe a 10-second timeout
  is sufficient to give the downloader a chance to finish but not wait
  indefinitely.

* We need to figure out how best to notify the user about progress. We
  can use the global AdwToast to do this notification, but will have
  to be careful with the priority to make sure it doesn't overlap with
  other toasts. Maybe a toast like: `Downloading puzzles (x/n)
  [Cancel]` Would be sufficient. We expect this to be a relatively
  quick operation.
  
* Alternatively, just doing it silently could be sufficient. The user
  may not need to know.


### Variants

* We should keep track of the last time we went into a puzzle set and
  determine if there are new puzzles available. These puzzles could
  be added either by the auto downloader or updating the puzzle set
  (eg, maya's puzzles). We could add a label saying "New puzzles
  available" in this instance in the main page.

* GNOME 44 has support for background apps. We can consider an option
  where we keep the app running and have it download crosswords daily
  regardless of whether you start it.

* Along the same lines, if we keep the app open for more than 24
  hours, do we want to trigger this again? It's pretty straightforward
  to add that timer, but is it something the user would expect?

## Auto clean of puzzles
