# Puzzle Naming

One result of the code is that we have a lot of files with similar
names that do different things. This even gets confusing for people
who are veterans of the code base. This is a quick description of each
file to make it easier to navigate:

## Play Widgets
* **play-border** — Border/corner of cells
* **play-cell** — Represents each cell
* **play-clue-list** — A visual list of clues on the main board
* **play-clue-row** — An individual clue
* **play-grid** — A grid representing an IpuzPuzzle. made up of cells
  and borders
* **play-window** — The main window
* **play-xword** — A crossword playing area
* **play-xword-column** — The custom widget that manages a **PlayXword**

## Picker Widgets
* **puzzle-picker** — Base widget for both PickerGrid and PickerList
* **picker-grid** — A grid of locked puzzles. Used for cats and dogs
* **picker-list** — A list of puzzles. Used for most puzzle sets
* **puzzle-button** — Button used by the PickerGrid
* **puzzle-button-icon** — An icon of a crossword for a puzzle button

# Puzzle Management
* **puzzle-set** — Base type of a set of puzzles. Can be a source to
  download new puzzles, or include existing puzzles. (*Example: Cats &
  Dogs*)
* **puzzle-set-config** — Parses puzzle.config and extracts resources
  from the GResource.
* **puzzle-set-list** — A global list of the PuzzleSets that the game
  has found. It's currently static, but could be dynamic in the
  future.
* **puzzle-set-model** — A **GListModel** that represents the puzzles in a
  **PuzzleSet**.
* **puzzle-downloader** — Object that PuzzleSets use to download new
  puzzles
* **puzzle-downloader-dialog** — Dialog to help manage the downloader.

# Editor widgets
* **edit-greeter** — Initial greeter for the editor. Let's you pick a
  puzzle type. Also triggered by "New Puzzle"
* **edit-greeter-details** — Interface to let the user customize
  puzzles from the greeter.
* **edit-greeter-crossword** — Details panel for crossword-type
  puzzles.
* **edit-window** — Main editor toplevel
* **edit-xword** — Page for editing the grid of a puzzle
* **edit-clues** — Page for editing the clues of a puzzle
* **edit-style** — Page for editing the style of a puzzle
* **edit-metadata** — Page for editing the metadata of a puzzle
* **edit-autofill-dialog** — Dialog to let the user autofill a grid
* **edit-puzzle-stack** — A stack of puzzles you can flip
  through. Used in the autofill dialog to show results from a autofill
  search.

# Control Flow
* **puzzle-stack** — Main control-flow of the app. Captures puzzle
  state in discrete chunks. Includes undo/redo support, as well as
  notifications to let listeners know about changes to the puzzle.

## Deprecated but still used
* **play-style** — The old style system. Replaced by CSS and used just
  for rendering svg icons now. Will be replaced once we implement
  printing and thumbnails.
