# Word Solver
**Status:** Draft | Implemented | July 2024

*Note: This code has been implemented, but hasn't been documented*

The word solver is complicated. Really complicated.

It provides an attempt at a crossword grid solver. The goal is to find
a set of clues that can be used to make a puzzle. It is the heart of
the autofill dialog.

## Overview

The `WordSolver` class has a deceptively simple API. The autofill
dialog creates one instance and keeps it around for the lifetime of
the dialog.

The object has three states. If it's running, then `get_count()` will
give an update on how many iterations it's done.

## Thread safety

The WordSolver stores information about the current run, as well as a
copy of the Crossword board. It is only modifiable from the main
thread with two exceptions:

* The `count` member keeps tabs of how many boards we've tried for
  informational purposes. It should only be accessed via the
  `g_atomic_int_*` functions

* The `solution` array is also accessed via threads, and all access
  should be done by holding the `solutions_mutex`.

