# Autofill
**Status**: draft | implemented

The Autofill functionality has three main parts.

* **`EditAutofillDetails`**: UI to edit autofill constraintts
* **`WordSolver`**: An object that manages the state of a solve attemps
* **`WordSolverTask`**: Task object that solves the grid.

## EditAutofillDetails

This lets the user set the configuration options for the solver.

## WordSolver

The WordSolver manages the state of the solve. It can be queried for
the number of potential boards, as well as return the ones it has
found. It has three internal states:

* **READY**: Ready to start a search. There are no solution boards
  available.
* **RUNNING**: In the middle of searching. Boards may or may not have
  been discovered.
* **COMPLETE**: A search either completed or was canceled.

We do _NOT_ get signals from the solver when boards are found, and for
state changes other than RUNNING → COMPLETE. Instead, we poll
externally from a timeout. This is for performance reasons; notifying
when boards are found would slow down the entire operation.


## WordSolverTask

The algorithm to actually fill the board is fairly simple. The task
will iterate through the open cells, finding a list of all the
possible characters for that cell by using
`word_list_find_intersection()`. It picks the most commonly used
character, guesses it, and iterates to the next cell. If a dead end is
reached, we backtrack to the oldest character in the across/down
direction, and try a the next one.

This is sufficient to fill small-to-medium areas of the board, but
cannot handle a full 15x15 grid.

**NOTE:** `word-solver-task.h` defines the interface, but the actual
implmentation is in `word-solver-task-cell.c`. This is to allow for
experimentations with other solver approaches until we find one that
works well.
