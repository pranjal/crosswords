================
Version 0.3.13.3
================

Release date: May 22, 2024

## Changes: Crosswords 0.3.13.3

Get the dates in the appstream file correct for editor

================
Version 0.3.13.2
================

Release date: May 21, 2024

## Changes: Crosswords 0.3.13.2

Get the dates in the appstream file correct

================
Version 0.3.13.1
================

Release date: May 21, 2024

## Changes: Crosswords 0.3.13.1

Fix appstream data to pass flathub's new rules

==============
Version 0.3.13
==============

Release date: May 19, 2024

## Changes: Crosswords 0.3.13
* Fix rendering bug when reveal error is disabled
* Warn when trying to load an invalid file
* Many behavioral fixes for keyboard nav
* Change undo to undo/redo differently depending on the direction
* Support loading .xd crosswords

## Changes: Crossword Editor 0.3.13
* Enable style tab: Set background icon and background/foreground color
* Change autofill dialog to be inline
* Autofill code now is correct, and runs a bit more efficiently
* Show only words that intersect in the word list
* Landed initial acrostic editor work. Only enabled for development builds

## Changes: Common
* Migrate to AdwToolbarView
* Load About Dialog metadata from gresource
* Make core widgets commutable
* Decompose GridState mode into behavior

## Special thanks for this release
* Tanmay for the thumbnailer, and fininshing
* Hariharan for hiding the back button on the main window, warn on
  loading an invalid puzzle, and fixing sensitivity of zoom
* Davide, for maintaining packit
* Adam, for fixing cursor behavior and glorious bug reports
* Federico, for CI and flatpak support
* Vinson, for Mac fixes
* Gwyneth, for xd support

==============
Version 0.3.12
==============

Release date: Jan 5, 2024

## Changes: Crosswords 0.3.12
* Tags are easier to read

## Changes: Crossword Editor 0.3.12

* Introduction of EditState to centralize state
* Migrate from libpanel to AdwOverlaySplitView and a custom
  panel. User visible results include:
    * Main puzzle grid is now resizable
    * A statistics panel for grids
    * Anagram and odd/even hints for clues
* Allow creation and fixing of empty cells
* Set initial vals in cells
* Clue list shows answers if the clue isn't set, or the clue if isn't set
* Keyboard navigation shortcuts for all stages
* Tags are easier to read


## Special thanks for this release
* Niko for massive design help with the editor
* Federico for advice on EditState
* Tanmay for acrostic work
* Pratham for anagram work
* Davide for docs fixes
* Christian for the great histogram rendering suggestion
* Rosanna for text editing

==============
Version 0.3.11
==============

Release date: Aug 31, 2023

* Fix all the path bugs when installing in a flatpak

==============
Version 0.3.10
==============

Release date: Aug 31, 2023

* Fix a path bug when installing in a flatpak

=============
Version 0.3.9
=============

Release date: Aug 29, 2023

## Changes: Crosswords 0.3.9

* Support for acrostic puzzles
* Color fading animation when moving the cursor
* Fixed longstanding bug that corrupted the undo stack

## Changes: Crossword Editor 0.3.9

* Totally new design. Based on libpanel
    * New libipuz editing interface used
* Support editing barred puzzles

## Special thanks for this release
* Niko for massive design help with the editor
* Federico for help with the new editing support and CI fixes
* Tanmay for adding acrostic support
* Vinson for spelling and warning fixes
* Pratham for anagram investigations

=============
Version 0.3.8
=============

Release date: Apr 1, 2023

## Changes: Crosswords 0.3.8

* Fully adaptive sizing. Crosswords will shrink to fit available space
* Fix end-of-game bugs where you could still edit the puzzle
* Use tags instead of labels for Puzzle Set metainfo
* Enumeration rendering fixes
* Misc bugfixes

## Changes: Crossword Editor 0.3.8

* Introduce a greeter to select puzzle type
* Use AdwEntryRow in the metadata editor
* New design doc in preparation for editing changes
* New, distinct icon

## Special thanks for this release
* Federico for advice on the new editing approach and reviewing the
  design doc
* Pratham for his work on rendering tags
* Philip for exporting translation location and icon work
* Tanmay for multiple bugfixes

===============
Version 0.3.7.2
===============

Release date: Jan 27, 2023

## Changes: Crosswords 0.3.7.2

* Bug fix release only
* Fix broken assertion with down-left arrowwords, #144
* Fix assertion when closing a toast, #137
* Fix lots of compile warnings

## Special thanks for this release
* Federico for great work with compiler warnings
* Philip for catching and help debug arrowwords
* Alice for help understanding and fixing the toast problems

===============
Version 0.3.7.1
===============

Release date: Jan 21, 2023

## Changes: Crosswords 0.3.7.1
* Emergency crasher release. Fixed a bug in the PuzzleSetModel

=============
Version 0.3.7
=============

Release date: Jan 21, 2023

## Changes: Crosswords 0.3.7
* Initial adaptive release for mobile and tablet
* Custom widget for adaptive layout supporting animations
* Support puzzles with zero or one column of clues
* New options for preferences dialog:
    * Preference: in for puzzle-sets. We hide them by default and let the user
      select the ones they want.
    * Preference: Hide puzzles after they're solved
* Add tagging to puzzle-sets to provide more information to users
* Add count of unsolved puzzles
* Fix zoom for all game UI elements

## Changes: ipuz spec compliance
* New supported puzzle type: Arrowword
* Support horizontal and vertical cell dividers

## Changes: Internal
* Move ClueId → IPuzClueId
* Merge PuzzleSetResource into PuzzleSet
* Move key file handling into PuzzleSetConfig
* Move puzzle management from Picker to PuzzleSetModel
* Move downloaders to PuzzleSet
* Clean save path to only save when the guesses change
* CI: 'packit' target to build Fedora RPMS

## Changes: Docs
* New development guide
* Development overview

## Special thanks for this release

* Thanks to Federico for arrow-drawing code and developer guide
* Thanks to Philip for help getting Arrowwords loading
* Thanks to Rosanna for words, and a new puzzles
* Thanks to Davide for Packit CI support, and new puzzle sets

=============
Version 0.3.6
=============

Release date: Nov 6, 2022

## Changes: Crosswords 0.3.6
* Adaptive layout to handle a wide variety of sizes
* Clean up of the appearance of the game
* Extra-small size to fit on smaller screen
* Remember window size on restart
* Define mime types for ipuz/jpz/puz files
* Load jpz/puz files from the command line
* Shape contrast and color improvements
* New setting: switch-on-move
* Translations: new Italian translation

## Changes: ipuz spec compliance
 * Support more complex enumeration use-cases
 * HTML support for clues and metadata
 * Better display and placement of intro/notes fields


## Changes: Convertors
* .puz convertor imports circles and rebus puzzles
* new .jpz convertor
* Both convertors create enumerations and html text to be closer to
  the ipuz spec
* Code refactor to allow additional convertors to be easily added

## Special thanks for this release
* Thanks to Federico for his massive refactoring of XwordState
* Thanks to Davide for build/distro fixes, cleanups, and mime support
* Thanks to Vinson for filing bugs against the Mac build
* Thanks to Philip for suggestions and bugs
* Thanks to Rosanna for wording and UX suggestions
* Thanks to the translators

=============
Version 0.3.5
=============

Release date: Sept 5, 2022

## Changes: Crosswords 0.3.5
* Multi-character input for rebus puzzles
* Barred crosswords are fully supported
* Acrostic puzzle improvements
* Enumerations are rendered
* Color styling support for borders and corners
* French Language support
* Big refactor of PlayState -> XwordState
   * New behavior to skip completed entries, with an option in the
     preference dialog
   * Clean up of State modes
* Browse mode when game is complete
* Fixed a bad bug where undo would break auto save functionality
* Numerous playability and style improvements

## Changes: Editor
* Devel flatpak for testing during development

## Special thanks for this release
* Thanks to Federico for help refactoring XwordState and for writing
  the overlay SVG widget
* Thanks to the translators

=============
Version 0.3.4
=============

Release date: July 31, 2022

## Changes: Crosswords 0.3.4
* Contrasting colors for the cursor
* Move behavior changes
* Style cleanups: info popover, intros, highlights, and clues
* Acrostic puzzles can be rendered
* Initial barred crossword support. Not rendered yet, though

## Changes: PuzzleSets
* Fixes to cats & dogs
* Added intro text and difficulties

## Changes: WordList
* Test suite and UTF-8 support
* Initial work on user-driven word lists

## Special thanks for this release

* Thanks to Heather, Jess, Rosanna, and Neil for user testing and new puzzles
* Thanks to Caroline for color consultation
* Thanks to Federico for WordList UTF-8 support and lots of advice
* Thanks to Philip for bug fixes and popover cleanup

=============
Version 0.3.3
=============

Release date: July 8, 2022

## Changes: Crosswords 0.3.3
* Preferences dialog to filter puzzle sets by language
* Translations. Dutch and Spanish translations
* Dutch-language crosswords work with the 'IJ' cell
* Don't grab focus when clicking on a row
* Copy/Paste support
* Undo/Redo support
* Numerous bug fixes
* Use the new libadwaita About dialog

## Changes: MacOS
* Input now works on the mac build


## Special thanks for this release

* Thanks to Lorenzo for Mac input fixes
* Thanks to Sam for updated icons
* Thanks to Philip for Dutch support and translations as well as bug fixes
* Thanks to Federico for translation framework and Spanish translations

=============
Version 0.3.2
=============

Release date: May 29, 2022

Emergency bug fix release

## Changes: Crosswords
* Fix a crasher in the keyboard shortcuts dialog, #67


=============
Version 0.3.1
=============

Release date: May 28, 2022

Bug fix release, with many UI cleanups.

## Changes: Crosswords
* Autosave on idle to guard against system/app crashes
* New, hig-compliant project icon
* PlayWindow redesign landed: Moved info into and hint/reveal/clear
  into popups
* Many padding and UI cleanups
* Zoom keybindings added (C-+ / C--, C-0)
* Add an about dialog

## Changes: puzzles
* Update the author and date

## Changes: Crossword-editor
* Change the animation in the autosave dialog

## Special thanks for this release

Thanks to Sam for design help and icons

Thanks to Rosanna for assistance with words

===========
Version 0.3
===========

Release date: May 6, 2022

First release suitable for flathub and general public consumption. Too
many behind-the-scene changes to list.

## Changes:

* Dark mode added
* [crosswords] Add a hint button that suggest words that could be used
* [crosswords] Fixes to puzzle sets
* [crosswords] PuzzleDownloader added. External plugins added
* [crosswords] warn when you fill the puzzle completely, but you have incorrect answers
* [crosswords] Finished "Cats and Dogs" puzzle set
* [crosswords] Use CSS to style everything
* [crosswords] Support all shapes in the ipuz spec (Federico)
* [crossword-editor] Save support added
* [crossword-editor] Warn when closing a window with unsaved changes
* [crossword-editor] Undo/Redo framework added
* [crossword-editor] Initial Clue and metadata pages created. With this, it's now possible to write a complete puzzle
* [crossword-editor] About dialog and preview (to console)
* [crossword-editor] Menu bar added
* [ipuz-convertor] Add a .puz convertor

## Special thanks for this release

Thanks to Rosanna for playtesting and helping with clues

Thanks to Federico for his help with the shapebg shapes and
refactoring support.

===========
Version 0.1
===========

First development release.
